package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class AbyssalDemonCombat extends NpcCombat {

  @Inject private Npc npc;
  private boolean usingSpecialAttack;
  private int specialAttackCount;
  private Tile specialAttackTile;
  private int specialAttackCooldown;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .rareDropTableDenominator(256)
            .clue(ClueScrollType.ELITE, 1200)
            .clue(ClueScrollType.HARD, 128);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(32768).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ABYSSAL_DAGGER_P_PLUS_PLUS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(6000).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ABYSSAL_HEAD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(512).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ABYSSAL_WHIP)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(25);
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ENSOULED_ABYSSAL_HEAD_13508)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_KITESHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_CHAINBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_HARRALANDER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_IRIT_LEAF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COSMIC_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEFENCE_POTION_3)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_GUAM_LEAF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_MARRENTILL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TARROMIN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 7)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LOBSTER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 60)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASHES)));
    drop.table(dropTable.build());

    var normalCombat = NpcCombatDefinition.builder();
    normalCombat.id(NpcId.ABYSSAL_DEMON_124);
    normalCombat.hitpoints(NpcCombatHitpoints.total(150));
    normalCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(97)
            .defenceLevel(135)
            .bonus(BonusType.MELEE_DEFENCE, 20)
            .bonus(BonusType.DEFENCE_RANGED, 20)
            .build());
    normalCombat.slayer(
        NpcCombatSlayer.builder().level(85).superiorId(NpcId.GREATER_ABYSSAL_DEMON_342).build());
    normalCombat.type(NpcCombatType.DEMON).deathAnimation(1538).blockAnimation(2309);
    normalCombat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(1537).attackSpeed(4);
    normalCombat.style(style.build());

    var catacombsCombat = NpcCombatDefinition.builder();
    catacombsCombat.id(NpcId.ABYSSAL_DEMON_124_7241);
    catacombsCombat.hitpoints(NpcCombatHitpoints.total(150));
    catacombsCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(97)
            .defenceLevel(135)
            .bonus(BonusType.MELEE_DEFENCE, 20)
            .bonus(BonusType.DEFENCE_RANGED, 20)
            .build());
    catacombsCombat.slayer(
        NpcCombatSlayer.builder().level(85).superiorId(NpcId.GREATER_ABYSSAL_DEMON_342).build());
    catacombsCombat.type(NpcCombatType.DEMON).deathAnimation(1538).blockAnimation(2309);
    catacombsCombat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(1537).attackSpeed(4);
    catacombsCombat.style(style.build());

    var cursedCombat = NpcCombatDefinition.builder();
    cursedCombat.id(NpcId.CURSED_ABYSSAL_DEMON_342_16010);
    cursedCombat.hitpoints(NpcCombatHitpoints.builder().total(225).build());
    cursedCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(145)
            .defenceLevel(202)
            .bonus(BonusType.MELEE_DEFENCE, 30)
            .bonus(BonusType.DEFENCE_RANGED, 30)
            .build());
    cursedCombat.slayer(NpcCombatSlayer.builder().level(85).build());
    cursedCombat.type(NpcCombatType.DEMON).deathAnimation(1538).blockAnimation(2309);
    cursedCombat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(31));
    style.animation(1537).attackSpeed(4);
    cursedCombat.style(style.build());

    var superiorCombat = NpcCombatDefinition.builder();
    superiorCombat.id(NpcId.GREATER_ABYSSAL_DEMON_342);
    superiorCombat.hitpoints(
        NpcCombatHitpoints.builder().total(400).barType(HitpointsBarType.GREEN_RED_60).build());
    superiorCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(300)
            .defenceLevel(240)
            .bonus(BonusType.MELEE_DEFENCE, 50)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    superiorCombat.slayer(NpcCombatSlayer.builder().level(85).experience(4200).build());
    superiorCombat.killCount(
        NpcCombatKillCount.builder().asName("Superior Slayer Creature").build());
    superiorCombat.type(NpcCombatType.DEMON).deathAnimation(1538).blockAnimation(2309);
    superiorCombat.drop(drop.rolls(3).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(31));
    style.animation(1537).attackSpeed(4);
    superiorCombat.style(style.build());

    return Arrays.asList(
        normalCombat.build(),
        catacombsCombat.build(),
        cursedCombat.build(),
        superiorCombat.build());
  }

  @Override
  public void spawnHook() {
    usingSpecialAttack = false;
    specialAttackCount = 0;
    specialAttackTile = null;
    specialAttackCooldown = 0;
  }

  @Override
  public void tickStartHook() {
    if (specialAttackCooldown > 0) {
      specialAttackCooldown--;
    }
    if ((npc.getId() == NpcId.GREATER_ABYSSAL_DEMON_342
            || npc.getId() == NpcId.CURSED_ABYSSAL_DEMON_342_16010)
        && isAttacking()
        && !isHitDelayed()
        && !usingSpecialAttack
        && specialAttackCooldown == 0
        && PRandom.randomE(20) == 0) {
      usingSpecialAttack = true;
      specialAttackTile = new Tile(npc);
      specialAttackCooldown = 20;
    }
  }

  @Override
  public int attackTickAttackSpeedHook(NpcCombatStyle combatStyle, Entity opponent) {
    return usingSpecialAttack ? 2 : combatStyle.getAttackSpeed();
  }

  @Override
  public void applyAttackEndHook(
      NpcCombatStyle combatStyle, Entity opponent, int applyAttackLoopCount, HitEvent hitEvent) {
    if (!usingSpecialAttack) {
      return;
    }
    if (++specialAttackCount >= 4) {
      npc.getMovement().teleport(specialAttackTile);
      usingSpecialAttack = false;
      specialAttackCount = 0;
      specialAttackTile = null;
    } else {
      var tile = new Tile(opponent);
      var tries = 0;
      while (tries++ < 8
          && (tile.matchesTile(opponent)
              || tile.matchesTile(npc)
              || !npc.getController().routeAllow(tile))) {
        tile.setTile(opponent);
        tile = PRandom.randomI(1) == 0 ? tile.randomizeX(1) : tile.randomizeY(1);
      }
      if (!npc.getController().routeAllow(tile)) {
        tile = npc;
      }
      npc.getMovement().teleport(tile);
    }
  }

  @Override
  public double accuracyHook(NpcCombatStyle combatStyle, double accuracy) {
    return usingSpecialAttack ? Integer.MAX_VALUE : accuracy;
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer() || npc.getId() != NpcId.CURSED_ABYSSAL_DEMON_342_16010) {
      return;
    }
    opponent.asPlayer().getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (npc.getId() != NpcId.CURSED_ABYSSAL_DEMON_342_16010) {
      return;
    }
    if (!player.getSkills().isWildernessSlayerTask(npc)) {
      return;
    }
    var table =
        NpcCombatDefinition.getDefinition(NpcId.ABYSSAL_SIRE_350_5908)
            .getDrop()
            .getTable(npc, player, 4, 0, true);
    if (table == null) {
      return;
    }
    table.dropItems(npc, player, dropTile);
  }

  @Override
  public NpcCombatDropTable deathDropItemsTableHook(
      Player player, int dropRateDivider, int roll, NpcCombatDropTable table) {
    if (npc.getId() == NpcId.CURSED_ABYSSAL_DEMON_342_16010) {
      if (!player.getSkills().isWildernessSlayerTask(npc)) {
        player.getGameEncoder().sendMessage("Without an assigned task, the loot turns to dust...");
        return null;
      }
    }
    return table;
  }
}
