package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PNumber;
import java.util.Arrays;
import java.util.List;

class MuttadileCombat extends NpcCombat {

  private static final Tile SPAWN_TILE = new Tile(3314, 5331, 1);
  private static final Tile BABY_SPAWN_TILE = new Tile(3310, 5317, 1);

  @Inject private Npc npc;
  private boolean loaded;
  private Npc baby;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().additionalPlayers(255);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BIG_BONES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OVERLOAD_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_ENHANCE_PLUS_4)));
    drop.table(dropTable.build());

    var bossCombat1 = NpcCombatDefinition.builder();
    bossCombat1.id(NpcId.MUTTADILE);
    bossCombat1.hitpoints(NpcCombatHitpoints.total(400));
    bossCombat1.stats(
        NpcCombatStats.builder()
            .attackLevel(250)
            .magicLevel(250)
            .rangedLevel(250)
            .defenceLevel(220)
            .bonus(BonusType.MELEE_ATTACK, 88)
            .bonus(BonusType.ATTACK_RANGED, 82)
            .bonus(BonusType.DEFENCE_MAGIC, 75)
            .build());
    bossCombat1.aggression(
        NpcCombatAggression.builder().range(18).always(true).checkWhileAttacking(true).build());
    bossCombat1.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    bossCombat1.focus(
        NpcCombatFocus.builder()
            .disableFacingOpponent(true)
            .disableFollowingOpponent(true)
            .bypassMapObjects(true)
            .build());
    bossCombat1.deathAnimation(7426);
    bossCombat1.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(30));
    style.attackSpeed(10).attackRange(18);
    style.projectile(NpcCombatProjectile.id(393));
    bossCombat1.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(30));
    style.attackSpeed(10).attackRange(18);
    style.projectile(NpcCombatProjectile.id(393));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    bossCombat1.style(style.build());

    var bossCombat2 = NpcCombatDefinition.builder();
    bossCombat2.id(NpcId.MUTTADILE_7563);
    bossCombat2.hitpoints(NpcCombatHitpoints.total(400));
    bossCombat2.stats(
        NpcCombatStats.builder()
            .attackLevel(150)
            .rangedLevel(150)
            .defenceLevel(138)
            .bonus(BonusType.MELEE_ATTACK, 71)
            .bonus(BonusType.ATTACK_RANGED, 83)
            .bonus(BonusType.DEFENCE_MAGIC, 60)
            .build());
    bossCombat2.aggression(
        NpcCombatAggression.builder().range(12).always(true).checkWhileAttacking(true).build());
    bossCombat2.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    bossCombat2.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    bossCombat2.deathAnimation(7426);
    bossCombat2.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7420).attackSpeed(5);
    bossCombat2.style(style.build());

    style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.builder().maximum(60).prayerEffectiveness(0.6).build());
    style.animation(7424).attackSpeed(5);
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    bossCombat2.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().maximum(45).prayerEffectiveness(0.5).build());
    style.animation(7421).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(1291));
    bossCombat2.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7422).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(393));
    bossCombat2.style(style.build());

    var babyDrop = NpcCombatDrop.builder().additionalPlayers(255);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BIG_BONES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OVERLOAD_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_ENHANCE_PLUS_4)));
    babyDrop.table(dropTable.build());

    var babyCombat = NpcCombatDefinition.builder();
    babyCombat.id(NpcId.MUTTADILE_7562);
    babyCombat.hitpoints(NpcCombatHitpoints.total(200));
    babyCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(150)
            .rangedLevel(150)
            .defenceLevel(138)
            .bonus(BonusType.MELEE_ATTACK, 71)
            .bonus(BonusType.ATTACK_RANGED, 83)
            .bonus(BonusType.DEFENCE_MAGIC, 60)
            .build());
    babyCombat.aggression(
        NpcCombatAggression.builder().range(12).always(true).checkWhileAttacking(true).build());
    babyCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    babyCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    babyCombat.deathAnimation(7426);
    babyCombat.drop(babyDrop.build());

    style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.maximum(12));
    style.animation(7420).attackSpeed(5);
    babyCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().maximum(12).ignorePrayer(true).build());
    style.animation(7421).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(1291));
    babyCombat.style(style.build());

    return Arrays.asList(bossCombat1.build(), bossCombat2.build(), babyCombat.build());
  }

  @Override
  public void despawnHook() {
    npc.getWorld().removeNpc(baby);
  }

  @Override
  public void tickStartHook() {
    if (!loaded) {
      loadProfile();
      return;
    }
    if (!npc.getController().isRegionLoaded()) {
      return;
    }
    if (baby != null
        && npc.getId() == NpcId.MUTTADILE
        && (!baby.isVisible() || baby.getCombat().isDead())) {
      clear();
      npc.setId(NpcId.MUTTADILE_7563);
      npc.setAnimation(7423);
      npc.setLock(7);
      npc.getMovement().clear();
      npc.getMovement().addMovement(npc.getX() - 6, npc.getY());
    }
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    if (hitStyleType == HitStyleType.MAGIC && PRandom.randomE(5) != 0) {
      return HitStyleType.RANGED;
    }
    return hitStyleType;
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    return npc.getId() != NpcId.MUTTADILE;
  }

  public void loadProfile() {
    if (loaded || npc.getController().getPlayers().isEmpty()) {
      return;
    }
    loaded = true;
    npc.getSpawn().tile((npc.getId() == NpcId.MUTTADILE_7562) ? BABY_SPAWN_TILE : SPAWN_TILE);
    npc.setTile(npc.getSpawn().getTile());
    var averageHP = 0;
    var playerMultiplier = 1.0;
    var players = npc.getController().getPlayers();
    for (var player : players) {
      averageHP += player.getCombat().getMaxHitpoints();
      playerMultiplier = PNumber.addDoubles(playerMultiplier, 0.5);
    }
    averageHP /= players.size();
    var hitpoints = (int) ((50 + (players.size() * 25) + (averageHP * 2)) * playerMultiplier);
    if (npc.getId() == NpcId.MUTTADILE_7562) {
      setMaxHitpoints(hitpoints / 2);
    } else {
      setMaxHitpoints(hitpoints);
      npc.setId(NpcId.MUTTADILE);
      baby = npc.getController().addNpc(new NpcSpawn(BABY_SPAWN_TILE, NpcId.MUTTADILE_7562));
      baby.getController().setMultiCombatFlag(true);
      baby.getSpawn().moveDistance(4);
    }
    setHitpoints(getMaxHitpoints());
  }
}
