package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Skills;
import java.util.Arrays;
import java.util.List;

class FightPitTzKihCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.TZ_KIH_22).id(NpcId.TZ_KIH_22_2190);
    combat.hitpoints(NpcCombatHitpoints.total(10));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(20)
            .magicLevel(30)
            .rangedLevel(15)
            .defenceLevel(15)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(10).always(true).build());
    combat.deathAnimation(2620).blockAnimation(2622);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(4));
    style.animation(2621).attackSpeed(4);
    style.effect(NpcCombatEffect.builder().includeMiss(true).statDrain(Skills.PRAYER, 1).build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    npc.getMovement().setIgnoreNpcs(false);
  }
}
