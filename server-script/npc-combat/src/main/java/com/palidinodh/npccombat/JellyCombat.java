package com.palidinodh.npccombat;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import java.util.Arrays;
import java.util.List;

class JellyCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().rareDropTableDenominator(256).clue(ClueScrollType.HARD, 128);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_BOOTS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_PLATEBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_RUNE, 5, 21)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 7)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.THREAD, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_FULL_HELM)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_KITESHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLD_BAR)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 11, 672)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.JELLY_78);
    combat.hitpoints(NpcCombatHitpoints.total(75));
    combat.stats(NpcCombatStats.builder().attackLevel(45).magicLevel(45).defenceLevel(120).build());
    combat.slayer(NpcCombatSlayer.builder().level(52).superiorId(NpcId.VITREOUS_JELLY_206).build());
    combat.deathAnimation(1587).blockAnimation(1585);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.MELEE)
            .subHitStyleType(HitStyleType.MAGIC)
            .build());
    style.damage(NpcCombatDamage.maximum(6));
    style.animation(1586).attackSpeed(4);
    combat.style(style.build());

    var warpedDrop =
        NpcCombatDrop.builder().rareDropTableDenominator(256).clue(ClueScrollType.HARD, 64);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_KITESHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_FULL_HELM)));
    warpedDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_BOOTS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_PLATEBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 45)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LOBSTER, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLD_BAR)));
    warpedDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_BATTLEAXE)));
    warpedDrop.table(dropTable.build());

    var warpedCombat = NpcCombatDefinition.builder();
    warpedCombat.id(NpcId.WARPED_JELLY_112);
    warpedCombat.hitpoints(NpcCombatHitpoints.total(140));
    warpedCombat.stats(
        NpcCombatStats.builder().attackLevel(95).magicLevel(95).defenceLevel(70).build());
    warpedCombat.slayer(
        NpcCombatSlayer.builder().level(52).superiorId(NpcId.VITREOUS_WARPED_JELLY_241).build());
    warpedCombat.killCount(NpcCombatKillCount.builder().asName("Jelly").build());
    warpedCombat.deathAnimation(1587).blockAnimation(1585);
    warpedCombat.drop(warpedDrop.build());

    style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.MELEE)
            .subHitStyleType(HitStyleType.MAGIC)
            .build());
    style.damage(NpcCombatDamage.maximum(10));
    style.animation(1586).attackSpeed(4);
    warpedCombat.style(style.build());

    var superiorCombat = NpcCombatDefinition.builder();
    superiorCombat.id(NpcId.VITREOUS_JELLY_206);
    superiorCombat.hitpoints(
        NpcCombatHitpoints.builder().total(175).barType(HitpointsBarType.GREEN_RED_60).build());
    superiorCombat.stats(
        NpcCombatStats.builder().attackLevel(145).magicLevel(145).defenceLevel(220).build());
    superiorCombat.slayer(NpcCombatSlayer.builder().level(52).experience(1900).build());
    superiorCombat.killCount(
        NpcCombatKillCount.builder().asName("Superior Slayer Creature").build());
    superiorCombat.deathAnimation(1587).blockAnimation(1585);
    superiorCombat.drop(drop.rolls(3).build());

    style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.MELEE)
            .subHitStyleType(HitStyleType.MAGIC)
            .build());
    style.damage(NpcCombatDamage.maximum(16));
    style.animation(1586).attackSpeed(4);
    superiorCombat.style(style.build());

    var warpedSuperiorCombat = NpcCombatDefinition.builder();
    warpedSuperiorCombat.id(NpcId.VITREOUS_WARPED_JELLY_241);
    warpedSuperiorCombat.hitpoints(
        NpcCombatHitpoints.builder().total(240).barType(HitpointsBarType.GREEN_RED_60).build());
    warpedSuperiorCombat.stats(
        NpcCombatStats.builder().attackLevel(195).magicLevel(195).defenceLevel(170).build());
    warpedSuperiorCombat.slayer(NpcCombatSlayer.builder().level(52).experience(2200).build());
    warpedSuperiorCombat.killCount(
        NpcCombatKillCount.builder().asName("Superior Slayer Creature").build());
    warpedSuperiorCombat.deathAnimation(1587).blockAnimation(1585);
    warpedSuperiorCombat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.MELEE)
            .subHitStyleType(HitStyleType.MAGIC)
            .build());
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(1586).attackSpeed(4);
    warpedSuperiorCombat.style(style.build());

    return Arrays.asList(
        combat.build(), warpedCombat.build(), superiorCombat.build(), warpedSuperiorCombat.build());
  }
}
