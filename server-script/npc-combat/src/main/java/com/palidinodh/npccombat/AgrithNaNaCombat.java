package com.palidinodh.npccombat;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.graphic.Graphic;
import java.util.Arrays;
import java.util.List;

class AgrithNaNaCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.AGRITH_NA_NA_146);
    combat.hitpoints(NpcCombatHitpoints.total(200));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(83)
            .magicLevel(100)
            .rangedLevel(100)
            .defenceLevel(82)
            .bonus(BonusType.MELEE_ATTACK, 100)
            .bonus(BonusType.MELEE_DEFENCE, 100)
            .bonus(BonusType.DEFENCE_MAGIC, 100)
            .bonus(BonusType.DEFENCE_RANGED, 100)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.deathAnimation(3503).blockAnimation(3500);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(16));
    style.animation(3501).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(16));
    style.animation(3502).attackSpeed(4);
    style.targetGraphic(new Graphic(131, 124));
    style.projectile(NpcCombatProjectile.id(130));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
