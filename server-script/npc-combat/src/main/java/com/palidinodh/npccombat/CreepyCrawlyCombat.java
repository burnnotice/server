package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class CreepyCrawlyCombat extends NpcCombat {

  @Inject private Npc npc;
  private int repathDelay;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var zombieCombat = NpcCombatDefinition.builder();
    zombieCombat.id(NpcId.ZOMBIE_16020);
    zombieCombat.hitpoints(NpcCombatHitpoints.total(25));
    zombieCombat.stats(
        NpcCombatStats.builder().attackLevel(150).bonus(BonusType.MELEE_ATTACK, 100).build());
    zombieCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    zombieCombat.deathAnimation(5580).blockAnimation(5579);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(7));
    style.animation(5578).attackSpeed(4);
    zombieCombat.style(style.build());

    var skeletonMageCombat = NpcCombatDefinition.builder();
    skeletonMageCombat.id(NpcId.SKELETON_MAGE_16021);
    skeletonMageCombat.hitpoints(NpcCombatHitpoints.total(40));
    skeletonMageCombat.stats(
        NpcCombatStats.builder().magicLevel(150).bonus(BonusType.ATTACK_MAGIC, 100).build());
    skeletonMageCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    skeletonMageCombat.focus(NpcCombatFocus.builder().keepWithinDistance(4).build());
    skeletonMageCombat.deathAnimation(5491).blockAnimation(5489);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(5485).attackSpeed(4);
    style.targetGraphic(new Graphic(128, 124));
    style.projectile(NpcCombatProjectile.id(127));
    skeletonMageCombat.style(style.build());

    var shadowSpiderCombat = NpcCombatDefinition.builder();
    shadowSpiderCombat.id(NpcId.SHADOW_SPIDER_16023);
    shadowSpiderCombat.hitpoints(NpcCombatHitpoints.total(28));
    shadowSpiderCombat.stats(
        NpcCombatStats.builder().attackLevel(150).bonus(BonusType.MELEE_ATTACK, 100).build());
    shadowSpiderCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    shadowSpiderCombat.deathAnimation(5329).blockAnimation(5328);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(5));
    style.effect(
        NpcCombatEffect.builder()
            .includeMiss(true)
            .statDrain(Skills.PRAYER, 1)
            .statDrainByDamage(Skills.PRAYER)
            .build());
    style.animation(5327).attackSpeed(4);
    shadowSpiderCombat.style(style.build());

    var feralVampyreCombat = NpcCombatDefinition.builder();
    feralVampyreCombat.id(NpcId.FERAL_VAMPYRE_16024);
    feralVampyreCombat.hitpoints(NpcCombatHitpoints.total(20));
    feralVampyreCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(150)
            .rangedLevel(150)
            .bonus(BonusType.MELEE_ATTACK, 100)
            .bonus(BonusType.ATTACK_RANGED, 100)
            .build());
    feralVampyreCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    feralVampyreCombat.focus(NpcCombatFocus.builder().keepWithinDistance(4).build());
    feralVampyreCombat.deathAnimation(836).blockAnimation(424);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(10));
    style.animation(393).attackSpeed(4);
    feralVampyreCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(10));
    style.animation(806).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(274));
    feralVampyreCombat.style(style.build());

    var skeletonFremennikCombat = NpcCombatDefinition.builder();
    skeletonFremennikCombat.id(NpcId.SKELETON_FREMENNIK_16025);
    skeletonFremennikCombat.hitpoints(NpcCombatHitpoints.total(20));
    skeletonFremennikCombat.stats(
        NpcCombatStats.builder().attackLevel(150).bonus(BonusType.MELEE_ATTACK, 100).build());
    skeletonFremennikCombat.aggression(
        NpcCombatAggression.builder().range(20).always(true).build());
    skeletonFremennikCombat.deathAnimation(836).blockAnimation(6129);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(6128).attackSpeed(6);
    skeletonFremennikCombat.style(style.build());

    var ghostCombat = NpcCombatDefinition.builder();
    ghostCombat.id(NpcId.GHOST_16027);
    ghostCombat.hitpoints(NpcCombatHitpoints.total(50));
    ghostCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(150)
            .rangedLevel(150)
            .bonus(BonusType.MELEE_ATTACK, 100)
            .bonus(BonusType.ATTACK_RANGED, 100)
            .build());
    ghostCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    ghostCombat.focus(NpcCombatFocus.builder().keepWithinDistance(4).build());
    ghostCombat.deathAnimation(5542).blockAnimation(5541);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(10));
    style.animation(5540).attackSpeed(4);
    ghostCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(10));
    style.animation(5545).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(37));
    ghostCombat.style(style.build());

    var mummyCombat = NpcCombatDefinition.builder();
    mummyCombat.id(NpcId.MUMMY_16029);
    mummyCombat.hitpoints(NpcCombatHitpoints.total(45));
    mummyCombat.stats(
        NpcCombatStats.builder().attackLevel(150).bonus(BonusType.MELEE_ATTACK, 100).build());
    mummyCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    mummyCombat.deathAnimation(5555).blockAnimation(5556);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(6));
    style.animation(5554).attackSpeed(4);
    mummyCombat.style(style.build());

    var shadeCombat = NpcCombatDefinition.builder();
    shadeCombat.id(NpcId.RIYL_SHADE_16028);
    shadeCombat.hitpoints(NpcCombatHitpoints.total(38));
    shadeCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(150)
            .magicLevel(150)
            .bonus(BonusType.MELEE_ATTACK, 100)
            .bonus(BonusType.ATTACK_MAGIC, 100)
            .build());
    shadeCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    shadeCombat.focus(NpcCombatFocus.builder().keepWithinDistance(4).build());
    shadeCombat.deathAnimation(1285).blockAnimation(1283);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(10));
    style.animation(1284).attackSpeed(6);
    style.effect(NpcCombatEffect.builder().includeMiss(true).statDrain(Skills.STRENGTH, 1).build());
    shadeCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(10));
    style.animation(1284).attackSpeed(6);
    style.projectile(NpcCombatProjectile.id(1382));
    shadeCombat.style(style.build());

    var leechCombat = NpcCombatDefinition.builder();
    leechCombat.id(NpcId.LEECH_16035);
    leechCombat.hitpoints(NpcCombatHitpoints.total(23));
    leechCombat.stats(
        NpcCombatStats.builder().attackLevel(150).bonus(BonusType.MELEE_ATTACK, 100).build());
    leechCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    leechCombat.deathAnimation(1272).blockAnimation(1271);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(3));
    style.effect(
        NpcCombatEffect.builder()
            .includeMiss(true)
            .statDrain(Skills.ATTACK, 1)
            .statDrain(Skills.DEFENCE, 1)
            .statDrain(Skills.STRENGTH, 1)
            .statDrain(Skills.MAGIC, 1)
            .statDrain(Skills.RANGED, 1)
            .build());
    style.animation(1273).attackSpeed(6);
    leechCombat.style(style.build());

    var hopelessCreatureCombat = NpcCombatDefinition.builder();
    hopelessCreatureCombat.id(NpcId.HOPELESS_CREATURE_16022);
    hopelessCreatureCombat.hitpoints(
        NpcCombatHitpoints.builder().total(177).barType(HitpointsBarType.GREEN_RED_60).build());
    hopelessCreatureCombat.stats(
        NpcCombatStats.builder().attackLevel(200).bonus(BonusType.MELEE_ATTACK, 100).build());
    hopelessCreatureCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    hopelessCreatureCombat.deathAnimation(3827).blockAnimation(3826);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(21));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(3823).attackSpeed(4);
    hopelessCreatureCombat.style(style.build());

    var fearReaperCombat = NpcCombatDefinition.builder();
    fearReaperCombat.id(NpcId.FEAR_REAPER_16030);
    fearReaperCombat.hitpoints(
        NpcCombatHitpoints.builder().total(143).barType(HitpointsBarType.GREEN_RED_60).build());
    fearReaperCombat.stats(
        NpcCombatStats.builder().attackLevel(200).bonus(BonusType.MELEE_ATTACK, 100).build());
    fearReaperCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    fearReaperCombat.deathAnimation(3813).blockAnimation(3811);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.damage(NpcCombatDamage.maximum(18));
    style.animation(3812).attackSpeed(4);
    fearReaperCombat.style(style.build());

    var nailBeastCombat = NpcCombatDefinition.builder();
    nailBeastCombat.id(NpcId.NAIL_BEAST_16031);
    nailBeastCombat.hitpoints(
        NpcCombatHitpoints.builder().total(188).barType(HitpointsBarType.GREEN_RED_60).build());
    nailBeastCombat.stats(
        NpcCombatStats.builder().attackLevel(200).bonus(BonusType.MELEE_ATTACK, 100).build());
    nailBeastCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    nailBeastCombat.deathAnimation(5990).blockAnimation(5988);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(4));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(5989).attackSpeed(7);
    style.attackCount(3);
    nailBeastCombat.style(style.build());

    var experimentNo2Combat = NpcCombatDefinition.builder();
    experimentNo2Combat.id(NpcId.EXPERIMENT_NO2_16032);
    experimentNo2Combat.hitpoints(
        NpcCombatHitpoints.builder().total(143).barType(HitpointsBarType.GREEN_RED_60).build());
    experimentNo2Combat.stats(
        NpcCombatStats.builder()
            .attackLevel(200)
            .rangedLevel(200)
            .bonus(BonusType.MELEE_ATTACK, 100)
            .bonus(BonusType.ATTACK_RANGED, 100)
            .build());
    experimentNo2Combat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    experimentNo2Combat.focus(NpcCombatFocus.builder().keepWithinDistance(4).build());
    experimentNo2Combat.deathAnimation(6512).blockAnimation(6515);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(17));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(6513).attackSpeed(4);
    experimentNo2Combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(17));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(6514).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(268));
    experimentNo2Combat.style(style.build());

    var shaededBeastCombat = NpcCombatDefinition.builder();
    shaededBeastCombat.id(NpcId.SHAEDED_BEAST_16033);
    shaededBeastCombat.hitpoints(
        NpcCombatHitpoints.builder().total(210).barType(HitpointsBarType.GREEN_RED_60).build());
    shaededBeastCombat.stats(
        NpcCombatStats.builder().magicLevel(200).bonus(BonusType.ATTACK_MAGIC, 100).build());
    shaededBeastCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    shaededBeastCombat.focus(NpcCombatFocus.builder().keepWithinDistance(4).build());
    shaededBeastCombat.deathAnimation(5619).blockAnimation(5618);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(30));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(5613).attackSpeed(4);
    style.targetGraphic(new Graphic(1684));
    shaededBeastCombat.style(style.build());

    var abominationCombat = NpcCombatDefinition.builder();
    abominationCombat.id(NpcId.ABOMINATION_16034);
    abominationCombat.hitpoints(
        NpcCombatHitpoints.builder().total(200).barType(HitpointsBarType.GREEN_RED_60).build());
    abominationCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(200)
            .rangedLevel(200)
            .bonus(BonusType.MELEE_ATTACK, 100)
            .bonus(BonusType.ATTACK_RANGED, 100)
            .build());
    abominationCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    abominationCombat.focus(NpcCombatFocus.builder().keepWithinDistance(4).build());
    abominationCombat.deathAnimation(8024).blockAnimation(8027);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE);
    style.damage(NpcCombatDamage.maximum(22));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8025).attackSpeed(5);
    abominationCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.RANGED).weight(4).build());
    style.damage(NpcCombatDamage.maximum(22));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8026).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(1533));
    abominationCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(22));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8026).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(1534));
    abominationCombat.style(style.build());

    var confusionBeastCombat = NpcCombatDefinition.builder();
    confusionBeastCombat.id(NpcId.CONFUSION_BEAST_16036);
    confusionBeastCombat.hitpoints(
        NpcCombatHitpoints.builder().total(160).barType(HitpointsBarType.GREEN_RED_60).build());
    confusionBeastCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(200)
            .magicLevel(200)
            .bonus(BonusType.MELEE_ATTACK, 100)
            .bonus(BonusType.ATTACK_MAGIC, 100)
            .build());
    confusionBeastCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    confusionBeastCombat.deathAnimation(3821).blockAnimation(3817);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(15));
    style.effect(NpcCombatEffect.builder().includeMiss(true).poison(6).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(3816).attackSpeed(4);
    confusionBeastCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(15));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(3818).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(146));
    confusionBeastCombat.style(style.build());

    return Arrays.asList(
        zombieCombat.build(),
        skeletonMageCombat.build(),
        hopelessCreatureCombat.build(),
        shadowSpiderCombat.build(),
        feralVampyreCombat.build(),
        skeletonFremennikCombat.build(),
        ghostCombat.build(),
        mummyCombat.build(),
        fearReaperCombat.build(),
        nailBeastCombat.build(),
        shadeCombat.build(),
        experimentNo2Combat.build(),
        shaededBeastCombat.build(),
        abominationCombat.build(),
        leechCombat.build(),
        confusionBeastCombat.build());
  }

  @Override
  public void tickEndHook() {
    findPath();
  }

  private void findPath() {
    if (isAttacking()) {
      return;
    }
    if (repathDelay-- > 0) {
      return;
    }
    repathDelay = PRandom.randomI(8, 16);
    var nearbyPlayers = npc.getController().getPlayers();
    if (nearbyPlayers.isEmpty()) {
      return;
    }
    nearbyPlayers.shuffle();
    var player = nearbyPlayers.getFirst();
    npc.getMovement().fullRoute(player, 0);
  }
}
