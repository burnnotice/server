package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class FragmentOfSerenWhirlwindCombat extends NpcCombat {

  @Inject private Npc npc;
  private Npc seren;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.CRYSTAL_WHIRLWIND);
    combat.hitpoints(NpcCombatHitpoints.total(1));
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    seren = npc.getController().getNpc(NpcId.FRAGMENT_OF_SEREN_16049);
  }

  @Override
  public void tickStartHook() {
    if (npc.isLocked()) {
      return;
    }
    if (seren == null || seren.isLocked() || npc.getHeight() != seren.getHeight()) {
      return;
    }
    if (isHitDelayed()) {
      return;
    }
    setHitDelay(4);
    var projectile =
        Graphic.Projectile.builder()
            .id(1694)
            .startTile(npc)
            .entity(seren)
            .speed(getProjectileSpeed(10))
            .build();
    seren
        .getCombat()
        .addHitEvent(
            new HitEvent(
                projectile.getEventDelay(), new Hit(PRandom.randomI(4, 8), HitMarkType.MAGENTA)));
    sendMapProjectile(projectile);
  }
}
