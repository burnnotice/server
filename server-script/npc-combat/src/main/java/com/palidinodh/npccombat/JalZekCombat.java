package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class JalZekCombat extends NpcCombat {

  @Inject private Npc npc;
  private List<Npc> monsters = new ArrayList<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.JAL_ZEK_490);
    combat.hitpoints(NpcCombatHitpoints.total(220));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(370)
            .magicLevel(300)
            .rangedLevel(510)
            .defenceLevel(260)
            .bonus(BonusType.ATTACK_MAGIC, 80)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    combat.immunity(NpcCombatImmunity.builder().venom(true).build());
    combat.deathAnimation(7613);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(70));
    style.animation(7612).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(70));
    style.animation(7610).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1376));
    combat.style(style.build());

    var zukSpawnedCombat = NpcCombatDefinition.builder();
    zukSpawnedCombat.id(NpcId.JAL_ZEK_490_7703);
    zukSpawnedCombat.hitpoints(NpcCombatHitpoints.total(220));
    zukSpawnedCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(370)
            .magicLevel(300)
            .rangedLevel(510)
            .defenceLevel(260)
            .bonus(BonusType.ATTACK_MAGIC, 80)
            .build());
    zukSpawnedCombat.aggression(
        NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    zukSpawnedCombat.immunity(NpcCombatImmunity.builder().venom(true).build());
    zukSpawnedCombat.deathAnimation(7613);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(70));
    style.animation(7612).attackSpeed(4);
    zukSpawnedCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(70));
    style.animation(7610).attackSpeed(4).attackRange(28);
    style.projectile(NpcCombatProjectile.id(1376));
    zukSpawnedCombat.style(style.build());

    return Arrays.asList(combat.build(), zukSpawnedCombat.build());
  }

  @Override
  public void despawnHook() {
    monsters.clear();
  }

  @Override
  public void tickStartHook() {
    if (npc.isLocked() || isLocked()) {
      return;
    }
    if (isHitDelayed()) {
      return;
    }
    if (!npc.withinDistance(getAttackingEntity(), 10)) {
      return;
    }
    if (monsters.isEmpty() || PRandom.randomE(8) != 0) {
      return;
    }
    Npc deadNpc = null;
    for (var i = 0; i < monsters.size(); i++) {
      var listNpc = monsters.get(PRandom.randomE(monsters.size()));
      if (listNpc.isVisible()) {
        continue;
      }
      monsters.remove(listNpc);
      deadNpc = listNpc;
      break;
    }
    if (deadNpc == null) {
      return;
    }
    var p = (Player) getAttackingEntity();
    Tile tile;
    if (npc.getId() == NpcId.JAL_ZEK_490_7703) {
      tile = new Tile(2275, 5351, npc.getHeight());
    } else {
      tile = new Tile(2267, 5340, npc.getHeight());
      tile.moveTile(PRandom.randomI(8), PRandom.randomI(2));
    }
    var summoned = npc.getController().addNpc(new NpcSpawn(tile, deadNpc.getId()));
    summoned.getMovement().setClipNpcs(true);
    summoned.getCombat().setHitpoints(summoned.getCombat().getMaxHitpoints() / 2);
    summoned.getCombat().setTarget(p);
    summoned.setGraphic(444, 100);
    p.getCombat().getTzHaar().addMonster(summoned);
    setHitDelay(8);
    setLock(4);
    npc.setAnimation(7611);
    npc.setFaceEntity(summoned);
    npc.setFaceTile(summoned);
  }

  @Override
  @SuppressWarnings("rawtypes")
  public Object script(String name, Object... args) {
    if (name.equals("monsters")) {
      List<Npc> list = PCollection.castList((List) args[0], Npc.class);
      for (Npc n2 : list) {
        if (npc == n2) {
          continue;
        }
        monsters.add(n2);
      }
    }
    return null;
  }
}
