package com.palidinodh.npccombat.hook;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDeathItemsHooks;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.Settings;
import java.util.ArrayList;
import java.util.List;

class NpcCombatDeathItems implements NpcCombatDeathItemsHooks {

  private static final NpcCombatDropTable SUPERIOR_DROP_TABLE =
      NpcCombatDropTable.builder()
          .probabilityDenominator(64)
          .log(true)
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.IMBUED_HEART).weight(1)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ETERNAL_GEM).weight(1)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DUST_BATTLESTAFF).weight(3)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIST_BATTLESTAFF).weight(3)))
          .build();
  private static final NpcCombatDropTable TOTEM_DROP_TABLE =
      NpcCombatDropTable.builder()
          .order(NpcCombatDropTable.Order.RANDOM_UNIQUE)
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM_BASE)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM_MIDDLE)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM_TOP)))
          .build();
  private static final NpcCombatDropTable SHARD_DROP_TABLE =
      NpcCombatDropTable.builder()
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_SHARD)))
          .build();

  @Override
  public List<NpcCombatDropTable> getAdditionalDropTables(int npcId) {
    var tables = new ArrayList<NpcCombatDropTable>();
    var combatDef = NpcCombatDefinition.getDefinition(npcId);
    var isSuperior = combatDef.getKillCountName(npcId).equals("Superior Slayer Creature");
    if (isSuperior) {
      tables.add(SUPERIOR_DROP_TABLE);
    }
    return tables;
  }

  @Override
  public void deathDropItems(
      Npc npc,
      Player player,
      int index,
      boolean isSlayerTask,
      boolean isWildernessSlayerTask,
      Tile dropTile,
      int dropRateDivider,
      boolean hasRowCharge) {
    if (!npc.getCombatDef().getDrop().getClues().isEmpty()) {
      for (var entry : npc.getCombatDef().getDrop().getClues().entrySet()) {
        var denominator =
            player
                .getCombat()
                .getDropRateDenominator(
                    entry.getValue(), entry.getKey().getScrollId(), npc.getId());
        denominator *= dropRateDivider;
        if (!PRandom.inRange(1, denominator)) {
          continue;
        }
        var clueItem = new Item(entry.getKey().getScrollId());
        if ((player.canDonatorPickupItem(clueItem.getId()))
            && player.getInventory().canAddItem(clueItem)) {
          player.getInventory().addItem(clueItem);
        } else {
          npc.getController().addMapItem(clueItem, dropTile, player);
        }
        player.getPlugin(TreasureTrailPlugin.class).resetProgress(entry.getKey());
      }
    }
    if (!npc.getCombatDef().getDrop().getPets().isEmpty()) {
      for (var entry : npc.getCombatDef().getDrop().getPets().entrySet()) {
        var itemId = entry.getValue();
        var denominator =
            player.getCombat().getDropRateDenominator(entry.getKey(), itemId, npc.getId());
        if (!PRandom.inRange(1, denominator)) {
          continue;
        }
        if (!player.getPlugin(FamiliarPlugin.class).unlockPet(itemId)) {
          continue;
        }
        player.getCombat().logNPCItem(npc.getCombatDef().getKillCountName(npc.getId()), itemId, 1);
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.LOGGED_DROPS, 1);
      }
    }
    if (npc.getCombatDef().getKillCount().isSendMessage()) {
      player.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.BOSS_KILLS, 1);
    }
    var allowSpecialDrops =
        (npc.getCombatDef().getDrop().hasDrops() && player.getController().canTeleport(false)
            || npc.getArea().inWilderness());
    var coinRate = 8;
    if (npc.getController().inMultiCombat() && !npc.getCombatDef().getKillCount().isSendMessage()) {
      coinRate = 32;
    }
    if (!Settings.getInstance().isSpawn() && allowSpecialDrops && PRandom.randomE(coinRate) == 0) {
      var value =
          StrictMath.hypot(
              npc.getDef().getCombatLevel(), npc.getCombatDef().getHitpoints().getTotal());
      var multiplier = Math.min(8, Math.sqrt(value)) * 8 + PRandom.randomI(8);
      var result = (int) Math.scalb(StrictMath.log(value), 8);
      var coins = (int) (result * multiplier / dropRateDivider);
      if (player.getGameMode().isIronType()) {
        coins /= 4;
      }
      var coinMultiplier = 1.0;
      coinMultiplier += player.getPlugin(BondPlugin.class).getDonatorRank().getMultiplier() - 1;
      coins *= coinMultiplier;
      if (coins > 0) {
        npc.getController().addMapItem(new Item(ItemId.COINS, coins), dropTile, player);
      }
    }
    if (!Settings.getInstance().isSpawn()
        && allowSpecialDrops
        && PRandom.randomE(npc.getController().inMultiCombat() ? 100 : 50) == 0) {
      var half1Count =
          (int)
              Math.min(
                  player.getItemCount(ItemId.TOOTH_HALF_OF_KEY)
                      + player.getItemCount(ItemId.TOOTH_HALF_OF_KEY_NOTED),
                  Item.MAX_AMOUNT);
      var half2Count =
          (int)
              Math.min(
                  player.getItemCount(ItemId.LOOP_HALF_OF_KEY)
                      + player.getItemCount(ItemId.LOOP_HALF_OF_KEY_NOTED),
                  Item.MAX_AMOUNT);
      int keyHalfId;
      if (half1Count < half2Count) {
        keyHalfId = ItemId.TOOTH_HALF_OF_KEY;
      } else if (half2Count < half1Count) {
        keyHalfId = ItemId.LOOP_HALF_OF_KEY;
      } else {
        keyHalfId = PRandom.randomI(1) == 0 ? ItemId.TOOTH_HALF_OF_KEY : ItemId.LOOP_HALF_OF_KEY;
      }
      if ((player.canDonatorPickupItem(keyHalfId) || hasRowCharge)
          && player.getInventory().canAddItem(keyHalfId, 1)) {
        player.getInventory().addItem(keyHalfId, 1);
      } else {
        npc.getController().addMapItem(new Item(keyHalfId), dropTile, player);
      }
    }
    if (isWildernessSlayerTask) {
      if (player.getPlugin(BountyHunterPlugin.class).hasEmblem()) {
        if (!player.getGameMode().isIronType()) {
          player
              .getInventory()
              .addOrDropItem(ItemId.BLOOD_MONEY, npc.getDef().getCombatLevel() / 2);
        }
        if (PRandom.randomE(8) == 0) {
          npc.getController().addMapItem(new Item(ItemId.DARK_CRAB), dropTile, player);
        }
      }
      if (npc.getCombat().getMaxHitpoints() * 0.8 < 320
          && PRandom.randomE((int) (320 - npc.getCombat().getMaxHitpoints() * 0.8) / 4) == 0) {
        npc.getController().addMapItem(new Item(ItemId.SLAYERS_ENCHANTMENT), dropTile, player);
      }
    }
    if (npc.getArea().inWilderness()
        && PRandom.randomE(30) == 0
        && !player.hasItem(ItemId.LOOTING_BAG)) {
      npc.getController().addMapItem(new Item(ItemId.LOOTING_BAG), dropTile, player);
    }
    var isCursed =
        npc.getDef().getName().startsWith("Cursed")
            && isWildernessSlayerTask
            && npc.getArea().inWilderness();
    var isSuperior =
        npc.getCombatDef().getKillCountName(npc.getId()).equals("Superior Slayer Creature");
    if (isSuperior || isCursed && PRandom.randomE(25) == 0) {
      var denominator =
          (int) (200 - (StrictMath.pow(npc.getCombatDef().getSlayer().getLevel() + 55, 2) / 125.0));
      denominator =
          player.getCombat().getDropRateDenominator(denominator, ItemId.IMBUED_HEART, npc.getId());
      if (PRandom.inRange(1, denominator)) {
        SUPERIOR_DROP_TABLE.dropItems(npc, player, dropTile);
      }
    }
    if (npc.getArea().inCatacombsOfKourend()) {
      var denominator = 500 - Math.min(npc.getCombat().getMaxHitpoints(), 400);
      denominator =
          player
              .getCombat()
              .getDropRateDenominator(denominator, ItemId.DARK_TOTEM_BASE, npc.getId());
      if (isSuperior || PRandom.inRange(1, denominator)) {
        TOTEM_DROP_TABLE.dropItems(npc, player, dropTile);
      }
      denominator = (int) (0.66 * (500 - Math.min(npc.getCombat().getMaxHitpoints(), 400)));
      denominator =
          player.getCombat().getDropRateDenominator(denominator, ItemId.ANCIENT_SHARD, npc.getId());
      if (PRandom.inRange(1, denominator)) {
        SHARD_DROP_TABLE.dropItems(npc, player, dropTile);
      }
    }
  }
}
