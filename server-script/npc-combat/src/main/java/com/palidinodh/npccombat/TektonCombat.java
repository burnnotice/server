package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.TileHitEvent;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class TektonCombat extends NpcCombat {

  private static final int MOVE_DELAY = 2;
  private static final int ANVIL_TIME = 20;
  private static final int SEARCH_TIME = 12;
  private static final int ATTACK_SPEED = 6;
  private static final Tile SPAWN_TILE = new Tile(3307, 5294, 1);
  private static final Tile ANVIL_TILE = new Tile(3305, 5294, 1);
  private static final NpcCombatStyle STAB_ATTACK =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MELEE_STAB)
          .animation(7482)
          .attackSpeed(ATTACK_SPEED)
          .damage(NpcCombatDamage.builder().maximum(50).prayerEffectiveness(0.5).build())
          .build();
  private static final NpcCombatStyle SLASH_ATTACK =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MELEE_SLASH)
          .animation(7483)
          .attackSpeed(ATTACK_SPEED)
          .damage(NpcCombatDamage.builder().maximum(50).prayerEffectiveness(0.5).build())
          .build();
  private static final NpcCombatStyle CRUSH_ATTACK =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MELEE_CRUSH)
          .animation(7484)
          .attackSpeed(ATTACK_SPEED)
          .damage(NpcCombatDamage.builder().maximum(50).prayerEffectiveness(0.5).build())
          .build();
  private static final NpcCombatStyle ENRAGED_STAB_ATTACK =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MELEE_STAB)
          .animation(7493)
          .attackSpeed(ATTACK_SPEED)
          .damage(NpcCombatDamage.builder().maximum(70).prayerEffectiveness(0.5).build())
          .build();
  private static final NpcCombatStyle ENRAGED_SLASH_ATTACK =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MELEE_SLASH)
          .animation(7494)
          .attackSpeed(ATTACK_SPEED)
          .damage(NpcCombatDamage.builder().maximum(70).prayerEffectiveness(0.5).build())
          .build();
  private static final NpcCombatStyle ENRAGED_CRUSH_ATTACK =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MELEE_CRUSH)
          .animation(7492)
          .attackSpeed(ATTACK_SPEED)
          .damage(NpcCombatDamage.builder().maximum(70).prayerEffectiveness(0.5).build())
          .build();
  private static final Tile[] LAVA_TILES = {
    new Tile(3304, 5286, 1),
    new Tile(3303, 5289, 1),
    new Tile(3301, 5291, 1),
    new Tile(3300, 5292, 1),
    new Tile(3300, 5294, 1),
    new Tile(3301, 5300, 1),
    new Tile(3302, 5302, 1),
    new Tile(3304, 5304, 1),
    new Tile(3305, 5304, 1),
    new Tile(3307, 5303, 1),
    new Tile(3313, 5303, 1),
    new Tile(3319, 5306, 1),
    new Tile(3322, 5300, 1),
    new Tile(3324, 5300, 1),
    new Tile(3322, 5294, 1),
    new Tile(3322, 5292, 1),
    new Tile(3316, 5285, 1),
    new Tile(3318, 5285, 1),
    new Tile(3320, 5286, 1),
    new Tile(3322, 5286, 1),
    new Tile(3323, 5285, 1)
  };

  static {
    ANVIL_TILE.setSize(2, 4);
  }

  @Inject private Npc npc;
  private boolean loaded;
  private int actionTime;
  private int enragedTime;
  private boolean combatMove;
  private NpcCombatStyle lastAttackStyle;
  private Tile.Direction lastDirectionFrom;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().additionalPlayers(255);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(450);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_ONYX)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STINKHORN_MUSHROOM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OVERLOAD_PLUS_4, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.REVITALISATION_PLUS_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_ENHANCE_PLUS_4)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.TEKTON).id(NpcId.TEKTON_7541).id(NpcId.TEKTON_7542).id(NpcId.TEKTON_7545);
    combat.hitpoints(NpcCombatHitpoints.total(400));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(390)
            .magicLevel(205)
            .defenceLevel(205)
            .bonus(BonusType.MELEE_ATTACK, 64)
            .bonus(BonusType.DEFENCE_STAB, 155)
            .bonus(BonusType.DEFENCE_SLASH, 165)
            .bonus(BonusType.DEFENCE_CRUSH, 105)
            .bonus(BonusType.DEFENCE_MAGIC, 600)
            .bonus(BonusType.DEFENCE_RANGED, 600)
            .build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    combat.deathAnimation(7495);
    combat.drop(drop.build());

    var enragedCombat = NpcCombatDefinition.builder();
    enragedCombat.id(NpcId.TEKTON_ENRAGED).id(NpcId.TEKTON_ENRAGED_7544);
    enragedCombat.hitpoints(NpcCombatHitpoints.total(400));
    enragedCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(390)
            .magicLevel(205)
            .defenceLevel(205)
            .bonus(BonusType.MELEE_ATTACK, 64)
            .bonus(BonusType.DEFENCE_STAB, 280)
            .bonus(BonusType.DEFENCE_SLASH, 290)
            .bonus(BonusType.DEFENCE_CRUSH, 180)
            .bonus(BonusType.DEFENCE_MAGIC, 600)
            .bonus(BonusType.DEFENCE_RANGED, 600)
            .build());
    enragedCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    enragedCombat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    enragedCombat.deathAnimation(7495);
    enragedCombat.drop(drop.build());

    return Arrays.asList(combat.build(), enragedCombat.build());
  }

  @Override
  public void tickStartHook() {
    if (!loaded) {
      loadProfile();
    }
    if (!npc.getController().isRegionLoaded()) {
      return;
    }
    if (npc.getId() == NpcId.TEKTON) {
      faceAnvil();
      var players = npc.getController().getPlayers();
      for (var player : players) {
        if (npc.withinDistance(player, 4)) {
          startCombatMove();
          break;
        }
      }
    } else {
      if (enragedTime > 0) {
        enragedTime--;
        if (enragedTime == 0) {
          if (npc.getId() == NpcId.TEKTON_ENRAGED) {
            npc.setId(NpcId.TEKTON_7541);
          } else if (npc.getId() == NpcId.TEKTON_ENRAGED_7544) {
            npc.setId(NpcId.TEKTON_7542);
          }
        }
      }
      if (moveTick()) {
      } else if (combatTick()) {
      } else if (anvilTick()) {
      }
    }
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    if (npc.getId() == NpcId.TEKTON || npc.getId() == NpcId.TEKTON_7545) {
      return false;
    } else if (hitStyleType != HitStyleType.MELEE) {
      if (sendMessage && opponent instanceof Player) {
        var player = opponent.asPlayer();
        player.getGameEncoder().sendMessage("This attack seems to have no affect.");
      }
      return false;
    }
    return true;
  }

  @Override
  public void applyDeadEndHook() {
    npc.getController().addMapObject(new MapObject(-1, 10, 0, npc));
  }

  public void loadProfile() {
    if (loaded || npc.getController().getPlayers().isEmpty()) {
      return;
    }
    loaded = true;
    npc.getSpawn().tile(SPAWN_TILE);
    npc.setTile(npc.getSpawn().getTile());
    var averageHP = 0;
    var playerMultiplier = 1.0;
    var players = npc.getController().getPlayers();
    for (var player : players) {
      averageHP += player.getCombat().getMaxHitpoints();
      playerMultiplier += 0.5;
    }
    averageHP /= players.size();
    var hitpoints = (int) ((50 + players.size() * 25 + averageHP * 2) * playerMultiplier);
    setMaxHitpoints(hitpoints);
    setHitpoints(getMaxHitpoints());
    npc.setId(NpcId.TEKTON);
  }

  public void startCombatMove() {
    var players = npc.getController().getPlayers();
    if (players.isEmpty()) {
      return;
    }
    if (players.size() > 1) {
      Collections.shuffle(players);
    }
    Player selected = null;
    for (var player : players) {
      if (player.isLocked()
          || !npc.withinDistance(player, 16)
          || !npc.getController().routeAllow(player)) {
        continue;
      }
      selected = player;
      break;
    }
    if (selected == null) {
      return;
    }
    unlock();
    combatMove = true;
    if (npc.getId() == NpcId.TEKTON) {
      npc.setId(NpcId.TEKTON_7541);
    } else {
      npc.setId(NpcId.TEKTON_ENRAGED);
      enragedTime = 25;
    }
    npc.setAnimation(7474);
    npc.setLock(MOVE_DELAY);
    npc.getController().setMagicBind(MOVE_DELAY);
    npc.getMovement().setFollowing(selected);
    actionTime = SEARCH_TIME;
  }

  public boolean moveTick() {
    if (npc.getId() != NpcId.TEKTON_7541 && npc.getId() != NpcId.TEKTON_ENRAGED
        || npc.getMovement().isRouting()
        || npc.getController().isMagicBound()) {
      return false;
    }
    if (combatMove) {
      if (actionTime > 0) {
        actionTime--;
      }
      var playerUnderneath = false;
      for (var player : npc.getController().getPlayers()) {
        if (npc.withinDistance(player, 0)) {
          playerUnderneath = true;
          break;
        }
      }
      if (playerUnderneath
          || actionTime == 0
          || !npc.getMovement().isRouting() && actionTime < SEARCH_TIME - MOVE_DELAY - 1) {
        combatMove = false;
        startAnvil();
        return true;
      }
      if (!npc.withinDistance(npc.getMovement().getFollowing(), 1)) {
        return true;
      }
      if (npc.getId() == NpcId.TEKTON_7541) {
        npc.setId(NpcId.TEKTON_7542);
      } else {
        npc.setId(NpcId.TEKTON_ENRAGED_7544);
      }
      npc.getMovement().setFollowing(null);
      npc.setFaceEntity(null);
      actionTime = 50 + PRandom.randomI(25);
      npc.getController().addMapObject(new MapObject(12810, 10, 0, npc));
    } else {
      npc.setId(NpcId.TEKTON_7545);
      npc.setAnimation(7475);
      faceAnvil();
      enragedTime = 0;
      actionTime = ANVIL_TIME;
    }
    lastDirectionFrom = null;
    lastAttackStyle = null;
    combatMove = false;
    return true;
  }

  public boolean combatTick() {
    if (npc.getId() != NpcId.TEKTON_7542 && npc.getId() != NpcId.TEKTON_ENRAGED_7544) {
      return false;
    }
    if (actionTime > 0) {
      actionTime--;
    }
    if (getHitDelay() == ATTACK_SPEED - 4) {
      for (var player : npc.getController().getPlayers()) {
        if (player.isLocked() || !npc.withinDistance(player, 6)) {
          continue;
        }
        if (npc.getDirection(player) != lastDirectionFrom) {
          continue;
        }
        applyAttack(lastAttackStyle, player, Graphic.getDefaultSpeed(), 0);
      }
    }
    if (isHitDelayed()) {
      return true;
    }
    if (actionTime == 0) {
      startAnvil();
      npc.getController().addMapObject(new MapObject(-1, 10, 0, npc));
      return true;
    }
    Player selected = null;
    var players = npc.getController().getPlayers();
    if (players.size() > 1) {
      Collections.shuffle(players);
    }
    for (var player : players) {
      if (player.isLocked() || !npc.withinDistance(player, 1) || npc.withinDistance(player, 0)) {
        continue;
      }
      selected = player;
      break;
    }
    if (selected == null) {
      actionTime = 0;
      return true;
    }
    var direction = npc.getDirection(selected);
    lastDirectionFrom = direction;
    if (direction == Tile.Direction.SOUTH) {
      npc.setFaceTile(new Tile(npc.getX() + npc.getSizeX() / 2, npc.getY() + npc.getSizeY() + 1));
    } else if (direction == Tile.Direction.NORTH) {
      npc.setFaceTile(new Tile(npc.getX() + npc.getSizeX() / 2, npc.getY() - 1));
    } else if (direction == Tile.Direction.WEST) {
      npc.setFaceTile(new Tile(npc.getX() + npc.getSizeX() + 1, npc.getY() + npc.getSizeY() / 2));
    } else if (direction == Tile.Direction.EAST) {
      npc.setFaceTile(new Tile(npc.getX() - 1, npc.getY() + npc.getSizeY() / 2));
    }
    if (npc.getId() == NpcId.TEKTON_7542) {
      var style = PRandom.randomI(2);
      lastAttackStyle = style == 0 ? STAB_ATTACK : style == 1 ? SLASH_ATTACK : CRUSH_ATTACK;
    } else {
      var style = PRandom.randomI(2);
      lastAttackStyle =
          style == 0
              ? ENRAGED_STAB_ATTACK
              : style == 1 ? ENRAGED_SLASH_ATTACK : ENRAGED_CRUSH_ATTACK;
    }
    npc.setAnimation(lastAttackStyle.getAnimation());
    setHitDelay(lastAttackStyle.getAttackSpeed());
    return true;
  }

  public void faceAnvil() {
    var direction = npc.getDirection(ANVIL_TILE);
    if (direction == Tile.Direction.SOUTH) {
      npc.setFaceTile(new Tile(npc.getX() + npc.getSizeX() / 2, npc.getY() + npc.getSizeY() + 1));
    } else if (direction == Tile.Direction.NORTH) {
      npc.setFaceTile(new Tile(npc.getX() + npc.getSizeX() / 2, npc.getY() - 1));
    } else if (direction == Tile.Direction.WEST) {
      npc.setFaceTile(new Tile(npc.getX() + npc.getSizeX() + 1, npc.getY() + npc.getSizeY() / 2));
    } else if (direction == Tile.Direction.EAST) {
      npc.setFaceTile(new Tile(npc.getX() - 1, npc.getY() + npc.getSizeY() / 2));
    }
  }

  public void startAnvil() {
    npc.getMovement().setFollowing(null);
    npc.setFaceEntity(null);
    npc.setId(NpcId.TEKTON_7541);
    enragedTime = 0;
    npc.setAnimation(7479);
    npc.getController().setMagicBind(MOVE_DELAY);
    npc.getMovement().clear();
    npc.getMovement().addMovement(SPAWN_TILE);
    lock();
  }

  public boolean anvilTick() {
    if (npc.getId() != NpcId.TEKTON_7545) {
      return false;
    }
    if (actionTime == ANVIL_TIME - 4
        || actionTime == ANVIL_TIME - 8
        || actionTime == ANVIL_TIME - 12
        || actionTime == ANVIL_TIME - 16) {
      var players = npc.getController().getPlayers();
      for (var player : players) {
        if (player.isLocked() || !npc.withinDistance(player, 16) || player.getY() < 5282) {
          continue;
        }
        for (var i = 0; i < 2; i++) {
          var lavaTile = LAVA_TILES[PRandom.randomE(LAVA_TILES.length)];
          var speed = getProjectileSpeed(lavaTile.getDistance(player));
          npc.getController()
              .sendMapGraphic(
                  player, new Graphic(659, 0, speed.getClientSpeed() + speed.getClientDelay()));
          npc.getController()
              .sendMapProjectile(
                  null,
                  lavaTile,
                  player,
                  660,
                  1,
                  1,
                  speed.getClientDelay(),
                  speed.getClientSpeed(),
                  16,
                  64);
          var the =
              new TileHitEvent(
                  speed.getEventDelay(), npc.getController(), player, 15, HitStyleType.TYPELESS);
          the.setRadius(1);
          the.setFullDamage(true);
          addEvent(the);
        }
      }
    }
    if (actionTime > 0) {
      actionTime--;
    }
    if (actionTime == 0) {
      startCombatMove();
    }
    return true;
  }
}
