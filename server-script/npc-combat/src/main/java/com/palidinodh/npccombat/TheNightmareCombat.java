package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.VarpId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatExplodingTile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatQuadrants;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatSummonNpc;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.MapObjectSpawn;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PEventTasks;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

class TheNightmareCombat extends NpcCombat {

  private static final NpcCombatDropTable EQUIPMENT_DROP_TABLE =
      NpcCombatDropTable.builder()
          .probabilityDenominator(120)
          .broadcast(true)
          .log(true)
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INQUISITORS_MACE)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INQUISITORS_GREAT_HELM)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INQUISITORS_HAUBERK)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INQUISITORS_PLATESKIRT)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NIGHTMARE_STAFF)))
          .build();
  private static final NpcCombatDropTable ORB_DROP_TABLE =
      NpcCombatDropTable.builder()
          .probabilityDenominator(600)
          .broadcast(true)
          .log(true)
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.HARMONISED_ORB)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VOLATILE_ORB)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ELDRITCH_ORB)))
          .build();
  private static final NpcCombatStyle HUSKS_ATTACK;
  private static final NpcCombatStyle FLOWER_POWER_ATTACK;
  private static final NpcCombatStyle GRASPING_CLAWS_ATTACK;
  private static final NpcCombatStyle CURSE_ATTACK;
  private static final NpcCombatStyle PARASITES_ATTACK;
  private static final NpcCombatStyle SPORES_ATTACK;
  private static final NpcCombatStyle SLEEPWALKERS_ATTACK = NpcCombatStyle.builder().build();
  private static final Map<Integer, List<NpcCombatStyle>> PHASE_SPECIAL_ATTACKS;
  private static final List<Integer> SHIELD_COMBAT_IDS =
      PCollection.toImmutableList(
          NpcId.THE_NIGHTMARE_814, NpcId.THE_NIGHTMARE_814_9427, NpcId.THE_NIGHTMARE_814_9429);
  private static final List<Integer> TOTEM_COMBAT_IDS =
      PCollection.toImmutableList(
          NpcId.THE_NIGHTMARE_814_9426, NpcId.THE_NIGHTMARE_814_9428, NpcId.THE_NIGHTMARE_814_9430);
  private static final Tile CENTER_TILE = new Tile(3872, 9951, 3);
  private static final Tile ARENA_SIZE_TILE = new Tile(9, 10);
  private static final List<NpcSpawn> TOTEM_SPAWNS =
      PCollection.toImmutableList(
          new NpcSpawn(CENTER_TILE.copy().moveTile(-9, -9), NpcId.TOTEM),
          new NpcSpawn(CENTER_TILE.copy().moveTile(7, -9), NpcId.TOTEM_9437),
          new NpcSpawn(CENTER_TILE.copy().moveTile(-9, 7), NpcId.TOTEM_9440),
          new NpcSpawn(CENTER_TILE.copy().moveTile(7, 7), NpcId.TOTEM_9443));

  static {
    var style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.MAGIC)
            .subHitStyleType(HitStyleType.TYPELESS)
            .build());
    style.animation(8599).attackSpeed(10).applyAttackDelay(3);
    style.projectile(NpcCombatProjectile.builder().id(1781).startHeight(80).delay(0).build());
    style.effect(NpcCombatEffect.builder().bind(Integer.MAX_VALUE).build());
    style.multiCombat(NpcCombatMulti.builder().chance(50).build());
    var summonNpc = NpcCombatSummonNpc.builder();
    summonNpc.type(NpcCombatSummonNpc.Type.OPPONENT).target(true);
    summonNpc.spawn(new NpcSpawn(NpcId.HUSK_48)).spawn(new NpcSpawn(NpcId.HUSK_48_9455));
    style.specialAttack(summonNpc.build());
    HUSKS_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder()
            .minimum(4)
            .maximum(6)
            .ignoreDefence(true)
            .ignorePrayer(true)
            .build());
    style
        .animation(8601)
        .attackSpeed(10)
        .nearbyMessage("<col=ff0000>The Nightmare splits the area into segments!</col>");
    var quadrants = NpcCombatQuadrants.builder();
    quadrants.centerTile(CENTER_TILE).size(ARENA_SIZE_TILE);
    var phase = NpcCombatQuadrants.Phase.builder();
    phase.safeMapObjects(
        NpcCombatQuadrants.MapObjectPhase.builder()
            .mapObject(new MapObject(ObjectId.NIGHTMARE_BLOSSOM, 10))
            .build());
    phase.unsafeMapObjects(
        NpcCombatQuadrants.MapObjectPhase.builder()
            .mapObject(new MapObject(ObjectId.NIGHTMARE_BERRIES, 10))
            .build());
    quadrants.phase(phase.build());
    phase = NpcCombatQuadrants.Phase.builder();
    phase.safeMapObjects(
        NpcCombatQuadrants.MapObjectPhase.builder()
            .mapObject(new MapObject(ObjectId.NIGHTMARE_BLOSSOM_37744, 10))
            .animation(8617)
            .build());
    phase.unsafeMapObjects(
        NpcCombatQuadrants.MapObjectPhase.builder()
            .mapObject(new MapObject(ObjectId.NIGHTMARE_BERRIES_37741, 10))
            .animation(8623)
            .build());
    quadrants.phase(phase.build());
    phase = NpcCombatQuadrants.Phase.builder();
    phase.delay(4).startDamage(true);
    phase.safeMapObjects(
        NpcCombatQuadrants.MapObjectPhase.builder()
            .mapObject(new MapObject(ObjectId.NIGHTMARE_BLOSSOM_37745, 10))
            .animation(8619)
            .build());
    phase.unsafeMapObjects(
        NpcCombatQuadrants.MapObjectPhase.builder()
            .mapObject(new MapObject(ObjectId.NIGHTMARE_BERRIES_37742, 10))
            .animation(8625)
            .build());
    quadrants.phase(phase.build());
    phase = NpcCombatQuadrants.Phase.builder();
    phase.delay(20);
    phase.safeMapObjects(NpcCombatQuadrants.MapObjectPhase.animation(8621));
    phase.unsafeMapObjects(NpcCombatQuadrants.MapObjectPhase.animation(8627));
    quadrants.phase(phase.build());
    style.specialAttack(quadrants.build());
    FLOWER_POWER_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.animation(8598).attackSpeed(10).targetTileGraphic(new Graphic(1767));
    style.damage(NpcCombatDamage.maximum(50));
    style.projectile(NpcCombatProjectile.builder().speedMaximumDistance(1).build());
    var targetTile = NpcCombatTargetTile.builder();
    targetTile.eventDelay(6);
    targetTile.breakOff(
        NpcCombatTargetTile.BreakOff.builder().count(48).distance(20).skipBoundTiles(true).build());
    style.specialAttack(targetTile.build());
    GRASPING_CLAWS_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style
        .animation(8599)
        .attackSpeed(10)
        .message("<col=ff0000>The Nightmare has cursed you, shuffling your prayers!</col>");
    style.multiCombat(NpcCombatMulti.builder().chance(50).build());
    CURSE_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style
        .animation(8606)
        .attackSpeed(10)
        .message("<col=ff0000>The Nightmare has impregnated you with a deadly parasite!</col>");
    style.projectile(NpcCombatProjectile.id(1770));
    style.multiCombat(NpcCombatMulti.builder().chance(50).build());
    PARASITES_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style
        .animation(8606)
        .attackSpeed(10)
        .nearbyMessage("<col=ff0000>The Nightmare summons some infectious spores!</col>");
    var explodingTile = NpcCombatExplodingTile.builder();
    explodingTile.duration(10).radius(1).activateWithinRadius(true);
    explodingTile.mapObjectSpawn(
        MapObjectSpawn.builder()
            .spawnMapObject(new MapObject(ObjectId.SPORE, 10))
            .spawnAnimation(8630)
            .spawnDelay(2)
            .mapObjectId(ObjectId.SPORE_37739)
            .despawnAnimation(8632)
            .despawnDelay(2)
            .build());
    explodingTile
        .tile(CENTER_TILE.copy())
        .tile(CENTER_TILE.copy().moveTile(-4, -4))
        .tile(CENTER_TILE.copy().moveTile(4, 4))
        .tile(CENTER_TILE.copy().moveTile(4, -4))
        .tile(CENTER_TILE.copy().moveTile(-4, 4))
        .tile(CENTER_TILE.copy().moveTile(0, 6))
        .tile(CENTER_TILE.copy().moveTile(6, 0))
        .tile(CENTER_TILE.copy().moveTile(0, -6))
        .tile(CENTER_TILE.copy().moveTile(-6, 0))
        .tile(CENTER_TILE.copy().moveTile(-5, 8))
        .tile(CENTER_TILE.copy().moveTile(5, 8))
        .tile(CENTER_TILE.copy().moveTile(8, 5))
        .tile(CENTER_TILE.copy().moveTile(8, -5))
        .tile(CENTER_TILE.copy().moveTile(-5, -8))
        .tile(CENTER_TILE.copy().moveTile(5, -8))
        .tile(CENTER_TILE.copy().moveTile(-8, 5))
        .tile(CENTER_TILE.copy().moveTile(-8, -5));
    style.specialAttack(explodingTile.build());
    SPORES_ATTACK = style.build();

    PHASE_SPECIAL_ATTACKS =
        PCollection.toImmutableMap(
            SHIELD_COMBAT_IDS.get(0),
            PCollection.toImmutableList(HUSKS_ATTACK, FLOWER_POWER_ATTACK, GRASPING_CLAWS_ATTACK),
            TOTEM_COMBAT_IDS.get(0),
            PCollection.toImmutableList(HUSKS_ATTACK, FLOWER_POWER_ATTACK, GRASPING_CLAWS_ATTACK),
            SHIELD_COMBAT_IDS.get(1),
            PCollection.toImmutableList(CURSE_ATTACK, PARASITES_ATTACK, GRASPING_CLAWS_ATTACK),
            TOTEM_COMBAT_IDS.get(1),
            PCollection.toImmutableList(CURSE_ATTACK, PARASITES_ATTACK, GRASPING_CLAWS_ATTACK),
            SHIELD_COMBAT_IDS.get(2),
            PCollection.toImmutableList(SPORES_ATTACK, GRASPING_CLAWS_ATTACK),
            TOTEM_COMBAT_IDS.get(2),
            PCollection.toImmutableList(GRASPING_CLAWS_ATTACK));
  }

  @Inject private Npc npc;
  private int realHitpoints;
  private int maxShield;
  private PEventTasks flowerPowerEvent;
  private PEventTasks sporesEvent;
  private int normalAttacks;
  private NpcCombatStyle specialAttack;
  private NpcCombatStyle lastSpecialAttack;
  private List<Npc> totems = new ArrayList<>();
  private PArrayList<Npc> sleepwalkers = new PArrayList<>();
  private List<Player> players = new ArrayList<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .clue(ClueScrollType.ELITE, 200)
            .pet(4000, ItemId.LITTLE_NIGHTMARE)
            .additionalPlayers(255);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(2000).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JAR_OF_DREAMS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_RUNE, 6, 138)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 24, 165)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 13, 129)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 12, 57)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_LOGS_NOTED, 14, 111)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_LOGS_NOTED, 3, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLD_ORE_NOTED, 14, 79)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 16, 253)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_ORE_NOTED, 15, 59)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_ORE_NOTED, 8, 33)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE_NOTED, 1, 7)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL_NOTED, 1, 16)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SARADOMIN_BREW_3, 1, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ZAMORAK_BREW_3, 1, 8)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SANFEW_SERUM_3, 1, 11)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_ARROW, 32, 488)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_ARROW, 12, 515)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COSMIC_RUNE, 15, 214)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 420, 3280)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_EMERALD_NOTED, 1, 26)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_RUBY, 2, 35)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHARK, 1, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BASS, 1, 18)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_POTION_3, 2, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 2717, 30_000)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BASS)));
    drop.table(dropTable.build());

    var shieldHitpoints =
        NpcCombatHitpoints.builder()
            .total(2400)
            .barType(HitpointsBarType.CYAN_DARKER_140)
            .hitMarkType(HitMarkType.CYAN);
    var stats =
        NpcCombatStats.builder()
            .attackLevel(150)
            .magicLevel(150)
            .rangedLevel(150)
            .defenceLevel(150)
            .bonus(BonusType.MELEE_ATTACK, 140)
            .bonus(BonusType.ATTACK_MAGIC, 140)
            .bonus(BonusType.ATTACK_RANGED, 140)
            .bonus(BonusType.DEFENCE_STAB, 120)
            .bonus(BonusType.DEFENCE_SLASH, 180)
            .bonus(BonusType.DEFENCE_CRUSH, 40)
            .bonus(BonusType.DEFENCE_MAGIC, 600)
            .bonus(BonusType.DEFENCE_RANGED, 600);
    var aggression =
        NpcCombatAggression.builder()
            .range(16)
            .always(true)
            .checkWhileAttacking(true)
            .highestDefenceType(HitStyleType.MELEE);
    var immunity = NpcCombatImmunity.builder().poison(true).venom(true);
    var focus = NpcCombatFocus.builder().keepWithinDistance(1);

    var meleeAttack = NpcCombatStyle.builder();
    meleeAttack.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.MELEE)
            .meleeAttackStyle(BonusType.ATTACK_SLASH)
            .directional(true)
            .build());
    meleeAttack.damage(
        NpcCombatDamage.builder()
            .maximum(25)
            .ignoreDefence(true)
            .prayerEffectiveness(0.2)
            .wrongPrayerEffectiveness(1.2)
            .delayedPrayerProtectable(true)
            .build());
    meleeAttack.animation(8594).attackSpeed(6).applyAttackDelay(3);
    meleeAttack.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);

    var rangedAttack = NpcCombatStyle.builder();
    rangedAttack.type(NpcCombatStyleType.RANGED);
    rangedAttack.damage(
        NpcCombatDamage.builder()
            .maximum(25)
            .prayerEffectiveness(0.2)
            .wrongPrayerEffectiveness(1.2)
            .delayedPrayerProtectable(true)
            .build());
    rangedAttack.animation(8596).attackSpeed(6).applyAttackDelay(3);
    rangedAttack.projectile(
        NpcCombatProjectile.builder().id(1766).startHeight(80).delay(0).build());
    rangedAttack.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);

    var magicAttack = NpcCombatStyle.builder();
    magicAttack.type(NpcCombatStyleType.MAGIC);
    magicAttack.damage(
        NpcCombatDamage.builder()
            .maximum(25)
            .prayerEffectiveness(0.2)
            .wrongPrayerEffectiveness(1.2)
            .delayedPrayerProtectable(true)
            .build());
    magicAttack
        .animation(8595)
        .attackSpeed(6)
        .applyAttackDelay(3)
        .targetGraphic(new Graphic(1765, 124));
    magicAttack.projectile(NpcCombatProjectile.builder().id(1764).startHeight(80).delay(0).build());
    magicAttack.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);

    var idleCombat = NpcCombatDefinition.builder();
    idleCombat.id(NpcId.THE_NIGHTMARE_814_9431).id(NpcId.THE_NIGHTMARE_814_9433);
    idleCombat.immunity(NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    idleCombat.aggression(NpcCombatAggression.NONE);
    idleCombat.drop(drop.build());

    var shield1Combat = NpcCombatDefinition.builder();
    shield1Combat.id(SHIELD_COMBAT_IDS.get(0));
    shield1Combat.spawn(
        NpcCombatSpawn.builder().animation(8611).respawnId(TOTEM_COMBAT_IDS.get(0)).build());
    shield1Combat.hitpoints(shieldHitpoints.build());
    shield1Combat.stats(stats.build());
    shield1Combat.aggression(aggression.build());
    shield1Combat.immunity(immunity.build());
    shield1Combat.focus(focus.build());
    shield1Combat.style(meleeAttack.build());
    shield1Combat.style(rangedAttack.build());
    shield1Combat.style(magicAttack.build());

    var shield2Combat = NpcCombatDefinition.builder();
    shield2Combat.id(SHIELD_COMBAT_IDS.get(1));
    shield2Combat.spawn(NpcCombatSpawn.builder().respawnId(TOTEM_COMBAT_IDS.get(1)).build());
    shield2Combat.hitpoints(shieldHitpoints.build());
    shield2Combat.stats(stats.build());
    shield2Combat.aggression(aggression.build());
    shield2Combat.immunity(immunity.build());
    shield2Combat.focus(focus.build());
    shield2Combat.style(meleeAttack.build());
    shield2Combat.style(rangedAttack.build());
    shield2Combat.style(magicAttack.build());

    var shield3Combat = NpcCombatDefinition.builder();
    shield3Combat.id(SHIELD_COMBAT_IDS.get(2));
    shield3Combat.spawn(NpcCombatSpawn.builder().respawnId(TOTEM_COMBAT_IDS.get(2)).build());
    shield3Combat.hitpoints(shieldHitpoints.build());
    shield3Combat.stats(stats.build());
    shield3Combat.aggression(aggression.build());
    shield3Combat.immunity(immunity.build());
    shield3Combat.focus(focus.build());
    shield3Combat.style(meleeAttack.build());
    shield3Combat.style(rangedAttack.build());
    shield3Combat.style(magicAttack.build());

    var totemCombat = NpcCombatDefinition.builder();
    TOTEM_COMBAT_IDS.forEach(
        i -> {
          if (i.equals(TOTEM_COMBAT_IDS.get(TOTEM_COMBAT_IDS.size() - 1))) {
            return;
          }
          totemCombat.id(i);
        });
    totemCombat.hitpoints(
        NpcCombatHitpoints.builder().total(2400).barType(HitpointsBarType.GREEN_RED_140).build());
    totemCombat.stats(stats.build());
    totemCombat.aggression(aggression.build());
    totemCombat.immunity(immunity.build());
    totemCombat.focus(focus.build());
    totemCombat.style(meleeAttack.build());
    totemCombat.style(rangedAttack.build());
    totemCombat.style(magicAttack.build());

    var lastTotemCombat = NpcCombatDefinition.builder();
    lastTotemCombat.id(TOTEM_COMBAT_IDS.get(TOTEM_COMBAT_IDS.size() - 1));
    lastTotemCombat.spawn(NpcCombatSpawn.builder().respawnId(NpcId.THE_NIGHTMARE_814_9432).build());
    lastTotemCombat.hitpoints(
        NpcCombatHitpoints.builder().total(2400).barType(HitpointsBarType.GREEN_RED_140).build());
    lastTotemCombat.stats(stats.build());
    lastTotemCombat.aggression(aggression.build());
    lastTotemCombat.immunity(immunity.build());
    lastTotemCombat.focus(focus.build());
    lastTotemCombat.deathAnimation(8612);
    lastTotemCombat.style(meleeAttack.build());
    lastTotemCombat.style(rangedAttack.build());
    lastTotemCombat.style(magicAttack.build());

    var deathCombat = NpcCombatDefinition.builder();
    deathCombat.id(NpcId.THE_NIGHTMARE_814_9432);
    deathCombat.immunity(NpcCombatImmunity.builder().playerAttack(true).npcAttack(true).build());
    deathCombat.aggression(NpcCombatAggression.NONE);
    deathCombat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    deathCombat.drop(drop.build());

    return Arrays.asList(
        idleCombat.build(),
        shield1Combat.build(),
        shield2Combat.build(),
        shield3Combat.build(),
        totemCombat.build(),
        lastTotemCombat.build(),
        deathCombat.build());
  }

  @Override
  public List<NpcCombatDropTable> getAdditionalDropTables() {
    return Arrays.asList(EQUIPMENT_DROP_TABLE, ORB_DROP_TABLE);
  }

  @Override
  public void npcApplyHitEndHook(Hit hit) {
    updateHealth();
  }

  @Override
  public void spawnHook() {
    start();
  }

  @Override
  public void despawnHook() {
    setEnterNpc(NpcId.THE_NIGHTMARE_814_9460, 8580);
    players.clear();
    flowerPowerEvent = null;
    sporesEvent = null;
  }

  @Override
  public void idChangedHook(int oldId, int newId) {
    if (TOTEM_COMBAT_IDS.contains(newId)) {
      addEvent(
          PEvent.singleEvent(
              1,
              e -> {
                setHitpoints(realHitpoints);
                updateHealth();
                players.forEach(
                    p -> {
                      p.getGameEncoder().setVarp(VarpId.HEALTH_OVERLAY_TYPE, 9432);
                      p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 13, 0xcc0000);
                      p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 14, 0x00cc00);
                    });
              }));
      npc.getController()
          .sendRegionMessage(
              "<col=ff0000>As the Nightmare's shield fails, the totems in the area are activated.</col>");
      totems.forEach(n -> n.setId(n.getId() + 1));
    } else if (newId == NpcId.THE_NIGHTMARE_814_9432) {
      dropItems();
      npc.lock();
      setEnterNpc(NpcId.THE_NIGHTMARE_814_9460, 8580);
      addEvent(
          PEvent.singleEvent(
              PTime.secToTick(30),
              e -> {
                if (!npc.isVisible()) {
                  return;
                }
                start();
              }));
    }
  }

  @Override
  public void tickStartHook() {
    players = npc.getController().getPlayers(npc.getRegionId());
    if (npc.isLocked()) {
      return;
    }
    if (isHitDelayed()) {
      return;
    }
    if (players.isEmpty()) {
      npc.getController().removeNpc(npc);
      return;
    }
    checkTotems();
    checkAttacks();
    if (getDefenceLevel() < 120) {
      setDefenceLevel(120);
    }
  }

  @Override
  public void tickEndHook() {
    if (npc.isLocked()) {
      return;
    }
    if (specialAttack != null) {
      lastSpecialAttack = specialAttack;
      specialAttack = null;
    }
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (specialAttack != null && specialAttack != SLEEPWALKERS_ATTACK) {
      return specialAttack;
    }
    return combatStyle;
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (specialAttack != null) {
      npc.getController().setMagicBind(combatStyle.getAttackSpeed());
    } else {
      npc.getController().setMagicBind(3);
    }
  }

  @Override
  public void applyAttackEndHook(
      NpcCombatStyle combatStyle, Entity opponent, int applyAttackLoopCount, HitEvent hitEvent) {
    if (!opponent.isPlayer()) {
      return;
    }
    if (combatStyle == CURSE_ATTACK) {
      opponent.asPlayer().getPlugin(BossPlugin.class).setTheNightmareCurse(30);
    } else if (combatStyle == PARASITES_ATTACK) {
      opponent.asPlayer().getPlugin(BossPlugin.class).setTheNightmareParasite(20);
    }
  }

  @Override
  public void quadrantsTasksHook(NpcCombatStyle combatStyle, Entity opponent, PEventTasks tasks) {
    flowerPowerEvent = tasks;
  }

  @Override
  public void explodingTileTasksHook(
      NpcCombatStyle combatStyle, Entity opponent, PEventTasks tasks) {
    sporesEvent = tasks;
  }

  @Override
  public void explodingTileHitEventHook(
      NpcCombatStyle combatStyle, Entity opponent, HitEvent hitEvent) {
    if (!opponent.isPlayer()) {
      return;
    }
    var player = opponent.asPlayer();
    player.getPlugin(BossPlugin.class).setTheNightmareSpore(20);
    player.getMovement().setRunning(false);
    player
        .getGameEncoder()
        .sendMessage(
            "<col=ff0000>The Nightmare's spores have infected you, making you feel drowsy!</col>");
  }

  @Override
  public boolean canAttackEntityHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (!opponent.isPlayer()) {
      return true;
    }
    if (combatStyle == CURSE_ATTACK
        && opponent.asPlayer().getPlugin(BossPlugin.class).getTheNightmareCurse() > 0) {
      return false;
    }
    return combatStyle != PARASITES_ATTACK
        || opponent.asPlayer().getPlugin(BossPlugin.class).getTheNightmareParasite() <= 0;
  }

  @Override
  public void applyDeadEndHook() {
    dropItems();
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (TOTEM_COMBAT_IDS.contains(npc.getId()) && opponent.isPlayer()) {
      damage = 0;
    }
    return damage;
  }

  @Override
  public void addToWorld() {
    TOTEM_SPAWNS.forEach(s -> totems.add(npc.getController().addNpc(s)));
  }

  @Override
  public void removeFromWorld() {
    totems.forEach(n -> npc.getController().removeNpc(n));
  }

  public void start() {
    npc.lock();
    setEnterNpc(NpcId.THE_NIGHTMARE_814_9461, 8573);
    var tasks = new PEventTasks();
    tasks.execute(
        PTime.secToTick(5),
        t ->
            npc.getController()
                .sendRegionMessage(
                    "The Nightmare will awaken in 30 seconds!", 15256, npc.getRegionId()));
    tasks.execute(
        PTime.secToTick(10),
        t ->
            npc.getController()
                .sendRegionMessage(
                    "The Nightmare will awaken in 20 seconds!", 15256, npc.getRegionId()));
    tasks.execute(
        PTime.secToTick(10),
        t ->
            npc.getController()
                .sendRegionMessage(
                    "The Nightmare will awaken in 10 seconds!", 15256, npc.getRegionId()));
    tasks.execute(
        PTime.secToTick(10),
        t -> {
          npc.setId(SHIELD_COMBAT_IDS.get(0));
          load();
          setEnterNpc(NpcId.THE_NIGHTMARE_814_9462, 8575);
          npc.getController()
              .sendRegionMessage(
                  "<col=ff0000>The Nightmare has awoken!</col>", 15256, npc.getRegionId());
        });
    tasks.execute(PTime.secToTick(6), t -> npc.unlock());
    addEvent(tasks);
  }

  public void updateHealth() {
    players.forEach(
        p -> {
          p.getGameEncoder()
              .setVarbit(VarbitId.HEALTH_OVERLAY_TOTAL, Math.max(1, getMaxHitpoints()));
          p.getGameEncoder()
              .setVarbit(VarbitId.HEALTH_OVERLAY_CURRENT, Math.max(1, getHitpoints()));
        });
  }

  public void checkTotems() {
    if (npc.isLocked()) {
      return;
    }
    if (areTotemsCharged()) {
      npc.getController().sendRegionMessage("<col=ff0000>All four totems are fully charged.</col>");
      totems.forEach(n -> n.setId(n.getId() - 2));
      var projectile =
          Graphic.Projectile.builder().id(1768).entity(npc).speed(getProjectileSpeed(10));
      npc.setGraphic(new Graphic(1769, 0, projectile.build().getContactDelay()));
      totems.forEach(n -> sendMapProjectile(projectile.startTile(n).build()));
      var tasks = new PEventTasks();
      tasks.execute(projectile.build().getEventDelay(), e -> addHit(new Hit(800)));
      tasks.execute(
          e -> {
            if (npc.getId() == TOTEM_COMBAT_IDS.get(TOTEM_COMBAT_IDS.size() - 1)) {
              return;
            }
            realHitpoints = getHitpoints();
            npc.setId(npc.getId() + 1);
            setHitpoints(maxShield);
            setMaxHitpoints(maxShield);
            specialAttack = SLEEPWALKERS_ATTACK;
            npc.setLock(2);
            setHitDelay(0);
          });
      addEvent(tasks);
    }
  }

  public boolean areTotemsInactive() {
    return getTotemMatches(NpcId.TOTEM, NpcId.TOTEM_9437, NpcId.TOTEM_9440, NpcId.TOTEM_9443) > 0;
  }

  public boolean areTotemsCharging() {
    return getTotemMatches(NpcId.TOTEM_9435, NpcId.TOTEM_9438, NpcId.TOTEM_9441, NpcId.TOTEM_9443)
        > 0;
  }

  public boolean areTotemsCharged() {
    return getTotemMatches(NpcId.TOTEM_9436, NpcId.TOTEM_9439, NpcId.TOTEM_9442, NpcId.TOTEM_9445)
        == totems.size();
  }

  public int getTotemMatches(int... totemIds) {
    var count = 0;
    for (var totem : totems) {
      for (var totemId : totemIds) {
        if (totem.getId() != totemId) {
          continue;
        }
        count++;
        break;
      }
    }
    return count;
  }

  public void checkAttacks() {
    if (npc.isLocked()) {
      return;
    }
    if (specialAttack == null
        && ++normalAttacks > 1
        && (PRandom.randomE(4) == 0 || normalAttacks == 10)
        && PHASE_SPECIAL_ATTACKS.containsKey(npc.getId())) {
      normalAttacks = 0;
      var specialAttacks = new ArrayList<>(PHASE_SPECIAL_ATTACKS.get(npc.getId()));
      if (specialAttacks != null) {
        specialAttacks.remove(lastSpecialAttack);
        if (flowerPowerEvent != null && flowerPowerEvent.isRunning()) {
          specialAttacks.remove(FLOWER_POWER_ATTACK);
        }
        if (sporesEvent != null && sporesEvent.isRunning()) {
          specialAttacks.remove(SPORES_ATTACK);
        }
        if (!specialAttacks.isEmpty()) {
          specialAttack = PRandom.listRandom(specialAttacks);
        }
      }
    }
    if (specialAttack == FLOWER_POWER_ATTACK || specialAttack == SLEEPWALKERS_ATTACK) {
      centerTeleport();
      return;
    }
    if (specialAttack == SLEEPWALKERS_ATTACK) {
      sleepwalkersAttack();
    }
  }

  public void sleepwalkersAttack() {
    if (players.isEmpty()) {
      return;
    }
    var currentId = npc.getId();
    npc.setId(NpcId.THE_NIGHTMARE_814_9431);
    var sleepwalkerCount = Math.min(Math.max(1, players.size() - 1), 24);
    if (players.size() == 2) {
      sleepwalkerCount = 2;
    }
    for (var i = 0; i < sleepwalkerCount; i++) {
      var randomX = PRandom.randomE(2) == 0;
      var reversed = PRandom.randomE(2) == 0;
      var x = randomX ? -6 + PRandom.randomI(12) : reversed ? -9 : 9;
      var y = randomX ? reversed ? -9 : 9 : -6 + PRandom.randomI(12);
      var tile = CENTER_TILE.copy().moveTile(x, y);
      sleepwalkers.add(addNpc(new NpcSpawn(tile, NpcId.SLEEPWALKER_3_9450 + PRandom.randomI(1))));
      sleepwalkers.forEach(n -> n.getCombat().setTarget(npc));
      // NpcId.SLEEPWALKER_3 + PRandom.randomI(5)
      // Diary boots got remade and their old model ids were replaced with the other sleepwalkers
      // TODO: Certain cache files will need updating to use all the sleepwalkers
    }
    npc.getController()
        .sendRegionMessage(
            "<col=ff0000>The Nightmare begins to charge up a devastating attack.</col>");
    var tasks = new PEventTasks();
    tasks.condition(4, t -> sleepwalkers.meetsIf(n -> !n.isVisible()));
    tasks.execute(
        t -> {
          var total = sleepwalkers.size();
          var remaining = sleepwalkers.countMeetsIf(n -> !n.getCombat().isDead());
          var percent = remaining / (double) total;
          sleepwalkers.removeEach(this::removeNpc);
          npc.setAnimation(8604);
          for (var player : players) {
            if (!npc.withinDistance(player, 32)) {
              continue;
            }
            var damage =
                Math.max(player.getCombat().getHitpoints(), player.getCombat().getMaxHitpoints())
                    * percent;
            damage = Math.min(damage, 200);
            player.getCombat().addHitEvent(new HitEvent(2, new Hit((int) Math.max(5, damage))));
            player.setGraphic(new Graphic(1782));
          }
        });
    tasks.execute(
        9,
        t -> {
          npc.setId(currentId);
          npc.getController()
              .sendRegionMessage("<col=ff0000>The Nightmare restores her shield.</col>");
          players.forEach(
              p -> {
                p.getGameEncoder().setVarp(VarpId.HEALTH_OVERLAY_TYPE, 9425);
                p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 13, 0x002020);
                p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 14, 0x00d0a8);
              });
          updateHealth();
        });
    addEvent(tasks);
  }

  public void setEnterNpc(int npcId, int animation) {
    var enterNpc =
        npc.getController()
            .getNpc(
                NpcId.THE_NIGHTMARE_814_9460,
                NpcId.THE_NIGHTMARE_814_9461,
                NpcId.THE_NIGHTMARE_814_9462,
                NpcId.THE_NIGHTMARE_814_9463,
                NpcId.THE_NIGHTMARE_814_9464);
    if (enterNpc == null) {
      return;
    }
    enterNpc.setId(npcId);
    enterNpc.setAnimation(animation);
  }

  public void centerTeleport() {
    if (npc.getX() == 3870 && npc.getY() == 9949) {
      return;
    }
    npc.lock();
    npc.setAnimation(8607);
    var tasks = new PEventTasks();
    tasks.execute(
        2,
        t -> {
          npc.getMovement().teleport(3870, 9949, 3);
          npc.setAnimation(8609);
        });
    tasks.execute(3, t -> npc.unlock());
    addEvent(tasks);
  }

  public void dropItems() {
    if (players.isEmpty()) {
      return;
    }
    players.sort(
        (p1, p2) ->
            Integer.compare(
                p2.getCombat().getDamageInflicted(), p1.getCombat().getDamageInflicted()));
    var weightedPlayers = new ArrayList<Player>();
    for (var i = 0; i < players.size(); i++) {
      var player = players.get(i);
      if (player.getCombat().getDamageInflicted() < 100) {
        continue;
      }
      var boneItem = new Item(i == 0 ? ItemId.BIG_BONES : ItemId.BONES);
      if (player.canDonatorNoteItem(boneItem.getId()) && boneItem.getDef().getNotedId() != -1) {
        boneItem = new Item(boneItem.getDef().getNotedId());
      }
      if (player.canDonatorPickupItem(boneItem.getId())
          && player.getInventory().canAddItem(boneItem)) {
        player.getInventory().addItem(boneItem);
      } else {
        player.getController().addMapItem(boneItem, npc, player);
      }
      deathDropItems(player, 0);
      for (var d = player.getCombat().getDamageInflicted(); d >= 0; d -= 100) {
        weightedPlayers.add(player);
      }
    }
    var rolls = 2;
    if (PRandom.inRange(Math.max(0, Math.min(players.size() - 5, 75)))) {
      rolls += 2;
    }
    for (var i = 0; i < rolls; i++) {
      var dropTable = i % 2 == 0 ? ORB_DROP_TABLE : EQUIPMENT_DROP_TABLE;
      var player = PRandom.listRandom(weightedPlayers);
      if (!dropTable.canDrop(npc, player)) {
        continue;
      }
      dropTable.dropItems(npc, player, npc);
    }
  }

  public void load() {
    players = npc.getController().getPlayers(npc.getRegionId());
    realHitpoints = npc.getCombatDef().getHitpoints().getTotal();
    maxShield = 400 * Math.min(Math.max(5, players.size()), 50);
    setHitpoints(maxShield);
    setMaxHitpoints(maxShield);
    updateHealth();
    addEvent(
        PEvent.singleEvent(
            2,
            e -> {
              players.forEach(
                  p -> {
                    p.getWidgetManager().sendOverlay(WidgetId.HEALTH_OVERLAY);
                    p.getGameEncoder().setVarp(VarpId.HEALTH_OVERLAY_TYPE, 9425);
                    p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 13, 0x002020);
                    p.getGameEncoder().sendWidgetColor(WidgetId.HEALTH_OVERLAY, 14, 0x00d0a8);
                    p.getCombat().setDamageInflicted(0);
                  });
            }));
  }
}
