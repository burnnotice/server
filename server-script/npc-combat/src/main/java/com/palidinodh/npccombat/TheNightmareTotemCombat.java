package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import java.util.Arrays;
import java.util.List;

class TheNightmareTotemCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var focus = NpcCombatFocus.builder().retaliationDisabled(true);
    var hitpoints =
        NpcCombatHitpoints.builder()
            .total(350)
            .reverse(true)
            .barType(HitpointsBarType.YELLOW_DARKER_100)
            .hitMarkType(HitMarkType.YELLOW);
    var immunity = NpcCombatImmunity.builder().venom(true).poison(true);

    var inactiveCombat = NpcCombatDefinition.builder();
    inactiveCombat
        .id(NpcId.TOTEM)
        .id(NpcId.TOTEM_9436)
        .id(NpcId.TOTEM_9437)
        .id(NpcId.TOTEM_9439)
        .id(NpcId.TOTEM_9440)
        .id(NpcId.TOTEM_9442)
        .id(NpcId.TOTEM_9443)
        .id(NpcId.TOTEM_9445);
    inactiveCombat.focus(focus.build());
    inactiveCombat.immunity(
        NpcCombatImmunity.builder().venom(true).poison(true).playerAttack(true).build());

    var combat1 = NpcCombatDefinition.builder();
    combat1.id(NpcId.TOTEM_9435);
    combat1.spawn(NpcCombatSpawn.builder().respawnId(NpcId.TOTEM_9436).build());
    combat1.hitpoints(hitpoints.build()).focus(focus.build()).immunity(immunity.build());

    var combat2 = NpcCombatDefinition.builder();
    combat2.id(NpcId.TOTEM_9438);
    combat2.spawn(NpcCombatSpawn.builder().respawnId(NpcId.TOTEM_9439).build());
    combat2.hitpoints(hitpoints.build()).focus(focus.build()).immunity(immunity.build());

    var combat3 = NpcCombatDefinition.builder();
    combat3.id(NpcId.TOTEM_9441);
    combat3.spawn(NpcCombatSpawn.builder().respawnId(NpcId.TOTEM_9442).build());
    combat3.hitpoints(hitpoints.build()).focus(focus.build()).immunity(immunity.build());

    var combat4 = NpcCombatDefinition.builder();
    combat4.id(NpcId.TOTEM_9444);
    combat4.spawn(NpcCombatSpawn.builder().respawnId(NpcId.TOTEM_9445).build());
    combat4.hitpoints(hitpoints.build()).focus(focus.build()).immunity(immunity.build());

    return Arrays.asList(
        inactiveCombat.build(), combat1.build(), combat2.build(), combat3.build(), combat4.build());
  }

  @Override
  public void applyDeadEndHook() {
    var direction = "";
    switch (npc.getId()) {
      case NpcId.TOTEM:
        direction = "south west";
        break;
      case NpcId.TOTEM_9437:
        direction = "south east";
        break;
      case NpcId.TOTEM_9440:
        direction = "north west";
        break;
      case NpcId.TOTEM_9443:
        direction = "north east";
        break;
    }
    npc.getController().sendRegionMessage("The " + direction + " totem is fully charged.");
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (hitStyleType == HitStyleType.MAGIC) {
      damage *= 1.4;
    }
    return damage;
  }
}
