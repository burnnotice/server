package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.shared.Movement;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class Spinolyp76Combat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.SPINOLYP_76);
    combat.hitpoints(NpcCombatHitpoints.total(100));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(10)
            .rangedLevel(2)
            .defenceLevel(10)
            .bonus(BonusType.MELEE_DEFENCE, 100)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(8).build());
    combat.focus(NpcCombatFocus.builder().disableFollowingOpponent(true).build());
    combat.deathAnimation(2866).blockAnimation(2869);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(10));
    style.animation(2868).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(294));
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(25).poison(6).build());
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.MAGIC)
            .subHitStyleType(HitStyleType.RANGED)
            .build());
    style.damage(NpcCombatDamage.maximum(10));
    style.animation(2868).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(94));
    style.effect(NpcCombatEffect.builder().statDrain(Skills.PRAYER, 1).build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    npc.getMovement().setType(Movement.Type.SWIM);
  }

  @Override
  public void tickStartHook() {
    if (isAttacking()) {
      npc.getMovement().clear();
    }
    if (isDead() || PRandom.randomE(100) != 0) {
      return;
    }
    if (npc.isVisible()) {
      npc.getController().sendMapGraphic(npc, new Graphic(68));
      npc.setVisible(false);
    } else {
      npc.setVisible(true);
    }
  }
}
