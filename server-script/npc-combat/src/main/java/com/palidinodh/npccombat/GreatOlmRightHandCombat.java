package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.npccombat.GreatOlmCombat.Configurations;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.Arrays;
import java.util.List;

class GreatOlmRightHandCombat extends NpcCombat {

  private static final Tile[] NPC_TILES = {null, new Tile(3238, 5743), null, new Tile(3223, 5733)};
  private static final Tile[] MAP_OBJECT_TILES = {
    null, new Tile(3238, 5743), null, new Tile(3220, 5733)
  };

  @Inject private Npc npc;
  private MapObject mapObject;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.GREAT_OLM_RIGHT_CLAW_549);
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(500).barType(HitpointsBarType.GREEN_RED_100).build());
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(250)
            .magicLevel(87)
            .rangedLevel(250)
            .defenceLevel(175)
            .bonus(BonusType.ATTACK_MAGIC, 60)
            .bonus(BonusType.ATTACK_RANGED, 60)
            .bonus(BonusType.MELEE_DEFENCE, 200)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .bonus(BonusType.DEFENCE_RANGED, 200)
            .build());
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void applyDeadStartHook(int deathDelay) {
    despawn(Configurations.DESPAWN_TIMES.getRightHand());
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    return hitStyleType == HitStyleType.MAGIC ? damage : 0;
  }

  public void setAnimation(int animation) {
    npc.getController().sendMapObjectAnimation(mapObject, animation);
  }

  public void travel(int direction) {
    var despawnedObjectId = Configurations.DESPAWNED_MAP_OBJECTS.getRightHand();
    var spawnedObjectId = Configurations.SPAWNED_MAP_OBJECTS.getRightHand();
    var animation = Configurations.SPAWN_ANIMATIONS.getRightHand();
    npc.setTile(NPC_TILES[direction]);
    npc.getSpawn().tile(npc);
    npc.setLock(8);
    npc.getController().setMultiCombatFlag(true);
    setHitpoints(getMaxHitpoints());
    mapObject = new MapObject(spawnedObjectId, 10, direction, MAP_OBJECT_TILES[direction]);
    npc.getController().addMapObject(new MapObject(despawnedObjectId, mapObject));
    addSingleEvent(1, e -> npc.getController().sendMapObjectAnimation(mapObject, animation));
    npc.getController().sendMapObjectAnimation(mapObject, animation);
    addSingleEvent(6, e -> npc.getController().addMapObject(mapObject));
  }

  public void despawn(int time) {
    var objectId = Configurations.DESPAWNED_MAP_OBJECTS.getRightHand();
    var animation = Configurations.DESPAWN_ANIMATION.getRightHand();
    if (time == 0) {
      clearEvents();
      if (mapObject != null) {
        npc.getController().addMapObject(new MapObject(objectId, mapObject));
      }
    } else {
      setAnimation(animation);
      npc.getController()
          .addSingleEvent(
              time, e -> npc.getController().addMapObject(new MapObject(objectId, mapObject)));
    }
  }
}
