package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.combat.TileHitEvent;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.route.Route;
import com.palidinodh.osrscore.model.map.route.WalkRoute;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

class GreatOlmCombat extends NpcCombat {

  private static final int ATTACK_SPEED = 4;
  private static final Tile[] NPC_TILES = {null, new Tile(3238, 5738), null, new Tile(3223, 5738)};
  private static final Tile[] MAP_OBJECT_TILES = {
    null, new Tile(3238, 5738), null, new Tile(3220, 5738)
  };

  @Inject private Npc npc;
  private MapObject mapObject;
  private Npc leftHandNpc;
  @Getter private Npc rightHandNpc;
  private int lastRightHandHitpoints;
  private boolean loaded;
  private boolean visible;
  private int handHitpoints;
  private HeadDirection direction = HeadDirection.CENTER;
  private PlayerLocation playerLocation = PlayerLocation.CENTER;
  private int directionDelay;
  private int directionConfusionDelay;
  @Getter private int phase = 1;
  private Cycle cycle = Cycle.first();
  private int specialAttackDelay;
  private boolean phase3FallingCrystals;
  private PEvent fallingCrystalsEvent;

  public static PlayerLocation getPlayerLocation(Player player, boolean exact) {
    if (player.getY() >= 5745) {
      return PlayerLocation.NORTH;
    }
    if (player.getY() <= 5735) {
      return PlayerLocation.SOUTH;
    }
    if (exact && player.getY() >= 5741) {
      return PlayerLocation.CENTER_NORTH;
    }
    if (exact && player.getY() <= 5739) {
      return PlayerLocation.CENTER_SOUTH;
    }
    return PlayerLocation.CENTER;
  }

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().additionalPlayers(255);

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.GREAT_OLM_1043);
    combat.spawn(NpcCombatSpawn.builder().lock(8).build());
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(680).barType(HitpointsBarType.GREEN_RED_100).build());
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(250)
            .magicLevel(250)
            .rangedLevel(250)
            .defenceLevel(150)
            .bonus(BonusType.ATTACK_MAGIC, 60)
            .bonus(BonusType.ATTACK_RANGED, 60)
            .bonus(BonusType.MELEE_DEFENCE, 200)
            .bonus(BonusType.DEFENCE_MAGIC, 200)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat.aggression(NpcCombatAggression.builder().always(true).range(32).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(NpcCombatFocus.builder().disableFollowingOpponent(true).build());
    combat.killCount(
        NpcCombatKillCount.builder().asName("Chambers of Xeric").sendMessage(true).build());
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().maximum(33).prayerEffectiveness(0.16).build());
    style.attackSpeed(ATTACK_SPEED).attackRange(32);
    style.projectile(NpcCombatProjectile.id(1340));
    style.multiCombat(NpcCombatMulti.nearOpponent(3));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(33).prayerEffectiveness(0.16).build());
    style.attackSpeed(ATTACK_SPEED).attackRange(32);
    style.projectile(NpcCombatProjectile.id(1339));
    style.multiCombat(NpcCombatMulti.nearOpponent(3));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void despawnHook() {
    despawn(0);
    npc.getWorld().removeNpc(leftHandNpc);
    npc.getWorld().removeNpc(rightHandNpc);
    if (fallingCrystalsEvent != null) {
      fallingCrystalsEvent.stop();
    }
  }

  @Override
  public void tickStartHook() {
    if (!loaded) {
      loadProfile();
    }
    if (!npc.getController().isRegionLoaded()) {
      return;
    }
    if (!visible) {
      for (var player : npc.getController().getPlayers()) {
        if (npc.withinDistance(player, 6)) {
          visible = true;
          break;
        }
      }
      if (visible) {
        travel(PRandom.randomI(1) == 0);
      }
      return;
    }
    if (directionDelay > 0) {
      directionDelay--;
      if (directionDelay == 0) {
        setAnimations(Configurations.DEFAULT_ANIMATIONS);
      }
    }
    if (directionConfusionDelay > 0) {
      directionConfusionDelay--;
    }
    if (npc.isLocked() || directionDelay > 0) {
      return;
    }
    if (specialAttackDelay > 0) {
      specialAttackDelay--;
    }
    if (getHitDelay() == ATTACK_SPEED - 2) {
      setAnimations(Configurations.DEFAULT_ANIMATIONS);
    }
    if (!isHitDelayed() && changeDirection()) {
      return;
    }
    if (isAttacking() && !isHitDelayed()) {
      if (cycle.isSkip()) {
        setHitDelay(ATTACK_SPEED);
        cycle = cycle.next();
      } else if (cycle.isAttack()) {
        if (specialAttackDelay == 0 && PRandom.randomE(4) == 0) {
          var attack = PRandom.randomI(2);
          if (attack == 0) {
            spheres();
          } else if (attack == 1) {
            acidSpray();
          } else if (attack == 2) {
            acidDrip();
          }
          specialAttackDelay = 50;
          cycle = cycle.next();
        }
      } else if (!leftHandNpc.isLocked()) {
        switch (cycle) {
          case CRYSTAL_BURST:
            crystalBurst();
            break;
          case LIGHTNING:
            lightning();
            break;
          case SWAMP:
            swamp();
            break;
          default:
            break;
        }
        cycle = cycle.next();
      }
    }
    if (phase == 3 && !phase3FallingCrystals && !npc.isLocked()) {
      if (!leftHandNpc.isVisible() && !rightHandNpc.isVisible()) {
        phase3FallingCrystals = true;
        fallingCrystals();
      }
    }
    if (phase < 3 && !leftHandNpc.isVisible() && !rightHandNpc.isVisible()) {
      travelStart();
    }
  }

  @Override
  public int attackTickAnimationHook(NpcCombatStyle combatStyle, Entity opponent) {
    setAnimations(Configurations.HEAD_ATTACK_ANIMATION);
    return -1;
  }

  @Override
  public int attackTickAttackSpeedHook(NpcCombatStyle combatStyle, Entity opponent) {
    return ATTACK_SPEED;
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    cycle = cycle.next();
  }

  @Override
  public boolean canAttackEntityHook(NpcCombatStyle combatStyle, Entity opponent) {
    return visible && mapObject != null;
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    if (!visible) {
      return false;
    }
    if (phase != 3) {
      return false;
    }
    return leftHandNpc.isLocked() && rightHandNpc.isLocked();
  }

  @Override
  public void applyDeadStartHook(int deathDelay) {
    despawn(Configurations.DESPAWN_TIMES.getHead(direction, phase));
    for (var player : npc.getController().getPlayers()) {
      player
          .getGameEncoder()
          .sendMessage(
              "As the Great Olm collapses, the crystal blocking your exit has been shattered.");
    }
    npc.getController().addMapObject(new MapObject(-1, 10, 0, new Tile(3232, 5749)));
    npc.getController().addMapObject(new MapObject(30028, 10, 0, new Tile(3233, 5751)));
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (additionalPlayerLoopCount != 0) {
      return;
    }
    player.getController().script("get_decide_rewards");
  }

  public void loadProfile() {
    if (loaded || npc.getController().getPlayers().isEmpty()) {
      return;
    }
    loaded = true;
    var averageHP = 0;
    var playerMultiplier = 1.0;
    var players = npc.getController().getPlayers();
    for (var player : players) {
      averageHP += player.getCombat().getMaxHitpoints();
      playerMultiplier += 0.5;
    }
    averageHP /= players.size();
    var hitpoints = (int) ((300 + (players.size() * 50) + (averageHP * 2)) * playerMultiplier);
    setMaxHitpoints(hitpoints);
    setHitpoints(getMaxHitpoints());
    handHitpoints = (int) ((150 + (players.size() * 50) + (averageHP * 2)) * playerMultiplier);
  }

  public void despawn(int time) {
    var objectId = Configurations.DESPAWNED_MAP_OBJECTS.getHead(direction, phase);
    var animationId = Configurations.DESPAWN_ANIMATION.getHead(direction, phase);
    if (time == 0) {
      clearEvents();
      if (mapObject != null) {
        npc.getController().addMapObject(new MapObject(objectId, mapObject));
      }
      if (leftHandNpc != null) {
        leftHandNpc.getCombat().as(GreatOlmLeftHandCombat.class).despawn(0);
      }
      if (rightHandNpc != null) {
        rightHandNpc.getCombat().as(GreatOlmRightHandCombat.class).despawn(0);
      }
    } else {
      setAnimation(animationId);
      npc.getController()
          .addSingleEvent(
              time, e -> npc.getController().addMapObject(new MapObject(objectId, mapObject)));
    }
  }

  public void setAnimations(Configurations config) {
    if (phase == 3) {
      switch (config) {
        case DEFAULT_ANIMATIONS:
          config = Configurations.GLOWING_DEFAULT_ANIMATIONS;
          break;
        case SPAWN_ANIMATIONS:
          config = Configurations.GLOWING_SPAWN_ANIMATIONS;
          break;
        case HEAD_ATTACK_ANIMATION:
          config = Configurations.GLOWING_HEAD_ATTACK_ANIMATION;
          break;
        default:
          break;
      }
    }
    setAnimation(config.getHead(direction, phase));
    if (config.getLeftHand() != -1 && !leftHandNpc.isLocked()) {
      leftHandNpc.getCombat().as(GreatOlmLeftHandCombat.class).setAnimation(config.getLeftHand());
    }
    if (config.getRightHand() != -1 && !rightHandNpc.isLocked()) {
      rightHandNpc
          .getCombat()
          .as(GreatOlmRightHandCombat.class)
          .setAnimation(config.getRightHand());
    }
  }

  public void setAnimation(int animation) {
    npc.getController().sendMapObjectAnimation(mapObject, animation);
  }

  public List<Player> getAreaPlayers() {
    var players = npc.getController().getPlayers();
    players.removeIf(
        player -> player.isLocked() || !npc.withinDistance(player, 32) || player.getY() < 5730);
    return players;
  }

  public boolean isAttackable(Player player) {
    var location = getPlayerLocation(player, true);
    if (playerLocation == PlayerLocation.NORTH) {
      return location == PlayerLocation.NORTH || location == PlayerLocation.CENTER_NORTH;
    }
    if (playerLocation == PlayerLocation.SOUTH) {
      return location == PlayerLocation.SOUTH || location == PlayerLocation.CENTER_SOUTH;
    }
    return playerLocation == location;
  }

  public void travelStart() {
    phase++;
    var currentlyWest = npc.matchesTile(NPC_TILES[3]);
    despawn(Configurations.DESPAWN_TIMES.getHead(direction, phase));
    npc.setVisible(false);
    npc.lock();
    var event =
        new PEvent(1) {
          @Override
          public void execute() {
            if (getExecutions() == 0) {
              return;
            } else if (getExecutions() == 17) {
              stop();
              npc.setVisible(true);
              npc.unlock();
              travel(!currentlyWest);
            }
            var count = 1 + PRandom.randomI(3);
            for (var i = 0; i < count; i++) {
              var tile = new Tile(3228, 5730);
              tile.moveTile(PRandom.randomI(9), PRandom.randomI(18));
              npc.getController()
                  .sendMapProjectile(
                      null, (new Tile(tile)).moveY(1), tile, 1357, 255, 10, 0, 51 + 120, 0, 0);
              npc.getController().sendMapGraphic(tile, new Graphic(1358, 0, 51 + 100));
              npc.getController().sendMapGraphic(tile, new Graphic(1449));
              var the = new TileHitEvent(5, npc.getController(), tile, 20, HitStyleType.TYPELESS);
              the.setAdjacentHalfDamage(true);
              addEvent(the);
            }
          }
        };
    addEvent(event);
  }

  public void travel(boolean moveWest) {
    var travelDirection = moveWest ? 3 : 1;
    leftHandNpc =
        npc.getController().addNpc(new NpcSpawn(new Tile(), NpcId.GREAT_OLM_LEFT_CLAW_750));
    leftHandNpc.getCombat().setMaxHitpoints(handHitpoints);
    rightHandNpc =
        npc.getController().addNpc(new NpcSpawn(new Tile(), NpcId.GREAT_OLM_RIGHT_CLAW_549));
    rightHandNpc.getCombat().setMaxHitpoints(handHitpoints);
    npc.setLock(8);
    npc.getMovement().teleport(NPC_TILES[travelDirection]);
    setHitpoints(getMaxHitpoints());
    var despawnedObjectId = Configurations.DESPAWNED_MAP_OBJECTS.getHead(direction, phase);
    var spawnedObjectId = Configurations.SPAWNED_MAP_OBJECTS.getHead(direction, phase);
    var animation = Configurations.SPAWN_ANIMATIONS.getHead(direction, phase);
    mapObject =
        new MapObject(spawnedObjectId, 10, travelDirection, MAP_OBJECT_TILES[travelDirection]);
    npc.getController().addMapObject(new MapObject(despawnedObjectId, mapObject));
    addSingleEvent(1, e -> npc.getController().sendMapObjectAnimation(mapObject, animation));
    addSingleEvent(6, e -> npc.getController().addMapObject(mapObject));
    leftHandNpc.getCombat().as(GreatOlmLeftHandCombat.class).setHeadNpc(npc);
    leftHandNpc.getCombat().as(GreatOlmLeftHandCombat.class).travel(travelDirection);
    rightHandNpc.getCombat().as(GreatOlmRightHandCombat.class).travel(travelDirection);
    cycle = Cycle.first();
  }

  public void crystalBurst() {
    setHitDelay(ATTACK_SPEED);
    leftHandNpc.getCombat().as(GreatOlmLeftHandCombat.class).setAnimation(7356);
    var crystals = new ArrayList<MapObject>();
    for (var player : getAreaPlayers()) {
      var crystal =
          new MapObject(ObjectId.SMALL_CRYSTALS, 10, MapObject.getRandomDirection(), player);
      var hasTileMatch = false;
      for (var aCrystal : crystals) {
        if (!crystal.matchesTile(aCrystal)) {
          continue;
        }
        hasTileMatch = true;
        break;
      }
      if (hasTileMatch) {
        continue;
      }
      crystals.add(crystal);
      npc.getController().addMapObject(crystal);
    }
    var event =
        new PEvent(3) {
          @Override
          public void execute() {
            if (getExecutions() == 0) {
              setTick(1);
              var players = npc.getController().getPlayers();
              for (var crystal : crystals) {
                for (var player : players) {
                  if (player.isLocked() || !crystal.withinDistance(player, 0)) {
                    continue;
                  }
                  player.getCombat().addHit(new Hit(30 + PRandom.randomI(15)));
                  player.getCombat().setRecentCombat();
                  player
                      .getGameEncoder()
                      .sendMessage(
                          "The crystal beneath your feet grows rapidly and shunts you to the side.");
                }
                npc.getController().addMapObject(new MapObject(ObjectId.LARGE_CRYSTALS, crystal));
              }
            } else {
              stop();
              for (var crystal : crystals) {
                npc.getController().addMapObject(new MapObject(-1, crystal));
                npc.getController().sendMapGraphic(crystal, new Graphic(1353));
              }
            }
          }
        };
    addEvent(event);
  }

  public void lightning() {
    setHitDelay(ATTACK_SPEED);
    leftHandNpc.getCombat().as(GreatOlmLeftHandCombat.class).setAnimation(7358);
    var tiles =
        PCollection.toList(
            new Tile(3228, 5748),
            new Tile(3229, 5748),
            new Tile(3230, 5748),
            new Tile(3231, 5748),
            new Tile(3232, 5748),
            new Tile(3233, 5748),
            new Tile(3234, 5748),
            new Tile(3235, 5748),
            new Tile(3236, 5748),
            new Tile(3237, 5747),
            new Tile(3228, 5731),
            new Tile(3229, 5731),
            new Tile(3230, 5731),
            new Tile(3231, 5730),
            new Tile(3232, 5730),
            new Tile(3233, 5730),
            new Tile(3234, 5730),
            new Tile(3235, 5731),
            new Tile(3236, 5731),
            new Tile(3237, 5731));
    Collections.shuffle(tiles);
    var selectedTiles = new Tile[] {tiles.get(0), tiles.get(1), tiles.get(2), tiles.get(3)};
    var directions = new ArrayList<Integer>();
    for (var tile : selectedTiles) {
      directions.add((tile.getY() > 5739) ? Tile.SOUTH : Tile.NORTH);
    }
    var event =
        new PEvent(0) {
          @Override
          public void execute() {
            var stillWorking = false;
            var players = npc.getController().getPlayers();
            for (var i = 0; i < selectedTiles.length; i++) {
              var tile = selectedTiles[i];
              var lightningDirection = directions.get(i);
              var toTile = (new Tile(tile)).moveY(lightningDirection == Tile.NORTH ? 1 : -1);
              var routeDirection =
                  lightningDirection == Tile.NORTH ? WalkRoute.NORTH_MASK : WalkRoute.SOUTH_MASK;
              if (Route.hasMask(npc, toTile, routeDirection)) {
                continue;
              }
              stillWorking = true;
              npc.getController().sendMapGraphic(tile, new Graphic(1356));
              for (var player : players) {
                if (player.isLocked() || !tile.withinDistance(player, 0)) {
                  continue;
                }
                player.getCombat().addHit(new Hit(PRandom.randomI(33)));
                player.getCombat().setRecentCombat();
                player.getPrayer().deactivate("protect from magic");
                player.getPrayer().deactivate("protect from missiles");
                player.getPrayer().deactivate("protect from melee");
                player.getPrayer().setDamageProtectionPrayerBlock(8);
                player.getController().setMagicBind(8, npc);
                player
                    .getGameEncoder()
                    .sendMessage("<col=ff0000>You've been eletrocuted to the spot!</col>");
                player
                    .getGameEncoder()
                    .sendMessage("You've been injured and can't use protection prayers!");
              }
              tile.moveY(lightningDirection == Tile.NORTH ? 1 : -1);
            }
            if (!stillWorking) {
              stop();
            }
          }
        };
    addEvent(event);
  }

  public void swamp() {
    setHitDelay(ATTACK_SPEED);
    leftHandNpc.getCombat().as(GreatOlmLeftHandCombat.class).setAnimation(7359);
    var playerMap = new HashMap<Player, Tile>();
    var players = getAreaPlayers();
    if (players.isEmpty()) {
      return;
    }
    Collections.shuffle(players);
    if (players.size() > 1 && (players.size() % 2) != 0) {
      players.remove(players.size() - 1);
    }
    if (players.size() == 1) {
      var tile = new Tile(3228, 5731);
      tile.moveTile(PRandom.randomI(9), PRandom.randomI(16));
      playerMap.put(players.get(0), tile);
      players
          .get(0)
          .getGameEncoder()
          .sendMessage(
              "You have been paired with <col=ff0000>a random location</col>! The magical power will enact soon.");
    } else {
      for (var i = 0; i < players.size(); i += 2) {
        var player1 = players.get(i);
        var player2 = players.get(i + 1);
        playerMap.put(player1, player2);
        player1
            .getGameEncoder()
            .sendMessage(
                "You have been paired with <col=ff0000>"
                    + player2.getUsername()
                    + "</col>! The magical power will enact soon.");
        player2
            .getGameEncoder()
            .sendMessage(
                "You have been paired with <col=ff0000>"
                    + player1.getUsername()
                    + "</col>! The magical power will enact soon.");
      }
    }
    var graphicIds = new int[] {1359, 1360, 1361, 1362};
    var event =
        new PEvent(0) {
          @Override
          public void execute() {
            var graphicIndex = -1;
            for (var entry : playerMap.entrySet()) {
              graphicIndex = (graphicIndex + 1) % graphicIds.length;
              var key = entry.getKey();
              var value = entry.getValue();
              if (!key.isVisible()) {
                continue;
              }
              if (!value.isVisible()) {
                continue;
              }
              if (!npc.withinDistance(key, 32)) {
                continue;
              }
              if (!npc.withinDistance(value, 32)) {
                continue;
              }
              if (key.getY() < 5730) {
                continue;
              }
              if (value.getY() < 5730) {
                continue;
              }
              if (getExecutions() == 8) {
                if (key.withinDistance(value, 0)) {
                  key.getGameEncoder().sendMessage("The teleport attack has no effect!");
                } else {
                  key.getMovement().teleport(value);
                  key.setGraphic(1039);
                  key.getCombat().addHit(new Hit(key.getDistance(value) * 5));
                  key.getCombat().setRecentCombat();
                  if (value instanceof Player) {
                    var player = (Player) value;
                    player.getMovement().teleport(key);
                    player.setGraphic(1039);
                    player.getCombat().addHit(new Hit(player.getDistance(key) * 5));
                    player.getCombat().setRecentCombat();
                    key.getGameEncoder()
                        .sendMessage(
                            "Yourself and " + player.getUsername() + " have swapped places!");
                    player
                        .getGameEncoder()
                        .sendMessage("Yourself and " + key.getUsername() + " have swapped places!");
                  }
                }
              } else {
                var graphic = new Graphic(graphicIds[graphicIndex]);
                key.setGraphic(graphic);
                if (value instanceof Player) {
                  ((Entity) value).setGraphic(graphic);
                } else {
                  npc.getController().sendMapGraphic(value, graphic);
                }
              }
            }
            if (getExecutions() == 8) {
              stop();
            }
          }

          @Override
          public void stopHook() {
            playerMap.clear();
          }
        };
    addEvent(event);
  }

  public void spheres() {
    setHitDelay(ATTACK_SPEED);
    setAnimations(Configurations.HEAD_ATTACK_ANIMATION);
    var players = getAreaPlayers();
    if (players.isEmpty()) {
      return;
    }
    var projectileIds = new int[] {-1, 1345, 1343, 1341};
    var contactIds = new int[] {-1, 1346, 1344, 1342};
    var types = new ArrayList<HitStyleType>();
    var speed = getProjectileSpeed(14);
    var styles = new PArrayList<>(HitStyleType.MELEE, HitStyleType.RANGED, HitStyleType.MAGIC);
    styles.shuffle();
    for (var player : players) {
      var hitStyleType = styles.removeLast();
      if (hitStyleType == null) {
        break;
      }
      types.add(hitStyleType);
      var message = "";
      if (hitStyleType == HitStyleType.MELEE) {
        message = "<col=ff0000>The Great olm fires a sphere of aggression your way.</col>";
      } else if (hitStyleType == HitStyleType.RANGED) {
        message =
            "<col=00ff00>The Great olm fires a sphere of accuracy and dexterity your way.</col>";
      } else if (hitStyleType == HitStyleType.MAGIC) {
        message = "<col=0000ff>The Great olm fires a sphere of magical power your way.</col>";
      }
      if (player.getPrayer().hasActive("protect from magic")
          || player.getPrayer().hasActive("protect from missiles")
          || player.getPrayer().hasActive("protect from melee")) {
        message += " Your prayers have been sapped.";
        player.getPrayer().deactivate("protect from magic");
        player.getPrayer().deactivate("protect from missiles");
        player.getPrayer().deactivate("protect from melee");
        player.getPrayer().changePoints(-(player.getPrayer().getPoints() / 2));
      }
      player.getGameEncoder().sendMessage(message);
      sendMapProjectile(
          Graphic.Projectile.builder()
              .id(projectileIds[hitStyleType.ordinal()])
              .speed(speed)
              .startTile(npc)
              .entity(player)
              .build());
      player.setGraphic(
          new Graphic(contactIds[hitStyleType.ordinal()], 124, speed.getContactDelay()));
    }
    var event =
        new PEvent(speed.getEventDelay()) {
          @Override
          public void execute() {
            stop();
            for (var i = 0; i < players.size() && i < types.size(); i++) {
              var player = players.get(i);
              var type = types.get(i);
              if (!player.isVisible()) {
                continue;
              }
              if (type == HitStyleType.MELEE
                  && player.getPrayer().hasActive("protect from melee")) {
                continue;
              }
              if (type == HitStyleType.RANGED
                  && player.getPrayer().hasActive("protect from missiles")) {
                continue;
              }
              if (type == HitStyleType.MAGIC
                  && player.getPrayer().hasActive("protect from magic")) {
                continue;
              }
              player.getCombat().addHit(new Hit(player.getCombat().getHitpoints() / 2));
              player.getCombat().setRecentCombat();
            }
          }
        };
    addEvent(event);
  }

  public void acidSpray() {
    setHitDelay(ATTACK_SPEED);
    setAnimations(Configurations.HEAD_ATTACK_ANIMATION);
    var speed = getProjectileSpeed(10);
    var pools = new ArrayList<MapObject>();
    var poolTiles = new ArrayList<Tile>();
    for (var i = 0; i < 10; i++) {
      var tile = new Tile(3228, 5730);
      tile.moveTile(PRandom.randomI(9), PRandom.randomI(18));
      if (npc.getController().getMapObjectByType(10, tile.getX(), tile.getY(), tile.getHeight())
          != null) {
        continue;
      }
      poolTiles.add(tile);
      sendMapProjectile(
          Graphic.Projectile.builder().id(1354).speed(speed).startTile(npc).endTile(tile).build());
    }
    var event =
        new PEvent(speed.getEventDelay()) {
          @Override
          public void execute() {
            if (getExecutions() == 0) {
              setTick(0);
              for (var poolTile : poolTiles) {
                var pool =
                    new MapObject(ObjectId.ACID_POOL, 10, MapObject.getRandomDirection(), poolTile);
                pools.add(pool);
                npc.getController().addMapObject(pool);
              }
            } else if (getExecutions() < 14) {
              for (var player : npc.getController().getPlayers()) {
                for (var pool : pools) {
                  if (player.isLocked() || !pool.withinDistance(player, 0)) {
                    continue;
                  }
                  player.getCombat().addHit(new Hit(3 + PRandom.randomI(3), HitMarkType.GREEN));
                  player.getCombat().setRecentCombat();
                }
              }
            } else {
              stop();
              for (var pool : pools) {
                npc.getController().addMapObject(new MapObject(-1, pool));
              }
            }
          }
        };
    addEvent(event);
  }

  public void acidDrip() {
    var players = getAreaPlayers();
    if (players.isEmpty()) {
      return;
    }
    Collections.shuffle(players);
    setHitDelay(ATTACK_SPEED);
    setAnimations(Configurations.HEAD_ATTACK_ANIMATION);
    var selectedPlayer = players.get(0);
    var projectile =
        Graphic.Projectile.builder()
            .id(1354)
            .speed(getProjectileSpeed(selectedPlayer))
            .startTile(npc)
            .entity(selectedPlayer)
            .build();
    sendMapProjectile(projectile);
    selectedPlayer
        .getGameEncoder()
        .sendMessage(
            "<col=ff0000>The Great Olm has smothered you in acid. It starts to drip off slowly.</col");
    if (!selectedPlayer.isPoisonImmune()) {
      selectedPlayer.setPoison(2);
    }
    var pools = new ArrayList<MapObject>();
    var times = new ArrayList<Integer>();
    var event =
        new PEvent(projectile.getEventDelay()) {
          @Override
          public void execute() {
            setTick(0);
            MapObject addedPool = null;
            MapObject addedPool2 = null;
            if (getExecutions() < 22
                && selectedPlayer.isVisible()
                && !selectedPlayer.isLocked()
                && npc.withinDistance(selectedPlayer, 32)
                && selectedPlayer.getY() >= 5730
                && npc.getController().getMapObjectByType(10, selectedPlayer) == null) {
              addedPool =
                  new MapObject(
                      ObjectId.ACID_POOL, 10, MapObject.getRandomDirection(), selectedPlayer);
              pools.add(addedPool);
              times.add(16);
              npc.getController().addMapObject(addedPool);
              if (selectedPlayer.getMovement().isRunning()
                  && selectedPlayer.getMovement().isRouting()) {
                var nextTile = selectedPlayer.getMovement().getNextTile();
                if (npc.getController().getMapObjectByType(10, nextTile) == null) {
                  addedPool2 =
                      new MapObject(
                          ObjectId.ACID_POOL, 10, MapObject.getRandomDirection(), nextTile);
                  pools.add(addedPool2);
                  times.add(16);
                  npc.getController().addMapObject(addedPool2);
                }
              }
            }
            for (var i = 0; i < times.size(); i++) {
              if (times.get(i) == 0) {
                npc.getController().addMapObject(new MapObject(-1, pools.get(i)));
                pools.get(i).setVisible(false);
              }
              times.set(i, times.get(i) - 1);
            }
            for (var player : npc.getController().getPlayers()) {
              for (var pool : pools) {
                if (addedPool == pool) {
                  continue;
                }
                if (addedPool2 == pool) {
                  continue;
                }
                if (player.isLocked()) {
                  continue;
                }
                if (!pool.withinDistance(player, 0)) {
                  continue;
                }
                player.getCombat().addHit(new Hit(3 + PRandom.randomI(3), HitMarkType.GREEN));
                player.getCombat().setRecentCombat();
              }
            }
            if (getExecutions() == 38) {
              stop();
              for (var pool : pools) {
                if (pool.isVisible()) {
                  npc.getController().addMapObject(new MapObject(-1, pool));
                }
              }
            }
          }
        };
    npc.getCombat().addEvent(event);
  }

  public void fallingCrystals() {
    fallingCrystalsEvent =
        new PEvent(1) {
          @Override
          public void execute() {
            var count = 1 + PRandom.randomI(3);
            for (var i = 0; i < count; i++) {
              var tile = new Tile(3228, 5730);
              tile.moveTile(PRandom.randomI(9), PRandom.randomI(18));
              npc.getController()
                  .sendMapProjectile(
                      null, (new Tile(tile)).moveY(1), tile, 1357, 255, 10, 0, 51 + 120, 0, 0);
              npc.getController().sendMapGraphic(tile, new Graphic(1358, 0, 51 + 100));
              npc.getController().sendMapGraphic(tile, new Graphic(1449));
              var the = new TileHitEvent(5, npc.getController(), tile, 20, HitStyleType.TYPELESS);
              the.setAdjacentHalfDamage(true);
              npc.getController().addEvent(the);
            }
          }
        };
    npc.getController().addEvent(fallingCrystalsEvent);
  }

  public boolean changeDirection() {
    if (npc.isLocked()) {
      return false;
    }
    Player player = null;
    if (isAttacking() && getAttackingEntity().isPlayer()) {
      player = getAttackingEntity().asPlayer();
    }
    if (player == null) {
      var players = getAreaPlayers();
      if (players.isEmpty()) {
        return false;
      }
      Collections.shuffle(players);
      players.sort(
          (p1, p2) -> {
            if (isAttackable(p1)) {
              return -1;
            }
            if (isAttackable(p2)) {
              return 1;
            }
            return 0;
          });
      player = players.get(0);
      setAttackingEntity(player);
    }
    var west = npc.matchesTile(NPC_TILES[3]);
    var exactLocation = getPlayerLocation(player, true);
    var roughLocation = getPlayerLocation(player, false);
    if (playerLocation == roughLocation) {
      return false;
    }
    if (lastRightHandHitpoints == rightHandNpc.getCombat().getHitpoints()) {
      exactLocation = roughLocation;
    }
    if (directionConfusionDelay > 0) {
      exactLocation = roughLocation;
    }
    if (west) {
      if (playerLocation == PlayerLocation.SOUTH || exactLocation == PlayerLocation.CENTER_NORTH) {
        exactLocation = roughLocation;
      } else if (exactLocation == PlayerLocation.CENTER_SOUTH) {
        roughLocation = PlayerLocation.SOUTH;
        directionConfusionDelay = 8;
      }
    } else {
      if (playerLocation == PlayerLocation.NORTH || exactLocation == PlayerLocation.CENTER_SOUTH) {
        exactLocation = roughLocation;
      } else if (exactLocation == PlayerLocation.CENTER_NORTH) {
        roughLocation = PlayerLocation.NORTH;
        directionConfusionDelay = 8;
      }
    }
    if (cycle.isSkip()) {
      if (exactLocation == playerLocation) {
        return false;
      }
    } else if (!isHitDelayed() && isAttackable(player)) {
      return false;
    }
    HeadDirection newDirection = null;
    var animation = -1;
    switch (exactLocation) {
      case CENTER:
        {
          newDirection = HeadDirection.CENTER;
          animation = direction.getToClose();
          break;
        }
      case CENTER_NORTH:
      case NORTH:
        {
          newDirection = west ? HeadDirection.LEFT : HeadDirection.RIGHT;
          animation =
              west && playerLocation == PlayerLocation.CENTER
                  ? direction.getToClose()
                  : direction.getToFar();
          break;
        }
      case CENTER_SOUTH:
      case SOUTH:
        {
          newDirection = west ? HeadDirection.RIGHT : HeadDirection.LEFT;
          animation =
              !west && playerLocation == PlayerLocation.CENTER
                  ? direction.getToClose()
                  : direction.getToFar();
          break;
        }
    }
    direction = newDirection;
    directionDelay = 3;
    setHitDelay(directionDelay + 1);
    playerLocation = roughLocation;
    setAnimation(animation);
    lastRightHandHitpoints = rightHandNpc.getCombat().getHitpoints();
    cycle = cycle.next();
    if (cycle.isSkip()) {
      cycle = cycle.next();
    }
    return true;
  }

  @AllArgsConstructor
  @Getter
  public enum HeadDirection {
    LEFT(7342, 7343),
    CENTER(7341, 7339),
    RIGHT(7340, 7344);

    private final int toClose;
    private final int toFar;
  }

  public enum PlayerLocation {
    NORTH,
    CENTER_NORTH,
    CENTER,
    CENTER_SOUTH,
    SOUTH
  }

  @AllArgsConstructor
  @Getter
  public enum Configurations {
    DESPAWNED_MAP_OBJECTS(
        ObjectId.LARGE_HOLE, -1, -1, ObjectId.LARGE_ROCK_29883, ObjectId.CRYSTALLINE_STRUCTURE),
    SPAWNED_MAP_OBJECTS(
        ObjectId.LARGE_HOLE_29881,
        -1,
        -1,
        ObjectId.LARGE_ROCK_29884,
        ObjectId.CRYSTALLINE_STRUCTURE_29887),
    DEFAULT_ANIMATIONS(7336, 7338, 7337, 7355, 7351),
    SPAWN_ANIMATIONS(7335, -1, -1, 7354, 7350),
    DESPAWN_ANIMATION(7348, -1, -1, 7370, 7352),
    DESPAWN_TIMES(2, -1, -1, 1, 1),
    HEAD_ATTACK_ANIMATION(7345, 7347, 7346, -1, -1),
    GLOWING_DEFAULT_ANIMATIONS(7374, 7375, 7376, 7355, 7351),
    GLOWING_SPAWN_ANIMATIONS(7383, -1, -1, 7354, 7350),
    GLOWING_HEAD_ATTACK_ANIMATION(7371, 7372, 7373, -1, -1);

    private final int head;
    private final int headLeft;
    private final int headRight;
    private final int leftHand;
    private final int rightHand;

    public int getHead(HeadDirection direction, int phase) {
      if (phase == 3) {
        switch (this) {
          case DEFAULT_ANIMATIONS:
            return GLOWING_DEFAULT_ANIMATIONS.getHead(direction, -1);
          case SPAWN_ANIMATIONS:
            return GLOWING_SPAWN_ANIMATIONS.getHead(direction, -1);
          case HEAD_ATTACK_ANIMATION:
            return GLOWING_HEAD_ATTACK_ANIMATION.getHead(direction, -1);
          default:
            break;
        }
      }
      if (direction == HeadDirection.LEFT && headLeft != -1) {
        return headLeft;
      } else if (direction == HeadDirection.RIGHT && headRight != -1) {
        return headRight;
      }
      return head;
    }
  }

  public enum Cycle {
    ATTACK_1,
    SKIP_1,
    ATTACK_2,
    CRYSTAL_BURST,
    ATTACK_3,
    SKIP_2,
    ATTACK_4,
    LIGHTNING,
    ATTACK_5,
    SKIP_3,
    ATTACK_6,
    SWAMP;

    public static Cycle first() {
      return ATTACK_1;
    }

    public boolean isAttack() {
      return name().startsWith("ATTACK");
    }

    public boolean isSkip() {
      return name().startsWith("SKIP");
    }

    public Cycle next() {
      var values = values();
      var ordinal = ordinal();
      if (ordinal + 1 >= values.length) {
        return values[0];
      }
      return values[ordinal + 1];
    }
  }
}
