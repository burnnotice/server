package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.util.PTime;
import java.util.Arrays;
import java.util.List;

class EventMimicCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().underKiller(true).additionalPlayers(2);

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.THE_MIMIC_186_8633);
    combat.spawn(NpcCombatSpawn.builder().respawnDelay(36000).build());
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(5_000).barType(HitpointsBarType.GREEN_RED_120).build());
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(185)
            .magicLevel(60)
            .defenceLevel(120)
            .bonus(BonusType.DEFENCE_SLASH, 160)
            .bonus(BonusType.DEFENCE_STAB, 165)
            .bonus(BonusType.DEFENCE_CRUSH, 160)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(8310);
    combat.drop(drop.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    if (opponent.isNpc()) {
      return true;
    }
    var player = opponent.asPlayer();
    if (player.getSkills().getCombatLevel() < 100) {
      player.getGameEncoder().sendMessage("You need a combat level of 100 to attack this.");
      return false;
    }
    return true;
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer()) {
      return;
    }
    var player = opponent.asPlayer();
    player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (damage <= 0) {
      return damage;
    }
    var recoil = (int) Math.ceil(damage * 0.1);
    opponent.getCombat().applyHit(new Hit(recoil));
    return damage;
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (additionalPlayerLoopCount == 0) {
      if (PTime.getHour24() == WildernessPlugin.HP_EVENT_BONDS_HOUR) {
        npc.getController().addMapItem(new Item(ItemId.BLOODIER_KEY_32396), dropTile, player);
      } else {
        npc.getController().addMapItem(new Item(ItemId.BLOODIER_KEY), dropTile, player);
      }
      npc.getController()
          .sendMessage(
              "<col=ff0000>A bloodier key has been dropped for " + player.getUsername() + "!", 16);
    } else {
      npc.getController().addMapItem(new Item(ItemId.BLOODY_KEY), dropTile, player);
      npc.getController()
          .sendMessage(
              "<col=ff0000>A bloody key has been dropped for " + player.getUsername() + "!", 16);
    }
  }
}
