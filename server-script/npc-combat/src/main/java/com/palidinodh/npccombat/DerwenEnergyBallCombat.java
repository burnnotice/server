package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.graphic.Graphic;
import java.util.Arrays;
import java.util.List;

class DerwenEnergyBallCombat extends NpcCombat {

  @Inject private Npc npc;
  private Npc derwen;
  private int healDelay;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.ENERGY_BALL);
    combat.hitpoints(NpcCombatHitpoints.total(20));
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    healDelay = 4;
  }

  @Override
  public void tickStartHook() {
    if (npc.isLocked() || --healDelay > 0) {
      return;
    }
    healDelay = 4;
    if (derwen == null) {
      derwen = npc.getController().getNpc(NpcId.DERWEN_235_7859);
    }
    if (derwen == null || derwen.isLocked()) {
      startDeath();
      return;
    }
    var projectile =
        Graphic.Projectile.builder()
            .id(1513)
            .startTile(npc)
            .entity(derwen)
            .speed(getProjectileSpeed(derwen))
            .build();
    sendMapProjectile(projectile);
    derwen
        .getCombat()
        .addHitEvent(new HitEvent(projectile.getEventDelay(), new Hit(5, HitMarkType.MAGENTA)));
  }
}
