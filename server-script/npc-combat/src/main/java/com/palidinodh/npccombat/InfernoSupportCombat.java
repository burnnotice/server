package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.map.MapObject;
import java.util.Arrays;
import java.util.List;

class InfernoSupportCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.ROCKY_SUPPORT).id(NpcId.ROCKY_SUPPORT_1);
    combat.hitpoints(NpcCombatHitpoints.total(175));
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    combat.deathAnimation(7561);

    return Arrays.asList(combat.build());
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    return false;
  }

  @Override
  public void applyDeadHook() {
    if (npc.getId() == NpcId.ROCKY_SUPPORT_1 && getRespawnDelay() == 1) {
      npc.getController().addMapObject(new MapObject(-1, 10, 0, npc));
      npc.setId(NpcId.ROCKY_SUPPORT);
    }
    if (getRespawnDelay() == 0) {
      for (var player : npc.getController().getNearbyPlayers()) {
        if (player.isLocked() || !npc.withinDistance(player, 1)) {
          continue;
        }
        player.getCombat().addHit(new Hit(49));
      }
      for (var npc2 : npc.getController().getNearbyNpcs()) {
        if (npc2.isLocked() || !npc.withinDistance(npc2, 1) || npc2 == npc) {
          continue;
        }
        if (npc2.getCombat().getHitpoints() == 0) {
          continue;
        }
        npc2.getCombat().addHit(new Hit(49));
      }
    }
  }
}
