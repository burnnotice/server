package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatSummonNpc;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.map.route.ProjectileRoute;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PEventTasks;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

class VerzikViturCombat extends NpcCombat {

  private static final List<Tile> PILLARS =
      Arrays.asList(
          new Tile(3161, 4318),
          new Tile(3161, 4312),
          new Tile(3161, 4306),
          new Tile(3173, 4312),
          new Tile(3173, 4318),
          new Tile(3173, 4312),
          new Tile(3173, 4306));
  private static final NpcCombatSummonNpc COMMON_NYLOCAS_SUMMONING;
  private static final NpcCombatStyle PHASE_2_MAGIC_ATTACK;
  private static final NpcCombatStyle STICKY_WEBS_ATTACK;
  private static final List<Phase3SpecialAttack> PHASE_3_SPECIAL_ATTACKS =
      Arrays.asList(Phase3SpecialAttack.values());

  static {
    var summonNpc = NpcCombatSummonNpc.builder();
    summonNpc
        .type(NpcCombatSummonNpc.Type.NEARBY)
        .minimumDistance(4)
        .maximumDistance(16)
        .follow(true);
    summonNpc.spawn(
        new NpcSpawn(
            NpcId.NYLOCAS_ISCHYROS_162_8381,
            NpcId.NYLOCAS_TOXOBOLOS_162_8382,
            NpcId.NYLOCAS_HAGIOS_162_8383));
    COMMON_NYLOCAS_SUMMONING = summonNpc.build();

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(40).build());
    style.animation(8114).attackSpeed(4);
    style.targetGraphic(new Graphic(1592));
    style.projectile(NpcCombatProjectile.id(1591));
    PHASE_2_MAGIC_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.animation(8127).attackSpeed(2);
    style.targetTileGraphic(new Graphic(1487));
    style.projectile(NpcCombatProjectile.builder().id(1486).speedMinimumDistance(10).build());
    var targetTile = NpcCombatTargetTile.builder().radius(2);
    targetTile.breakOff(
        NpcCombatTargetTile.BreakOff.builder()
            .count(2)
            .distance(3)
            .afterTargettedTile(true)
            .build());
    style.specialAttack(targetTile.build());
    STICKY_WEBS_ATTACK = style.build();
  }

  @Inject private Npc npc;
  private Phase phase = Phase.FIRST;
  private PArrayList<Npc> pillars = new PArrayList<>(PILLARS.size());
  private int redSpiderDelay;
  private int attackCount;
  private Npc athanatos;
  private int athanatosCooldown;
  private Deque<Phase3SpecialAttack> phase3SpecialAttacks =
      new ArrayDeque<>(PHASE_3_SPECIAL_ATTACKS.size());

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat1 = NpcCombatDefinition.builder();
    combat1.id(Phase.FIRST.getNpcId());
    combat1.spawn(NpcCombatSpawn.builder().respawnId(Phase.PRE_SECOND.getNpcId()).build());
    combat1.hitpoints(
        NpcCombatHitpoints.builder().total(2000).barType(HitpointsBarType.GREEN_RED_100).build());
    combat1.stats(
        NpcCombatStats.builder()
            .attackLevel(400)
            .magicLevel(400)
            .rangedLevel(400)
            .defenceLevel(20)
            .bonus(BonusType.ATTACK_MAGIC, 80)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .bonus(BonusType.MELEE_DEFENCE, 20)
            .bonus(BonusType.DEFENCE_MAGIC, 20)
            .bonus(BonusType.DEFENCE_RANGED, 20)
            .build());
    combat1.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat1.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    combat1.blockAnimation(8110);

    var preCombat2 = NpcCombatDefinition.builder();
    preCombat2.id(Phase.PRE_SECOND.getNpcId());
    preCombat2.spawn(NpcCombatSpawn.builder().animation(8114).build());

    // TODO: melee when in distance
    var combat2 = NpcCombatDefinition.builder();
    combat2.id(Phase.SECOND.getNpcId());
    combat2.spawn(NpcCombatSpawn.builder().respawnId(Phase.PRE_THIRD.getNpcId()).build());
    combat2.hitpoints(
        NpcCombatHitpoints.builder().total(3250).barType(HitpointsBarType.GREEN_RED_100).build());
    combat2.stats(
        NpcCombatStats.builder()
            .attackLevel(400)
            .magicLevel(400)
            .rangedLevel(400)
            .defenceLevel(200)
            .bonus(BonusType.ATTACK_MAGIC, 80)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .bonus(BonusType.DEFENCE_STAB, 100)
            .bonus(BonusType.DEFENCE_SLASH, 60)
            .bonus(BonusType.DEFENCE_CRUSH, 100)
            .bonus(BonusType.DEFENCE_MAGIC, 70)
            .bonus(BonusType.DEFENCE_RANGED, 250)
            .build());
    combat2.aggression(NpcCombatAggression.builder().range(20).build());
    combat2.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat2.focus(
        NpcCombatFocus.builder()
            .meleeUnlessUnreachable(true)
            .disableFollowingOpponent(true)
            .build());

    var style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.RANGED)
            .subHitStyleType(HitStyleType.TYPELESS)
            .build());
    style.damage(NpcCombatDamage.builder().minimum(24).maximum(41).build());
    style.animation(8114).attackSpeed(4);
    style.targetTileGraphic(new Graphic(1584));
    style.projectile(NpcCombatProjectile.builder().id(1583).speedMinimumDistance(8).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.specialAttack(NpcCombatTargetTile.builder().build());
    combat2.style(style.build());

    var preCombat3 = NpcCombatDefinition.builder();
    preCombat3.id(Phase.PRE_THIRD.getNpcId());
    preCombat3.spawn(NpcCombatSpawn.builder().animation(8119).build());

    var combat3 = NpcCombatDefinition.builder();
    combat3.id(Phase.THIRD.getNpcId());
    combat3.spawn(NpcCombatSpawn.builder().phrase("Behold my true nature!").build());
    combat3.hitpoints(
        NpcCombatHitpoints.builder().total(3250).barType(HitpointsBarType.GREEN_RED_100).build());
    combat3.stats(
        NpcCombatStats.builder()
            .attackLevel(400)
            .magicLevel(300)
            .rangedLevel(300)
            .defenceLevel(150)
            .bonus(BonusType.MELEE_ATTACK, 80)
            .bonus(BonusType.ATTACK_MAGIC, 80)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .bonus(BonusType.DEFENCE_STAB, 70)
            .bonus(BonusType.DEFENCE_SLASH, 20)
            .bonus(BonusType.DEFENCE_CRUSH, 70)
            .bonus(BonusType.DEFENCE_MAGIC, 100)
            .bonus(BonusType.DEFENCE_RANGED, 230)
            .build());
    combat3.aggression(NpcCombatAggression.builder().range(20).build());
    combat3.focus(NpcCombatFocus.builder().keepWithinDistance(1).singleTargetFocus(true).build());
    combat3.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(
        NpcCombatDamage.builder()
            .maximum(62)
            .prayerEffectiveness(0.5)
            .delayedPrayerProtectable(true)
            .build());
    style.animation(8125).attackSpeed(7);
    combat3.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(
        NpcCombatDamage.builder()
            .maximum(32)
            .prayerEffectiveness(0.5)
            .delayedPrayerProtectable(true)
            .build());
    style.animation(8125).attackSpeed(7);
    style.projectile(NpcCombatProjectile.builder().id(1593).speedMinimumDistance(8).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat3.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder()
            .maximum(32)
            .prayerEffectiveness(0.5)
            .delayedPrayerProtectable(true)
            .build());
    style.animation(8123).attackSpeed(7);
    style.targetGraphic(new Graphic(1581));
    style.projectile(NpcCombatProjectile.builder().id(1594).speedMinimumDistance(8).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat3.style(style.build());

    return Arrays.asList(
        combat1.build(), preCombat2.build(), combat2.build(), preCombat3.build(), combat3.build());
  }

  @Override
  public void npcApplyHitStartHook(Hit hit) {
    if (redSpiderDelay > 35) {
      hit.setHitMarkType(HitMarkType.MAGENTA);
    }
  }

  @Override
  public void spawnHook() {
    PILLARS.forEach(tile -> pillars.add(addNpc(new NpcSpawn(tile, NpcId.SUPPORTING_PILLAR))));
    setHitDelay(19);
    npc.setLargeVisibility();
  }

  @Override
  public void despawnHook() {
    pillars.clear();
  }

  @Override
  public void idChangedHook(int oldId, int newId) {
    phase = Phase.getPhase(npc.getId());
    if (phase == Phase.PRE_SECOND) {
      var tile = new Tile(3166, 4312);
      var tasks = new PEventTasks();
      tasks.execute(
          2,
          t -> {
            npc.getMovement().clear();
            npc.getMovement().addMovement(tile);
          });
      tasks.condition(t -> npc.matchesTile(tile));
      tasks.execute(t -> npc.setId(Phase.SECOND.getNpcId()));
      addEvent(tasks);
    } else if (phase == Phase.PRE_THIRD) {
      addSingleEvent(2, t -> npc.setId(Phase.THIRD.getNpcId()));
    }
  }

  @Override
  public void tickStartHook() {
    if (npc.isLocked()) {
      return;
    }
    switch (phase) {
      case FIRST:
        phase1Tick();
        break;
      case SECOND:
        phase2Tick();
        break;
      case THIRD:
      case THIRD_20_PERCENT:
        phase3Tick();
        break;
    }
    if (getDefenceLevel() < getBaseDefenceLevel()) {
      setDefenceLevel(getBaseDefenceLevel());
      npc.getController().sendMessage("Verzik Vitur absorbs the defence reduction!");
    }
  }

  @Override
  public PArrayList<NpcCombatStyle> attackTickCombatStylesHook(
      PArrayList<NpcCombatStyle> combatStyles, Entity opponent) {
    if (phase == Phase.SECOND && PRandom.getPercent(getHitpoints(), getMaxHitpoints()) < 35) {
      combatStyles.add(PHASE_2_MAGIC_ATTACK);
    }
    return combatStyles;
  }

  @Override
  public int attackTickAttackSpeedHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (phase != Phase.THIRD_20_PERCENT) {
      return combatStyle.getAttackSpeed();
    }
    return combatStyle.getAttackSpeed() - 2;
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    attackCount++;
  }

  @Override
  public double defenceHook(
      Entity opponent, HitStyleType hitStyleType, BonusType meleeAttackStyle, double defence) {
    if (phase != Phase.FIRST) {
      return defence;
    }
    if (hitStyleType != HitStyleType.MAGIC) {
      return defence;
    }
    if (!opponent.isPlayer()) {
      return defence;
    }
    var player = opponent.asPlayer();
    if (player.getEquipment().getWeaponId() != ItemId.DAWNBRINGER) {
      return defence;
    }
    return 0;
  }

  @Override
  public void applyDeadStartHook(int deathDelay) {
    pillars.removeIf(Entity::isLocked);
    pillars.removeEach(n -> n.getCombat().startDeath());
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (phase == Phase.FIRST) {
      if (hitStyleType != HitStyleType.MAGIC
          || !opponent.isPlayer()
          || opponent.asPlayer().getEquipment().getWeaponId() != ItemId.DAWNBRINGER) {
        // TODO: re-enable after beta
        // return Math.min(damage, hitStyleType == HitStyleType.MELEE ? 10 : 3);
      }
    }
    return damage;
  }

  private void phase1Tick() {
    if (getHitDelay() == 2) {
      npc.setAnimation(8109);
      return;
    }
    if (isHitDelayed()) {
      return;
    }
    var sourceTile = new Tile(npc).setSize(npc.getSizeX(), 1);
    var players = getAttackablePlayers();
    var hitEntities = new PArrayList<Entity>();
    for (var player : players) {
      if (ProjectileRoute.INSTANCE.allow(npc.getController(), sourceTile, player)) {
        hitEntities.add(player);
      } else {
        var closestPillar = (Npc) player.getClosest(pillars);
        hitEntities.addIf(closestPillar, e -> !hitEntities.contains(e));
      }
    }
    for (var entity : hitEntities) {
      var projectile =
          Graphic.Projectile.builder()
              .id(1580)
              .startTile(npc)
              .entity(entity)
              .speed(getProjectileSpeed(10))
              .build();
      sendMapProjectile(projectile);
      var damage = PRandom.randomI(138);
      if (entity.isPlayer()) {
        if (entity.asPlayer().getPrayer().hasActive("protect from magic")) {
          damage *= 0.5;
        }
      }
      entity.setGraphic(
          new Graphic(entity.isPlayer() ? 1581 : 1582, 0, projectile.getContactDelay()));
      entity
          .getCombat()
          .addHitEvent(new HitEvent(projectile.getEventDelay(), new Hit(damage), npc));
    }
    setHitDelay(14);
  }

  private void phase2Tick() {
    redSpiderDelay--;
    athanatosCooldown--;
    if (PRandom.getPercent(getHitpoints(), getMaxHitpoints()) < 35) {
      if (redSpiderDelay <= 0) {
        redSpiderDelay = 45;
        addNpc(new NpcSpawn(2, new Tile(3163, 4314), NpcId.NYLOCAS_MATOMENOS_115_8385));
        addNpc(new NpcSpawn(2, new Tile(3172, 4314), NpcId.NYLOCAS_MATOMENOS_115_8385));
      }
    }
    if (!isHitDelayed() && attackCount == 4) {
      attackCount = 0;
      electricBallAttack();
      return;
    }
    if (!isHitDelayed() && PRandom.randomE(4) == 0) {
      spawnNylocasAthanatos();
    }
  }

  private void phase3Tick() {
    if (phase3SpecialAttacks.isEmpty()) {
      phase3SpecialAttacks.addAll(PHASE_3_SPECIAL_ATTACKS);
    }
    if (phase == Phase.THIRD && PRandom.getPercent(getHitpoints(), getMaxHitpoints()) <= 20) {
      phase = Phase.THIRD_20_PERCENT;
      tornadoAttack();
    } else if (!isHitDelayed() && attackCount == 4) {
      attackCount = 0;
      var specialAttack = phase3SpecialAttacks.poll();
      if (specialAttack == Phase3SpecialAttack.NYLOCAS) {
        spawnCommonNylocas();
      }
    }
  }

  private void electricBallAttack() {
    var firstOpponent = getAttackingEntity();
    if (firstOpponent == null || firstOpponent.isLocked() || !firstOpponent.isPlayer()) {
      return;
    }
    setHitDelay(npc.getCombatDef().getAttackSpeed());
    npc.setAnimation(npc.getCombatDef().getAttackAnimation());
    var players = getAttackablePlayers();
    players.remove(firstOpponent);
    players.shuffle();
    electricBallAttack(npc, firstOpponent.asPlayer(), players, 1);
  }

  private void electricBallAttack(
      Entity fromEntity, Entity toEntity, PArrayList<Player> players, int count) {
    var projectile =
        Graphic.Projectile.builder()
            .id(1585)
            .startTile(fromEntity)
            .entity(toEntity)
            .speed(getProjectileSpeed(10))
            .build();
    sendMapProjectile(projectile);
    if (toEntity == npc) {
      return;
    }
    addSingleEvent(
        projectile.getEventDelay(),
        e -> {
          if (toEntity.isPlayer() && (count % 2 == 0 || players.isEmpty())) {
            toEntity
                .getCombat()
                .addHitEvent(
                    new HitEvent(projectile.getEventDelay(), new Hit(PRandom.randomI(40, 48))));
          }
          players.removeIf(p -> !canAttackEntity(p));
          Entity toNewEntity = players.removeFirst();
          if (toNewEntity == null) {
            return;
          }
          var path = ProjectileRoute.INSTANCE.getPath(npc.getController(), toEntity, toNewEntity);
          if (path.containsIf(t -> npc.withinDistance(t, 0))) {
            toNewEntity = npc;
          }
          electricBallAttack(toEntity, toNewEntity, players, count + 1);
        });
  }

  private void spawnNylocasAthanatos() {
    if (athanatosCooldown > 0) {
      return;
    }
    if (athanatos != null && !athanatos.isLocked()) {
      return;
    }
    athanatosCooldown = 100;
    setHitDelay(npc.getCombatDef().getAttackSpeed());
    npc.setAnimation(npc.getCombatDef().getAttackAnimation());
    var athanatosTile = new Tile();
    var direction = PRandom.randomE(4);
    switch (direction) {
      case 0:
        athanatosTile.setTile(npc.getX() - 4, npc.getY(), npc.getHeight());
        break;
      case 1:
        athanatosTile.setTile(npc.getX() + npc.getSizeX() + 1, npc.getY(), npc.getHeight());
        break;
      case 2:
        athanatosTile.setTile(npc.getX(), npc.getY() - 4, npc.getHeight());
        break;
      default:
        athanatosTile.setTile(npc.getX(), npc.getY() + npc.getSizeY() + 1, npc.getHeight());
        break;
    }
    spawnCommonNylocas();
    var projectile =
        Graphic.Projectile.builder()
            .id(1586)
            .startTile(npc)
            .endTile(athanatosTile)
            .speed(getProjectileSpeed(10))
            .endHeight(0)
            .build();
    sendMapProjectile(projectile);
    addSingleEvent(
        projectile.getEventDelay(),
        e -> athanatos = addNpc(new NpcSpawn(athanatosTile, NpcId.NYLOCAS_ATHANATOS_350)));
  }

  private void spawnCommonNylocas() {
    setHitDelay(npc.getCombatDef().getAttackSpeed());
    var players = getAttackablePlayers();
    players.forEach(p -> COMMON_NYLOCAS_SUMMONING.applyAttack(npc, p));
  }

  private void tornadoAttack() {
    npc.setForceMessage("I'm not done with you yet!");
  }

  private void stickyWebsAttack() {
    var tasks = new PEventTasks();
    npc.lock();
    npc.setAnimation(8127);
    npc.getMovement().quickRoute(3165, 4312);
    tasks.condition(t -> !npc.getMovement().isRouting());
    tasks.execute(
        t -> {
          for (var player : getAttackablePlayers()) {
            /*var positions = webPositions(player.getPosition().copy(), Random.get(2, 3));
            for (var position : positions) {
              var delay = VERZIK_PHASE_THREE_WEBS.send(npc.getPosition().relative(3, 2), position);
              var ticks = (((25 * delay)) / 600) - 1;*/

            /*wave.getMap().addEvent("VERZIK_VITUR_WEBS_MAP",event1 -> {
              event1.delay(ticks);
              if (position.getTile().npcCount <= 0) {
                var webObject = new GameObject(32734, position.copy(), 10, 0).skipClipping(true).spawn();
                event1.delay(2);
                var web = wave.spawnNPC(8376, position, true);
                VerzikWebs webCombat = (VerzikWebs) web.getCombat();
                webCombat.setWebObject(webObject);
              }
            });*/
            // }
          }
          if (t.getExecutions() < 14) {
            t.delay(2);
          }
        });
    tasks.execute(2, t -> npc.unlock());
    addEvent(tasks);
  }

  @AllArgsConstructor
  @Getter
  private enum Phase {
    FIRST(NpcId.VERZIK_VITUR_1040_8370),
    PRE_SECOND(NpcId.VERZIK_VITUR_1040_8371),
    SECOND(NpcId.VERZIK_VITUR_1265),
    PRE_THIRD(NpcId.VERZIK_VITUR_1265_8373),
    THIRD(NpcId.VERZIK_VITUR_1520),
    THIRD_20_PERCENT(NpcId.VERZIK_VITUR_1520);

    private final int npcId;

    public static Phase getPhase(int npcId) {
      for (var phase : values()) {
        if (phase.getNpcId() != npcId) {
          continue;
        }
        return phase;
      }
      return null;
    }
  }

  private enum Phase3SpecialAttack {
    NYLOCAS,
    STICKY_WEBS,
    YELLOW_PROJECTILE,
    GREEN_BALL
  }
}
