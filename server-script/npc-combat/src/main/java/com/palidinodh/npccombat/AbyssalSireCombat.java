package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class AbyssalSireCombat extends NpcCombat {

  private static final Tile[][] RESPIRATORY_SYSTEMS = {
    {new Tile(2964, 4844), new Tile(2992, 4843), new Tile(2967, 4834), new Tile(2995, 4833)},
    {new Tile(3089, 4844), new Tile(3117, 4843), new Tile(3092, 4834), new Tile(3120, 4833)},
    {new Tile(2954, 4780), new Tile(2982, 4779), new Tile(2957, 4770), new Tile(2985, 4769)},
    {new Tile(3094, 4780), new Tile(3122, 4779), new Tile(3097, 4770), new Tile(3125, 4769)}
  };
  private static final Tile[][] TENTACLES = {
    {
      new Tile(2967, 4844),
      new Tile(2984, 4844),
      new Tile(2970, 4835),
      new Tile(2982, 4835),
      new Tile(2968, 4826),
      new Tile(2985, 4826)
    },
    {
      new Tile(3092, 4844),
      new Tile(3109, 4844),
      new Tile(3095, 4835),
      new Tile(3107, 4835),
      new Tile(3093, 4826),
      new Tile(3110, 4826)
    },
    {
      new Tile(2957, 4780),
      new Tile(2974, 4780),
      new Tile(2960, 4771),
      new Tile(2972, 4771),
      new Tile(2958, 4762),
      new Tile(2975, 4762)
    },
    {
      new Tile(3097, 4780),
      new Tile(3114, 4780),
      new Tile(3100, 4771),
      new Tile(3112, 4771),
      new Tile(3098, 4762),
      new Tile(3115, 4762)
    }
  };
  @Inject private Npc npc;
  private int phase;
  private Npc[] respiratorySystems;
  private Npc[] vents;
  private Npc[] tentacles;
  private List<Npc> spawns = new ArrayList<>();
  private Player combatWith;
  private int countdown;
  private int status;
  private Tile moveTo;
  private int fumeDelay;
  private Tile fumeTile;
  private int disorientingState;
  private int disorientingDelay;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .underKiller(true)
            .rareDropTableDenominator(256)
            .clue(ClueScrollType.ELITE, 180);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(100);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNSIRED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SPIRIT_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BODY_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIND_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.EARTH_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COSMIC_TALISMAN)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATEBODY_NOTED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_SWORD_NOTED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_FULL_HELM_NOTED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_KITESHIELD_NOTED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BATTLESTAFF_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_BATTLESTAFF_NOTED, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_AIR_STAFF_NOTED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_LAVA_STAFF_NOTED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.EARTH_ORB_NOTED, 47, 53)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BAR_NOTED, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE_NOTED, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM_NOTED, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME_NOTED, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE_NOTED, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED_NOTED, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATERMELON_SEED, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANARR_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PAPAYA_TREE_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PALM_TREE_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WILLOW_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAPLE_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SARADOMIN_BREW_3_NOTED, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_RESTORE_4, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 300, 370)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 225, 275)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 160, 210)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ONYX_BOLT_TIPS, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND_NOTED, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BINDING_NECKLACE_NOTED, 25)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.JUG_OF_WATER_NOTED, 254, 350)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CANNONBALL, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL_NOTED, 1, 5)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 600)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 380, 420)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_LOGS_NOTED, 51, 70)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHILLI_POTATO_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COSMIC_RUNE, 350)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 9000, 51989)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASHES)));
    drop.table(dropTable.build());

    var combat1 = NpcCombatDefinition.builder();
    combat1
        .id(NpcId.ABYSSAL_SIRE_350)
        .id(NpcId.ABYSSAL_SIRE_350_5887)
        .id(NpcId.ABYSSAL_SIRE_350_5888);
    combat1.hitpoints(
        NpcCombatHitpoints.builder().total(400).barType(HitpointsBarType.GREEN_RED_60).build());
    combat1.stats(
        NpcCombatStats.builder()
            .attackLevel(180)
            .magicLevel(200)
            .defenceLevel(250)
            .bonus(BonusType.MELEE_ATTACK, 65)
            .bonus(BonusType.DEFENCE_STAB, 40)
            .bonus(BonusType.DEFENCE_SLASH, 60)
            .bonus(BonusType.DEFENCE_CRUSH, 50)
            .bonus(BonusType.DEFENCE_MAGIC, 20)
            .bonus(BonusType.DEFENCE_RANGED, 60)
            .build());
    combat1.slayer(NpcCombatSlayer.builder().level(85).taskOnly(true).build());
    combat1.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat1.focus(
        NpcCombatFocus.builder().retaliationDisabled(true).bypassMapObjects(true).build());
    combat1.type(NpcCombatType.DEMON);

    var combat2 = NpcCombatDefinition.builder();
    combat2.id(NpcId.ABYSSAL_SIRE_350_5889).id(NpcId.ABYSSAL_SIRE_350_5890);
    combat2.hitpoints(
        NpcCombatHitpoints.builder().total(400).barType(HitpointsBarType.GREEN_RED_60).build());
    combat2.stats(
        NpcCombatStats.builder()
            .attackLevel(180)
            .magicLevel(200)
            .defenceLevel(250)
            .bonus(BonusType.MELEE_ATTACK, 65)
            .bonus(BonusType.DEFENCE_STAB, 40)
            .bonus(BonusType.DEFENCE_SLASH, 60)
            .bonus(BonusType.DEFENCE_CRUSH, 50)
            .bonus(BonusType.DEFENCE_MAGIC, 20)
            .bonus(BonusType.DEFENCE_RANGED, 60)
            .build());
    combat2.slayer(NpcCombatSlayer.builder().level(85).taskOnly(true).build());
    combat2.aggression(NpcCombatAggression.PLAYERS);
    combat2.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat2.focus(
        NpcCombatFocus.builder().bypassMapObjects(true).disableFollowingOpponent(true).build());
    combat2.type(NpcCombatType.DEMON);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.builder().maximum(8).prayerEffectiveness(0.4).build());
    style.animation(5751).attackSpeed(7);
    combat2.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.builder().maximum(18).prayerEffectiveness(0.4).build());
    style.animation(5369).attackSpeed(7);
    combat2.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.builder().maximum(36).prayerEffectiveness(0.4).build());
    style.animation(5755).attackSpeed(7);
    combat2.style(style.build());

    var combat3 = NpcCombatDefinition.builder();
    combat3.id(NpcId.ABYSSAL_SIRE_350_5891).id(NpcId.ABYSSAL_SIRE_350_5908);
    combat3.hitpoints(
        NpcCombatHitpoints.builder().total(400).barType(HitpointsBarType.GREEN_RED_60).build());
    combat3.stats(
        NpcCombatStats.builder()
            .attackLevel(180)
            .magicLevel(200)
            .defenceLevel(250)
            .bonus(BonusType.MELEE_ATTACK, 65)
            .bonus(BonusType.DEFENCE_STAB, 40)
            .bonus(BonusType.DEFENCE_SLASH, 60)
            .bonus(BonusType.DEFENCE_CRUSH, 50)
            .bonus(BonusType.DEFENCE_MAGIC, 20)
            .bonus(BonusType.DEFENCE_RANGED, 60)
            .build());
    combat3.slayer(NpcCombatSlayer.builder().level(85).taskOnly(true).build());
    combat3.aggression(NpcCombatAggression.PLAYERS);
    combat3.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat3.focus(
        NpcCombatFocus.builder()
            .retaliationDisabled(true)
            .bypassMapObjects(true)
            .disableFollowingOpponent(true)
            .build());
    combat3.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat3.type(NpcCombatType.DEMON).deathAnimation(7100);
    combat3.drop(drop.build());

    return Arrays.asList(combat1.build(), combat2.build(), combat3.build());
  }

  @Override
  public void restoreHook() {
    reset();
  }

  @Override
  public void tickStartHook() {
    if (!npc.isLocked()
        && !isLocked()
        && phase > 0
        && (getLastAttackedByDelay() > 500 || !npc.withinMapDistance(combatWith))) {
      npc.restore();
    }
    if (!npc.isLocked() && !isLocked()) {
      npc.getMovement().setImmobile(true);
    }
    setDisableAutoRetaliate(combatWith != null);
    if (countdown > 0) {
      countdown--;
    }
    updateAttacking();
    fumeTick();
    spawnTick();
    if (phase == 1) {
      phase1Tick();
    } else if (phase == 2) {
      phase2Tick();
    } else if (phase == 3) {
      phase3Tick();
    } else if (phase == 4) {
      phase4Tick();
    }
  }

  @Override
  public void tickEndHook() {
    updateAttacking();
    if (moveTo != null) {
      npc.getMovement().clear();
      npc.getMovement().addMovement(moveTo);
      if (npc.withinDistance(moveTo, 0)) {
        moveTo = null;
      }
    }
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    if (!(opponent instanceof Player)) {
      return false;
    }
    var player = (Player) opponent;
    if (combatWith != null && player != combatWith) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("Abyssal Sire is busy with someone else.");
      }
      return false;
    }
    if (phase == 1 && disorientingDelay > 0) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("The Abyssal Sire is currently disoriented.");
      }
      return false;
    }
    return true;
  }

  @Override
  public void applyDeadHook() {
    combatWith = null;
    lockTentacles(Integer.MAX_VALUE);
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (!opponent.isPlayer()) {
      return 0;
    }
    var player = opponent.asPlayer();
    combatWith = player;
    if (phase == 0) {
      wakeUp();
    } else if (phase == 1) {
      if (disorientingDelay == 0 && npc.getId() != NpcId.ABYSSAL_SIRE_350) {
        if (hitStyleType == HitStyleType.MAGIC
            && player.getMagic().getActiveSpell() != null
            && player.getMagic().getActiveSpell().getName().contains("shadow")) {
          if (player.getMagic().getActiveSpell().getName().contains("rush")) {
            disorientingState += 25;
          } else if (player.getMagic().getActiveSpell().getName().contains("burst")) {
            disorientingState += 50;
          } else if (player.getMagic().getActiveSpell().getName().contains("blitz")) {
            disorientingState += 75;
          } else if (player.getMagic().getActiveSpell().getName().contains("barrage")) {
            disorientingState += 100;
          }
        } else {
          disorientingState += (int) (damage * 1.33);
        }
      } else if (disorientingDelay > 0) {
        damage = 0;
      }
    }
    if (phase != 4 && damage > getHitpoints()) {
      damage = getHitpoints() - 1;
    }
    return damage;
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("combat_with")) {
      return combatWith;
    }
    if (name.equals("phase")) {
      return phase;
    }
    if (name.equals("disorienting_delay")) {
      return disorientingDelay;
    }
    return null;
  }

  public void updateAttacking() {
    if (!isDead() && npc.isVisible() && npc.withinDistance(combatWith, 32)) {
      setAttackingEntity(combatWith);
      if (npc.isLocked() || isLocked() || !npc.getCombatDef().hasAttack()) {
        npc.setFaceEntity(null);
      }
      for (var spawnedNpc : tentacles) {
        spawnedNpc.getCombat().setAttackingEntity(combatWith);
        if (spawnedNpc.isLocked() || !spawnedNpc.getCombatDef().hasAttack()) {
          spawnedNpc.setFaceEntity(null);
        }
      }
      for (var spawnedNpc : spawns) {
        spawnedNpc.getCombat().setAttackingEntity(combatWith);
        if (spawnedNpc.isLocked() || !spawnedNpc.getCombatDef().hasAttack()) {
          spawnedNpc.setFaceEntity(null);
        }
      }
    } else if (combatWith != null) {
      combatWith = null;
    }
  }

  public void wakeUp() {
    if (npc.getId() != NpcId.ABYSSAL_SIRE_350) {
      return;
    }
    phase = 1;
    endDisoriented();
  }

  public void lockTentacles(int lock) {
    for (var spawnedNpc : tentacles) {
      if (spawnedNpc.getId() != NpcId.TENTACLE_5913) {
        spawnedNpc.setId(NpcId.TENTACLE_5913);
        spawnedNpc.setAnimation(7112);
        spawnedNpc.setFaceTile(new Tile(spawnedNpc).moveTile(1, -1));
        spawnedNpc.setLock(lock);
      }
    }
  }

  public void unlockTentacles() {
    for (var spawnedNpc : tentacles) {
      if (spawnedNpc.getId() != NpcId.TENTACLE_5912) {
        spawnedNpc.setId(NpcId.TENTACLE_5912);
        spawnedNpc.setAnimation(7114);
        spawnedNpc.setLock(5);
      }
    }
  }

  public void teleportPlayer() {
    if (combatWith != null && combatWith.isVisible()) {
      combatWith
          .getMovement()
          .animatedTeleport(new Tile(npc).moveTile(2, -1), 1816, new Graphic(342, 100), 2);
    }
  }

  public void fumeTick() {
    if (isLocked()) {
      return;
    }
    if (fumeDelay > 0) {
      fumeDelay--;
      if (fumeTile != null && fumeDelay < 4 && combatWith != null && !combatWith.isLocked()) {
        if (fumeTile.withinDistance(combatWith, 0)) {
          combatWith.getCombat().addHit(new Hit(28, HitMarkType.GREEN));
        } else if (fumeTile.withinDistance(combatWith, 1)) {
          combatWith.getCombat().addHit(new Hit(14, HitMarkType.GREEN));
        }
        if (!combatWith.isPoisonImmune()) {
          combatWith.setPoison(8);
        }
      }
    } else if (combatWith != null
        && !combatWith.isLocked()
        && fumeDelay == 0
        && !npc.isLocked()
        && (phase != 1 || disorientingDelay == 0)
        && PRandom.randomE(20) == 0) {
      fumeDelay = 6;
      fumeTile = new Tile(combatWith);
      npc.getController().sendMapGraphic(fumeTile, new Graphic(1275));
    }
  }

  public void spawnTick() {
    if (spawns.isEmpty()) {
      return;
    }
    spawns.removeIf(npc -> npc.getCombat().isDead());
  }

  public void summonSpawn(int x, int y) {
    if (isDead() || !npc.isVisible()) {
      return;
    }
    var spawnedNpc =
        npc.getController().addNpc(new NpcSpawn(new Tile(x, y, npc.getHeight()), NpcId.SPAWN_60));
    spawns.add(spawnedNpc);
  }

  public void phase1Tick() {
    var areDead = true;
    for (int i = 0; i < respiratorySystems.length; ++i) {
      var spawnedNpc = respiratorySystems[i];
      if (spawnedNpc.isVisible()) {
        areDead = false;
        if (spawnedNpc.getCombat().isDead()) {
          vents[i].setAnimation(7102);
        } else if (disorientingDelay > 0) {
          vents[i].setAnimation(7105);
        }
      } else {
        vents[i].setVisible(false);
      }
    }
    if (areDead) {
      startPhase2();
    } else if (disorientingDelay == 0 && disorientingState >= 100) {
      startDisoriented();
    } else if (disorientingDelay > 0) {
      disorientingDelay--;
      if (disorientingDelay == 0) {
        endDisoriented();
      }
    }
  }

  public void startDisoriented() {
    disorientingDelay = 40;
    npc.setId(NpcId.ABYSSAL_SIRE_350_5888);
    npc.setAnimation(4531);
    setLock(4);
    lockTentacles(2);
  }

  public void endDisoriented() {
    disorientingState = 0;
    npc.setId(NpcId.ABYSSAL_SIRE_350_5887);
    npc.setAnimation(4528);
    setLock(8);
    unlockTentacles();
    for (var vent : vents) {
      vent.setAnimation(-1);
    }
  }

  public void phase2Tick() {
    if (!npc.getMovement().isRouting()
        && combatWith != null
        && combatWith.getCombat().getAttackingEntity() == npc
        && !isHitDelayed()
        && !npc.withinDistance(combatWith, 1)) {
      teleportPlayer();
    }
    if (getHitpoints() < getMaxHitpoints() / 2) {
      startPhase3();
    }
  }

  public void startPhase2() {
    phase = 2;
    clearHitEvents();
    npc.setId(NpcId.ABYSSAL_SIRE_350_5890);
    setHitpoints(npc.getCombatDef().getHitpoints().getTotal());
    npc.setAnimation(4532);
    npc.setLock(19);
    npc.getMovement().setImmobile(false);
    npc.setFaceEntity(null);
    npc.getMovement().setFollowing(null);
    npc.getController().setMagicBind(8, null);
    moveTo = new Tile(npc.getX(), npc.getY() - 10, npc.getHeight());
    npc.getMovement().clear();
    npc.getMovement().addMovement(moveTo);
    lockTentacles(2);
  }

  public void phase3Tick() {
    if (status == 0 && countdown == 0) {
      status = 1;
      countdown = 6;
      npc.setId(NpcId.ABYSSAL_SIRE_350_5891);
      npc.setAnimation(7096);
    } else if (status == 1 && countdown == 0) {
      status = 2;
      unlockTentacles();
      summonSpawn(npc.getX(), npc.getY());
      summonSpawn(npc.getX() + 1, npc.getY());
      summonSpawn(npc.getX(), npc.getY() + 1);
      summonSpawn(npc.getX() + 1, npc.getY() + 1);
    }
    if (getHitpoints() < getMaxHitpoints() * 0.35) {
      startPhase4();
    }
  }

  public void startPhase3() {
    phase = 3;
    clearHitEvents();
    countdown = 10;
    npc.setLock(countdown + 6);
    npc.setFaceEntity(null);
    npc.getMovement().setImmobile(false);
    npc.getMovement().setFollowing(null);
    moveTo = new Tile(npc.getX(), npc.getY() - 9, npc.getHeight());
    npc.getMovement().clear();
    npc.getMovement().addMovement(moveTo);
  }

  public void phase4Tick() {
    if (status == 0 && countdown == 0) {
      status = 1;
      if (combatWith != null && npc.withinDistance(combatWith, 1)) {
        combatWith.getCombat().addHit(new Hit(PRandom.randomI(72)));
      }
      summonSpawn(npc.getX(), npc.getY());
      summonSpawn(npc.getX() + 1, npc.getY());
      summonSpawn(npc.getX(), npc.getY() + 1);
      summonSpawn(npc.getX() + 1, npc.getY() + 1);
    }
  }

  public void startPhase4() {
    phase = 4;
    clearHitEvents();
    status = 0;
    countdown = 6;
    npc.setLock(6);
    npc.setId(NpcId.ABYSSAL_SIRE_350_5908);
    npc.setAnimation(7098);
    teleportPlayer();
  }

  public int getRegionIndex() {
    if (npc.getRegionId() == 12363) {
      return 1;
    }
    if (npc.getRegionId() == 11850) {
      return 2;
    }
    if (npc.getRegionId() == 12362) {
      return 3;
    }
    return 0;
  }

  public void reset() {
    if (npc.isVisible()) {
      npc.getMovement().teleport(npc.getSpawn().getTile());
    }
    combatWith = null;
    countdown = 0;
    status = 0;
    fumeDelay = 0;
    disorientingState = 0;
    disorientingDelay = 0;
    moveTo = null;
    phase = 0;
    npc.getWorld().removeNpcs(respiratorySystems);
    npc.getWorld().removeNpcs(vents);
    npc.getWorld().removeNpcs(tentacles);
    npc.getWorld().removeNpcs(spawns);
    spawns.clear();
    if (npc.isVisible()) {
      vents = new Npc[4];
      respiratorySystems = new Npc[vents.length];
      for (var i = 0; i < vents.length; i++) {
        var tile = RESPIRATORY_SYSTEMS[getRegionIndex()][i];
        vents[i] = npc.getController().addNpc(new NpcSpawn(tile, NpcId.VENT));
        respiratorySystems[i] =
            npc.getController().addNpc(new NpcSpawn(tile, NpcId.RESPIRATORY_SYSTEM));
      }
      tentacles = new Npc[6];
      for (var i = 0; i < tentacles.length; i++) {
        var id = -1;
        if (i == 0 || i == 1) {
          id = NpcId.TENTACLE_5910;
        } else if (i == 2 || i == 3) {
          id = NpcId.TENTACLE_5911;
        } else if (i == 4 || i == 5) {
          id = NpcId.TENTACLE_5909;
        }
        var tile2 = TENTACLES[getRegionIndex()][i];
        tentacles[i] = npc.getController().addNpc(new NpcSpawn(tile2, id));
      }
      for (var spawnedNpc : respiratorySystems) {
        spawnedNpc.getMovement().setImmobile(true);
      }
      for (var spawnedNpc : tentacles) {
        spawnedNpc.getMovement().setImmobile(true);
      }
    }
  }
}
