package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.Arrays;
import java.util.List;

class TzKekCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.TZ_KEK_45_3118).id(NpcId.TZ_KEK_45_3119);
    combat.hitpoints(NpcCombatHitpoints.total(20));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(40)
            .magicLevel(60)
            .rangedLevel(30)
            .defenceLevel(30)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.deathAnimation(2627).blockAnimation(2626);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(7));
    style.animation(2625).attackSpeed(4);
    combat.style(style.build());

    var combatLevel22 = NpcCombatDefinition.builder();
    combatLevel22.id(NpcId.TZ_KEK_22);
    combatLevel22.hitpoints(NpcCombatHitpoints.total(10));
    combatLevel22.stats(
        NpcCombatStats.builder()
            .attackLevel(20)
            .magicLevel(30)
            .rangedLevel(15)
            .defenceLevel(15)
            .build());
    combatLevel22.aggression(NpcCombatAggression.PLAYERS);
    combatLevel22.deathAnimation(2627).blockAnimation(2626);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(4));
    style.animation(2625).attackSpeed(4);
    combatLevel22.style(style.build());

    return Arrays.asList(combat.build(), combatLevel22.build());
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (npc.getId() == NpcId.TZ_KEK_22) {
      return;
    }
    var npc2 = npc.getController().addNpc(new NpcSpawn(npc, NpcId.TZ_KEK_22));
    npc2.getMovement().setClipNpcs(true);
    npc2.getCombat().setTarget(player);
    player.getCombat().getTzHaar().addMonster(npc2);
    var npc3 =
        npc.getController()
            .addNpc(
                new NpcSpawn(
                    new Tile(npc.getX() + 1, npc.getY(), npc.getHeight()), NpcId.TZ_KEK_22));
    npc3.getMovement().setClipNpcs(true);
    npc3.getCombat().setTarget(player);
    player.getCombat().getTzHaar().addMonster(npc3);
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (npc.getId() == NpcId.TZ_KEK_22) {
      return damage;
    }
    if (hitStyleType == HitStyleType.MELEE) {
      opponent.getCombat().addHit(new Hit(1));
    }
    return damage;
  }
}
