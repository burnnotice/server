package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;
import lombok.Setter;

class YtHurkotCombat extends NpcCombat {

  @Inject private Npc npc;
  private int healDelay;
  @Setter private Npc jad;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat108 = NpcCombatDefinition.builder();
    combat108.id(NpcId.YT_HURKOT_108);
    combat108.hitpoints(NpcCombatHitpoints.total(60));
    combat108.stats(
        NpcCombatStats.builder()
            .attackLevel(140)
            .magicLevel(120)
            .rangedLevel(120)
            .defenceLevel(60)
            .bonus(BonusType.DEFENCE_MAGIC, 100)
            .bonus(BonusType.DEFENCE_RANGED, 100)
            .build());
    combat108.deathAnimation(2638).blockAnimation(2635);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(14));
    style.animation(2637).attackSpeed(4);
    combat108.style(style.build());

    var combat141 = NpcCombatDefinition.builder();
    combat141.id(NpcId.YT_HURKOT_141);
    combat141.hitpoints(NpcCombatHitpoints.total(90));
    combat141.stats(
        NpcCombatStats.builder()
            .attackLevel(165)
            .magicLevel(150)
            .rangedLevel(150)
            .defenceLevel(100)
            .bonus(BonusType.ATTACK_MAGIC, 100)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .bonus(BonusType.DEFENCE_MAGIC, 130)
            .bonus(BonusType.DEFENCE_RANGED, 130)
            .build());
    combat141.aggression(
        NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    combat141.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat141.deathAnimation(2638).blockAnimation(2635);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(18));
    style.animation(2637).attackSpeed(4);
    combat141.style(style.build());

    return Arrays.asList(combat108.build(), combat141.build());
  }

  @Override
  public void spawnHook() {
    healDelay = 0;
    jad = null;
  }

  @Override
  public void tickStartHook() {
    Npc toHeal = null;
    if (!isAttacking()) {
      if (npc.withinDistance(jad, 4)
          && jad.getCombat().getHitpoints() < jad.getCombat().getMaxHitpoints()) {
        toHeal = jad;
      }
      npc.setFaceEntity(jad);
      npc.getMovement().setFollowing(jad);
    } else if (getHitpoints() < getMaxHitpoints() / 2) {
      toHeal = npc;
    }
    if (!isDead()
        && npc.isVisible()
        && !isHitDelayed()
        && toHeal != null
        && !toHeal.getCombat().isDead()
        && toHeal.isVisible()
        && healDelay-- <= 0) {
      healDelay = 2;
      setHitDelay(4);
      npc.setAnimation(2639);
      toHeal.setGraphic(444, 100);
      toHeal.getCombat().changeHitpoints(1 + PRandom.randomE(9), 0);
    }
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("jad")) {
      jad = (Npc) args[0];
    }
    return null;
  }
}
