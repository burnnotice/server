package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.npccombat.GreatOlmCombat.Configurations;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEventTasks;
import java.util.Arrays;
import java.util.List;
import lombok.Setter;

class GreatOlmLeftHandCombat extends NpcCombat {

  private static final Tile[] NPC_TILES = {null, new Tile(3238, 5733), null, new Tile(3223, 5743)};
  private static final Tile[] MAP_OBJECT_TILES = {
    null, new Tile(3238, 5733), null, new Tile(3220, 5743)
  };

  @Inject private Npc npc;
  @Setter private Npc headNpc;
  private MapObject mapObject;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.GREAT_OLM_LEFT_CLAW_750);
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(500).barType(HitpointsBarType.GREEN_RED_100).build());
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(250)
            .magicLevel(175)
            .rangedLevel(250)
            .defenceLevel(175)
            .bonus(BonusType.ATTACK_MAGIC, 60)
            .bonus(BonusType.ATTACK_RANGED, 60)
            .bonus(BonusType.MELEE_DEFENCE, 50)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void applyDeadStartHook(int deathDelay) {
    despawn(Configurations.DESPAWN_TIMES.getLeftHand());
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    var currentPercent = PRandom.getPercent(getHitpoints(), getMaxHitpoints());
    var newPercent = PRandom.getPercent(getHitpoints() - damage, getMaxHitpoints());
    if (headNpc == null) {
      return damage;
    }
    var headCombat = headNpc.getCombat().as(GreatOlmCombat.class);
    if (headCombat.getPhase() < 3
        && !headCombat.getRightHandNpc().isLocked()
        && currentPercent - newPercent >= 5) {
      clenchHand();
    }
    return hitStyleType == HitStyleType.MELEE ? damage : 0;
  }

  public void setAnimation(int animation) {
    npc.getController().sendMapObjectAnimation(mapObject, animation);
  }

  public void travel(int direction) {
    var despawnedObjectId = Configurations.DESPAWNED_MAP_OBJECTS.getLeftHand();
    var spawnedObjectId = Configurations.SPAWNED_MAP_OBJECTS.getLeftHand();
    var animation = Configurations.SPAWN_ANIMATIONS.getLeftHand();
    npc.setTile(NPC_TILES[direction]);
    npc.getSpawn().tile(npc);
    npc.setLock(8);
    npc.getController().setMultiCombatFlag(true);
    setHitpoints(getMaxHitpoints());
    mapObject = new MapObject(spawnedObjectId, 10, direction, MAP_OBJECT_TILES[direction]);
    npc.getController().addMapObject(new MapObject(despawnedObjectId, mapObject));
    addSingleEvent(1, e -> npc.getController().sendMapObjectAnimation(mapObject, animation));
    npc.getController().sendMapObjectAnimation(mapObject, animation);
    addSingleEvent(6, e -> npc.getController().addMapObject(mapObject));
  }

  public void despawn(int time) {
    var objectId = Configurations.DESPAWNED_MAP_OBJECTS.getLeftHand();
    var animation = Configurations.DESPAWN_ANIMATION.getLeftHand();
    if (time == 0) {
      clearEvents();
      if (mapObject != null) {
        npc.getController().addMapObject(new MapObject(objectId, mapObject));
      }
    } else {
      setAnimation(animation);
      npc.getController()
          .addSingleEvent(
              time, e -> npc.getController().addMapObject(new MapObject(objectId, mapObject)));
    }
  }

  private void clenchHand() {
    var tasks = new PEventTasks();
    tasks.execute(
        6,
        t -> {
          npc.getController()
              .sendMessage("The Great Olm's left claw clenches to protect itself temporarily.");
          npc.setLock(45);
          setAnimation(7360);
        });
    tasks.execute(e -> setAnimation(7361));
    tasks.execute(38, e -> setAnimation(7362));
    tasks.execute(2, e -> setAnimation(Configurations.DEFAULT_ANIMATIONS.getLeftHand()));
    addEvent(tasks);
  }
}
