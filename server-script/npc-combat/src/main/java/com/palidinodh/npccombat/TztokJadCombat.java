package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;
import lombok.Setter;

class TztokJadCombat extends NpcCombat {

  @Inject private Npc npc;
  private HitStyleType currentHitStyle = HitStyleType.MAGIC;
  private Npc[] healers;
  @Setter private boolean spawnHealersNorth;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.TZTOK_JAD_702);
    combat.hitpoints(NpcCombatHitpoints.total(250));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(640)
            .magicLevel(480)
            .rangedLevel(960)
            .defenceLevel(480)
            .bonus(BonusType.ATTACK_MAGIC, 60)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().venom(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(2654);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(97));
    style.animation(2655).attackSpeed(8);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(97));
    style.attackSpeed(8).attackRange(14);
    style.targetTileGraphic(new Graphic(451));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(97));
    style.attackSpeed(8).attackRange(14);
    style.castGraphic(new Graphic(447, 500));
    style.projectile(NpcCombatProjectile.builder().id(448).startHeight(124).build());
    combat.style(style.build());

    var combatLevel900 = NpcCombatDefinition.builder();
    combatLevel900.id(NpcId.JALTOK_JAD_900);
    combatLevel900.hitpoints(NpcCombatHitpoints.total(350));
    combatLevel900.stats(
        NpcCombatStats.builder()
            .attackLevel(750)
            .magicLevel(510)
            .rangedLevel(1020)
            .defenceLevel(480)
            .bonus(BonusType.ATTACK_STAB, 80)
            .bonus(BonusType.ATTACK_SLASH, 80)
            .bonus(BonusType.ATTACK_CRUSH, 80)
            .bonus(BonusType.ATTACK_MAGIC, 100)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .build());
    combatLevel900.aggression(
        NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    combatLevel900.immunity(NpcCombatImmunity.builder().venom(true).build());
    combatLevel900.focus(NpcCombatFocus.builder().bypassMapObjects(true).build());
    combatLevel900.deathAnimation(7594);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(113));
    style.animation(7590).attackSpeed(8);
    combatLevel900.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(113));
    style.attackSpeed(8).attackRange(14);
    style.targetTileGraphic(new Graphic(451));
    combatLevel900.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(113));
    style.attackSpeed(8).attackRange(14);
    style.projectile(NpcCombatProjectile.builder().id(448).startHeight(124).build());
    combatLevel900.style(style.build());

    var combatLevel900_2 = NpcCombatDefinition.builder();
    combatLevel900_2.id(NpcId.JALTOK_JAD_900_7704);
    combatLevel900_2.hitpoints(NpcCombatHitpoints.total(350));
    combatLevel900_2.stats(
        NpcCombatStats.builder()
            .attackLevel(750)
            .magicLevel(510)
            .rangedLevel(1020)
            .defenceLevel(480)
            .bonus(BonusType.ATTACK_STAB, 80)
            .bonus(BonusType.ATTACK_SLASH, 80)
            .bonus(BonusType.ATTACK_CRUSH, 80)
            .bonus(BonusType.ATTACK_MAGIC, 100)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .build());
    combatLevel900_2.aggression(
        NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    combatLevel900_2.immunity(NpcCombatImmunity.builder().venom(true).build());
    combatLevel900_2.focus(NpcCombatFocus.builder().bypassMapObjects(true).build());
    combatLevel900_2.deathAnimation(7594);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(113));
    style.animation(7590).attackSpeed(9);
    combatLevel900_2.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(113));
    style.attackSpeed(9).attackRange(14);
    style.targetTileGraphic(new Graphic(451));
    combatLevel900_2.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(113));
    style.attackSpeed(9).attackRange(14);
    style.projectile(NpcCombatProjectile.builder().id(448).startHeight(124).build());
    combatLevel900_2.style(style.build());

    return Arrays.asList(combat.build(), combatLevel900.build(), combatLevel900_2.build());
  }

  @Override
  public void spawnHook() {
    if (!isHitDelayed()) {
      setHitDelay(8);
    }
  }

  @Override
  public void despawnHook() {
    if (healers != null) {
      for (var npc2 : healers) {
        npc.getWorld().removeNpc(npc2);
      }
      healers = null;
    }
  }

  @Override
  public void tickStartHook() {
    if ((!isAttacking()
            || npc.withinDistance(getAttackingEntity(), 0)
            || !canAttackEntity(getAttackingEntity(), null)
            || currentHitStyle == HitStyleType.MELEE
                && !npc.withinDistance(getAttackingEntity(), 1))
        && (!isHitDelayed() || getHitDelay() == 3)) {
      setHitDelay(4);
    }
    if (getHitDelay() == 3) {
      currentHitStyle = getHitStyleType(getAttackingEntity(), null);
      if (currentHitStyle == HitStyleType.RANGED) {
        if (npc.getId() == NpcId.TZTOK_JAD_702 || npc.getId() == NpcId.TZTOK_JAD_702_6506) {
          npc.setAnimation(2652);
        } else if (npc.getId() == NpcId.JALTOK_JAD_900
            || npc.getId() == NpcId.JALTOK_JAD_900_7704) {
          npc.setAnimation(7593);
        }
      } else if (currentHitStyle == HitStyleType.MAGIC) {
        if (npc.getId() == NpcId.TZTOK_JAD_702 || npc.getId() == NpcId.TZTOK_JAD_702_6506) {
          npc.setAnimation(2656);
        } else if (npc.getId() == NpcId.JALTOK_JAD_900
            || npc.getId() == NpcId.JALTOK_JAD_900_7704) {
          npc.setAnimation(7592);
        }
      }
    }
    if (getHitpoints() < getMaxHitpoints() / 2
        && !isDead()
        && healers == null
        && getAttackingEntity() instanceof Player) {
      var player = (Player) getAttackingEntity();
      var healerId = -1;
      if (npc.getId() == NpcId.TZTOK_JAD_702 || npc.getId() == NpcId.TZTOK_JAD_702_6506) {
        healerId = NpcId.YT_HURKOT_108;
        healers = new Npc[4];
      } else if (npc.getId() == NpcId.JALTOK_JAD_900 || npc.getId() == NpcId.JALTOK_JAD_900_7704) {
        healerId = NpcId.YT_HURKOT_141;
        healers = new Npc[npc.getId() == NpcId.JALTOK_JAD_900 ? 5 : 3];
      }
      if (healerId == -1) {
        return;
      }
      for (int i = 0; i < healers.length; i++) {
        int[] coords = null;
        if (player.getCombat().getTzHaar().getFightCaveWave() > 0) {
          coords = player.getCombat().getTzHaar().getFightCaveCoords(healerId);
        } else if (player.getCombat().getTzHaar().getInfernoWave() > 0) {
          if (spawnHealersNorth) {
            coords =
                new int[] {npc.getX() + PRandom.randomI(4), npc.getY() + 4 + PRandom.randomI(2)};
          } else {
            coords =
                new int[] {
                  npc.getX() - 4 + PRandom.randomI(8), npc.getY() - 4 + PRandom.randomI(8)
                };
          }
        }
        if (coords == null) {
          break;
        }
        healers[i] =
            npc.getController()
                .addNpc(new NpcSpawn(new Tile(coords[0], coords[1], npc.getHeight()), healerId));
        healers[i].getMovement().setClipNpcs(true);
        healers[i].getCombat().script("jad", npc);
      }
    }
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    return currentHitStyle;
  }

  @Override
  public Graphic.ProjectileSpeed projectileSpeedHook(
      NpcCombatStyle combatStyle,
      Graphic.ProjectileSpeed speed,
      Tile tile,
      HitStyleType hitStyleType,
      int minimumDistance,
      int maximumDistance) {
    speed.setClientSpeed(speed.getClientSpeed() + speed.getClientDelay() / 4);
    speed.setClientDelay(speed.getClientDelay() / 4);
    return speed;
  }

  @Override
  public Graphic.Projectile mapProjectileHook(Graphic.Projectile projectile) {
    if (projectile.getId() == 448) {
      npc.getController()
          .sendMapProjectile(
              projectile
                  .rebuilder()
                  .id(449)
                  .speed(
                      new Graphic.ProjectileSpeed(
                          projectile.getEventDelay(),
                          projectile.getDelay() + 4,
                          projectile.getSpeed()))
                  .build());
      npc.getController()
          .sendMapProjectile(
              projectile
                  .rebuilder()
                  .id(450)
                  .speed(
                      new Graphic.ProjectileSpeed(
                          projectile.getEventDelay(),
                          projectile.getDelay() + 8,
                          projectile.getSpeed()))
                  .build());
    }
    return projectile;
  }
}
