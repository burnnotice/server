package com.palidinodh.npccombat;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.Arrays;
import java.util.List;

class ElvargCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.ELVARG_83_6349);
    combat.hitpoints(NpcCombatHitpoints.total(80));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(70)
            .magicLevel(70)
            .defenceLevel(70)
            .bonus(BonusType.DEFENCE_STAB, 20)
            .bonus(BonusType.DEFENCE_SLASH, 40)
            .bonus(BonusType.DEFENCE_CRUSH, 40)
            .bonus(BonusType.DEFENCE_MAGIC, 30)
            .bonus(BonusType.DEFENCE_RANGED, 20)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(4).build());
    combat.type(NpcCombatType.DRAGON);
    combat.deathAnimation(92).blockAnimation(89);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(80).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.DRAGONFIRE);
    style.damage(NpcCombatDamage.maximum(50));
    style.animation(81).attackSpeed(4);
    style.castGraphic(new Graphic(1, 100));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    player.getCombat().setDragonSlayer(true);
    player.getMovement().teleport(3109, 3514);
    player.getGameEncoder().sendMessage("<col=ff0000>You have completed Dragon Slayer!");
    player.getInventory().addOrDropItem(ItemId.COINS, 25_000);
  }
}
