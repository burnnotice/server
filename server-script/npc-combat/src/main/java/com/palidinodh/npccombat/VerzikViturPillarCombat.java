package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.map.MapObject;
import java.util.Arrays;
import java.util.List;

class VerzikViturPillarCombat extends NpcCombat {

  private static final int MAP_OBJECT_ID_SUPPORTING_PILLAR = 32687;

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var pillar = NpcCombatDefinition.builder();
    pillar.id(NpcId.SUPPORTING_PILLAR).id(NpcId.COLLAPSING_PILLAR);
    pillar.hitpoints(NpcCombatHitpoints.builder().total(200).build());
    pillar.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    return Arrays.asList(pillar.build());
  }

  @Override
  public void spawnHook() {
    npc.getController().addMapObject(new MapObject(MAP_OBJECT_ID_SUPPORTING_PILLAR, 10, 0, npc));
  }

  @Override
  public void despawnHook() {
    removeMapObject();
    if (npc.getId() == NpcId.COLLAPSING_PILLAR) {
      for (var player : npc.getController().getPlayers()) {
        if (player.isLocked()) {
          continue;
        }
        player.getCombat().addHit(new Hit(104));
      }
    }
  }

  @Override
  public void applyDeadStartHook(int deathDelay) {
    removeMapObject();
    npc.setId(NpcId.COLLAPSING_PILLAR);
    npc.setAnimation(8052);
  }

  private void removeMapObject() {
    npc.getController().removeMapObject(new MapObject(MAP_OBJECT_ID_SUPPORTING_PILLAR, 10, 0, npc));
  }
}
