package com.palidinodh.npccombatv0;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import java.util.Arrays;
import java.util.List;

class PoisonScorpion20Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.POISON_SCORPION_20);
    combat.hitpoints(NpcCombatHitpoints.total(23));
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.deathAnimation(6256).blockAnimation(6255);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(2));
    style.animation(6254).attackSpeed(4);
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(25).poison(3).build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
