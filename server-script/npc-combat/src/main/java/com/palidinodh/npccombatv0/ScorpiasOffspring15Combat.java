package com.palidinodh.npccombatv0;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import java.util.Arrays;
import java.util.List;

class ScorpiasOffspring15Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.SCORPIAS_OFFSPRING_15);
    combat.hitpoints(NpcCombatHitpoints.total(2));
    combat.stats(
        NpcCombatStats.builder()
            .rangedLevel(30)
            .bonus(BonusType.ATTACK_RANGED, 900)
            .bonus(BonusType.DEFENCE_RANGED, -40)
            .build());
    combat.deathAnimation(6260).blockAnimation(6259);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(2));
    style.animation(6261).attackSpeed(4);
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(25).poison(6).build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
