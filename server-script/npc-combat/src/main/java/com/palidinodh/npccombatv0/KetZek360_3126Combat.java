package com.palidinodh.npccombatv0;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.graphic.Graphic;
import java.util.Arrays;
import java.util.List;

class KetZek360_3126Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.KET_ZEK_360_3126);
    combat.hitpoints(NpcCombatHitpoints.total(160));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(320)
            .magicLevel(240)
            .rangedLevel(480)
            .defenceLevel(240)
            .bonus(BonusType.ATTACK_MAGIC, 60)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.deathAnimation(2646).blockAnimation(2645);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(54));
    style.animation(2644).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(49));
    style.animation(2647).attackSpeed(4);
    style.targetGraphic(new Graphic(446));
    style.projectile(NpcCombatProjectile.builder().id(445).startHeight(100).build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
