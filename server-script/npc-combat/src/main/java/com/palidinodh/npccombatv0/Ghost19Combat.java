package com.palidinodh.npccombatv0;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import java.util.Arrays;
import java.util.List;

class Ghost19Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat
        .id(NpcId.GHOST_19)
        .id(NpcId.GHOST_19_86)
        .id(NpcId.GHOST_19_87)
        .id(NpcId.GHOST_19_88)
        .id(NpcId.GHOST_19_89)
        .id(NpcId.GHOST_19_90)
        .id(NpcId.GHOST_19_91)
        .id(NpcId.GHOST_19_92)
        .id(NpcId.GHOST_19_93)
        .id(NpcId.GHOST_19_94)
        .id(NpcId.GHOST_19_95)
        .id(NpcId.GHOST_19_96)
        .id(NpcId.GHOST_19_97);
    combat.hitpoints(NpcCombatHitpoints.total(27));
    combat.stats(NpcCombatStats.builder().attackLevel(15).defenceLevel(17).build());
    combat.type(NpcCombatType.UNDEAD).deathAnimation(5542).blockAnimation(5541);

    var style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.maximum(2));
    style.animation(5540).attackSpeed(4);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
