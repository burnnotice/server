package com.palidinodh.npccombatv0;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import java.util.Arrays;
import java.util.List;

class JalAkrekKet70Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.JAL_AKREK_KET_70);
    combat.hitpoints(NpcCombatHitpoints.total(15));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(120)
            .defenceLevel(95)
            .bonus(BonusType.ATTACK_STAB, 25)
            .bonus(BonusType.ATTACK_SLASH, 25)
            .bonus(BonusType.ATTACK_CRUSH, 25)
            .bonus(BonusType.MELEE_DEFENCE, 25)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    combat.immunity(NpcCombatImmunity.builder().venom(true).build());
    combat.deathAnimation(7584).blockAnimation(7585);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(18));
    style.animation(7587).attackSpeed(4);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
