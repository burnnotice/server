package com.palidinodh.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.ban.BannedByUser;
import com.palidinodh.rs.ban.BannedUser;
import com.palidinodh.rs.ban.Bans;
import com.palidinodh.rs.ban.ComputerBan;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.Settings;

@ReferenceName("pcban")
class ComputerBanCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Override
  public boolean canUse(Player player) {
    return !Settings.getInstance().isLocal();
  }

  @Override
  public void execute(Player player, String name, String message) {
    player
        .getGameEncoder()
        .sendEnterString(
            "Username",
            ue -> {
              var player2 = player.getWorld().getPlayerByUsername(ue);
              if (player2 == null) {
                player.getGameEncoder().sendMessage("Unable to find user " + ue + ".");
                return;
              }
              player
                  .getGameEncoder()
                  .sendEnterAmount(
                      "Number of Hours (Permanent: 0)",
                      he -> {
                        player
                            .getGameEncoder()
                            .sendEnterString(
                                "Reason",
                                re -> {
                                  Bans.addBan(
                                      new ComputerBan(
                                          new BannedUser(
                                              player2.getId(),
                                              player2.getUsername().toLowerCase(),
                                              player2.getSession().getRemoteAddress(),
                                              player2.getSession().getMacAddress(),
                                              player2.getSession().getUuid()),
                                          new BannedByUser(player.getId(), player.getUsername()),
                                          he,
                                          re));
                                  player2.unlock();
                                  player2.getGameEncoder().sendLogout();
                                  player2.setVisible(false);
                                  var duration = he > 0 ? "for " + he + " hours" : "permanently";
                                  player
                                      .getGameEncoder()
                                      .sendMessage(
                                          ue + " has been computer banned " + duration + ".");
                                  player.log(
                                      PlayerLogType.STAFF,
                                      "computer banned " + player2.getLogName() + " " + duration);
                                });
                      });
            });
  }
}
