package com.palidinodh.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.UserRank;

@ReferenceName({"dzone", "dz", "donator"})
class DonatorZoneCommand implements CommandHandler, CommandHandler.Teleport {

  @Override
  public String getExample(String name) {
    return "- Teleports you to the donator zone";
  }

  @Override
  public boolean canUse(Player player) {
    return player.isUsergroup(UserRank.SAPPHIRE_MEMBER) || player.isStaff();
  }

  @Override
  public void execute(Player player, String name, String message) {
    if (!player.getController().canTeleport(true)) {
      return;
    }
    if (player.getCombat().inRecentCombat()) {
      player.getGameEncoder().sendMessage("You can't do this while in combat.");
      return;
    }
    SpellTeleport.normalTeleport(player, new Tile(3053, 3503));
    player.getController().stopWithTeleport();
    player.getGameEncoder().sendMessage("You teleport to the donator zone.");
  }
}
