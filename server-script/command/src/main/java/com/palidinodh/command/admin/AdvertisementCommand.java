package com.palidinodh.command.admin;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("advert")
class AdvertisementCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Override
  public String getExample(String name) {
    return "message";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player.getGameEncoder().sendMessage(message);
    DiscordBot.sendMessage(DiscordChannel.ADVERTISEMENTS, message);
  }
}
