package com.palidinodh.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("easts")
class WildernessEastDragonsCommand implements CommandHandler, CommandHandler.Teleport {

  @Override
  public String getExample(String name) {
    return "- Teleports you to the east wilderness dragons";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player.openDialogue(
        new OptionsDialogue(
            "Are you sure you want to teleport into the wilderness?",
            new DialogueOption(
                "Yes, teleport me into the wilderness!",
                (c, s) -> {
                  if (!player.getController().canTeleport(true)) {
                    return;
                  }
                  if (player.getCombat().inRecentCombat()) {
                    player.getGameEncoder().sendMessage("You can't do this while in combat.");
                    return;
                  }
                  SpellTeleport.normalTeleport(player, new Tile(3336, 3663));
                  player.getController().stopWithTeleport();
                  player.getGameEncoder().sendMessage("You teleport to the east dragons.");
                }),
            new DialogueOption("No!")));
  }
}
