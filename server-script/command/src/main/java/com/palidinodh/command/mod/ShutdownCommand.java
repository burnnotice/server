package com.palidinodh.command.mod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;

@ReferenceName("shutdown")
class ShutdownCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Override
  public String getExample(String name) {
    return "minutes";
  }

  @Override
  public boolean canUse(Player player) {
    return player.getUsername().equalsIgnoreCase("palidino") || !Settings.getInstance().isLocal();
  }

  @Override
  public void execute(Player player, String name, String message) {
    var minutes = Integer.parseInt(message);
    player.getWorld().startShutdown(minutes);
    PLogger.println(
        player.getUsername()
            + " shut the server down with a countdown of "
            + minutes
            + " minutes.");
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION,
        player.getUsername()
            + " shut the server down with a countdown of "
            + minutes
            + " minutes.");
  }
}
