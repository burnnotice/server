package com.palidinodh.command.overseer;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("broadcast")
class BroadcastCommand implements CommandHandler, CommandHandler.OverseerRank {

  @Override
  public String getExample(String name) {
    return "message";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player
        .getWorld()
        .sendBroadcast(
            player.getMessaging().getIconImage() + player.getUsername() + ": " + message);
  }
}
