package com.palidinodh.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("home")
class HomeCommand implements CommandHandler, CommandHandler.Teleport {

  @Override
  public String getExample(String name) {
    return "- Teleports you to home";
  }

  @Override
  public void execute(Player player, String name, String message) {
    if (!player.getController().canTeleport(true)) {
      return;
    }
    if (player.getCombat().inRecentCombat()) {
      player.getGameEncoder().sendMessage("You can't do this while in combat.");
      return;
    }
    SpellTeleport.normalTeleport(player, new Tile(3093, 3495));
    player.getController().stopWithTeleport();
    player.getGameEncoder().sendMessage("You teleport to home.");
  }
}
