package com.palidinodh.command.owner;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("fd")
class FakeDropCommand implements CommandHandler, CommandHandler.OwnerRank {

  @Override
  public String getExample(String name) {
    return "message";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = message.split(",");
    var username = messages[0];
    var item = messages[1];
    var boss = messages[2];

    player.getWorld().sendNews(username + " has received a " + item + " from " + boss + "!");
  }
}
