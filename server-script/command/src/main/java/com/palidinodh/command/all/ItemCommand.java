package com.palidinodh.command.all;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;

@ReferenceName("item")
class ItemCommand implements CommandHandler {

  @Override
  public String getExample(String name) {
    return "id_or_name (quantity)";
  }

  @Override
  public boolean canUse(Player player) {
    if (player.isUsergroup(UserRank.ADMINISTRATOR)) {
      return true;
    }
    if (Main.eventPriviledges(player)) {
      return true;
    }
    if (Settings.getInstance().isLocal()) {
      return true;
    }
    return Settings.getInstance().isBeta();
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var id =
        messages[0].matches("[0-9]+")
            ? Integer.parseInt(messages[0])
            : ItemId.valueOf(messages[0].replace(" ", "_").toUpperCase());
    if (id == -1) {
      player.getGameEncoder().sendMessage("Couldn't find item.");
      return;
    }
    var amount = messages.length == 2 ? Integer.parseInt(messages[1]) : 1;
    player.getInventory().addItem(id, amount);
  }
}
