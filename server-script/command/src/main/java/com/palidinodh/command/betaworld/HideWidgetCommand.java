package com.palidinodh.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("hidewidget")
class HideWidgetCommand implements CommandHandler, CommandHandler.BetaWorld {

  @Override
  public String getExample(String name) {
    return "id child_id true/false";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var id = Integer.parseInt(messages[0]);
    var childId = Integer.parseInt(messages[1]);
    var hidden = Boolean.parseBoolean(messages[2]);
    player.getGameEncoder().sendHideWidget(id, childId, hidden);
  }
}
