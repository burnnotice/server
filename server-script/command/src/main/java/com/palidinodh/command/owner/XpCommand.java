package com.palidinodh.command.owner;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("xp")
class XpCommand implements CommandHandler, CommandHandler.OwnerRank {

  @Override
  public String getExample(String name) {
    return "skill_id skill_xp username_or_userid";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var split = message.split(" ");
    var skillID = Integer.parseInt(split[0]);
    if (skillID < 0 || skillID > Skills.SKILL_COUNT) {
      player.getGameEncoder().sendMessage("Max skill ID is " + Skills.SKILL_COUNT);
      return;
    }
    var skillXP = Integer.parseInt(split[1]);
    if (skillXP < 0 || skillXP > Skills.MAX_XP) {
      player.getGameEncoder().sendMessage("Skill level can be 0-" + Skills.MAX_XP);
      return;
    }
    var baseString = skillID + " " + skillXP;
    var username = message.substring(message.indexOf(baseString) + baseString.length() + 1);
    var player2 = player.getWorld().getPlayerByUsername(username);
    if (player2 == null) {
      var userID = -1;
      userID = Integer.parseInt(username);
      if (userID != -1) {
        player2 = player.getWorld().getPlayerById(userID);
      }
    }
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + username);
      return;
    }
    player2
        .getGameEncoder()
        .sendMessage(
            player.getUsername()
                + " has adjusted your "
                + Skills.SKILL_NAMES[skillID]
                + " to "
                + Skills.getLevelSuppliedXP(skillXP));
    player
        .getGameEncoder()
        .sendMessage(
            "You have adjusted "
                + player2.getUsername()
                + "'s "
                + Skills.SKILL_NAMES[skillID]
                + " to "
                + Skills.getLevelSuppliedXP(skillXP));
    player2.getSkills().setXP(skillID, skillXP);
    player2.getSkills().setLevel(skillID, Skills.getLevelSuppliedXP(skillXP));
    player2.getSkills().setCombatLevel();
    player2.getGameEncoder().sendSkillLevel(skillID);
  }
}
