package com.palidinodh.command.betaworld;

import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("widgetsettings")
class WidgetSettingsCommand implements CommandHandler, CommandHandler.BetaWorld {

  @Override
  public String getExample(String name) {
    return "widget_id child_count";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.splitInt(message);
    for (var i = 1; i < messages[1]; i++) {
      player
          .getGameEncoder()
          .sendWidgetSettings(
              messages[0],
              i,
              0,
              1000,
              WidgetSetting.OPTION_0,
              WidgetSetting.ON_WIDGET,
              WidgetSetting.ON_INVENTORY,
              WidgetSetting.USED_ON,
              WidgetSetting.DEPTH_0,
              WidgetSetting.DEPTH_1,
              WidgetSetting.DEPTH_2,
              WidgetSetting.DEPTH_3,
              WidgetSetting.DEPTH_4,
              WidgetSetting.DEPTH_5,
              WidgetSetting.DEPTH_6,
              WidgetSetting.DEPTH_7);
    }
  }
}
