package com.palidinodh.command.overseer;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName({"sz", "staffzone"})
class StaffZoneCommand
    implements CommandHandler, CommandHandler.Teleport, CommandHandler.OverseerRank {

  @Override
  public String getExample(String name) {
    return "- Teleports you to the staff zone zone";
  }

  @Override
  public void execute(Player player, String name, String message) {
    if (!player.getController().canTeleport(true)) {
      return;
    }
    if (player.getCombat().inRecentCombat()) {
      player.getGameEncoder().sendMessage("You can't do this while in combat.");
      return;
    }
    SpellTeleport.normalTeleport(player, new Tile(2896, 2726));
    player.getController().stopWithTeleport();
    player.getGameEncoder().sendMessage("You teleport to the staff zone.");
  }
}
