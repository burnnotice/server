package com.palidinodh.command.owner;

import com.palidinodh.io.FileManager;
import com.palidinodh.io.Writers;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PTime;
import java.io.File;

@ReferenceName("savespeed")
class SaveSpeedCommand implements CommandHandler, CommandHandler.OwnerRank {

  @Override
  public void execute(Player player, String name, String message) {
    var timer = PTime.nanoTime();
    var file = player.saveFile(true);
    var completion = (PTime.nanoTime() - timer) / 1_000_000.0;
    player.getGameEncoder().sendMessage("saveFile(): " + completion + "ms");

    timer = PTime.nanoTime();
    var isSerializable = Writers.isSerializable(file);
    completion = (PTime.nanoTime() - timer) / 1_000_000.0;
    player.getGameEncoder().sendMessage("Writers.isSerializable(): " + completion + "ms");

    timer = PTime.nanoTime();
    var fileBytes = Writers.serialize(file);
    completion = (PTime.nanoTime() - timer) / 1_000_000.0;
    player.getGameEncoder().sendMessage("Writers.serialize(): " + completion + "ms");

    timer = PTime.nanoTime();
    FileManager.getInstance()
        .addSaveRequest(
            new File(Settings.getInstance().getPlayerMapOnlineDirectory(), player.getId() + ".dat"),
            fileBytes);
    completion = (PTime.nanoTime() - timer) / 1_000_000.0;
    player.getGameEncoder().sendMessage("addSaveRequest(): " + completion + "ms");

    timer = PTime.nanoTime();
    Main.saveBondInfos();
    completion = (PTime.nanoTime() - timer) / 1_000_000.0;
    player.getGameEncoder().sendMessage("saveBondInfos(): " + completion + "ms");
  }
}
