package com.palidinodh.command.mod;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("wildslayer")
class WildSlayerCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Override
  public String getExample(String name) {
    return "true/false";
  }

  @Override
  public void execute(Player player, String name, String message) {
    SlayerPlugin.setWildernessTasksEnabled(Boolean.parseBoolean(message));
  }
}
