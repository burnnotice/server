package com.palidinodh.command.mod;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("barricade")
class BarricadeCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Override
  public void execute(Player player, String name, String message) {
    player.getController().addMapObject(new MapObject(ObjectId.BARRICADE, 10, 0, player));
  }
}
