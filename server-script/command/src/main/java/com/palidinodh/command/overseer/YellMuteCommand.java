package com.palidinodh.command.overseer;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName({"ymute", "yellmute", "ym", "yunmute", "yellunmute", "yum"})
class YellMuteCommand implements CommandHandler, CommandHandler.OverseerRank {

  @Override
  public String getExample(String name) {
    switch (name) {
      case "yellmute":
      case "ymute":
      case "ym":
        return "\"username or userid\" hours";
      case "yellunmute":
      case "yunmute":
      case "yum":
        return "username or userid";
    }
    return "";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var username = messages[0].replace("_", " ");
    var hours = 0;
    if (messages.length == 2) {
      hours = Integer.parseInt(messages[1]);
    }
    if (hours > 168) {
      player.getGameEncoder().sendMessage("Max mute time is 168 hours(a week).");
      return;
    }
    var targetPlayer = player.getWorld().getPlayerByUsername(username);
    if (targetPlayer == null) {
      var userId = -1;
      try {
        userId = Integer.parseInt(username);
      } catch (Exception e) {
      }
      if (userId != -1) {
        targetPlayer = player.getWorld().getPlayerById(userId);
      }
    }
    if (targetPlayer == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + username + ".");
      return;
    }
    if (player == targetPlayer) {
      player.getGameEncoder().sendMessage("You can't (un)mute yourself.");
      return;
    }
    if (name.equals("ymute") || name.equals("yellmute") || name.equals("ym")) {
      targetPlayer
          .getGameEncoder()
          .sendMessage(player.getUsername() + " has yellmuted you for " + hours + " hours.");
      targetPlayer.getMessaging().setYellMuteTime(hours * 60, player.getUsername());
      player
          .getGameEncoder()
          .sendMessage(username + " has been yellmuted for " + hours + " hours.");
      player
          .getWorld()
          .sendStaffMessage(
              player.getUsername()
                  + " has yellmuted "
                  + targetPlayer.getUsername()
                  + " for "
                  + hours
                  + " hours.");
      player.log(
          PlayerLogType.STAFF,
          "yellmuted " + targetPlayer.getLogName() + " for " + hours + " hours");
    } else if (name.equals("yunmute") || name.equals("yellunmute") || name.equals("yum")) {
      targetPlayer
          .getGameEncoder()
          .sendMessage(player.getUsername() + " has unmuted you from yell.");
      targetPlayer.getMessaging().setYellMuteTime(0, null);
      player.getGameEncoder().sendMessage(username + " has been unmuted from yell.");
      player
          .getWorld()
          .sendStaffMessage(
              player.getUsername() + " has unmuted " + targetPlayer.getUsername() + " from yell.");
      player.log(PlayerLogType.STAFF, "unmuted " + targetPlayer.getLogName() + " from yell.");
    }
  }
}
