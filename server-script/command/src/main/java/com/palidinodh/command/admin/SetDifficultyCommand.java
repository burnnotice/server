package com.palidinodh.command.admin;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.adaptive.RsDifficultyMode;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("setdiff")
class SetDifficultyCommandCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var username = messages[0].replace("_", " ");
    var player2 = player.getWorld().getPlayerByUsername(username);
    var difficulty = 0;
    if (messages.length == 2) {
      difficulty = Integer.parseInt(messages[1]);
    }
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + username + ".");
      return;
    }
    switch (difficulty) {
      case 0:
        player2.setDifficultyMode(RsDifficultyMode.NORMAL);
        player2
            .getGameEncoder()
            .sendMessage("Your difficulty has been set to normal mode by " + player.getUsername());
        player
            .getGameEncoder()
            .sendMessage("You set " + player2.getUsername() + " difficulty to normal");
        break;
      case 1:
        player2.setDifficultyMode(RsDifficultyMode.HARD);
        player2
            .getGameEncoder()
            .sendMessage("Your difficulty has been set to hard mode by " + player.getUsername());
        player
            .getGameEncoder()
            .sendMessage("You set " + player2.getUsername() + " difficulty to hard");
        break;
      case 2:
        player2.setDifficultyMode(RsDifficultyMode.ELITE);
        player2
            .getGameEncoder()
            .sendMessage("Your difficulty has been set to elite mode by " + player.getUsername());
        player
            .getGameEncoder()
            .sendMessage("You set " + player2.getUsername() + " difficulty to elite");
        break;
      default:
        player.getGameEncoder().sendMessage("0=normal, 1=hard, 2=elite");
        break;
    }
  }
}
