package com.palidinodh.command.all;

import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;

@ReferenceName("master")
class MasterCommand implements CommandHandler {

  @Override
  public boolean canUse(Player player) {
    if (player.isUsergroup(UserRank.ADMINISTRATOR)) {
      return true;
    }
    if (Main.eventPriviledges(player)) {
      return true;
    }
    if (Settings.getInstance().isLocal()) {
      return true;
    }
    return Settings.getInstance().isBeta();
  }

  @Override
  public void execute(Player player, String name, String message) {
    for (var id = 0; id < Skills.SKILL_COUNT; id++) {
      player.getSkills().setLevel(id, 99);
      player.getSkills().setXP(id, Skills.XP_TABLE[99]);
      player.getGameEncoder().sendSkillLevel(id);
    }
    player.getSkills().setCombatLevel();
    player.getCombat().setMaxHitpoints(99);
    player.rejuvenate();
    player.restore();
  }
}
