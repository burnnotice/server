package com.palidinodh.command.overseer;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("sc")
class ScCommand implements CommandHandler, CommandHandler.OverseerRank {

  @Override
  public String getExample(String name) {
    return "message_to_staff";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player
        .getWorld()
        .sendStaffMessage(
            player.getMessaging().getIconImage()
                + player.getUsername()
                + ": "
                + message
                + "</col>");
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION, "[Staff chat] " + player.getUsername() + ": " + message);
  }
}
