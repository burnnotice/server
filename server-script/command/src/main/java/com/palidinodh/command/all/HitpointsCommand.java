package com.palidinodh.command.all;

import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;

@ReferenceName("hp")
class HitpointsCommand implements CommandHandler {

  @Override
  public String getExample(String name) {
    return "amount";
  }

  @Override
  public boolean canUse(Player player) {
    if (player.isUsergroup(UserRank.ADMINISTRATOR)) {
      return true;
    }
    if (Main.eventPriviledges(player)) {
      return true;
    }
    if (Settings.getInstance().isLocal()) {
      return true;
    }
    return Settings.getInstance().isBeta();
  }

  @Override
  public void execute(Player player, String name, String message) {
    var amount = Integer.parseInt(message);
    if (amount < 1) {
      return;
    }
    player.getMovement().setEnergy(amount);
    player.getCombat().setHitpoints(amount);
    player
        .getGameEncoder()
        .sendMessage("<col=ff0000> You have set your hitpoints to " + amount + "!");
  }
}
