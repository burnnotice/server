package com.palidinodh.command.betaworld;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.Region;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("mapids")
class MapIdsCommand implements CommandHandler, CommandHandler.BetaWorld {

  @Override
  public void execute(Player player, String name, String message) {
    var id = player.getRegionId();
    var base = new Tile((id >> 8) * Region.SIZE, (id & 255) * Region.SIZE);
    var referenceName = base.getX() / Region.SIZE + "_" + base.getY() / Region.SIZE;
    // var landId = CacheManager.getInstance().getArchiveId(IndexType.MAP, "l" + referenceName);
    // var mapId = CacheManager.getInstance().getArchiveId(IndexType.MAP, "m" + referenceName);
    // player.getGameEncoder().sendMessage("Files - " + referenceName + ": " + landId + ", " +
    // mapId);
  }
}
