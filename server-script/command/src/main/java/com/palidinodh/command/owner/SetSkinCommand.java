package com.palidinodh.command.owner;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("setskin")
class SetSkinCommand implements CommandHandler, CommandHandler.OwnerRank {

  @Override
  public String getExample(String name) {
    return "\"username\" id";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var username = messages[0].replace("_", " ");
    var id = Integer.parseInt(messages[1]);
    var player2 = player.getWorld().getPlayerByUsername(username);
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find player.");
      return;
    }
    player2.getAppearance().setColor(4, id);
  }
}
