package com.palidinodh.worldevent.pvptournament;

import com.palidinodh.osrscore.model.tile.Tile;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ModeMap {
  ONE_ROOM(null, Arrays.asList(new ModeMapRoom(new Tile(2594, 5406, 1), new Tile(2594, 5416, 1)))),
  FIVE_BY_FIVE_ROOMS(
      new Tile(2592, 5470),
      Arrays.asList(
          new ModeMapRoom(new Tile(2596, 5475), new Tile(2598, 5475)),
          new ModeMapRoom(new Tile(2596, 5481), new Tile(2598, 5481)),
          new ModeMapRoom(new Tile(2602, 5481), new Tile(2604, 5481)),
          new ModeMapRoom(new Tile(2602, 5475), new Tile(2604, 5475)),
          new ModeMapRoom(new Tile(2596, 5466), new Tile(2598, 5466)),
          new ModeMapRoom(new Tile(2602, 5466), new Tile(2604, 5466)),
          new ModeMapRoom(new Tile(2602, 5460), new Tile(2604, 5460)),
          new ModeMapRoom(new Tile(2596, 5460), new Tile(2598, 5460)),
          new ModeMapRoom(new Tile(2587, 5466), new Tile(2589, 5466)),
          new ModeMapRoom(new Tile(2587, 5460), new Tile(2589, 5460)),
          new ModeMapRoom(new Tile(2581, 5460), new Tile(2583, 5460)),
          new ModeMapRoom(new Tile(2581, 5466), new Tile(2583, 5466)),
          new ModeMapRoom(new Tile(2587, 5481), new Tile(2589, 5481)),
          new ModeMapRoom(new Tile(2587, 5475), new Tile(2589, 5475)),
          new ModeMapRoom(new Tile(2581, 5475), new Tile(2583, 5475)),
          new ModeMapRoom(new Tile(2581, 5481), new Tile(2583, 5481))));

  private final Tile viewingTile;
  private final List<ModeMapRoom> rooms;
}
