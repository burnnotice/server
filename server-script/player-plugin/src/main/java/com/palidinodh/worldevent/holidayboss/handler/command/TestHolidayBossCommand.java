package com.palidinodh.worldevent.holidayboss.handler.command;

import com.google.inject.Inject;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.worldevent.holidayboss.HolidayBossEvent;

@ReferenceName("testhb")
class TestHolidayBossCommand implements CommandHandler, CommandHandler.AdministratorRank {

  @Inject private HolidayBossEvent event;

  @Override
  public void execute(Player player, String name, String message) {
    event.startEvent();
  }
}
