package com.palidinodh.worldevent.holidayboss.handler.command;

import com.google.inject.Inject;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.worldevent.holidayboss.HolidayBossEvent;
import com.palidinodh.worldevent.holidayboss.HolidayBossType;

@ReferenceName("sethb")
class SetHolidayBossCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Inject private HolidayBossEvent event;

  @Override
  public String getExample(String name) {
    return "name";
  }

  @Override
  public void execute(Player player, String name, String message) {
    if (message.equalsIgnoreCase("null")) {
      event.setType(null);
      player.getGameEncoder().sendMessage("Holiday Boss event stopped.");
    } else {
      event.setType(HolidayBossType.valueOf(message.replace(" ", "_").toUpperCase()));
      player.getGameEncoder().sendMessage("Holiday Boss set to: " + event.getType() + ".");
    }
  }
}
