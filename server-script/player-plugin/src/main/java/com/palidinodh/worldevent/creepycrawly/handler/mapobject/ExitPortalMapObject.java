package com.palidinodh.worldevent.creepycrawly.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.worldevent.creepycrawly.CreepyCrawlyController;

@ReferenceId(ObjectId.PORTAL_7475)
class ExitPortalMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (!player.getController().is(CreepyCrawlyController.class)) {
      return;
    }
    var controller = player.getController().as(CreepyCrawlyController.class);
    var minigame = controller.getMinigame();
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption("Exit the game.", (c, s) -> minigame.removePlayer(player)),
            new DialogueOption("Nevermind.")));
  }
}
