package com.palidinodh.worldevent.creepycrawly.util;

import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PArrayList;
import lombok.Getter;
import lombok.Setter;

@Getter
public class CreepyBarrier {

  private int cost;
  private PArrayList<Tile> tiles;
  private PArrayList<CreepyRoom> rooms;
  @Setter private boolean unlocked;

  public CreepyBarrier(int cost, PArrayList<Tile> tiles, PArrayList<CreepyRoom> rooms) {
    this.cost = cost;
    this.tiles = tiles;
    this.rooms = rooms;
  }

  public void unlock(Player player) {
    if (unlocked) {
      return;
    }
    unlocked = true;
    if (rooms != null) {
      rooms.forEach(r -> r.setUnlocked(true));
    }
    tiles.forEach(tile -> player.getController().removeMapObject(new MapObject(-1, 0, 0, tile)));
  }

  public boolean matches(Tile tile) {
    return tiles.containsIf(t -> t.matchesTile(tile));
  }
}
