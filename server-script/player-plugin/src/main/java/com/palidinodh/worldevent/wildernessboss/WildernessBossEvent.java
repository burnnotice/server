package com.palidinodh.worldevent.wildernessboss;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PTime;

public class WildernessBossEvent extends WorldEvent {

  private static final boolean ENABLED = false;
  private static final int ADVERTISEMENT_MINUTES = 15;
  private static final int SOON_MINUTES = 5;
  private static final String[] TIME = {
    "0:00", "2:00", "4:00", "6:00", "8:00", "10:00", "12:00", "14:00", "16:00", "18:00", "20:00",
    "22:00"
  };
  private static final int[] HOURS;
  private static final int[] MINUTES;

  static {
    if (TIME == null || TIME.length == 0) {
      HOURS = null;
      MINUTES = null;
    } else {
      HOURS = new int[TIME.length];
      MINUTES = new int[TIME.length];
      for (var i = 0; i < TIME.length; i++) {
        var data = TIME[i].split(":");
        HOURS[i] = Integer.parseInt(data[0]);
        MINUTES[i] = Integer.parseInt(data[1]);
      }
    }
  }

  @Inject private transient World world;
  private transient BossSpawn spawn;
  private transient Npc bossNpc;

  public WildernessBossEvent() {
    super(4);
    rotateSpawn();
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("wilderness_boss_name")) {
      return spawn != null ? spawn.getFormattedName() : "None";
    }
    if (name.equals("wilderness_boss_message")) {
      return getNextTimeText();
    }
    return null;
  }

  @Override
  public void execute() {
    setTick(4);
    if (!canRun()) {
      return;
    }
    if (bossNpc != null && !bossNpc.isVisible()) {
      world.removeNpc(bossNpc);
      bossNpc = null;
      rotateSpawn();
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    var dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    var nextTime = getNextTime();
    if (nextTime == null) {
      return;
    }
    var remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (remainingMinutes == ADVERTISEMENT_MINUTES) {
      world.sendNotice(
          "<col=ff0000>"
              + spawn.getFormattedName()
              + " will spawn in "
              + ADVERTISEMENT_MINUTES
              + " minutes!");
      setTick(105);
    } else if (remainingMinutes == SOON_MINUTES) {
      world.sendNotice(
          "<col=ff0000>"
              + spawn.getFormattedName()
              + " will spawn in "
              + SOON_MINUTES
              + " minutes!");
      setTick(105);
    } else if (remainingMinutes == 0) {
      startEvent();
      setTick(105);
    }
  }

  private String getNextTimeText() {
    if (bossNpc != null) {
      return spawn.getFormattedName();
    }
    if (!canRun()) {
      return "N/A";
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  private void startEvent() {
    if (!canRun()) {
      return;
    }
    world.removeNpc(bossNpc);
    world.addNpc(spawn.getNpc());
    world.sendBroadcast(spawn.getDescription());
  }

  private int[] getNextTime() {
    if (!canRun() || bossNpc != null) {
      return null;
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    for (var i = 0; i < HOURS.length; i++) {
      var hour = HOURS[i];
      var minute = MINUTES[i];
      if (currentHour > hour || currentHour == hour && currentMinute > minute) {
        continue;
      }
      return new int[] {hour, minute};
    }
    return new int[] {HOURS[0], MINUTES[0]};
  }

  private void rotateSpawn() {
    if (spawn == null) {
      spawn = PRandom.arrayRandom(BossSpawn.values());
      return;
    }
    var spawns = BossSpawn.values();
    for (var i = 0; i < spawns.length; i++) {
      if (spawn != spawns[i]) {
        continue;
      }
      spawn = i + 1 < spawns.length ? spawns[i + 1] : spawns[0];
      break;
    }
  }

  public boolean canRun() {
    if (!ENABLED) {
      return false;
    }
    if (TIME == null) {
      return false;
    }
    if (TIME.length == 0) {
      return false;
    }
    return world.isPrimary();
  }
}
