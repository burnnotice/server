package com.palidinodh.worldevent.pvptournament.mode;

import com.palidinodh.playerplugin.clanwars.ClanWarsRule;
import com.palidinodh.playerplugin.clanwars.ClanWarsRuleOption;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

public interface Mode {

  List<Mode> MODES =
      Arrays.asList(
          new MainMode(),
          new BerserkerMode(),
          new PureMode(),
          new HybridMode(),
          new NhMode(),
          new MiscMode());

  static SubMode getRandomSubMode() {
    var mode = PRandom.listRandom(MODES);
    return PRandom.listRandom(mode.getSubModes());
  }

  static int[] buildRules(Object... settings) {
    var rules = ClanWarsRule.getDefault();
    for (var i = 0; i < settings.length; i += 2) {
      if (!(settings[i] instanceof ClanWarsRule)
          || !(settings[i + 1] instanceof ClanWarsRuleOption)) {
        break;
      }
      ClanWarsRule rule = (ClanWarsRule) settings[i];
      ClanWarsRuleOption option = (ClanWarsRuleOption) settings[i + 1];
      rules[rule.ordinal()] = option.getIndex();
    }
    return rules;
  }

  List<SubMode> getSubModes();
}
