package com.palidinodh.worldevent.creepycrawly.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;
import com.palidinodh.worldevent.creepycrawly.CreepyCrawlyController;
import com.palidinodh.worldevent.creepycrawly.CreepyCrawlyMinigame;
import com.palidinodh.worldevent.creepycrawly.util.CreepyBarrier;

@ReferenceId(ObjectId.BARRIER_65011)
class BarrierMapObject implements MapObjectHandler {

  private static void unlock(Player player, CreepyCrawlyMinigame minigame, CreepyBarrier barrier) {
    if (barrier.isUnlocked()) {
      return;
    }
    if (minigame.getGold() < barrier.getCost()) {
      player.getGameEncoder().sendMessage("You need more gold to unlock this.");
      return;
    }
    minigame.setGold(minigame.getGold() - barrier.getCost());
    barrier.unlock(player);
  }

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (!player.getController().is(CreepyCrawlyController.class)) {
      return;
    }
    var controller = player.getController().as(CreepyCrawlyController.class);
    var minigame = controller.getMinigame();
    var barrier = minigame.getMap().getBarrier(mapObject);
    if (barrier == null) {
      return;
    }
    if (option == 0) {
      player.openDialogue(
          new OptionsDialogue(
              new DialogueOption(
                  "Unlock for " + PNumber.formatNumber(barrier.getCost()) + " gold.",
                  (c, s) -> unlock(player, minigame, barrier)),
              new DialogueOption("Nevermind.")));
    } else if (option == 1) {
      unlock(player, minigame, barrier);
    }
  }
}
