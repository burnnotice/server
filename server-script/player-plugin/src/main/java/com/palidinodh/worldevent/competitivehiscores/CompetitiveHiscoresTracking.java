package com.palidinodh.worldevent.competitivehiscores;

import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.util.PTime;
import java.util.Calendar;
import lombok.Getter;

@Getter
class CompetitiveHiscoresTracking {

  private CompetitiveHiscoresDurationType duration;
  private String date = PTime.getExactDate();
  private CompetitiveHiscoresTrackingEntries current = new CompetitiveHiscoresTrackingEntries();
  private CompetitiveHiscoresTrackingEntries previous = new CompetitiveHiscoresTrackingEntries();

  CompetitiveHiscoresTracking(CompetitiveHiscoresDurationType duration) {
    this.duration = duration;
  }

  public void update(Player player, CompetitiveHiscoresCategoryType category, long value) {
    var individualEntry = current.getIndividuals().get(player.getId());
    if (individualEntry == null) {
      current
          .getIndividuals()
          .put(player.getId(), individualEntry = new CompetitiveHiscoresIndividual(player));
    }
    individualEntry.update(player, category, value);
    if (player.getMessaging().getClanChatUserId() <= 0) {
      return;
    }
    var clanEntry = current.getClans().get(player.getMessaging().getClanChatUserId());
    if (clanEntry == null) {
      current
          .getClans()
          .put(
              player.getMessaging().getClanChatUserId(),
              clanEntry = new CompetitiveHiscoresClan(player));
    }
    clanEntry.update(player);
    individualEntry = clanEntry.getIndividuals().get(player.getId());
    if (individualEntry == null) {
      clanEntry
          .getIndividuals()
          .put(player.getId(), individualEntry = new CompetitiveHiscoresIndividual(player));
    }
    individualEntry.update(player, category, value);
  }

  public boolean isExpired() {
    var hiscoresCalendar = PTime.getExactDateCalendar(date);
    var hiscoresValue = hiscoresCalendar.get(duration.getCalendarIdentifier());
    var currentCalendar = PTime.getCalendar();
    var currentValue = currentCalendar.get(duration.getCalendarIdentifier());
    if (duration.getHours() != -1) {
      if (hiscoresCalendar.get(Calendar.DAY_OF_MONTH)
          != currentCalendar.get(Calendar.DAY_OF_MONTH)) {
        return true;
      }
      if (hiscoresValue % duration.getHours() != 0) {
        hiscoresValue -= hiscoresValue % duration.getHours();
      }
      return currentValue - hiscoresValue > duration.getHours();
    }
    return hiscoresValue != currentValue;
  }

  public void checkExpiration() {
    if (!isExpired()) {
      return;
    }
    reset();
  }

  public void reset() {
    date = PTime.getExactDate();
    previous = current;
    current = new CompetitiveHiscoresTrackingEntries();
  }
}
