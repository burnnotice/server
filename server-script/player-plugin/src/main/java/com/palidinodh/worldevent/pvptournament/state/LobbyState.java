package com.palidinodh.worldevent.pvptournament.state;

import com.palidinodh.osrscore.Main;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlugin;
import com.palidinodh.util.PTime;
import com.palidinodh.worldevent.pvptournament.PvpTournamentEvent;
import com.palidinodh.worldevent.pvptournament.mode.Mode;
import com.palidinodh.worldevent.pvptournament.prize.DefaultPrize;

public class LobbyState implements State {

  private PvpTournamentEvent tournament;
  private int time = PvpTournamentEvent.LOBBY_JOIN_TIME;

  public LobbyState(PvpTournamentEvent tournament) {
    this.tournament = tournament;
    var selectedMode = tournament.getMode();
    if (selectedMode == null) {
      var bonds = PTime.getHour24() == 15 ? 50 : 0;
      var osgp = bonds == 50 ? 10_000_000 : 0;
      tournament.setPrize(new DefaultPrize(bonds, osgp));
      var attempts = 0;
      while (attempts++ < 16
          && (selectedMode == null
              || tournament.getRecentModes().contains(selectedMode.getName()))) {
        selectedMode = Mode.getRandomSubMode();
      }
      if (selectedMode == null) {
        selectedMode = Mode.getRandomSubMode();
      }
      tournament.setMode(selectedMode);
      if (!tournament.getRecentModes().contains(selectedMode.getName())) {
        tournament.getRecentModes().add(selectedMode.getName());
      }
      if (tournament.getRecentModes().size() > 2) {
        tournament.getRecentModes().remove(0);
      }
    }
    tournament.getPlayers().clear();
  }

  @Override
  public String getMessage() {
    var timeRemaining = "";
    var minutes = (int) PTime.tickToMin(time);
    if (minutes > 0) {
      timeRemaining = minutes + (minutes == 1 ? " minute" : " minutes");
    } else {
      timeRemaining = PTime.tickToSec(time) + " seconds";
    }
    return "Lobby: " + timeRemaining;
  }

  @Override
  public int getTime() {
    return time;
  }

  @Override
  public void execute() {
    if (time-- == PvpTournamentEvent.LOBBY_JOIN_TIME) {
      var minutes = (int) PTime.tickToMin(time + 1);
      var minutesAsString = minutes + " minutes.";
      if (minutes <= 1) {
        minutesAsString = minutes + " minute.";
      }
      var message =
          "The tournament lobby is open"
              + (isCustom() ? " for a custom match" : "")
              + " as "
              + tournament.getMode().getName()
              + ", and it will begin in "
              + minutesAsString;
      var prizeMessage = tournament.getPrize().getMessage();
      if (prizeMessage != null) {
        message += " " + prizeMessage;
      }
      Main.getWorld().sendBroadcast(message);
    } else if (time == 0) {
      var hasEnoughPlayers =
          isCustom() || tournament.getPlayers().size() >= PvpTournamentEvent.MINIMUM_PLAYERS;
      if (hasEnoughPlayers) {
        for (var player : tournament.getPlayers()) {
          player.getPlugin(ClanWarsPlugin.class).setCountdown(time);
        }
        tournament.setState(new RoundsState(tournament));
      } else {
        while (!tournament.getPlayers().isEmpty()) {
          var player = tournament.getPlayers().get(0);
          player
              .getGameEncoder()
              .sendMessage(
                  "<col=ff0000>At least "
                      + PvpTournamentEvent.MINIMUM_PLAYERS
                      + " players are needed to start.");
          player.getController().stop();
        }
        tournament.setState(new IdleState(tournament));
      }
    }
  }

  public boolean isCustom() {
    return !(tournament.getPrize() instanceof DefaultPrize);
  }
}
