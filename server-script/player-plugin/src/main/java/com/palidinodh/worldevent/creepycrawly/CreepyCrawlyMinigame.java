package com.palidinodh.worldevent.creepycrawly;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.Controller;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.playerplugin.creepycrawly.CreepyCrawlyPlugin;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PTime;
import com.palidinodh.worldevent.creepycrawly.gamemap.CreepyCastleMap;
import com.palidinodh.worldevent.creepycrawly.gamemap.CreepyMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

public class CreepyCrawlyMinigame extends PEvent {

  public static final List<Integer> GAME_ITEMS =
      Arrays.asList(
          ItemId.CORRUPTED_HALBERD_BASIC_32381,
          ItemId.CORRUPTED_HALBERD_ATTUNED_32382,
          ItemId.CORRUPTED_HALBERD_PERFECTED_32383,
          ItemId.CORRUPTED_STAFF_BASIC_32384,
          ItemId.CORRUPTED_STAFF_ATTUNED_32385,
          ItemId.CORRUPTED_STAFF_PERFECTED_32386,
          ItemId.CORRUPTED_BOW_BASIC_32387,
          ItemId.CORRUPTED_BOW_ATTUNED_32388,
          ItemId.CORRUPTED_BOW_PERFECTED_32389,
          ItemId.EXPLOSIVE_POTION_32390,
          ItemId.ANGLERFISH,
          ItemId.SUPER_RESTORE_4,
          ItemId.SUPER_RESTORE_3,
          ItemId.SUPER_RESTORE_2,
          ItemId.SUPER_RESTORE_1,
          ItemId.ABSORPTION_4,
          ItemId.ABSORPTION_3,
          ItemId.ABSORPTION_2,
          ItemId.ABSORPTION_1,
          ItemId.SUPER_COMBAT_POTION_4,
          ItemId.SUPER_COMBAT_POTION_3,
          ItemId.SUPER_COMBAT_POTION_2,
          ItemId.SUPER_COMBAT_POTION_1,
          ItemId.BASTION_POTION_4,
          ItemId.BASTION_POTION_3,
          ItemId.BASTION_POTION_2,
          ItemId.BASTION_POTION_1,
          ItemId.BATTLEMAGE_POTION_4,
          ItemId.BATTLEMAGE_POTION_3,
          ItemId.BATTLEMAGE_POTION_2,
          ItemId.BATTLEMAGE_POTION_1,
          ItemId.STAMINA_POTION_4,
          ItemId.STAMINA_POTION_3,
          ItemId.STAMINA_POTION_2,
          ItemId.STAMINA_POTION_1,
          ItemId.OVERLOAD_4,
          ItemId.OVERLOAD_3,
          ItemId.OVERLOAD_2,
          ItemId.OVERLOAD_1);
  public static final List<RandomItem> RARE_ITEMS =
      RandomItem.buildList(
          new RandomItem(ItemId.CORRUPTED_HALBERD_BASIC_32381),
          new RandomItem(ItemId.CORRUPTED_HALBERD_ATTUNED_32382),
          new RandomItem(ItemId.CORRUPTED_HALBERD_PERFECTED_32383),
          new RandomItem(ItemId.CORRUPTED_STAFF_BASIC_32384),
          new RandomItem(ItemId.CORRUPTED_STAFF_ATTUNED_32385),
          new RandomItem(ItemId.CORRUPTED_STAFF_PERFECTED_32386),
          new RandomItem(ItemId.CORRUPTED_BOW_BASIC_32387),
          new RandomItem(ItemId.CORRUPTED_BOW_ATTUNED_32388),
          new RandomItem(ItemId.CORRUPTED_BOW_PERFECTED_32389));
  public static final List<RandomItem> SUPPLY_ITEMS =
      RandomItem.buildList(
          new RandomItem(ItemId.ANGLERFISH).weight(16),
          new RandomItem(ItemId.SUPER_RESTORE_4).weight(8),
          new RandomItem(ItemId.EXPLOSIVE_POTION_32390).weight(4),
          new RandomItem(ItemId.OVERLOAD_4).weight(1),
          new RandomItem(ItemId.STAMINA_POTION_4).weight(1),
          new RandomItem(ItemId.ABSORPTION_4).weight(1));
  private static final List<Integer> COMMON_NPCS =
      Arrays.asList(
          NpcId.ZOMBIE_16020,
          NpcId.SKELETON_MAGE_16021,
          NpcId.SHADOW_SPIDER_16023,
          NpcId.FERAL_VAMPYRE_16024,
          NpcId.SKELETON_FREMENNIK_16025,
          NpcId.GHOST_16027,
          NpcId.MUMMY_16029,
          NpcId.RIYL_SHADE_16028,
          NpcId.LEECH_16035);
  private static final List<Integer> UNCOMMON_NPCS =
      Arrays.asList(
          NpcId.HOPELESS_CREATURE_16022,
          NpcId.FEAR_REAPER_16030,
          NpcId.NAIL_BEAST_16031,
          NpcId.EXPERIMENT_NO2_16032,
          NpcId.SHAEDED_BEAST_16033,
          NpcId.ABOMINATION_16034,
          NpcId.CONFUSION_BEAST_16036);
  private static final List<Integer> BOSS_NPCS =
      Arrays.asList(
          NpcId.XARPUS_16038,
          NpcId.THE_MAIDEN_OF_SUGADINTI_16039,
          NpcId.SARACHNIS_16040,
          NpcId.CORRPUTED_HUNLLEF_16045,
          NpcId.VASA_NISTIRIO_16042,
          NpcId.RANIS_DRAKAN_16043,
          NpcId.PORAZDIR_16044,
          NpcId.CORRPUTED_HUNLLEF_16045,
          NpcId.FRAGMENT_OF_SEREN_16046);
  private static final int POWER_SURGE_RESTORE = (int) (PCombat.MAX_SPECIAL_ATTACK * 0.2);
  private static final int FIRST_ROUND_DELAY = (int) PTime.secToTick(30);

  @Inject private World world;
  private Controller controller;
  @Getter private CreepyMap map;
  private List<Player> lobbyPlayers = new ArrayList<>();
  private List<Player> fightPlayers = new ArrayList<>();
  private PArrayList<Npc> npcs = new PArrayList<>();
  private Map<MapObject, PArrayList<Integer>> supplyChests = new HashMap<>();
  private Map<MapObject, PArrayList<Integer>> rareChests = new HashMap<>();
  @Getter @Setter private int gold;
  private int time;
  private int round;
  private int roundTime;
  private int remainingNpcSpawns;
  private Tile zapperPowerUp;
  private MapObject ultimateForcePowerUp;
  private MapObject powerSurgePowerUp;
  private int powerSurgeTimer;
  private MapObject recurrentDamagePowerUp;
  @Getter private int recurrentDamageTimer;

  public CreepyCrawlyMinigame() {
    super(5);
    map = new CreepyCastleMap();
    controller = Controller.getDefaultController(map.getPlayerSpawn());
    controller.startInstance();
  }

  private static int calculatePoints(Player player) {
    var points = player.getCombat().getDamageInflicted() / 100;
    var multiplier = 1;
    if (player.getController().is(CreepyCrawlyController.class)) {
      var rounds = player.getController().as(CreepyCrawlyController.class).getRoundsParticipated();
      rounds = Math.min(rounds, 25);
      multiplier = rounds;
      multiplier = Math.max(1, multiplier / 5);
    }
    return points * multiplier;
  }

  @Override
  public void execute() {
    var timerAdjustment = Math.max(1, getTick());
    time += timerAdjustment;
    lobbyPlayers.forEach(this::sendOverlay);
    if (round == 0) {
      if (time >= FIRST_ROUND_DELAY) {
        moveLobby();
      } else {
        return;
      }
    }
    roundTime += timerAdjustment;
    if (powerSurgeTimer > 0) {
      powerSurgeTimer -= timerAdjustment;
    }
    if (recurrentDamageTimer > 0) {
      recurrentDamageTimer -= timerAdjustment;
    }
    if (fightPlayers.isEmpty()) {
      stop();
      return;
    }
    if (remainingNpcSpawns == 0 && npcs.isEmpty()) {
      startRound();
    }
    fightPlayers.forEach(
        p -> {
          sendOverlay(p);
          if (powerSurgeTimer > 0) {
            p.getCombat()
                .setSpecialAttackAmount(
                    p.getCombat().getSpecialAttackAmount() + POWER_SURGE_RESTORE);
          }
        });
    npcs.removeIf(
        n -> {
          if (!n.isVisible()) {
            gold += n.getCombatDef().getHitpoints().getTotal();
            if (PRandom.randomE(20) == 0) {
              spawnPowerUp(n);
            }
            return true;
          }
          return false;
        });
    if (remainingNpcSpawns > 0 && roundTime > 0 && (roundTime % 5) == 0) {
      spawnNpcs(roundTime == 5);
    }
  }

  @Override
  public void stopHook() {
    var players = new ArrayList<Player>();
    players.addAll(lobbyPlayers);
    players.addAll(fightPlayers);
    players.forEach(this::removePlayer);
    world.removeNpcs(npcs);
    npcs.clear();
  }

  public void addPlayer(Player player) {
    if (lobbyPlayers.contains(player) || fightPlayers.contains(player)) {
      return;
    }
    for (var item : player.getInventory()) {
      if (player.getController().isFood(item.getId())
          || player.getController().isDrink(item.getId())) {
        player.getGameEncoder().sendMessage("Food and drinks can't be taken in.");
        return;
      }
    }
    lobbyPlayers.add(player);
    player.setController(new CreepyCrawlyController(this));
    player.getController().joinInstance(controller);
    player.getMovement().teleport(1372, 9118);
    player.setLargeVisibility();
    player.getCombat().setDamageInflicted(0);
  }

  public void removePlayer(Player player) {
    player.restore();
    GAME_ITEMS.forEach(
        i -> {
          player.getInventory().deleteAll(i);
          player.getEquipment().deleteAll(i);
        });
    if (!lobbyPlayers.contains(player) && !fightPlayers.contains(player)) {
      return;
    }
    lobbyPlayers.remove(player);
    fightPlayers.remove(player);
    var plugin = player.getPlugin(CreepyCrawlyPlugin.class);
    plugin.setPoints(plugin.getPoints() + calculatePoints(player));
    player.getCombat().setDamageInflicted(0);
    if (player.getController().is(CreepyCrawlyController.class)) {
      if (round >= 10
          && player.getController().as(CreepyCrawlyController.class).getRoundsParticipated() >= 5) {
        player.getPlugin(FamiliarPlugin.class).rollPet(ItemId.YOUNGLLEF, 0.05);
      }
    }
    player.getController().stop();
  }

  public PArrayList<Integer> getSupplyChestPlayerIds(MapObject mapObject) {
    var players = supplyChests.get(mapObject);
    if (players == null) {
      supplyChests.put(mapObject, players = new PArrayList<>());
    }
    return players;
  }

  public PArrayList<Integer> getRareChestPlayerIds(MapObject mapObject) {
    var players = rareChests.get(mapObject);
    if (players == null) {
      rareChests.put(mapObject, players = new PArrayList<>());
    }
    return players;
  }

  public void explosivePotion(Player player) {
    player.setAnimation(4069);
    player.setGraphic(1028, 0, 40);
    npcs.forEach(
        n -> {
          if (n.isLocked()) {
            return;
          }
          if (!player.withinDistance(n, 1)) {
            return;
          }
          if (BOSS_NPCS.contains(n.getId())) {
            player.getCombat().increaseDamageInflicted(n.getCombat().getHitpoints() / 10);
            n.getCombat().addHit(n.getCombat().getHitpoints() / 10);
          } else {
            player.getCombat().increaseDamageInflicted(n.getCombat().getHitpoints());
            n.getCombat().addHit(n.getCombat().getHitpoints());
          }
        });
  }

  public void zapperPowerUp() {
    if (zapperPowerUp instanceof MapObject) {
      controller.removeMapObject((MapObject) zapperPowerUp);
    }
    zapperPowerUp = controller.addNpc(new NpcSpawn(zapperPowerUp, NpcId.ZAPPER_16047));
    fightPlayers.forEach(
        p ->
            p.getGameEncoder()
                .sendMessage(
                    "<col=4443FA>A power-up has been activated:</col> <col=ff0000>Zapper</col>"));
  }

  public void powerSurgePowerUp() {
    controller.removeMapObject(powerSurgePowerUp);
    powerSurgeTimer = (int) PTime.secToTick(45);
    fightPlayers.forEach(
        p ->
            p.getGameEncoder()
                .sendMessage(
                    "<col=4443FA>A power-up has been activated:</col> <col=ff0000>Power surge</col>"));
  }

  public void recurrentDamagePowerUp() {
    controller.removeMapObject(recurrentDamagePowerUp);
    recurrentDamageTimer = (int) PTime.secToTick(45);
    fightPlayers.forEach(
        p ->
            p.getGameEncoder()
                .sendMessage(
                    "<col=4443FA>A power-up has been activated:</col> <col=ff0000>Recurrent damage</col>"));
  }

  public void ultimateForcePowerUp() {
    controller.removeMapObject(ultimateForcePowerUp);
    var totalDamage = 0;
    for (Npc npc : npcs) {
      if (npc.isLocked()) {
        continue;
      }
      if (BOSS_NPCS.contains(npc.getId())) {
        totalDamage += npc.getCombat().getHitpoints() / 5;
        npc.getCombat().addHit(npc.getCombat().getHitpoints() / 5);
      } else {
        totalDamage += npc.getCombat().getHitpoints();
        npc.getCombat().addHit(npc.getCombat().getHitpoints());
      }
    }
    for (Player player : fightPlayers) {
      player.getCombat().increaseDamageInflicted(totalDamage);
      player
          .getGameEncoder()
          .sendMessage(
              "<col=4443FA>A power-up has been activated:</col> <col=ff0000>Ultimate force</col>");
    }
  }

  private boolean moveLobby() {
    if (lobbyPlayers.isEmpty()) {
      return false;
    }
    lobbyPlayers.forEach(p -> p.getMovement().teleport(map.getPlayerSpawn()));
    fightPlayers.addAll(lobbyPlayers);
    var newFightPlayers = new PArrayList<>(lobbyPlayers);
    world.addSingleEvent(
        2,
        e -> {
          for (var chest : supplyChests.entrySet()) {
            var mapObject = chest.getKey();
            var players = chest.getValue();
            for (var player : newFightPlayers) {
              if (!players.contains(player.getId())) {
                continue;
              }
              player
                  .getGameEncoder()
                  .sendMapObject(new MapObject(map.getOpenedSupplyChestId(), mapObject));
            }
          }
          for (var chest : rareChests.entrySet()) {
            var mapObject = chest.getKey();
            var players = chest.getValue();
            for (var player : newFightPlayers) {
              if (!players.contains(player.getId())) {
                continue;
              }
              player.getGameEncoder().sendMapObject(new MapObject(ObjectId.CHEST_65010, mapObject));
            }
          }
        });
    lobbyPlayers.clear();
    return true;
  }

  private void startRound() {
    if (round == 0) {
      time = 0;
    }
    moveLobby();
    round++;
    roundTime = 0;
    remainingNpcSpawns = Math.max(1, round * 2);
    fightPlayers.forEach(
        p -> {
          p.getGameEncoder().sendMessage("Round " + round + " will start soon!");
          p.rejuvenate();
          var playerController = p.getController().as(CreepyCrawlyController.class);
          playerController.setRoundsParticipated(playerController.getRoundsParticipated() + 1);
        });
    var areAllRoomsUnlocked = map.areAllRoomsUnlocked();
    if (areAllRoomsUnlocked) {
      var mapObject = PRandom.collectionRandom(supplyChests.keySet());
      fightPlayers.forEach(
          p -> {
            p.getGameEncoder().sendMessage("A supply chest has been reset!");
            p.getGameEncoder().sendMapObject(mapObject);
          });
      supplyChests.remove(mapObject);
    }
    if ((round % 5) == 0) {
      if (areAllRoomsUnlocked && rareChests.size() == map.getRareChestCount()) {
        var mapObject = PRandom.collectionRandom(rareChests.keySet());
        fightPlayers.forEach(
            p -> {
              p.getGameEncoder().sendMessage("A rare chest has been reset!");
              p.getGameEncoder().sendMapObject(mapObject);
            });
        rareChests.remove(mapObject);
      }
    }
  }

  private void spawnNpcs(boolean first) {
    if (first && round >= 10 && (round % 5) == 0) {
      var npc =
          controller.addNpc(new NpcSpawn(64, map.getBossTile(), PRandom.listRandom(BOSS_NPCS)));
      npcs.add(npc);
      var hitpoints = npc.getCombat().getMaxHitpoints();
      if (fightPlayers.size() > 1) {
        hitpoints += hitpoints * 0.15 * (fightPlayers.size() - 1);
      }
      npc.getCombat().setMaxHitpoints(hitpoints);
      npc.getCombat().setHitpoints(hitpoints);
    }
    var tiles = getNpcSpawnTiles();
    var numToSpawn = Math.min(Math.max(1, round / 2), remainingNpcSpawns);
    for (var i = 0; i < numToSpawn; i++) {
      var uncommon = (remainingNpcSpawns % 10) == 0;
      var npcId = PRandom.listRandom(uncommon ? UNCOMMON_NPCS : COMMON_NPCS);
      var npc = controller.addNpc(new NpcSpawn(64, PRandom.listRandom(tiles), npcId));
      npcs.add(npc);
      npc.getMovement().setClipNpcs(!uncommon);
      var hitpoints = npc.getCombat().getMaxHitpoints();
      if (fightPlayers.size() > 1) {
        if (uncommon) {
          hitpoints += hitpoints * 0.5 * (fightPlayers.size() - 1);
        } else {
          hitpoints += hitpoints * 0.25 * (fightPlayers.size() - 1);
        }
      }
      npc.getCombat().setMaxHitpoints(hitpoints);
      npc.getCombat().setHitpoints(hitpoints);
      if (uncommon) {
        npc.getCombat().setMinimumDamageAdditive(round / 10);
        npc.getCombat().setMaximumDamageAdditive(round / 15);
      } else {
        npc.getCombat().setMinimumDamageAdditive(round / 5);
        npc.getCombat().setMaximumDamageAdditive(round / 10);
      }
      remainingNpcSpawns--;
    }
  }

  private List<Tile> getNpcSpawnTiles() {
    var tiles = new ArrayList<Tile>();
    for (var room : map.getRooms()) {
      if (!room.isUnlocked()) {
        continue;
      }
      tiles.addAll(room.getNpcSpawns());
    }
    return tiles;
  }

  private void spawnPowerUp(Tile tile) {
    var type = PRandom.randomI(8);
    switch (type) {
      case 0:
      case 1:
        controller.addMapItem(new Item(ItemId.EXPLOSIVE_POTION_32390), tile);
        break;
      case 2:
      case 3:
        {
          if (zapperPowerUp != null && zapperPowerUp.isVisible()) {
            break;
          }
          if (zapperPowerUp instanceof MapObject) {
            controller.removeMapObject((MapObject) zapperPowerUp);
          }
          zapperPowerUp = new MapObject(ObjectId.COLFF9040ZAPPER, 10, 0, tile);
          controller.addMapObject((MapObject) zapperPowerUp);
          break;
        }
      case 4:
      case 5:
        {
          if (powerSurgePowerUp != null && powerSurgePowerUp.isVisible()) {
            break;
          }
          if (powerSurgeTimer > 0) {
            break;
          }
          controller.removeMapObject(powerSurgePowerUp);
          powerSurgePowerUp = new MapObject(ObjectId.COLFF9040POWER_SURGE, 10, 0, tile);
          controller.addMapObject(powerSurgePowerUp);
          break;
        }
      case 6:
      case 7:
        {
          if (recurrentDamagePowerUp != null && recurrentDamagePowerUp.isVisible()) {
            break;
          }
          if (recurrentDamageTimer > 0) {
            break;
          }
          controller.removeMapObject(recurrentDamagePowerUp);
          recurrentDamagePowerUp = new MapObject(ObjectId.COLFF9040RECURRENT_DAMAGE, 10, 0, tile);
          controller.addMapObject(recurrentDamagePowerUp);
          break;
        }
      case 8:
        {
          if (ultimateForcePowerUp != null && ultimateForcePowerUp.isVisible()) {
            break;
          }
          controller.removeMapObject(ultimateForcePowerUp);
          ultimateForcePowerUp = new MapObject(ObjectId.COLFF9040ULTIMATE_FORCE, 10, 0, tile);
          controller.addMapObject(ultimateForcePowerUp);
          break;
        }
    }
  }

  private void sendOverlay(Player player) {
    if (player.getWidgetManager().getOverlay() != WidgetId.TEXT_OVERLAY_1019) {
      player.getWidgetManager().sendOverlay(WidgetId.TEXT_OVERLAY_1019);
    }
    var playerController = player.getController().as(CreepyCrawlyController.class);
    String text;
    if (round == 0) {
      text = "Preparing Game...<br>Countdown: " + PTime.ticksToDuration(FIRST_ROUND_DELAY - time);
    } else {
      text =
          "Round: "
              + round
              + "<br>Round Time: "
              + PTime.ticksToDuration(roundTime)
              + "<br>Total Time: "
              + PTime.ticksToDuration(time)
              + "<br>Players: "
              + fightPlayers.size()
              + "<br>Creeps: "
              + (npcs.size() + remainingNpcSpawns)
              + "<br>Gold: "
              + PNumber.formatNumber(gold)
              + "<br> Points: "
              + PNumber.formatNumber(calculatePoints(player))
              + "<br>";
      if (playerController.getAbsorption() > 0) {
        text += "Absorption: " + playerController.getAbsorption() + "<br>";
      }
    }
    player.getGameEncoder().sendWidgetText(WidgetId.TEXT_OVERLAY_1019, 0, text);
  }
}
