package com.palidinodh.worldevent.pvptournament.mode;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Loadout;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.clanwars.ClanWarsRule;
import com.palidinodh.playerplugin.clanwars.ClanWarsRuleOption;
import com.palidinodh.worldevent.pvptournament.ModeMap;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
class MiscMode implements Mode {

  private List<SubMode> subModes = new ArrayList<>();

  MiscMode() {
    var mode =
        SubMode.builder()
            .name("Void Range")
            .map(ModeMap.FIVE_BY_FIVE_ROOMS)
            .spellbook(Magic.LUNAR_MAGIC)
            .brewCap(3);
    mode.rules(Mode.buildRules(ClanWarsRule.PRAYER, ClanWarsRuleOption.NO_OVERHEADS));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(42)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.DEATH_RUNE).rune(ItemId.ASTRAL_RUNE).rune(ItemId.EARTH_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out",
            new Item[] {
              new Item(ItemId.VOID_RANGER_HELM),
              new Item(ItemId.AVAS_ASSEMBLER),
              new Item(ItemId.NECKLACE_OF_ANGUISH),
              new Item(ItemId.DRAGON_KNIFE, 8000),
              new Item(ItemId.VOID_KNIGHT_TOP),
              new Item(ItemId.RUNE_KITESHIELD),
              null,
              new Item(ItemId.VOID_KNIGHT_ROBE),
              null,
              new Item(ItemId.VOID_KNIGHT_GLOVES),
              new Item(ItemId.SNAKESKIN_BOOTS),
              null,
              new Item(ItemId.ARCHERS_RING_I),
              null
            },
            new Item[] {
              new Item(ItemId.RANGING_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              null,
              null,
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "+ Heavy Ballista",
            new Item(ItemId.HEAVY_BALLISTA),
            new Item(ItemId.DRAGON_JAVELIN_P_PLUS_PLUS, 8000)));
    mode.loadout(
        new Loadout.Entry(
            "+ Dark Bow",
            new Item(ItemId.DARK_BOW),
            new Item(ItemId.DRAGON_ARROW_P_PLUS_PLUS, 8000)));
    mode.loadout(
        new Loadout.Entry(
            "+ Armadyl Crossbow",
            new Item(ItemId.ARMADYL_CROSSBOW),
            new Item(ItemId.DRAGONSTONE_DRAGON_BOLTS_E, 8000)));
    mode.loadout(new Loadout.Entry("+ Dragon Thrownaxe", new Item(ItemId.DRAGON_THROWNAXE, 8000)));
    subModes.add(mode.build());

    mode =
        SubMode.builder()
            .name("F2P No Arm Main")
            .map(ModeMap.FIVE_BY_FIVE_ROOMS)
            .spellbook(Magic.LUNAR_MAGIC)
            .brewCap(0);
    mode.rules(
        Mode.buildRules(
            ClanWarsRule.PRAYER,
            ClanWarsRuleOption.NO_OVERHEADS,
            ClanWarsRule.IGNORE_FREEZING,
            ClanWarsRuleOption.ALLOWED));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(80)
        .hitpointsLevel(99)
        .prayerLevel(59);
    mode.rune(ItemId.DEATH_RUNE).rune(ItemId.BLOOD_RUNE).rune(ItemId.WATER_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out #1",
            new Item[] {
              new Item(ItemId.RED_HALLOWEEN_MASK),
              new Item(ItemId.BLACK_CAPE),
              new Item(ItemId.STRENGTH_AMULET_T),
              new Item(ItemId.MAPLE_SHORTBOW),
              new Item(ItemId.MONKS_ROBE_TOP),
              null,
              null,
              new Item(ItemId.MONKS_ROBE),
              null,
              new Item(ItemId.GREEN_DHIDE_VAMB),
              new Item(ItemId.LEATHER_BOOTS),
              null,
              null,
              new Item(ItemId.ADAMANT_ARROW, 8000)
            },
            new Item[] {
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.RUNE_2H_SWORD),
              new Item(ItemId.STRENGTH_POTION_4),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Load-out #2",
            new Item[] {
              new Item(ItemId.RED_HALLOWEEN_MASK),
              new Item(ItemId.BLACK_CAPE),
              new Item(ItemId.STRENGTH_AMULET_T),
              new Item(ItemId.RUNE_SCIMITAR),
              new Item(ItemId.MONKS_ROBE_TOP),
              null,
              null,
              new Item(ItemId.MONKS_ROBE),
              null,
              new Item(ItemId.LEATHER_GLOVES),
              new Item(ItemId.LEATHER_BOOTS),
              null,
              null,
              new Item(ItemId.ADAMANT_ARROW, 8000)
            },
            new Item[] {
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.RUNE_2H_SWORD),
              new Item(ItemId.STRENGTH_POTION_4),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH)
            },
            -1));
    mode.loadout(new Loadout.Entry("+ Rune Scimitar", new Item(ItemId.RUNE_SCIMITAR)));
    mode.loadout(new Loadout.Entry("+ Rune Sword", new Item(ItemId.RUNE_SWORD)));
    mode.loadout(new Loadout.Entry("+ Rune Battleaxe", new Item(ItemId.RUNE_BATTLEAXE)));
    mode.loadout(new Loadout.Entry("+ Rune Longsword", new Item(ItemId.RUNE_LONGSWORD)));
    subModes.add(mode.build());

    mode =
        SubMode.builder()
            .name("F2P Pure")
            .map(ModeMap.FIVE_BY_FIVE_ROOMS)
            .spellbook(Magic.LUNAR_MAGIC)
            .brewCap(0);
    mode.rules(
        Mode.buildRules(
            ClanWarsRule.PRAYER,
            ClanWarsRuleOption.NO_OVERHEADS,
            ClanWarsRule.IGNORE_FREEZING,
            ClanWarsRuleOption.ALLOWED));
    mode.attackLevel(40)
        .strengthLevel(80)
        .rangedLevel(80)
        .magicLevel(69)
        .defenceLevel(1)
        .hitpointsLevel(80)
        .prayerLevel(59);
    mode.rune(ItemId.DEATH_RUNE).rune(ItemId.BLOOD_RUNE).rune(ItemId.WATER_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out #1",
            new Item[] {
              new Item(ItemId.GREEN_HALLOWEEN_MASK),
              new Item(ItemId.BLACK_CAPE),
              new Item(ItemId.STRENGTH_AMULET_T),
              new Item(ItemId.RUNE_SCIMITAR),
              new Item(ItemId.MONKS_ROBE_TOP),
              null,
              null,
              new Item(ItemId.GREEN_DHIDE_CHAPS_G),
              null,
              new Item(ItemId.LEATHER_GLOVES),
              new Item(ItemId.LEATHER_BOOTS),
              null,
              null,
              new Item(ItemId.ADAMANT_ARROW, 8000)
            },
            new Item[] {
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.RUNE_2H_SWORD),
              new Item(ItemId.STRENGTH_POTION_4),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Load-out #2",
            new Item[] {
              new Item(ItemId.GREEN_HALLOWEEN_MASK),
              new Item(ItemId.BLACK_CAPE),
              new Item(ItemId.STRENGTH_AMULET_T),
              new Item(ItemId.MAPLE_SHORTBOW),
              new Item(ItemId.MONKS_ROBE_TOP),
              null,
              null,
              new Item(ItemId.GREEN_DHIDE_CHAPS_G),
              null,
              new Item(ItemId.GREEN_DHIDE_VAMB),
              new Item(ItemId.LEATHER_BOOTS),
              null,
              null,
              new Item(ItemId.ADAMANT_ARROW, 8000)
            },
            new Item[] {
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.RUNE_2H_SWORD),
              new Item(ItemId.STRENGTH_POTION_4),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.ANCHOVY_PIZZA),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH),
              new Item(ItemId.SWORDFISH)
            },
            -1));
    mode.loadout(new Loadout.Entry("+ Rune Scimitar", new Item(ItemId.RUNE_SCIMITAR)));
    mode.loadout(new Loadout.Entry("+ Rune Sword", new Item(ItemId.RUNE_SWORD)));
    mode.loadout(new Loadout.Entry("+ Rune Battleaxe", new Item(ItemId.RUNE_BATTLEAXE)));
    mode.loadout(new Loadout.Entry("+ Rune Longsword", new Item(ItemId.RUNE_LONGSWORD)));
    subModes.add(mode.build());
  }
}
