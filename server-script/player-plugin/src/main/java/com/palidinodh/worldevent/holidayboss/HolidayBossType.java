package com.palidinodh.worldevent.holidayboss;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum HolidayBossType {
  HALLOWEEN_1(
      NpcId.FRAGMENT_OF_SEREN_16049,
      64,
      new Tile(3036, 5361, 4),
      new Tile(3038, 5347, 4),
      ItemId.PUMPKIN_TOKEN_32338,
      NpcId.DEATH_16050),
  CHRISTMAS_1(
      NpcId.ANTI_SANTA_16019,
      0,
      new Tile(2269, 4062, 4),
      new Tile(2272, 4045, 4),
      ItemId.SNOWBALL_TOKEN_32339,
      -1);

  private final int bossNpcId;
  private final int bossNpcMoveDistance;
  private final Tile bossNpcTile;
  private final Tile teleportTile;
  private final int itemId;
  private final int shopNpcId;
}
