package com.palidinodh.worldevent.pvptournament.prize;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.util.PNumber;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public class DefaultPrize implements Prize {

  private int bonds;
  private int osgp;

  public DefaultPrize(int bonds, int osgp) {
    this.bonds = bonds;
    this.osgp = osgp;
  }

  @Override
  public List<Item> getItems(int position) {
    List<Item> items = new ArrayList<>();
    switch (position) {
      case 0:
        items.add(new Item(ItemId.COINS, 5_000_000));
        break;
      case 1:
        items.add(new Item(ItemId.COINS, 500_000));
        break;
      case 2:
        items.add(new Item(ItemId.COINS, 250_000));
        break;
    }
    if (Main.getHolidayToken() != -1) {
      items.add(new Item(Main.getHolidayToken(), 10));
    }
    return items;
  }

  @Override
  public String getMessage() {
    var message = "Prizes include 5M";
    if (bonds > 0 && osgp > 0) {
      message += " and 50 bonds or " + PNumber.abbreviateNumber(osgp) + " OSGP";
    } else if (bonds > 0) {
      message += " and 50 bonds";
    } else if (osgp > 0) {
      message += " and " + PNumber.abbreviateNumber(osgp) + " OSGP";
    }
    return message + ".";
  }
}
