package com.palidinodh.worldevent.holidayboss.handler.command;

import com.google.inject.Inject;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.worldevent.holidayboss.HolidayBossEvent;

@ReferenceName("stophb")
class StopHolidayBossCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Inject private HolidayBossEvent event;

  @Override
  public void execute(Player player, String name, String message) {
    event.setType(null);
    player.getGameEncoder().sendMessage("Holiday Boss event stopped.");
  }
}
