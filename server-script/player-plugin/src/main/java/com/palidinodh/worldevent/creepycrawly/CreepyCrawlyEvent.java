package com.palidinodh.worldevent.creepycrawly;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import java.util.HashMap;
import java.util.Map;

public class CreepyCrawlyEvent extends WorldEvent {

  @Inject private transient World world;
  private transient CreepyCrawlyMinigame publicGame;
  private transient Map<String, CreepyCrawlyMinigame> privateGames = new HashMap<>();

  @Override
  public void execute() {
    privateGames.values().removeIf(g -> !g.isRunning());
  }

  @Override
  public boolean useHandlers(Player player) {
    return player.getArea().is("CreepyCrawlyArea");
  }

  public void joinPublicGame(Player player) {
    if (!canEnter(player)) {
      return;
    }
    if (publicGame == null || !publicGame.isRunning()) {
      world.addEvent(publicGame = new CreepyCrawlyMinigame());
    }
    publicGame.addPlayer(player);
  }

  public void joinPrivateGame(Player player, String name) {
    if (!canEnter(player)) {
      return;
    }
    var game = privateGames.get(name);
    if (game == null) {
      player.getGameEncoder().sendMessage("A game with this name doesn't exist.");
      return;
    }
    game.addPlayer(player);
  }

  public void createPrivateGame(Player player, String name) {
    if (!canEnter(player)) {
      return;
    }
    var game = privateGames.get(name);
    if (game != null) {
      player.getGameEncoder().sendMessage("A game with this name already exists.");
      return;
    }
    world.addEvent(game = new CreepyCrawlyMinigame());
    privateGames.put(name, game);
    game.addPlayer(player);
  }

  private boolean canEnter(Player player) {
    for (var item : player.getInventory()) {
      if (player.getController().isFood(item.getId())
          || player.getController().isDrink(item.getId())) {
        player.getGameEncoder().sendMessage("Food and drinks can't be taken in.");
        return false;
      }
    }
    return true;
  }
}
