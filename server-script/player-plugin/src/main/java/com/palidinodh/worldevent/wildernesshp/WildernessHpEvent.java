package com.palidinodh.worldevent.wildernesshp;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.util.PTime;
import lombok.Setter;

public class WildernessHpEvent extends WorldEvent {

  private static final int ADVERTISEMENT_MINUTES = 5;
  private static final int SOON_MINUTES = 5;
  private static final int MAX_WAIT_TIME = (int) PTime.minToTick(15);
  private static final int NPC_ID = NpcId.THE_MIMIC_186_8633;
  private static final String[] TIME = {"1:00", "7:00", "13:00", "19:00"};
  private static final int[] HOURS;
  private static final int[] MINUTES;

  static {
    if (TIME == null || TIME.length == 0) {
      HOURS = null;
      MINUTES = null;
    } else {
      HOURS = new int[TIME.length];
      MINUTES = new int[TIME.length];
      for (var i = 0; i < TIME.length; i++) {
        var data = TIME[i].split(":");
        HOURS[i] = Integer.parseInt(data[0]);
        MINUTES[i] = Integer.parseInt(data[1]);
      }
    }
  }

  @Inject private transient World world;
  @Setter private transient boolean enabled = true;
  private transient boolean announcedStartingSoon;
  private transient Spawn spawn;
  private transient Npc npc;

  public WildernessHpEvent() {
    super(4);
    rotateSpawn();
  }

  @Override
  public Object script(String name, Object... args) {
    boolean isPrimary = isPrimary();
    if (name.equals("world_event_name")) {
      return isPrimary ? "Wilderness HP" : null;
    }
    if (name.equals("world_event_tile")) {
      return isPrimary ? spawn.getTeleportTile() : null;
    }
    if (name.equals("wilderness_hp_message")) {
      return getNextTimeText();
    }
    return null;
  }

  @Override
  public void execute() {
    setTick(4);
    if (!canRun()) {
      return;
    }
    checkNpcState();
    if (npc != null && !npc.isVisible()) {
      world.removeNpc(npc);
      npc = null;
      rotateSpawn();
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    var dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    var nextTime = getNextTime();
    if (nextTime == null) {
      return;
    }
    var remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (remainingMinutes == ADVERTISEMENT_MINUTES) {
      DiscordBot.sendMessage(
          DiscordChannel.ADVERTISEMENTS,
          NpcDefinition.getName(NPC_ID)
              + " will spawn near the "
              + spawn.getLocation()
              + " in "
              + ADVERTISEMENT_MINUTES
              + " minutes! Use the combat room portal to get there quickly.");
      announcedStartingSoon = true;
      setTick(105);
    } else if (remainingMinutes == SOON_MINUTES) {
      world.sendNotice(
          NpcDefinition.getName(NPC_ID)
              + " will spawn near the "
              + spawn.getLocation()
              + " in "
              + SOON_MINUTES
              + " minutes! Use the combat room portal to get there quickly.");
      announcedStartingSoon = true;
      setTick(105);
    } else if (remainingMinutes == 0) {
      startEvent();
      setTick(105);
    }
  }

  private void checkNpcState() {
    if (npc == null) {
      return;
    }
    if (!npc.isVisible()) {
      return;
    }
    if (npc.getTotalTicks() < MAX_WAIT_TIME) {
      return;
    }
    if (npc.getCombat().getHitpoints() != npc.getCombat().getMaxHitpoints()) {
      return;
    }
    npc.setVisible(false);
  }

  private boolean isPrimary() {
    if (!canRun()) {
      return false;
    }
    if (spawn == null) {
      return false;
    }
    return announcedStartingSoon || npc != null;
  }

  private String getNextTimeText() {
    if (npc != null && spawn != null) {
      return spawn.getLocation();
    }
    if (!canRun()) {
      return "N/A";
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  private void startEvent() {
    announcedStartingSoon = false;
    if (!canRun()) {
      return;
    }
    world.removeNpc(npc);
    npc = world.addNpc(new NpcSpawn(spawn.getNpcTile(), NPC_ID));
    if (PTime.getHour24() == WildernessPlugin.HP_EVENT_BONDS_HOUR) {
      world.sendBroadcast(
          NpcDefinition.getName(NPC_ID)
              + " has spawned near the "
              + spawn.getLocation()
              + "! It will a 50 bond bloodier key!");
    } else {
      world.sendBroadcast(
          NpcDefinition.getName(NPC_ID) + " has spawned near the " + spawn.getLocation() + "!");
    }
  }

  private int[] getNextTime() {
    if (!canRun() || npc != null) {
      return null;
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    for (var i = 0; i < HOURS.length; i++) {
      var hour = HOURS[i];
      var minute = MINUTES[i];
      if (currentHour > hour || currentHour == hour && currentMinute > minute) {
        continue;
      }
      return new int[] {hour, minute};
    }
    return new int[] {HOURS[0], MINUTES[0]};
  }

  private void rotateSpawn() {
    if (spawn == null) {
      spawn = PRandom.arrayRandom(Spawn.values());
      return;
    }
    var spawns = Spawn.values();
    for (var i = 0; i < spawns.length; i++) {
      if (spawn != spawns[i]) {
        continue;
      }
      spawn = i + 1 < spawns.length ? spawns[i + 1] : spawns[0];
      break;
    }
  }

  public boolean canRun() {
    if (!enabled) {
      return false;
    }
    if (TIME == null) {
      return false;
    }
    if (TIME.length == 0) {
      return false;
    }
    return world.isPrimary();
  }
}
