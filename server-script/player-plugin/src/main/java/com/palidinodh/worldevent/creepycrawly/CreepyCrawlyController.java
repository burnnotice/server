package com.palidinodh.worldevent.creepycrawly;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills.Drink;
import com.palidinodh.osrscore.model.entity.player.controller.PController;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.util.PNumber;
import lombok.Getter;
import lombok.Setter;

public class CreepyCrawlyController extends PController {

  @Inject private Player player;
  @Getter private CreepyCrawlyMinigame minigame;
  @Getter @Setter private int roundsParticipated;
  private OverloadPotion overloadPotion = new OverloadPotion();
  private AbsorptionPotion absorptionPotion = new AbsorptionPotion();
  @Getter private int absorption;

  public CreepyCrawlyController(CreepyCrawlyMinigame minigame) {
    this.minigame = minigame;
  }

  @Override
  public void startHook() {
    setItemStorageDisabledType(ItemStorageDisabledType.ALL_BUT_RUNE_POUCH);
    setTeleportsDisabled(true);
    setKeepItemsOnDeath(true);
    setExitTile(getFirstTile());
  }

  @Override
  public void stopHook() {
    minigame.removePlayer(player);
  }

  @Override
  public void applyDeadCompleteHook() {
    minigame.removePlayer(player);
  }

  @Override
  public double damageInflictedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceTypes) {
    if (damage > 0 && minigame.getRecurrentDamageTimer() > 0) {
      opponent.getCombat().addHit(new Hit((int) (damage * 0.75)));
      player.getCombat().increaseDamageInflicted((int) (damage * 0.75));
    }
    return damage;
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (absorption > 0) {
      var absorbed = Math.min(damage, absorption);
      damage -= absorbed;
      absorption -= absorbed;
      if (absorption == 0) {
        player.getGameEncoder().sendMessage("<col=ff0000>You have run out of absorption.</col>");
      }
    }
    return damage;
  }

  @Override
  public MapItem addMapItemHook(MapItem mapItem) {
    if (mapItem.getId() == ItemId.VIAL) {
      return null;
    }
    if (!mapItem.getDef().getUntradable()
        && (isFood(mapItem.getId()) || isDrink(mapItem.getId()))) {
      mapItem.setAlwaysAppear();
    }
    return mapItem;
  }

  @Override
  public void logoutHook() {
    minigame.removePlayer(player);
  }

  @SuppressWarnings("ReturnOfInnerClass")
  @Override
  public Drink getDrinkHook(int itemId, Drink drink) {
    if (absorptionPotion.isDose(itemId)) {
      return absorptionPotion;
    }
    if (overloadPotion.isDose(itemId)) {
      return overloadPotion;
    }
    return drink;
  }

  @Override
  public int getExpMultiplier(int id) {
    var multiplier = super.getExpMultiplier(id);
    if (CreepyCrawlyMinigame.GAME_ITEMS.contains(player.getEquipment().getWeaponId())) {
      return multiplier / 2;
    }
    return multiplier;
  }

  private static final class OverloadPotion extends Drink {

    private OverloadPotion() {
      super(
          ItemId.OVERLOAD_4,
          ItemId.OVERLOAD_3,
          ItemId.OVERLOAD_2,
          ItemId.OVERLOAD_1,
          p -> p.getSkills().setOverload(0.15, 5));
    }
  }

  private final class AbsorptionPotion extends Drink {

    private AbsorptionPotion() {
      super(
          ItemId.ABSORPTION_4,
          ItemId.ABSORPTION_3,
          ItemId.ABSORPTION_2,
          ItemId.ABSORPTION_1,
          p -> {
            absorption += 50;
            p.getGameEncoder()
                .sendMessage(
                    "You have " + PNumber.formatNumber(absorption) + " points of absorption.");
          });
    }
  }
}
