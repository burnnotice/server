package com.palidinodh.playermisc;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.playerplugin.bountyhunter.MysteriousEmblem;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.worldevent.wildernesskey.WildernessKeyEvent;

public class DropItem {

  public static void drop(Player player, int slot) {
    var item = player.getInventory().getItem(slot);
    if (item == null) {
      return;
    }
    var itemId = item.getId();
    player.getWidgetManager().removeInteractiveWidgets();
    if (FamiliarPlugin.isPetItem(itemId)) {
      player.getPlugin(FamiliarPlugin.class).summonByItem(itemId);
      return;
    }
    if (MysteriousEmblem.isEmblem(itemId) && player.getArea().inWilderness()) {
      player.getGameEncoder().sendMessage("You can't drop this here.");
      return;
    }
    if (!canDropUntradeable(player, item)) {
      player.getGameEncoder().sendMessage("You can't drop this right now.");
      return;
    }
    if (WildernessPlugin.isActiveBloodyKey(item.getId())) {
      var keyDropTile = player;
      var appearTime = MapItem.ALWAYS_APPEAR;
      var damagedByPlayer = player.getCombat().getPlayerFromHitCount(false);
      if (player.withinVisibilityDistance(damagedByPlayer)) {
        keyDropTile = damagedByPlayer;
        appearTime = MapItem.NORMAL_TIME - 20;
      }
      player
          .getWorld()
          .getWorldEvent(WildernessKeyEvent.class)
          .addMapItem(itemId, keyDropTile, MapItem.NORMAL_TIME, appearTime);
    } else if (item.getId() == ItemId.DARK_ESSENCE_FRAGMENTS) {
      player.getCharges().decreaseDarkEssenceCharges(player.getCharges().getDarkEssenceFragments());
    } else if (player.isUsergroup(UserRank.YOUTUBER)) {
      player.getController().addMapItem(item, player, MapItem.NORMAL_TIME, MapItem.NEVER_APPEAR);
    } else if (player.getController().isInstanced()) {
      player.getController().addMapItem(item, player, MapItem.LONG_TIME, MapItem.NORMAL_APPEAR);
    } else if (player.getArea().inWilderness() && !item.getInfoDef().getUntradable()) {
      if (player.getController().isFood(itemId) || player.getController().isDrink(itemId)) {
        player.getController().addMapItem(item, player, MapItem.NORMAL_TIME, MapItem.NEVER_APPEAR);
      } else {
        player.getController().addMapItem(item, player, MapItem.NORMAL_TIME, MapItem.ALWAYS_APPEAR);
      }
    } else {
      player.getController().addMapItem(item, player, player);
    }
    player.log(PlayerLogType.MAP_ITEM, "dropped " + item.getLogName());
    player.getInventory().deleteItem(itemId, item.getAmount(), slot);
    player.getController().sendMapSound(2739);
  }

  private static boolean canDropUntradeable(Player player, Item item) {
    if (!item.getInfoDef().getUntradable()) {
      return true;
    }
    if (!player.getArea().inWilderness()) {
      return true;
    }
    return !WildernessPlugin.isBloodyKey(item.getId());
  }
}
