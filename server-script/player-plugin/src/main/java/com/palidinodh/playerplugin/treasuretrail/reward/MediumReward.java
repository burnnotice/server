package com.palidinodh.playerplugin.treasuretrail.reward;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import java.util.List;

class MediumReward implements TreasureTrailReward {

  private List<RandomItem> uniqueItems =
      RandomItem.buildList(
          new RandomItem(ItemId.MITHRIL_FULL_HELM_T),
          new RandomItem(ItemId.MITHRIL_PLATEBODY_T),
          new RandomItem(ItemId.MITHRIL_PLATELEGS_T),
          new RandomItem(ItemId.MITHRIL_PLATESKIRT_T),
          new RandomItem(ItemId.MITHRIL_KITESHIELD_T),
          new RandomItem(ItemId.MITHRIL_FULL_HELM_G),
          new RandomItem(ItemId.MITHRIL_PLATEBODY_G),
          new RandomItem(ItemId.MITHRIL_PLATELEGS_G),
          new RandomItem(ItemId.MITHRIL_PLATESKIRT_G),
          new RandomItem(ItemId.MITHRIL_KITESHIELD_G),
          new RandomItem(ItemId.ADAMANT_FULL_HELM_T),
          new RandomItem(ItemId.ADAMANT_PLATEBODY_T),
          new RandomItem(ItemId.ADAMANT_PLATELEGS_T),
          new RandomItem(ItemId.ADAMANT_PLATESKIRT_T),
          new RandomItem(ItemId.ADAMANT_KITESHIELD_T),
          new RandomItem(ItemId.ADAMANT_FULL_HELM_G),
          new RandomItem(ItemId.ADAMANT_PLATEBODY_G),
          new RandomItem(ItemId.ADAMANT_PLATELEGS_G),
          new RandomItem(ItemId.ADAMANT_PLATESKIRT_G),
          new RandomItem(ItemId.ADAMANT_KITESHIELD_G),
          new RandomItem(ItemId.CLIMBING_BOOTS_G),
          new RandomItem(ItemId.SPIKED_MANACLES),
          new RandomItem(ItemId.RANGER_BOOTS),
          new RandomItem(ItemId.HOLY_SANDALS),
          new RandomItem(ItemId.WIZARD_BOOTS),
          new RandomItem(ItemId.BLACK_HEADBAND),
          new RandomItem(ItemId.RED_HEADBAND),
          new RandomItem(ItemId.BROWN_HEADBAND),
          new RandomItem(ItemId.PINK_HEADBAND),
          new RandomItem(ItemId.GREEN_HEADBAND),
          new RandomItem(ItemId.BLUE_HEADBAND),
          new RandomItem(ItemId.WHITE_HEADBAND),
          new RandomItem(ItemId.GOLD_HEADBAND),
          new RandomItem(ItemId.RED_BOATER),
          new RandomItem(ItemId.ORANGE_BOATER),
          new RandomItem(ItemId.GREEN_BOATER),
          new RandomItem(ItemId.BLUE_BOATER),
          new RandomItem(ItemId.BLACK_BOATER),
          new RandomItem(ItemId.PINK_BOATER),
          new RandomItem(ItemId.PURPLE_BOATER),
          new RandomItem(ItemId.WHITE_BOATER),
          new RandomItem(ItemId.GREEN_DHIDE_BODY_T),
          new RandomItem(ItemId.GREEN_DHIDE_CHAPS_T),
          new RandomItem(ItemId.GREEN_DHIDE_BODY_G),
          new RandomItem(ItemId.GREEN_DHIDE_CHAPS_G),
          new RandomItem(ItemId.ADAMANT_HELM_H1),
          new RandomItem(ItemId.ADAMANT_HELM_H2),
          new RandomItem(ItemId.ADAMANT_HELM_H3),
          new RandomItem(ItemId.ADAMANT_HELM_H4),
          new RandomItem(ItemId.ADAMANT_HELM_H5),
          new RandomItem(ItemId.ADAMANT_PLATEBODY_H1),
          new RandomItem(ItemId.ADAMANT_PLATEBODY_H2),
          new RandomItem(ItemId.ADAMANT_PLATEBODY_H3),
          new RandomItem(ItemId.ADAMANT_PLATEBODY_H4),
          new RandomItem(ItemId.ADAMANT_PLATEBODY_H5),
          new RandomItem(ItemId.ADAMANT_SHIELD_H1),
          new RandomItem(ItemId.ADAMANT_SHIELD_H2),
          new RandomItem(ItemId.ADAMANT_SHIELD_H3),
          new RandomItem(ItemId.ADAMANT_SHIELD_H4),
          new RandomItem(ItemId.ADAMANT_SHIELD_H5),
          new RandomItem(ItemId.BLACK_ELEGANT_SHIRT),
          new RandomItem(ItemId.BLACK_ELEGANT_LEGS),
          new RandomItem(ItemId.WHITE_ELEGANT_BLOUSE),
          new RandomItem(ItemId.WHITE_ELEGANT_SKIRT),
          new RandomItem(ItemId.PURPLE_ELEGANT_SHIRT),
          new RandomItem(ItemId.PURPLE_ELEGANT_LEGS),
          new RandomItem(ItemId.PURPLE_ELEGANT_BLOUSE),
          new RandomItem(ItemId.PURPLE_ELEGANT_SKIRT),
          new RandomItem(ItemId.PINK_ELEGANT_SHIRT),
          new RandomItem(ItemId.PINK_ELEGANT_LEGS),
          new RandomItem(ItemId.PINK_ELEGANT_BLOUSE),
          new RandomItem(ItemId.PINK_ELEGANT_SKIRT),
          new RandomItem(ItemId.GOLD_ELEGANT_SHIRT),
          new RandomItem(ItemId.GOLD_ELEGANT_LEGS),
          new RandomItem(ItemId.GOLD_ELEGANT_BLOUSE),
          new RandomItem(ItemId.GOLD_ELEGANT_SKIRT),
          new RandomItem(ItemId.WOLF_MASK),
          new RandomItem(ItemId.WOLF_CLOAK),
          new RandomItem(ItemId.STRENGTH_AMULET_T),
          new RandomItem(ItemId.ADAMANT_CANE),
          new RandomItem(ItemId.GUTHIX_MITRE),
          new RandomItem(ItemId.SARADOMIN_MITRE),
          new RandomItem(ItemId.ZAMORAK_MITRE),
          new RandomItem(ItemId.ANCIENT_MITRE),
          new RandomItem(ItemId.BANDOS_MITRE),
          new RandomItem(ItemId.ARMADYL_MITRE),
          new RandomItem(ItemId.GUTHIX_CLOAK),
          new RandomItem(ItemId.SARADOMIN_CLOAK),
          new RandomItem(ItemId.ZAMORAK_CLOAK),
          new RandomItem(ItemId.ANCIENT_CLOAK),
          new RandomItem(ItemId.BANDOS_CLOAK),
          new RandomItem(ItemId.ARMADYL_CLOAK),
          new RandomItem(ItemId.ANCIENT_STOLE),
          new RandomItem(ItemId.ARMADYL_STOLE),
          new RandomItem(ItemId.BANDOS_STOLE),
          new RandomItem(ItemId.ANCIENT_CROZIER),
          new RandomItem(ItemId.ARMADYL_CROZIER),
          new RandomItem(ItemId.BANDOS_CROZIER),
          new RandomItem(ItemId.CAT_MASK),
          new RandomItem(ItemId.PENGUIN_MASK),
          new RandomItem(ItemId.GNOMISH_FIRELIGHTER),
          new RandomItem(ItemId.CRIER_HAT),
          new RandomItem(ItemId.CRIER_BELL),
          new RandomItem(ItemId.CRIER_COAT),
          new RandomItem(ItemId.LEPRECHAUN_HAT),
          new RandomItem(ItemId.BLACK_LEPRECHAUN_HAT),
          new RandomItem(ItemId.BLACK_UNICORN_MASK),
          new RandomItem(ItemId.WHITE_UNICORN_MASK),
          new RandomItem(ItemId.ARCEUUS_BANNER),
          new RandomItem(ItemId.HOSIDIUS_BANNER),
          new RandomItem(ItemId.LOVAKENGJ_BANNER),
          new RandomItem(ItemId.PISCARILIUS_BANNER),
          new RandomItem(ItemId.SHAYZIEN_BANNER),
          new RandomItem(ItemId.CABBAGE_ROUND_SHIELD),
          new RandomItem(ItemId.YEW_COMP_BOW),
          new RandomItem(ItemId.CLUELESS_SCROLL));

  private List<RandomItem> commonItems =
      RandomItem.combine(
          BASE_COMMON,
          RandomItem.buildList(
              new RandomItem(ItemId.ADAMANT_FULL_HELM),
              new RandomItem(ItemId.ADAMANT_PLATEBODY),
              new RandomItem(ItemId.ADAMANT_PLATELEGS),
              new RandomItem(ItemId.ADAMANT_LONGSWORD),
              new RandomItem(ItemId.ADAMANT_DAGGER),
              new RandomItem(ItemId.ADAMANT_BATTLEAXE),
              new RandomItem(ItemId.ADAMANT_AXE),
              new RandomItem(ItemId.ADAMANT_PICKAXE),
              new RandomItem(ItemId.GREEN_DHIDE_BODY),
              new RandomItem(ItemId.GREEN_DHIDE_CHAPS),
              new RandomItem(ItemId.YEW_SHORTBOW),
              new RandomItem(ItemId.FIRE_BATTLESTAFF),
              new RandomItem(ItemId.YEW_LONGBOW),
              new RandomItem(ItemId.AMULET_OF_POWER),
              new RandomItem(ItemId.AIR_RUNE, 50, 100),
              new RandomItem(ItemId.MIND_RUNE, 50, 100),
              new RandomItem(ItemId.WATER_RUNE, 50, 100),
              new RandomItem(ItemId.EARTH_RUNE, 50, 100),
              new RandomItem(ItemId.FIRE_RUNE, 50, 100),
              new RandomItem(ItemId.CHAOS_RUNE, 10, 20),
              new RandomItem(ItemId.NATURE_RUNE, 10, 20),
              new RandomItem(ItemId.LAW_RUNE, 10, 20),
              new RandomItem(ItemId.DEATH_RUNE, 10, 20),
              new RandomItem(ItemId.LOBSTER_NOTED, 8, 12),
              new RandomItem(ItemId.SWORDFISH_NOTED, 8, 12)));

  @Override
  public Item getCommon(Player player) {
    return RandomItem.getItem(commonItems);
  }

  @Override
  public Item getUnique(Player player) {
    return RandomItem.getItem(uniqueItems);
  }

  @Override
  public int getRandomRolls() {
    return PRandom.randomI(3, 5);
  }

  @Override
  public int getRandomCoinQuantity() {
    return PRandom.randomI(75_000, 150_000);
  }

  @Override
  public int getMasterScrollRate() {
    return 30;
  }
}
