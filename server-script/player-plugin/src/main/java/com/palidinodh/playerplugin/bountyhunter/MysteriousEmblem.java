package com.palidinodh.playerplugin.bountyhunter;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.item.ItemDef;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MysteriousEmblem {
  ANTIQUE_TIER_0(ItemId.ANTIQUE_EMBLEM_TIER_0_32393, 50_000, 0, 1),
  ANTIQUE_TIER_1(ItemId.ANTIQUE_EMBLEM_TIER_1, 50_000, 5_000, 1),
  ANTIQUE_TIER_2(ItemId.ANTIQUE_EMBLEM_TIER_2, 100_000, 10_000, 2),
  ANTIQUE_TIER_3(ItemId.ANTIQUE_EMBLEM_TIER_3, 200_000, 15_000, 3),
  ANTIQUE_TIER_4(ItemId.ANTIQUE_EMBLEM_TIER_4, 400_000, 20_000, 4),
  ANTIQUE_TIER_5(ItemId.ANTIQUE_EMBLEM_TIER_5, 750_000, 25_000, 5),
  ANTIQUE_TIER_6(ItemId.ANTIQUE_EMBLEM_TIER_6, 1_200_000, 30_000, 6),
  ANTIQUE_TIER_7(ItemId.ANTIQUE_EMBLEM_TIER_7, 1_750_000, 35_000, 7),
  ANTIQUE_TIER_8(ItemId.ANTIQUE_EMBLEM_TIER_8, 2_500_000, 40_000, 8),
  ANTIQUE_TIER_9(ItemId.ANTIQUE_EMBLEM_TIER_9, 3_500_000, 45_000, 9),
  ANTIQUE_TIER_10(ItemId.ANTIQUE_EMBLEM_TIER_10, 5_000_000, 50_000, 10);

  private final int itemId;
  private final int coinValue;
  private final int bloodMoneyValue;
  private final int varbitValue;

  public static int getCoinValue(int itemId) {
    for (var emblem : values()) {
      if (itemId == emblem.itemId || ItemDef.getUnnotedId(itemId) == emblem.itemId) {
        return emblem.coinValue;
      }
    }
    return 0;
  }

  public static int getBloodMoneyValue(int itemId) {
    for (var emblem : values()) {
      if (itemId == emblem.itemId || ItemDef.getUnnotedId(itemId) == emblem.itemId) {
        return emblem.bloodMoneyValue;
      }
    }
    return 0;
  }

  public static int getPreviousId(int itemId) {
    var values = values();
    for (int i = 0; i < values.length; i++) {
      var emblem = values[i];
      if (itemId != emblem.itemId) {
        continue;
      }
      return i - 1 >= 0 ? values[i - 1].itemId : itemId;
    }
    return -1;
  }

  public static int getNextId(int itemId) {
    var values = values();
    for (int i = 0; i < values.length; i++) {
      var emblem = values()[i];
      if (itemId != emblem.itemId) {
        continue;
      }
      return i + 1 < values.length ? values[i + 1].itemId : itemId;
    }
    return -1;
  }

  public static boolean isEmblem(int itemId) {
    return getNextId(itemId) != -1;
  }
}
