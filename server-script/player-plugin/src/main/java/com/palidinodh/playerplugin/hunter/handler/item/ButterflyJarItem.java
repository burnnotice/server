package com.palidinodh.playerplugin.hunter.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BLACK_WARLOCK,
  ItemId.SNOWY_KNIGHT,
  ItemId.SAPPHIRE_GLACIALIS,
  ItemId.RUBY_HARVEST
})
class ButterflyJarItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.replace(new Item(ItemId.BUTTERFLY_JAR));
  }
}
