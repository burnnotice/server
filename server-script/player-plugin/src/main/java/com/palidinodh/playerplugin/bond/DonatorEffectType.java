package com.palidinodh.playerplugin.bond;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.util.PString;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum DonatorEffectType {
  FORCE_AGGRESSION(false, null, null),
  PICKUP_BONES(
      true,
      null,
      Arrays.asList(
          ItemId.BONES,
          ItemId.BAT_BONES,
          ItemId.BIG_BONES,
          ItemId.BABYDRAGON_BONES,
          ItemId.DRAGON_BONES,
          ItemId.WOLF_BONES,
          ItemId.SHAIKAHAN_BONES,
          ItemId.JOGRE_BONES,
          ItemId.MONKEY_BONES,
          ItemId.ZOGRE_BONES,
          ItemId.FAYRG_BONES,
          ItemId.RAURG_BONES,
          ItemId.OURG_BONES,
          ItemId.DAGANNOTH_BONES,
          ItemId.WYVERN_BONES,
          ItemId.LAVA_DRAGON_BONES,
          ItemId.SUPERIOR_DRAGON_BONES,
          ItemId.WYRM_BONES,
          ItemId.DRAKE_BONES,
          ItemId.HYDRA_BONES)),
  PICKUP_CLUES(
      true,
      null,
      Arrays.asList(
          ItemId.CLUE_SCROLL_EASY,
          ItemId.CLUE_SCROLL_MEDIUM,
          ItemId.CLUE_SCROLL_HARD,
          ItemId.CLUE_SCROLL_ELITE,
          ItemId.CLUE_SCROLL_MASTER)),
  PICKUP_HERBS(
      true,
      null,
      Arrays.asList(
          ItemId.GRIMY_GUAM_LEAF,
          ItemId.GRIMY_MARRENTILL,
          ItemId.GRIMY_TARROMIN,
          ItemId.GRIMY_HARRALANDER,
          ItemId.GRIMY_RANARR_WEED,
          ItemId.GRIMY_IRIT_LEAF,
          ItemId.GRIMY_AVANTOE,
          ItemId.GRIMY_KWUARM,
          ItemId.GRIMY_CADANTINE,
          ItemId.GRIMY_DWARF_WEED,
          ItemId.GRIMY_TORSTOL,
          ItemId.GRIMY_LANTADYME,
          ItemId.GRIMY_TOADFLAX,
          ItemId.GRIMY_SNAPDRAGON)),
  PICKUP_ENSOULED_HEADS(
      true,
      null,
      Arrays.asList(
          ItemId.ENSOULED_GOBLIN_HEAD,
          ItemId.ENSOULED_GOBLIN_HEAD_13448,
          ItemId.ENSOULED_MONKEY_HEAD,
          ItemId.ENSOULED_MONKEY_HEAD_13451,
          ItemId.ENSOULED_IMP_HEAD,
          ItemId.ENSOULED_IMP_HEAD_13454,
          ItemId.ENSOULED_MINOTAUR_HEAD,
          ItemId.ENSOULED_MINOTAUR_HEAD_13457,
          ItemId.ENSOULED_SCORPION_HEAD,
          ItemId.ENSOULED_SCORPION_HEAD_13460,
          ItemId.ENSOULED_BEAR_HEAD,
          ItemId.ENSOULED_BEAR_HEAD_13463,
          ItemId.ENSOULED_UNICORN_HEAD,
          ItemId.ENSOULED_UNICORN_HEAD_13466,
          ItemId.ENSOULED_DOG_HEAD,
          ItemId.ENSOULED_DOG_HEAD_13469,
          ItemId.ENSOULED_CHAOS_DRUID_HEAD,
          ItemId.ENSOULED_CHAOS_DRUID_HEAD_13472,
          ItemId.ENSOULED_GIANT_HEAD,
          ItemId.ENSOULED_GIANT_HEAD_13475,
          ItemId.ENSOULED_OGRE_HEAD,
          ItemId.ENSOULED_OGRE_HEAD_13478,
          ItemId.ENSOULED_ELF_HEAD,
          ItemId.ENSOULED_ELF_HEAD_13481,
          ItemId.ENSOULED_TROLL_HEAD,
          ItemId.ENSOULED_TROLL_HEAD_13484,
          ItemId.ENSOULED_HORROR_HEAD,
          ItemId.ENSOULED_HORROR_HEAD_13487,
          ItemId.ENSOULED_KALPHITE_HEAD,
          ItemId.ENSOULED_KALPHITE_HEAD_13490,
          ItemId.ENSOULED_DAGANNOTH_HEAD,
          ItemId.ENSOULED_DAGANNOTH_HEAD_13493,
          ItemId.ENSOULED_BLOODVELD_HEAD,
          ItemId.ENSOULED_BLOODVELD_HEAD_13496,
          ItemId.ENSOULED_TZHAAR_HEAD,
          ItemId.ENSOULED_TZHAAR_HEAD_13499,
          ItemId.ENSOULED_DEMON_HEAD,
          ItemId.ENSOULED_DEMON_HEAD_13502,
          ItemId.ENSOULED_AVIANSIE_HEAD,
          ItemId.ENSOULED_AVIANSIE_HEAD_13505,
          ItemId.ENSOULED_ABYSSAL_HEAD,
          ItemId.ENSOULED_ABYSSAL_HEAD_13508,
          ItemId.ENSOULED_DRAGON_HEAD,
          ItemId.ENSOULED_DRAGON_HEAD_13511)),
  PICKUP_TOKKUL(true, null, Arrays.asList(ItemId.TOKKUL)),
  PICKUP_NUMULITE(true, null, Arrays.asList(ItemId.NUMULITE)),
  PICKUP_ANCIENT_SHARDS(true, null, Arrays.asList(ItemId.ANCIENT_SHARD)),
  PICKUP_DARK_TOTEMS(
      true,
      null,
      Arrays.asList(ItemId.DARK_TOTEM_BASE, ItemId.DARK_TOTEM_MIDDLE, ItemId.DARK_TOTEM_TOP)),
  PICKUP_KEYS(
      true,
      null,
      Arrays.asList(
          ItemId.TOOTH_HALF_OF_KEY,
          ItemId.LOOP_HALF_OF_KEY,
          ItemId.BRIMSTONE_KEY,
          ItemId.LARRANS_KEY)),
  NOTE_BONES(true, PICKUP_BONES, null),
  NOTE_ENSOULED_HEADS(true, PICKUP_ENSOULED_HEADS, null);

  private final boolean defaultOn;
  private final DonatorEffectType pickupType;
  private final List<Integer> pickupItemIds;

  public boolean isPickupItemId(int itemId) {
    if (pickupItemIds == null) {
      return false;
    }
    return pickupItemIds.contains(itemId)
        || pickupItemIds.contains(ItemDefinition.getDefinition(itemId).getUnnotedId());
  }

  public String getFormattedName() {
    return PString.formatName(name().toLowerCase().replace('_', ' '));
  }
}
