package com.palidinodh.playerplugin.boss;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.PrayerChild;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.random.PRandom;
import lombok.Getter;
import lombok.Setter;

public class BossPlugin implements PlayerPlugin {

  @Inject private transient Player player;
  @Getter @Setter private transient int theNightmareCurse;
  @Getter @Setter private transient int theNightmareParasite;
  @Setter private transient int theNightmareSpore;
  private transient boolean theNightmareParasitePotion;

  public static Npc getIdleNightmare(Player player) {
    return player
        .getController()
        .getNpc(
            NpcId.THE_NIGHTMARE_814,
            NpcId.THE_NIGHTMARE_814_9426,
            NpcId.THE_NIGHTMARE_814_9427,
            NpcId.THE_NIGHTMARE_814_9428,
            NpcId.THE_NIGHTMARE_814_9429,
            NpcId.THE_NIGHTMARE_814_9430,
            NpcId.THE_NIGHTMARE_814_9431);
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("boss_instance_plugin_cancel")) {
      cancel();
      return Boolean.TRUE;
    }
    if (name.equals("boss_instance_plugin_the_nightmare_spore")) {
      return theNightmareSpore > 0 ? Boolean.TRUE : Boolean.FALSE;
    }
    return null;
  }

  @Override
  public void restore() {
    theNightmareCurse = 0;
    theNightmareParasite = 0;
    theNightmareParasitePotion = false;
  }

  @Override
  public void tick() {
    theNightmareTick();
  }

  @Override
  public boolean widgetHook(int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId == WidgetId.PRAYER
        && theNightmareCurse > 0
        && player.getArea().is("SisterhoodSanctuaryArea")) {
      var child = PrayerChild.get(childId);
      switch (child) {
        case PROTECT_FROM_MAGIC:
          player.getPrayer().deactivate(child);
          player.getPrayer().activate(PrayerChild.PROTECT_FROM_MISSILES);
          return true;
        case PROTECT_FROM_MISSILES:
          player.getPrayer().deactivate(child);
          player.getPrayer().activate(PrayerChild.PROTECT_FROM_MELEE);
          return true;
        case PROTECT_FROM_MELEE:
          player.getPrayer().deactivate(child);
          player.getPrayer().activate(PrayerChild.PROTECT_FROM_MAGIC);
          return true;
        default:
          return false;
      }
    }
    if (widgetId == WidgetId.INVENTORY) {
      if (theNightmareParasite > 0
          && !theNightmareParasitePotion
          && player.getArea().is("SisterhoodSanctuaryArea")) {
        var itemName = ItemDefinition.getLowerCaseName(itemId);
        if (itemName.startsWith("sanfew serum") || itemName.startsWith("relicym's balm")) {
          theNightmareParasitePotion = true;
          player
              .getGameEncoder()
              .sendMessage("<col=005500>The parasite within you has been weakened.</col>");
        }
      }
    }
    return false;
  }

  public void start(int npcId, boolean isPrivate) {
    BossInstance.start(player, npcId, isPrivate);
  }

  public void openBossInstanceDialogue(int npcId) {
    openBossInstanceDialogue(npcId, false);
  }

  public void openBossInstanceDialogue(int npcId, boolean isPrivate) {
    if (isPrivate) {
      player.openDialogue(
          new OptionsDialogue(
              new DialogueOption("Enter Area", (c, s) -> BossInstance.join(player, npcId, false)),
              new DialogueOption(
                  "Start Instance", (c, s) -> BossInstance.start(player, npcId, isPrivate))));
    } else {
      player.openDialogue(
          new OptionsDialogue(
              new DialogueOption("Enter Area", (c, s) -> BossInstance.join(player, npcId, false)),
              new DialogueOption("Join Instance", (c, s) -> BossInstance.join(player, npcId, true)),
              new DialogueOption(
                  "Start Instance", (c, s) -> BossInstance.start(player, npcId, isPrivate))));
    }
  }

  private void cancel() {
    var clanChatUsername = player.getMessaging().getClanChatUsername();
    var playerInstance =
        player.getWorld().getPlayerBossInstance(clanChatUsername, player.getController());
    if (playerInstance == null || !playerInstance.is(BossInstanceController.class)) {
      player.getGameEncoder().sendMessage("There is no boss instance for this Clan Chat.");
      return;
    }
    if (!player.getMessaging().canClanChatEvent()) {
      player
          .getGameEncoder()
          .sendMessage("Your Clan Chat privledges aren't high enough to do that.");
      return;
    }
    playerInstance.as(BossInstanceController.class).expire();
  }

  private void theNightmareTick() {
    if (!player.getArea().is("SisterhoodSanctuaryArea")) {
      return;
    }
    var theNightmare = getIdleNightmare(player);
    if (theNightmare == null || theNightmare.getCombat().isDead()) {
      restore();
      return;
    }
    if (theNightmareCurse > 0) {
      theNightmareCurse--;
      if (theNightmareCurse == 0) {
        player
            .getGameEncoder()
            .sendMessage(
                "<col=005500>You feel the effects of the Nightmare's curse wear off.</col>");
      }
    }
    if (theNightmareParasite > 0) {
      theNightmareParasite--;
      if (theNightmareParasite == 0) {
        player
            .getGameEncoder()
            .sendMessage("<col=ff0000>The parasite bursts out of you, fully grown!</col>");
        if (theNightmareParasitePotion) {
          theNightmare
              .getCombat()
              .addNpc(new NpcSpawn(player, NpcId.PARASITE_57))
              .getCombat()
              .setTarget(theNightmare);
          player.getCombat().addHit(new Hit(5));
        } else {
          var additionalDamage = PRandom.randomI(60);
          var parasite = theNightmare.getCombat().addNpc(new NpcSpawn(player, NpcId.PARASITE_86));
          parasite.getCombat().setTarget(theNightmare);
          parasite.getCombat().setHitpoints(parasite.getCombat().getHitpoints() + additionalDamage);
          parasite.getCombat().setMaxHitpoints(parasite.getCombat().getHitpoints());
          player.getCombat().addHit(new Hit(10));
          player.getCombat().addHit(new Hit(additionalDamage));
        }
        theNightmareParasitePotion = false;
      }
    }
    if (theNightmareSpore > 0) {
      theNightmareSpore--;
      if (theNightmareSpore == 0) {
        player
            .getGameEncoder()
            .sendMessage("<col=005500>The Nightmare's infection has worn off.</col>");
      }
    }
  }
}
