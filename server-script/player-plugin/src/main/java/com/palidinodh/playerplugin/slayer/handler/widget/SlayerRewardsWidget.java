package com.palidinodh.playerplugin.slayer.handler.widget;

import com.palidinodh.cache.definition.osrs.EnumDefinition;
import com.palidinodh.cache.id.EnumId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.SLAYER_REWARDS)
class SlayerRewardsWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    switch (childId) {
      case 8:
        switch (slot) {
          case 4:
            plugin.unlock(SlayerUnlock.NEED_MORE_DARKNESS);
            break;
          case 5:
            plugin.unlock(SlayerUnlock.MALEVOLENT_MASQUERADE);
            break;
          case 7:
            plugin.unlock(SlayerUnlock.BROADER_FLETCHING);
            break;
          case 8:
            plugin.unlock(SlayerUnlock.ANKOU_VERY_MUCH);
            break;
          case 10:
            plugin.unlock(SlayerUnlock.FIRE_AND_DARKNESS);
            break;
          case 11:
            plugin.unlock(SlayerUnlock.PEDAL_TO_THE_METALS);
            break;
          case 13:
            plugin.unlock(SlayerUnlock.AUGMENT_MY_ABBIES);
            break;
          case 14:
            plugin.unlock(SlayerUnlock.ITS_DARK_IN_HERE);
            break;
          case 15:
            plugin.unlock(SlayerUnlock.GREATER_CHALLENGE);
            break;
          case 16:
            plugin.unlock(SlayerUnlock.I_HOPE_YOU_MITH_ME);
            break;
          case 17:
            plugin.unlock(SlayerUnlock.WATCH_THE_BIRDIE);
            break;
          case 18:
            plugin.unlock(SlayerUnlock.HOT_STUFF);
            break;
          case 19:
            plugin.unlock(SlayerUnlock.LIKE_A_BOSS);
            break;
          case 20:
            plugin.unlock(SlayerUnlock.BLEED_ME_DRY);
            break;
          case 21:
            plugin.unlock(SlayerUnlock.SMELL_YA_LATER);
            break;
          case 22:
            plugin.unlock(SlayerUnlock.BIRDS_OF_A_FEATHER);
            break;
          case 23:
            plugin.unlock(SlayerUnlock.I_REALLY_MITH_YOU);
            break;
          case 24:
            plugin.unlock(SlayerUnlock.HORRORIFIC);
            break;
          case 25:
            plugin.unlock(SlayerUnlock.TO_DUST_YOU_SHALL_RETURN);
            break;
          case 26:
            plugin.unlock(SlayerUnlock.WYVERNOTHER_ONE);
            break;
          case 27:
            plugin.unlock(SlayerUnlock.GET_SMASHED);
            break;
          case 28:
            plugin.unlock(SlayerUnlock.NECHS_PLEASE);
            break;
          case 29:
            plugin.unlock(SlayerUnlock.KRACK_ON);
            break;
          case 30:
            plugin.unlock(SlayerUnlock.REPTILE_GOT_RIPPED);
            break;
          case 31:
            plugin.unlock(SlayerUnlock.KING_BLACK_BONNET);
            break;
          case 32:
            plugin.unlock(SlayerUnlock.KALPHITE_KHAT);
            break;
          case 33:
            plugin.unlock(SlayerUnlock.UNHOLY_HELMET);
            break;
          case 34:
            plugin.unlock(SlayerUnlock.SEEING_RED);
            break;
          case 35:
            plugin.unlock(SlayerUnlock.BIGGER_BADDER);
            break;
          case 37:
            plugin.unlock(SlayerUnlock.DULY_NOTED);
            break;
          case 38:
            plugin.unlock(SlayerUnlock.DARK_MANTLE);
            break;
          case 39:
            plugin.unlock(SlayerUnlock.WYVER_NOTHER_TWO);
            break;
          case 40:
            plugin.unlock(SlayerUnlock.ADAMIND_SOME_MORE);
            break;
          case 41:
            plugin.unlock(SlayerUnlock.RUUUUUNE);
            break;
          case 42:
            plugin.unlock(SlayerUnlock.UNDEAD_HEAD);
            break;
          case 44:
            plugin.unlock(SlayerUnlock.DOUBLE_TROUBLE);
            break;
          case 45:
            plugin.unlock(SlayerUnlock.USE_MORE_HEAD);
            break;
          case 46:
            plugin.cancelTask();
            break;
          case 47:
            plugin.blockTask();
            break;
          case 48:
          case 49:
          case 50:
          case 51:
          case 52:
          case 53:
            plugin.unblockTask(slot - 48);
            break;
        }
        break;
      case 23:
        {
          var itemQuantity =
              EnumDefinition.getDefinition(EnumId.SLAYER_REWARDS_ITEM_QUANTITIES)
                  .getIntValue(itemId);
          var itemPrice =
              EnumDefinition.getDefinition(EnumId.SLAYER_REWARDS_ITEM_PRICES).getIntValue(itemId);
          if (itemPrice == 0) {
            player.getGameEncoder().sendMessage("This item can't be purchased.");
          }
          var multiplier = 0;
          switch (option) {
            case 1:
              multiplier = 1;
              break;
            case 2:
              multiplier = 5;
              break;
            case 3:
              multiplier = 10;
              break;
            case 4:
              multiplier = 50;
              break;
          }
          if (multiplier == 0) {
            return;
          }
          plugin.buy(new Item(itemId, itemQuantity * multiplier), itemPrice * multiplier);
          break;
        }
    }
  }
}
