package com.palidinodh.playerplugin.treasuretrail.stage;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.treasuretrail.action.ClueScrollAction;
import com.palidinodh.playerplugin.treasuretrail.action.DigClueScrollActon;
import com.palidinodh.playerplugin.treasuretrail.action.MapObjectClueScrollActon;
import lombok.Getter;

@Getter
enum MapClueStageType implements ClueStage {
  MAP_AL_KHARID_MINE(new DigClueScrollActon(new Tile(3300, 3291)), WidgetId.CLUE_MAP_2),
  MAP_ZULANDRA(new DigClueScrollActon(new Tile(2202, 3062)), WidgetId.CLUE_MAP_6),
  MAP_SOUL_ALTAR(new DigClueScrollActon(new Tile(1815, 3852)), WidgetId.CLUE_MAP_7),
  MAP_FALADOR_CROSSROADS(new DigClueScrollActon(new Tile(2970, 3415)), WidgetId.CLUE_MAP_8),
  MAP_WILDERNESS_1(new DigClueScrollActon(new Tile(3021, 3912)), WidgetId.CLUE_MAP_9),
  MAP_OURANIA_CAVE(new DigClueScrollActon(new Tile(2454, 3230)), WidgetId.CLUE_MAP_13),
  MAP_VARROCK_MINE(new DigClueScrollActon(new Tile(3290, 3374)), WidgetId.CLUE_MAP_17),
  MAP_DRAYNOR_TREES(new DigClueScrollActon(new Tile(3093, 3226)), WidgetId.CLUE_MAP_18),
  MAP_FALADOR_STONES(new DigClueScrollActon(new Tile(3043, 3399)), WidgetId.CLUE_MAP_21),
  MAP_WIZARDS_TOWER(new DigClueScrollActon(new Tile(3110, 3152)), WidgetId.CLUE_MAP_26),
  MAP_WILDERNESS_2(
      new MapObjectClueScrollActon(ObjectId.CRATE_354, new Tile(3026, 3628)), WidgetId.CLUE_MAP_29),
  MAP_RIMMINGTON_CHEMIST(new DigClueScrollActon(new Tile(2924, 3210)), WidgetId.CLUE_MAP_32);

  private final ClueScrollAction action;
  private final int widgetId;

  MapClueStageType(ClueScrollAction action, int widgetId) {
    this.action = action;
    this.widgetId = widgetId;
  }

  @Override
  public int getWidgetId() {
    return widgetId;
  }

  @Override
  public String getText() {
    return null;
  }
}
