package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class AbyssalOrphanPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(ItemId.ABYSSAL_ORPHAN, NpcId.ABYSSAL_ORPHAN, NpcId.ABYSSAL_ORPHAN_5884));
    return builder;
  }
}
