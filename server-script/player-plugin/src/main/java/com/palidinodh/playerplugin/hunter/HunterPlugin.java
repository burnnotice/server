package com.palidinodh.playerplugin.hunter;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.util.PEvent;
import java.util.ArrayDeque;
import java.util.Deque;
import lombok.Getter;

public class HunterPlugin implements PlayerPlugin {

  public static final int TRAP_EXPIRIY = 100;

  @Inject private transient Player player;
  @Getter private transient Deque<TempMapObject> traps = new ArrayDeque<>();

  public static CapturedHunterTrap getCapturedTrap(int objectId) {
    for (var i = 0; i < CapturedHunterTrap.getCapturedTrapEntries().size(); i++) {
      var trap = CapturedHunterTrap.getCapturedTrapEntries().get(i);
      if (trap.getId() != objectId) {
        continue;
      }
      return trap;
    }
    return null;
  }

  public static int getCapturedTrapLevelRequirement(int objectId) {
    var trap = getCapturedTrap(objectId);
    return trap != null ? trap.getLevel() : 1;
  }

  public void removeLostTraps() {
    traps.removeIf(tempTrap -> !tempTrap.isRunning());
  }

  public boolean layTrap(int itemId, MapObject fromMapObject) {
    var maxTraps = 1;
    var nextLevel = 20;
    if (player.getSkills().getLevel(Skills.HUNTER) >= 80) {
      maxTraps = 5;
    } else if (player.getSkills().getLevel(Skills.HUNTER) >= 60) {
      maxTraps = 4;
      nextLevel = 80;
    } else if (player.getSkills().getLevel(Skills.HUNTER) >= 40) {
      maxTraps = 3;
      nextLevel = 60;
    } else if (player.getSkills().getLevel(Skills.HUNTER) >= 20) {
      maxTraps = 2;
      nextLevel = 40;
    }
    if (player.getArea().inWilderness()) {
      maxTraps++;
    }
    Item[] items;
    MapObject[] trap;
    if (itemId == ItemId.BIRD_SNARE) {
      items = new Item[] {new Item(itemId)};
      trap = new MapObject[] {new MapObject(ObjectId.BIRD_SNARE_9345, 10, 0, player)};
    } else if (fromMapObject != null
        && (fromMapObject.getId() == ObjectId.YOUNG_TREE_8732
            || fromMapObject.getId() == ObjectId.YOUNG_TREE_8990
            || fromMapObject.getId() == ObjectId.YOUNG_TREE_8999
            || fromMapObject.getId() == ObjectId.YOUNG_TREE_9000
            || fromMapObject.getId() == ObjectId.YOUNG_TREE_9341)) {
      if (player.getX() != fromMapObject.getX() && player.getY() != fromMapObject.getY()) {
        return false;
      }
      items = new Item[] {new Item(ItemId.SMALL_FISHING_NET), new Item(ItemId.ROPE)};
      var direction = 0;
      if (player.getX() == fromMapObject.getX() && player.getY() < fromMapObject.getY()) {
        direction = 2;
      } else if (player.getX() > fromMapObject.getX() && player.getY() == fromMapObject.getY()) {
        direction = 1;
      } else if (player.getX() < fromMapObject.getX() && player.getY() == fromMapObject.getY()) {
        direction = 3;
      }
      trap =
          new MapObject[] {
            new MapObject(ObjectId.NET_TRAP_9343, 10, direction, player),
            new MapObject(ObjectId.YOUNG_TREE_8989, 10, direction, fromMapObject)
                .setOriginal(fromMapObject)
          };
    } else if (itemId == ItemId.BOX_TRAP) {
      items = new Item[] {new Item(ItemId.BOX_TRAP)};
      trap = new MapObject[] {new MapObject(ObjectId.BOX_TRAP_9380, 10, 0, player)};
    } else {
      return false;
    }
    var plugin = player.getPlugin(HunterPlugin.class);
    plugin.removeLostTraps();
    for (var item : items) {
      if (player.getInventory().getCount(item.getId()) < item.getAmount()) {
        player
            .getGameEncoder()
            .sendMessage("You need " + item.getAmount() + " " + item.getName() + " to do this.");
        return false;
      }
    }
    if (player.getController().hasSolidMapObject(trap[0])) {
      player.getGameEncoder().sendMessage("You can't set a trap here.");
      return false;
    }
    if (plugin.getTraps().size() >= maxTraps) {
      if (nextLevel > 0) {
        player
            .getGameEncoder()
            .sendMessage("You can't set any more traps until level " + nextLevel + ".");
      } else {
        player.getGameEncoder().sendMessage("You can't set any more traps.");
      }
      return false;
    }
    for (var i = 1; i < trap.length; i++) {
      var existing = player.getController().getSolidMapObject(trap[i]);
      if (existing != null && existing.getAttachment() instanceof TempMapObject) {
        player.getGameEncoder().sendMessage("You can't set a trap here.");
        return false;
      }
    }
    var tempTrap = new HunterTrap(player, trap, items);
    if (!tempTrap.isRunning()) {
      return false;
    }
    player.getWorld().addEvent(tempTrap);
    plugin.getTraps().add(tempTrap);
    for (var item : items) {
      player.getInventory().deleteItem(item.getId(), item.getAmount());
    }
    player.lock();
    player.setAnimation(5208);
    player
        .getWorld()
        .addEvent(
            PEvent.singleEvent(
                3,
                e -> {
                  if (player.getController().routeAllow(player.getX() - 1, player.getY())) {
                    player.getMovement().quickRoute(player.getX() - 1, player.getY());
                  } else if (player.getController().routeAllow(player.getX() + 1, player.getY())) {
                    player.getMovement().quickRoute(player.getX() + 1, player.getY());
                  } else if (player.getController().routeAllow(player.getX(), player.getY() - 1)) {
                    player.getMovement().quickRoute(player.getX(), player.getY() - 1);
                  } else if (player.getController().routeAllow(player.getX(), player.getY() + 1)) {
                    player.getMovement().quickRoute(player.getX(), player.getY() + 1);
                  }
                  player.unlock();
                }));
    return true;
  }

  public boolean pickupTrap(MapObject trap) {
    if (!(trap.getAttachment() instanceof TempMapObject)) {
      return false;
    }
    var tempTrap = (TempMapObject) trap.getAttachment();
    if (!(tempTrap.getAttachment() instanceof Integer)) {
      return false;
    }
    var userId = (Integer) tempTrap.getAttachment();
    if (player.getId() != userId) {
      player.getGameEncoder().sendMessage("This trap isn't yours.");
      return false;
    }
    switch (trap.getName().toLowerCase()) {
      case "bird snare":
        {
          player.getInventory().addOrDropItem(ItemId.BIRD_SNARE);
          break;
        }
      case "net trap":
        {
          player.getInventory().addOrDropItem(ItemId.SMALL_FISHING_NET);
          player.getInventory().addOrDropItem(ItemId.ROPE);
          break;
        }
      case "shaking box":
      case "box trap":
        {
          player.getInventory().addOrDropItem(ItemId.BOX_TRAP);
          break;
        }
      default:
        return false;
    }
    tempTrap.setDisableScript(true);
    tempTrap.execute();
    var capturedTrap = getCapturedTrap(trap.getId());
    if (capturedTrap == null) {
      return true;
    }
    var xp = capturedTrap.getExperience();
    if (player.getEquipment().wearingLarupiaOutfit()) {
      xp *= 1.1;
    }
    player.getSkills().addXp(Skills.HUNTER, xp);
    if (capturedTrap.getItems() != null) {
      for (var item : capturedTrap.getItems()) {
        player.getInventory().addOrDropItem(item.getId(), item.getRandomAmount());
      }
    }
    if (trap.getId() == ObjectId.SHAKING_BOX_9382
        || trap.getId() == ObjectId.SHAKING_BOX_9383
        || trap.getId() == ObjectId.SHAKING_BOX) {
      player
          .getPlugin(FamiliarPlugin.class)
          .rollSkillPet(Skills.HUNTER, 131395, ItemId.BABY_CHINCHOMPA_13324);
    }
    return true;
  }

  public boolean canCaptureTrap(int objectId) {
    return player.getSkills().getLevel(Skills.HUNTER) >= getCapturedTrapLevelRequirement(objectId);
  }

  public boolean success(Npc npc, int level) {
    var entry = SkillEntry.builder().level(level).build();
    return !SkillContainer.getBySkillId(Skills.HUNTER)
        .skipActionHook(player, null, npc, null, entry);
  }
}
