package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class ChambersOfXericPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.OLMLET, NpcId.OLMLET, NpcId.OLMLET_7520));
    builder.entry(new Pet.Entry(ItemId.PUPPADILE, NpcId.PUPPADILE, NpcId.PUPPADILE_8201));
    builder.entry(new Pet.Entry(ItemId.TEKTINY, NpcId.TEKTINY, NpcId.TEKTINY_8202));
    builder.entry(new Pet.Entry(ItemId.VANGUARD, NpcId.VANGUARD_8198, NpcId.VANGUARD_8203));
    builder.entry(new Pet.Entry(ItemId.VASA_MINIRIO, NpcId.VASA_MINIRIO, NpcId.VASA_MINIRIO_8204));
    builder.entry(new Pet.Entry(ItemId.VESPINA, NpcId.VESPINA, NpcId.VESPINA_8205));
    return builder;
  }
}
