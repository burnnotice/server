package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class GrotesqueGuardiansPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.NOON, NpcId.NOON, NpcId.NOON_7892));
    builder.entry(new Pet.Entry(ItemId.MIDNIGHT, NpcId.MIDNIGHT, NpcId.MIDNIGHT_7893));
    return builder;
  }
}
