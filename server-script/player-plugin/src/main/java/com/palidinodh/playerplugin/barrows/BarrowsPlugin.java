package com.palidinodh.playerplugin.barrows;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public class BarrowsPlugin implements PlayerPlugin {

  private static final List<RandomItem> REGULAR_LOOT =
      RandomItem.buildList(
          new RandomItem(ItemId.COINS, 2, 774).weight(380),
          new RandomItem(ItemId.CHAOS_RUNE, 250, 750).weight(125),
          new RandomItem(ItemId.DEATH_RUNE, 80, 325).weight(125),
          new RandomItem(ItemId.BLOOD_RUNE, 70, 275).weight(125),
          new RandomItem(ItemId.BOLT_RACK, 300, 1200).weight(125),
          new RandomItem(ItemId.TOOTH_HALF_OF_KEY).weight(6),
          new RandomItem(ItemId.LOOP_HALF_OF_KEY).weight(6),
          new RandomItem(ItemId.DRAGON_MED_HELM).weight(1));
  private static final Tile DUNGEON_ENTER_TILE = new Tile(3551, 9690);
  private static final Tile DUNGEON_EXIT_TILE = new Tile(3565, 3307);
  private static final Tile DUNGEON_SPAWN_TILE = new Tile(3553, 9691);

  @Inject private transient Player player;

  private List<BrotherType> killed = new ArrayList<>();

  @Override
  public void npcKilled(Npc npc) {
    var brother = BrotherType.getByNpcId(npc.getId());
    if (brother == null) {
      return;
    }
    if (killed.contains(brother)) {
      return;
    }
    killed.add(brother);
    setVarbits();
  }

  @Override
  public boolean digHook() {
    for (var brother : BrotherType.values()) {
      if (!brother.getMoundBounds().within(player)) {
        continue;
      }
      player
          .getController()
          .addSingleEvent(1, e -> player.getMovement().teleport(brother.getCryptEnterTile()));
      return true;
    }
    return false;
  }

  public void searchSarcophagus(BrotherType brother) {
    if (player.getController().getTargetNpc(brother.getNpcId()) != null) {
      player
          .getGameEncoder()
          .sendMessage("Something sinister is already outside of this sarcophagus.");
      return;
    }
    var brotherCount = BrotherType.values().length;
    if (killed.contains(brother) && killed.size() < brotherCount) {
      player.getGameEncoder().sendMessage("You search the sarcophagus but find nothing.");
      return;
    }
    var spawnTile = brother.getCryptSpawnTile();
    if (killed.size() >= brotherCount - 1) {
      if (player.getClientHeight() == brother.getCryptEnterTile().getHeight()) {
        player.getMovement().teleport(DUNGEON_ENTER_TILE);
        return;
      }
      spawnTile = DUNGEON_SPAWN_TILE;
    }
    spawnTile = new Tile(spawnTile.getX(), spawnTile.getY(), player.getHeight());
    var npc = player.getController().addNpc(new NpcSpawn(spawnTile, brother.getNpcId()));
    npc.setForceMessage("You dare disturb my rest!");
    npc.getCombat().setTarget(player);
  }

  public void openChest() {
    BrotherType brother = null;
    for (var b : BrotherType.values()) {
      if (killed.contains(b)) {
        continue;
      }
      brother = b;
      break;
    }
    if (brother != null) {
      searchSarcophagus(brother);
      return;
    }
    if (player.getInventory().getRemainingSlots() < killed.size()) {
      player.getInventory().notEnoughSpace();
      return;
    }
    player.getMovement().teleport(DUNGEON_EXIT_TILE);
    player.getCombat().logNPCKill("Barrows chest", -1);
    player
        .getGameEncoder()
        .sendMessage(
            "Your Barrows Chests count is: <col=ff0000>"
                + player.getCombat().getNPCKillCount("Barrows chest")
                + "</col>.");
    for (var k : killed) {
      reward(k.getItemIds());
    }
    var coinQuantity = PRandom.randomI(50_000, 100_000);
    if (player.getGameMode().isIronType()) {
      coinQuantity /= 4;
    }
    if (coinQuantity > 0) {
      player.getInventory().addOrDropItem(ItemId.COINS, coinQuantity);
    }
    killed.clear();
    setVarbits();
  }

  public void reward(List<Integer> barrowsItemIds) {
    var count = player.getCombat().getNPCKillCount("Barrows chest");
    var barrowsId = PRandom.listRandom(barrowsItemIds);
    var rolledItemId = -1;
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(
                1024, ItemId.AMULET_OF_THE_DAMNED_FULL, NpcId.AHRIM_THE_BLIGHTED_98))) {
      player.getCombat().logNPCItem("Barrows chest", ItemId.AMULET_OF_THE_DAMNED_FULL, 1, count);
      player.getInventory().addOrDropItem(ItemId.AMULET_OF_THE_DAMNED_FULL);
      return;
    }
    if (PRandom.inRange(
        1,
        player.getCombat().getDropRateDenominator(102, barrowsId, NpcId.AHRIM_THE_BLIGHTED_98))) {
      player.getCombat().logNPCItem("Barrows chest", barrowsId, 1, count);
      player.getInventory().addOrDropItem(barrowsId);
      return;
    }
    if (PRandom.randomE(800) == 0) {
      player.getInventory().addOrDropItem(ClueScrollType.MASTER.getScrollId());
      return;
    }
    if (PRandom.randomE(200) == 0) {
      player.getInventory().addOrDropItem(ClueScrollType.ELITE.getScrollId());
      return;
    }
    if (PRandom.randomE(150) == 0) {
      player.getInventory().addOrDropItem(ClueScrollType.HARD.getScrollId());
      return;
    }
    if (PRandom.randomE(100) == 0) {
      player.getInventory().addOrDropItem(ClueScrollType.MEDIUM.getScrollId());
      return;
    }
    if (PRandom.randomE(50) == 0) {
      player.getInventory().addOrDropItem(ClueScrollType.EASY.getScrollId());
      return;
    }
    var commonItem = RandomItem.getItem(REGULAR_LOOT);
    commonItem.setAmount((int) (commonItem.getAmount() * player.getDonatorRank().getMultiplier()));
    player.getInventory().addOrDropItem(commonItem);
  }

  public void setVarbits() {
    var percent = 16.7 * killed.size();
    int value = Math.min((int) percent, 100) * 512;
    if (killed.contains(BrotherType.AHRIM)) {
      value += 1;
    }
    if (killed.contains(BrotherType.DHAROK)) {
      value += 2;
    }
    if (killed.contains(BrotherType.GUTHAN)) {
      value += 4;
    }
    if (killed.contains(BrotherType.KARIL)) {
      value += 8;
    }
    if (killed.contains(BrotherType.TORAG)) {
      value += 16;
    }
    if (killed.contains(BrotherType.VERAC)) {
      value += 32;
    }
    player.getGameEncoder().setVarp(453, value);
  }
}
