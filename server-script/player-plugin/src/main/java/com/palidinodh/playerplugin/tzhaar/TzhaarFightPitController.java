package com.palidinodh.playerplugin.tzhaar;

import com.google.inject.Inject;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.controller.PController;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.Getter;
import lombok.Setter;

public class TzhaarFightPitController extends PController {

  @Inject private Player player;
  @Getter @Setter private TzhaarFightPitState state = TzhaarFightPitState.LOBBY;

  @Override
  public void startHook() {
    TzhaarFightPitMinigame.getInstance().addPlayer(player);
    setItemStorageDisabled(true);
    setTeleportsDisabled(true);
    setKeepItemsOnDeath(true);
    setAreaLocked("TzHaarFightPitArea");
    setExitTile(getFirstTile());
    player.getGameEncoder().sendPlayerOption("Attack", 2, false);
    player.getMovement().teleport(new Tile(2399, 5174));
    player.getWidgetManager().sendOverlay(WidgetId.FIGHT_PIT_OVERLAY);
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.FIGHT_PIT_OVERLAY,
            4,
            "Current Champion: " + TzhaarFightPitMinigame.getWinnerUsername());
  }

  @Override
  public void stopHook() {
    TzhaarFightPitMinigame.getInstance().removePlayer(player);
    player.getGameEncoder().sendPlayerOption("null", 2, false);
    player.getWidgetManager().removeOverlay();
    player.restore();
  }

  @Override
  public void applyDeadCompleteHook() {
    stop();
  }

  @Override
  public boolean canAttackPlayer(Player opponent, boolean sendMessage, HitStyleType hitStyleType) {
    if (state != TzhaarFightPitState.FIGHT) {
      return false;
    }
    if (!opponent.getController().is(TzhaarFightPitController.class)) {
      return false;
    }
    var opponentController = opponent.getController().as(TzhaarFightPitController.class);
    return opponentController.getState() == TzhaarFightPitState.FIGHT;
  }
}
