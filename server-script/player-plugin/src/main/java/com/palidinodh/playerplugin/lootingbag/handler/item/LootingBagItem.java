package com.palidinodh.playerplugin.lootingbag.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.lootingbag.LootingBagPlugin;
import com.palidinodh.playerplugin.lootingbag.LootingBagStoreType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.LOOTING_BAG, ItemId.LOOTING_BAG_22586})
class LootingBagItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(LootingBagPlugin.class);
    switch (option.getText()) {
      case "open":
      case "close":
        item.replace(
            new Item(
                item.getId() == ItemId.LOOTING_BAG
                    ? ItemId.LOOTING_BAG_22586
                    : ItemId.LOOTING_BAG));
        break;
      case "check":
        plugin.getItems().displayItemList();
        break;
      case "deposit":
        if (!player.getArea().inWilderness() && !player.getArea().inPvpWorld()) {
          player
              .getGameEncoder()
              .sendMessage("You can't put items in the bag unless you're in the Wilderness.");
          return;
        }
        player.getWidgetManager().sendInventoryOverlay(WidgetId.LOOTING_BAG_DEPOSIT);
        player.getGameEncoder().sendScript(ScriptId.WILDERNESS_LOOTINGBAG_SETUP, 1, "Looting bag");
        player
            .getGameEncoder()
            .sendWidgetSettings(
                WidgetId.LOOTING_BAG_DEPOSIT, 5, 0, player.getInventory().getCapacity() - 1, 1086);
        player.getInventory().setUpdate(true);
        break;
      case "settings":
        player.openDialogue(
            new OptionsDialogue(
                new DialogueOption(
                    "Always Ask",
                    (c, s) -> {
                      plugin.setStoreType(LootingBagStoreType.ASK);
                    }),
                new DialogueOption(
                    "Always Store-1",
                    (c, s) -> {
                      plugin.setStoreType(LootingBagStoreType.STORE_1);
                    }),
                new DialogueOption(
                    "Always Store-5",
                    (c, s) -> {
                      plugin.setStoreType(LootingBagStoreType.STORE_5);
                    }),
                new DialogueOption(
                    "Always Store-All",
                    (c, s) -> {
                      plugin.setStoreType(LootingBagStoreType.STORE_ALL);
                    }),
                new DialogueOption(
                    "Always Store-X",
                    (c, s) -> {
                      plugin.setStoreType(LootingBagStoreType.STORE_X);
                    })));
        break;
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var plugin = player.getPlugin(LootingBagPlugin.class);
    var itemSlot = useItem.getId() == ItemId.LOOTING_BAG ? onItem.getSlot() : useItem.getSlot();
    if (plugin.getStoreType() == LootingBagStoreType.ASK) {
      player.openDialogue(
          new OptionsDialogue(
              new DialogueOption(
                  "Store-1",
                  (c, s) -> {
                    plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_1);
                  }),
              new DialogueOption(
                  "Store-5",
                  (c, s) -> {
                    plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_5);
                  }),
              new DialogueOption(
                  "Store-All",
                  (c, s) -> {
                    plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_ALL);
                  }),
              new DialogueOption(
                  "Store-X",
                  (c, s) -> {
                    plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_X);
                  })));
      return true;
    } else if (plugin.getStoreType() == LootingBagStoreType.STORE_1) {
      plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_1);
    } else if (plugin.getStoreType() == LootingBagStoreType.STORE_5) {
      plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_5);
    } else if (plugin.getStoreType() == LootingBagStoreType.STORE_ALL) {
      plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_ALL);
    } else if (plugin.getStoreType() == LootingBagStoreType.STORE_X) {
      plugin.storeItemFromInventory(itemSlot, LootingBagStoreType.STORE_X);
    }
    return true;
  }
}
