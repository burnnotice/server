package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class GeneralGraardorPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.PET_GENERAL_GRAARDOR,
            NpcId.GENERAL_GRAARDOR_JR_6644,
            NpcId.GENERAL_GRAARDOR_JR));
    return builder;
  }
}
