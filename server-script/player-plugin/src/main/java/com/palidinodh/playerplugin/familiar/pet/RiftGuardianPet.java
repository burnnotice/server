package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class RiftGuardianPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(ItemId.RIFT_GUARDIAN, NpcId.RIFT_GUARDIAN, NpcId.RIFT_GUARDIAN_7354));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20667, NpcId.RIFT_GUARDIAN_7338, NpcId.RIFT_GUARDIAN_7355));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20669, NpcId.RIFT_GUARDIAN_7339, NpcId.RIFT_GUARDIAN_7356));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20671, NpcId.RIFT_GUARDIAN_7340, NpcId.RIFT_GUARDIAN_7357));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20673, NpcId.RIFT_GUARDIAN_7341, NpcId.RIFT_GUARDIAN_7358));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20675, NpcId.RIFT_GUARDIAN_7342, NpcId.RIFT_GUARDIAN_7359));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20677, NpcId.RIFT_GUARDIAN_7343, NpcId.RIFT_GUARDIAN_7360));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20679, NpcId.RIFT_GUARDIAN_7344, NpcId.RIFT_GUARDIAN_7361));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20681, NpcId.RIFT_GUARDIAN_7345, NpcId.RIFT_GUARDIAN_7362));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20683, NpcId.RIFT_GUARDIAN_7346, NpcId.RIFT_GUARDIAN_7363));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20685, NpcId.RIFT_GUARDIAN_7347, NpcId.RIFT_GUARDIAN_7364));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20687, NpcId.RIFT_GUARDIAN_7348, NpcId.RIFT_GUARDIAN_7365));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20689, NpcId.RIFT_GUARDIAN_7349, NpcId.RIFT_GUARDIAN_7366));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_20691, NpcId.RIFT_GUARDIAN_7350, NpcId.RIFT_GUARDIAN_7367));
    builder.entry(
        new Pet.Entry(
            ItemId.RIFT_GUARDIAN_21990, NpcId.RIFT_GUARDIAN_8024, NpcId.RIFT_GUARDIAN_8028));
    return builder;
  }
}
