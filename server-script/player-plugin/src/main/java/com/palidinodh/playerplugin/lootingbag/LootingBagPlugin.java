package com.palidinodh.playerplugin.lootingbag;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.ValueEnteredEvent;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.model.item.ItemList;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import lombok.Getter;
import lombok.Setter;

public class LootingBagPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  private ItemList items;
  @Getter @Setter private LootingBagStoreType storeType = LootingBagStoreType.ASK;

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("looting_bag_items")) {
      return items != null ? getItems() : null;
    }
    return null;
  }

  @Override
  public void login() {
    if (items != null) {
      items.widget(-1, 516);
    }
  }

  @Override
  public boolean approvedPickupMapItemHook(MapItem mapItem) {
    var item = mapItem.getItem();
    if (player.getController().isItemStorageDisabled()) {
      return false;
    }
    if (!player.getInventory().hasItem(ItemId.LOOTING_BAG_22586)) {
      return false;
    }
    if (ItemDef.getStackable(item.getId()) && player.getInventory().hasItem(item.getId())) {
      return false;
    }
    if (!canStoreItem(item.getId(), false)) {
      return false;
    }
    if (!getItems().canAddItem(item)) {
      return false;
    }
    getItems().addItem(item);
    return true;
  }

  public void bankDeposit() {
    if (items == null) {
      return;
    }
    if (player.getController().isItemStorageDisabled()) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    while (!getItems().isEmpty()) {
      var slot = getItems().getFirstUsedSlot();
      if (!player
          .getBank()
          .deposit(getItems(), getItems().getId(slot), slot, getItems().getAmount(slot))) {
        break;
      }
    }
    if (getItems().isEmpty()) {
      items.sendItems();
      items = null;
    }
  }

  public void bankDeposit(int slot, int id, int quantity) {
    if (items == null) {
      return;
    }
    if (player.getController().isItemStorageDisabled()) {
      player.getWidgetManager().removeInteractiveWidgets();
      return;
    }
    var bag = getItems();
    var item = bag.getItem(slot);
    if (item == null || id != item.getId()) {
      return;
    }
    quantity = Math.min(quantity, bag.getCount(item.getId()));
    player.getBank().deposit(bag, item.getId(), slot, quantity);
    if (bag.isEmpty()) {
      items.sendItems();
      items = null;
    }
  }

  public ItemList getItems() {
    if (items == null) {
      items = (ItemList) new ItemList(28).name("looting bag").widget(-1, 516);
    }
    items.player(player);
    return items;
  }

  public void storeItemFromInventory(int itemSlot, LootingBagStoreType storeType) {
    var itemId = player.getInventory().getId(itemSlot);
    var amount = 0;
    if (storeType == LootingBagStoreType.STORE_1) {
      amount = 1;
    } else if (storeType == LootingBagStoreType.STORE_5) {
      amount = 5;
    } else if (storeType == LootingBagStoreType.STORE_ALL) {
      amount =
          ItemDef.getStackOrNote(itemId)
              ? player.getInventory().getAmount(itemSlot)
              : Item.MAX_AMOUNT;
    }
    ValueEnteredEvent.IntegerEvent valueEntered =
        value -> {
          if (!canStoreItem(itemId, true)) {
            return;
          }
          value = Math.min(value, player.getInventory().getCount(itemId));
          value = getItems().canAddAmount(itemId, value);
          if (value == 0) {
            getItems().notEnoughSpace();
            return;
          }
          player.getInventory().deleteItem(itemId, value, itemSlot);
          getItems().addItem(itemId, value);
        };
    if (storeType == LootingBagStoreType.ASK || storeType == LootingBagStoreType.STORE_X) {
      player.getGameEncoder().sendEnterAmount(valueEntered);
    } else {
      valueEntered.execute(amount);
    }
  }

  private boolean canStoreItem(int itemId, boolean sendMessage) {
    if (player.getController().isItemStorageDisabled()) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("Item storage is currently disabled.");
      }
      return false;
    }
    if (!player.getArea().inWilderness() && !player.getArea().inPvpWorld()) {
      if (sendMessage) {
        player
            .getGameEncoder()
            .sendMessage("You can't put items in the bag unless you're in the Wilderness.");
      }
      return false;
    }
    if (ItemDef.getUntradable(itemId)) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can only put tradeable items in the bag.");
      }
      return false;
    }
    if (WildernessPlugin.isBloodyKey(itemId)) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("This key can't be stored in the bag.");
      }
      return false;
    }
    if (itemId == -1) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("Invalid item selected.");
      }
      return false;
    }
    return true;
  }
}
