package com.palidinodh.playerplugin.treasuretrail.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.CLUE_BOTTLE_EASY,
  ItemId.CLUE_GEODE_EASY,
  ItemId.CLUE_NEST_EASY,
  ItemId.CLUE_BOTTLE_MEDIUM,
  ItemId.CLUE_GEODE_MEDIUM,
  ItemId.CLUE_NEST_MEDIUM,
  ItemId.CLUE_BOTTLE_HARD,
  ItemId.CLUE_GEODE_HARD,
  ItemId.CLUE_NEST_HARD,
  ItemId.CLUE_BOTTLE_ELITE,
  ItemId.CLUE_GEODE_ELITE,
  ItemId.CLUE_NEST_ELITE
})
class ContainedClueItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var type = ClueScrollType.getById(item.getId());
    if (type == null) {
      return;
    }
    item.replace(new Item(type.getScrollId()));
    player.getPlugin(TreasureTrailPlugin.class).resetProgress(type);
    switch (item.getId()) {
      case ItemId.CLUE_NEST_EASY:
      case ItemId.CLUE_NEST_MEDIUM:
      case ItemId.CLUE_NEST_HARD:
      case ItemId.CLUE_NEST_ELITE:
        player.getInventory().addOrDropItem(ItemId.BIRD_NEST_5075);
        break;
    }
  }
}
