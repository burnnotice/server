package com.palidinodh.playerplugin.familiar;

import static com.palidinodh.playerplugin.familiar.FamiliarPlugin.INSURANCE_FEE;
import static com.palidinodh.playerplugin.familiar.FamiliarPlugin.RECLAIM_FEE;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.chat.NormalChatDialogue;
import com.palidinodh.util.PNumber;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class InsurePetDialogue {

  private final Player player;
  private final int itemId;
  private FamiliarPlugin plugin;
  private Pet pet;

  public InsurePetDialogue start() {
    plugin = player.getPlugin(FamiliarPlugin.class);
    pet = Pet.getPetByItem(itemId);
    if (pet == null) {
      player.getGameEncoder().sendMessage("Probita only insures pets.");
      return this;
    }
    if (pet.getInsuranceIndex() == -1) {
      player.getGameEncoder().sendMessage("This pet can't be insured.");
      return this;
    }
    if (plugin.getInsured().contains(pet.getName())) {
      player.getGameEncoder().sendMessage("This pet is already insured.");
      return this;
    }
    if (player.getInventory().getCount(ItemId.COINS) < INSURANCE_FEE) {
      player
          .getGameEncoder()
          .sendMessage("You need " + PNumber.formatNumber(INSURANCE_FEE) + " coins to do this.");
      return this;
    }
    player.openDialogue(
        new NormalChatDialogue(
            NpcId.PROBITA,
            "My insurance fee is "
                + PNumber.formatNumber(INSURANCE_FEE)
                + " coins. Once you've paid that, the pet's insured forever, and you can reclaim it here unlimited times for a reclaimation fee of "
                + PNumber.formatNumber(RECLAIM_FEE)
                + " coins.",
            (c, s) -> {
              confirmation();
            }));
    return this;
  }

  private void confirmation() {
    player.openDialogue(
        new OptionsDialogue(
            ItemDefinition.getName(itemId)
                + ": Insure for "
                + PNumber.formatNumber(INSURANCE_FEE)
                + " coins?",
            new DialogueOption(
                "Yes.",
                (c1, s1) -> {
                  completed();
                }),
            new DialogueOption("No.")));
  }

  private void completed() {
    plugin.getInsured().add(pet.getName());
    player.getInventory().deleteItem(ItemId.COINS, INSURANCE_FEE);
    player.openDialogue(
        new MessageDialogue(
            "Your pet is now insured.<br>You can reclaim it from Probita if you ever lose it."));
  }
}
