package com.palidinodh.playerplugin.combat.hook;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.combat.PlayerCombatHitChanceHooks;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.random.PRandom;

class PlayerCombatHitChance implements PlayerCombatHitChanceHooks {

  private static final double ACCURACY_BONUS_MULTIPLIER = 1.08;

  @Override
  public boolean success(
      Player player, Entity entity, HitStyleType hitStyleType, double accuracy, double defence) {
    accuracy = Math.max(1, accuracy);
    defence = Math.max(1, defence);

    if (player.getCombat().isUsingSpecialAttack()
        && hitStyleType == HitStyleType.MELEE
        && ItemDefinition.getLowerCaseName(player.getEquipment().getWeaponId())
            .contains("vesta's longsword")) {
      defence *= 0.25;
    }

    var hitChance = 0.0;
    if (accuracy < defence) {
      hitChance = accuracy / (2 * (defence + 1));
    } else {
      hitChance = 1 - (defence + 2) / (2 * (accuracy + 1));
    }

    return PRandom.nextDouble() <= hitChance;
  }

  @Override
  public int getAccuracy(Player player, HitStyleType hitStyleType, BonusType meleeBonus) {
    var skillId =
        hitStyleType == HitStyleType.RANGED
            ? Skills.RANGED
            : hitStyleType == HitStyleType.MAGIC ? Skills.MAGIC : Skills.ATTACK;
    var level = player.getSkills().getLevel(skillId);
    var xpStyle = player.getCombat().getXPStyle();
    var prayerBoost = player.getPrayer().getAttackBoost(hitStyleType);
    var equipmentBonus = getEquipmentAccuracy(player, hitStyleType, meleeBonus);
    var attackingEntity = player.getCombat().getAttackingEntity();
    var usingSpecialAttack =
        player.getCombat().isUsingSpecialAttack()
            && SpecialAttack.isHitStyleTypeMatch(player, hitStyleType);

    var effectiveLevel = (level * prayerBoost) + 8;
    if (xpStyle == Skills.ATTACK) {
      effectiveLevel += 3;
    } else if (xpStyle == PCombat.SHARED) {
      effectiveLevel += 1;
    }
    if (player.getEquipment().wearingFullVoid(hitStyleType)) {
      effectiveLevel *= hitStyleType == HitStyleType.MAGIC ? 1.45 : 1.1;
    }
    var max = (effectiveLevel * (equipmentBonus + 64.0));

    if (attackingEntity != null && attackingEntity.isNpc()) {
      var isUndead = attackingEntity.asNpc().getCombatDef().isTypeUndead();
      var isSlayerTask = player.getSkills().isAnySlayerTask(attackingEntity.asNpc());
      var neckId = player.getEquipment().getNeckId();
      var headName = ItemDefinition.getLowerCaseName(player.getEquipment().getHeadId());

      if (isUndead
          && (hitStyleType == HitStyleType.MELEE && neckId == ItemId.SALVE_AMULET
              || neckId == ItemId.SALVE_AMULET_I)) {
        max *= 1.15;
      } else if (isUndead
          && (hitStyleType == HitStyleType.MELEE && neckId == ItemId.SALVE_AMULET_E
              || neckId == ItemId.SALVE_AMULET_EI)) {
        max *= 1.2;
      } else if (isSlayerTask
          && (hitStyleType == HitStyleType.MELEE || headName.contains("(i)"))
          && (headName.contains("black mask") || headName.contains("slayer helmet"))) {
        max *= 1.15;
      }
    }

    if (hitStyleType == HitStyleType.MELEE) {
      max *= getMeleeWeaponMultiplier(player);
    } else if (hitStyleType == HitStyleType.RANGED) {
      max *= getRangedWeaponMultiplier(player);
    } else if (hitStyleType == HitStyleType.MAGIC) {
      max *= getMagicWeaponMultiplier(player);
    }

    if (hitStyleType == HitStyleType.MELEE && meleeBonus == BonusType.ATTACK_CRUSH) {
      var inquisitorsEffect = 0;
      if (player.getEquipment().getHeadId() == ItemId.INQUISITORS_GREAT_HELM) {
        inquisitorsEffect += 5;
      }
      if (player.getEquipment().getChestId() == ItemId.INQUISITORS_HAUBERK) {
        inquisitorsEffect += 5;
      }
      if (player.getEquipment().getLegId() == ItemId.INQUISITORS_PLATESKIRT) {
        inquisitorsEffect += 5;
      }
      if (inquisitorsEffect == 15) {
        inquisitorsEffect += 10;
      }
      if (inquisitorsEffect > 0) {
        max *= 1 + (inquisitorsEffect / 1000.0);
      }
    }

    if (usingSpecialAttack) {
      var specialAttack = SpecialAttack.getSpecialAttack(player.getEquipment().getWeaponId());
      if (specialAttack != null) {
        max *= specialAttack.getAccuracyModifier();
      }
    }

    if (ACCURACY_BONUS_MULTIPLIER != 0) {
      max *= ACCURACY_BONUS_MULTIPLIER;
    }

    return (int) max;
  }

  @Override
  public int getDefence(Player player, HitStyleType hitStyleType, BonusType meleeBonus) {
    var level = player.getSkills().getLevel(Skills.DEFENCE);
    var xpStyle = player.getCombat().getXPStyle();
    var prayerBoost = player.getPrayer().getDefenceBoost();
    var equipmentBonus = getEquipmentDefence(player, hitStyleType, meleeBonus);

    var effectiveLevel = (level * prayerBoost) + 8;
    if (xpStyle == Skills.DEFENCE) {
      effectiveLevel += 3;
    } else if (xpStyle == PCombat.SHARED) {
      effectiveLevel += 1;
    }
    if (hitStyleType == HitStyleType.MAGIC) {
      var magicPrayerBoost = player.getPrayer().getAttackBoost(HitStyleType.MAGIC);
      var effectiveMagicLevel = (player.getSkills().getLevel(Skills.MAGIC) * magicPrayerBoost);
      effectiveLevel = (effectiveLevel * 0.3 + effectiveMagicLevel * 0.7);
    }
    var max = effectiveLevel * (equipmentBonus + 64.0);

    return (int) max;
  }

  @Override
  public int getEquipmentAccuracy(Player player, HitStyleType hitStyleType, BonusType meleeBonus) {
    if (hitStyleType == HitStyleType.MELEE) {
      if (meleeBonus == BonusType.ATTACK_STAB) {
        return player.getEquipment().getBonus(BonusType.ATTACK_STAB);
      }
      if (meleeBonus == BonusType.ATTACK_SLASH) {
        return player.getEquipment().getBonus(BonusType.ATTACK_SLASH);
      }
      if (meleeBonus == BonusType.ATTACK_CRUSH) {
        return player.getEquipment().getBonus(BonusType.ATTACK_CRUSH);
      }
      var stabAttack = player.getEquipment().getBonus(BonusType.ATTACK_STAB);
      var slashAttack = player.getEquipment().getBonus(BonusType.ATTACK_SLASH);
      var crushAttack = player.getEquipment().getBonus(BonusType.ATTACK_CRUSH);
      return Math.max(stabAttack, Math.max(slashAttack, crushAttack));
    }
    if (hitStyleType == HitStyleType.RANGED) {
      return player.getEquipment().getBonus(BonusType.ATTACK_RANGED);
    }
    if (hitStyleType == HitStyleType.MAGIC) {
      return player.getEquipment().getBonus(BonusType.ATTACK_MAGIC);
    }
    return 0;
  }

  @Override
  public int getEquipmentDefence(Player player, HitStyleType hitStyleType, BonusType meleeBonus) {
    if (hitStyleType == HitStyleType.MELEE) {
      if (meleeBonus == BonusType.ATTACK_STAB) {
        return player.getEquipment().getBonus(BonusType.DEFENCE_STAB);
      }
      if (meleeBonus == BonusType.ATTACK_SLASH) {
        return player.getEquipment().getBonus(BonusType.DEFENCE_SLASH);
      }
      if (meleeBonus == BonusType.ATTACK_CRUSH) {
        return player.getEquipment().getBonus(BonusType.DEFENCE_CRUSH);
      }
      var stabDefence = player.getEquipment().getBonus(BonusType.DEFENCE_STAB);
      var slashDefence = player.getEquipment().getBonus(BonusType.DEFENCE_SLASH);
      var crushDefence = player.getEquipment().getBonus(BonusType.DEFENCE_CRUSH);
      return Math.max(stabDefence, Math.max(slashDefence, crushDefence));
    }
    if (hitStyleType == HitStyleType.RANGED) {
      return player.getEquipment().getBonus(BonusType.DEFENCE_RANGED);
    }
    if (hitStyleType == HitStyleType.MAGIC) {
      return player.getEquipment().getBonus(BonusType.DEFENCE_MAGIC);
    }
    return 0;
  }

  public double getMeleeWeaponMultiplier(Player player) {
    var weaponId = player.getEquipment().getWeaponId();
    var attackingEntity = player.getCombat().getAttackingEntity();

    if (player.getEquipment().wearingObsidianArmourSetup()) {
      return 1.1;
    }
    if (attackingEntity != null && attackingEntity.isNpc()) {
      var npc = attackingEntity.asNpc();

      if (npc.getCombatDef().isTypeDemon() && weaponId == ItemId.ARCLIGHT) {
        return 1.7;
      }
      if (npc.getCombatDef().isTypeDragon() && weaponId == ItemId.DRAGON_HUNTER_LANCE) {
        return 1.2;
      }
      if (weaponId == ItemId.VIGGORAS_CHAINMACE
          && player.getEquipment().getItem(Equipment.Slot.WEAPON).getCharges() > 1000
          && player.getArea().inWilderness()) {
        return 1.5;
      }
    }
    return 1;
  }

  public double getRangedWeaponMultiplier(Player player) {
    var weaponId = player.getEquipment().getWeaponId();
    var attackingEntity = player.getCombat().getAttackingEntity();

    if (attackingEntity != null) {
      if (weaponId == ItemId.TWISTED_BOW) {
        var oppLevel = 1;
        if (attackingEntity.isNpc()) {
          oppLevel = attackingEntity.asNpc().getCombatDef().getStats().getMagicLevel();
        } else if (attackingEntity.isPlayer()) {
          var oppMagicLevel = attackingEntity.asPlayer().getSkills().getLevel(Skills.MAGIC);
          var oppMagicBonus =
              getEquipmentAccuracy(attackingEntity.asPlayer(), HitStyleType.MAGIC, null);
          oppLevel = Math.max(oppMagicLevel, oppMagicBonus);
        }
        oppLevel = Math.min(oppLevel, 140);
        return (140.0
                + (3.0 * oppLevel - 10.0) / 100.0
                - StrictMath.pow(0.3 * oppLevel - 100.0, 2.0) / 100.0)
            / 100.0;
      }
    }
    if (attackingEntity != null && attackingEntity.isNpc()) {
      var npc = attackingEntity.asNpc();

      if (npc.getCombatDef().isTypeDragon() && weaponId == ItemId.DRAGON_HUNTER_CROSSBOW) {
        return 1.3;
      }
      if (weaponId == ItemId.CRAWS_BOW
          && player.getEquipment().getItem(Equipment.Slot.WEAPON).getCharges() > 1000
          && player.getArea().inWilderness()) {
        return 1.5;
      }
    }
    return 1;
  }

  public double getMagicWeaponMultiplier(Player player) {
    var weaponId = player.getEquipment().getWeaponId();
    var attackingEntity = player.getCombat().getAttackingEntity();
    var spell = player.getMagic().getActiveSpell();
    var spellName = spell != null ? spell.getName() : "none";

    if ((weaponId == ItemId.SMOKE_BATTLESTAFF || weaponId == ItemId.MYSTIC_SMOKE_STAFF)
        && player.getMagic().getSpellbook() == Magic.STANDARD_MAGIC) {
      return 1.1;
    }
    if (weaponId == ItemId.ZURIELS_STAFF_CHARGED_32257
        && player.getMagic().getSpellbook() == Magic.ANCIENT_MAGIC
        && spellName.startsWith("ice")) {
      return 1.1;
    }
    if (attackingEntity != null && attackingEntity.isNpc()) {
      if (weaponId == ItemId.THAMMARONS_SCEPTRE
          && player.getEquipment().getItem(Equipment.Slot.WEAPON).getCharges() > 1000
          && player.getArea().inWilderness()) {
        return 2;
      }
    }
    return 1;
  }
}
