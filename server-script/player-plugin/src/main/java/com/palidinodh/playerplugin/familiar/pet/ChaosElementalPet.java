package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class ChaosElementalPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.PET_CHAOS_ELEMENTAL, NpcId.CHAOS_ELEMENTAL_JR_5907, NpcId.CHAOS_ELEMENTAL_JR));
    return builder;
  }
}
