package com.palidinodh.playerplugin.familiar.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.PET_INSURANCE)
class WildernessWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (childId) {
      case 14:
        player.getPlugin(FamiliarPlugin.class).reclaimPet(itemId);
        break;
    }
  }
}
