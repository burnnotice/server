package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class RockGolemPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.ROCK_GOLEM, NpcId.ROCK_GOLEM_7439, NpcId.ROCK_GOLEM_7451));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21187, NpcId.ROCK_GOLEM_7440, NpcId.ROCK_GOLEM_7452));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21188, NpcId.ROCK_GOLEM_7441, NpcId.ROCK_GOLEM_7453));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21189, NpcId.ROCK_GOLEM_7442, NpcId.ROCK_GOLEM_7454));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21190, NpcId.ROCK_GOLEM_7443, NpcId.ROCK_GOLEM_7455));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21191, NpcId.ROCK_GOLEM_7444, NpcId.ROCK_GOLEM_7642));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21192, NpcId.ROCK_GOLEM_7445, NpcId.ROCK_GOLEM_7643));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21193, NpcId.ROCK_GOLEM_7446, NpcId.ROCK_GOLEM_7644));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21194, NpcId.ROCK_GOLEM_7447, NpcId.ROCK_GOLEM_7645));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21195, NpcId.ROCK_GOLEM_7448, NpcId.ROCK_GOLEM_7646));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21196, NpcId.ROCK_GOLEM_7449, NpcId.ROCK_GOLEM_7647));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21197, NpcId.ROCK_GOLEM_7450, NpcId.ROCK_GOLEM_7648));
    builder.entry(new Pet.Entry(ItemId.ROCK_GOLEM_21340, NpcId.ROCK_GOLEM, NpcId.ROCK_GOLEM_7711));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21358, NpcId.ROCK_GOLEM_7736, NpcId.ROCK_GOLEM_7739));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21359, NpcId.ROCK_GOLEM_7737, NpcId.ROCK_GOLEM_7740));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21360, NpcId.ROCK_GOLEM_7738, NpcId.ROCK_GOLEM_7741));
    return builder;
  }
}
