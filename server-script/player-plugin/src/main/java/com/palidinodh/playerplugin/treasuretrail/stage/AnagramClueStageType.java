package com.palidinodh.playerplugin.treasuretrail.stage;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.playerplugin.treasuretrail.action.ClueScrollAction;
import com.palidinodh.playerplugin.treasuretrail.action.NpcClueScrollActon;
import lombok.Getter;

@Getter
enum AnagramClueStageType implements ClueStage {
  ANAGRAM_BARAEK(
      new NpcClueScrollActon(
          NpcId.BARAEK,
          "VarrockArea",
          new OptionsDialogue(
              "How many stalls are there in Varrock Square?",
              DialogueOption.toOptions("2", "3", "4", "<answer>5", "6"))),
      "A BAKER"),
  ANAGRAM_CAPTAIN_TOBIAS(
      new NpcClueScrollActon(
          NpcId.CAPTAIN_TOBIAS,
          "PortSarimArea",
          new OptionsDialogue(
              "How many ships are there docked at Port Sarim currently?",
              DialogueOption.toOptions("2", "3", "4", "5", "<answer>6"))),
      "A BASIC ANTI POT"),
  ANAGRAM_ZENESHA(new NpcClueScrollActon(NpcId.ZENESHA, "EastArdougneArea"), "A ZEN SHE"),
  ANAGRAM_CAM_THE_CAMEL(
      new NpcClueScrollActon(NpcId.CAM_THE_CAMEL, "AlKharidArea"), "ACE MATCH ELM"),
  ANAGRAM_CHARLIE_THE_TRAMP(
      new NpcClueScrollActon(
          NpcId.CHARLIE_THE_TRAMP,
          "VarrockArea",
          new OptionsDialogue(
              "How many coins would I have if I had 0 coins and attempted to buy 3 loaves of bread?",
              DialogueOption.toOptions("<answer>0", "1", "2", "3", "4"))),
      "ARMCHAIR THE PELT"),
  ANAGRAM_FATHER_AERECK(
      new NpcClueScrollActon(
          NpcId.FATHER_AERECK,
          "LumbridgeArea",
          new OptionsDialogue(
              "How many gravestones are in the church graveyard?",
              DialogueOption.toOptions(
                  "16 or 17", "17 or 18", "18 or 19", "<answer>19 or 20", "20 or 21"))),
      "AREA CHEF TREK"),
  ANAGRAM_LUMBRIDGE_GUIDE(new NpcClueScrollActon(NpcId.GUIDE, "LumbridgeArea"), "BLUE GRIM GUIDED"),
  ANAGRAM_MADAME_CALDARIUM(
      new NpcClueScrollActon(
          NpcId.MADAME_CALDARIUM,
          "CorsairCoveArea",
          new OptionsDialogue(
              "What is 3(5-3)?", DialogueOption.toOptions("5", "<answer>6", "7", "8", "9"))),
      "CALAMARI MADE MUD"),
  ANAGRAM_ZUL_GWENWYNIG(
      new NpcClueScrollActon(NpcId.PRIESTESS_ZUL_GWENWYNIG_2033, "ZulandraArea"), "CAR IF ICES"),
  ANAGRAM_WILDERNESS_EDWARD(new NpcClueScrollActon(NpcId.EDWARD, "WildernessArea"), "DED WAR"),
  ANAGRAM_MAGE_OF_ZAMORAK(
      new NpcClueScrollActon(
          NpcId.MAGE_OF_ZAMORAK_7425,
          "AbyssArea",
          new OptionsDialogue(
              "How many rifts are found here in the abyss?",
              DialogueOption.toOptions("11", "12", "<answer>13", "14", "15"))),
      "DEKAGRAM"),
  ANAGRAM_DOOMSAYER(
      new NpcClueScrollActon(
          NpcId.DOOMSAYER,
          "LumbridgeArea",
          new OptionsDialogue(
              "What is 40 divided by 1/2 plus 15?",
              DialogueOption.toOptions("75", "80", "85", "90", "<answer>95"))),
      "DO SAY MORE"),
  ANAGRAM_WILDERNESS_MANDRITH(
      new NpcClueScrollActon(NpcId.MANDRITH, "WildernessArea"), "DIM THARN"),
  ANAGRAM_WILDERNESS_MANDRITH_2(
      new NpcClueScrollActon(
          NpcId.MANDRITH,
          "WildernessArea",
          new OptionsDialogue(
              "How many scorpions live under the pit?",
              DialogueOption.toOptions("<answer>28", "29", "30", "31", "32"))),
      "DR HITMAN"),
  ANAGRAM_BRUNDT_THE_CHIEFTAIN(
      new NpcClueScrollActon(
          NpcId.BRUNDT_THE_CHIEFTAIN,
          "RellekkaArea",
          new OptionsDialogue(
              "How many people are waiting for the next bard to perform?",
              DialogueOption.toOptions("2", "<answer>4", "6", "8", "10"))),
      "DT RUN B"),
  ANAGRAM_LOWE(new NpcClueScrollActon(NpcId.LOWE, "VarrockArea"), "EL OW"),
  ANAGRAM_KAYLEE(
      new NpcClueScrollActon(
          NpcId.KAYLEE,
          "FaladorArea",
          new OptionsDialogue(
              "How many chairs are there in the Rising Sun?",
              DialogueOption.toOptions("<answer>18", "19", "20", "21", "22"))),
      "LEAKEY"),
  ANAGRAM_COOK(
      new NpcClueScrollActon(
          NpcId.COOK_4626,
          "LumbridgeArea",
          new OptionsDialogue(
              "How many cannons does Lumbridge Castle have?",
              DialogueOption.toOptions("5", "6", "7", "8", "<answer>9"))),
      "OK CO"),
  ANAGRAM_PROFESSOR_ONGLEWIP(
      new NpcClueScrollActon(NpcId.PROFESSOR_ONGLEWIP, "WizardsTowerArea"), "PROFS LOSE WRONG PIE"),
  ANAGRAM_KARIM(
      new NpcClueScrollActon(
          NpcId.KARIM,
          "AlKharidArea",
          new OptionsDialogue(
              "I have 16 kebabs, I eat one myself and share the rest equally between 3 friends. How many do they have each?",
              DialogueOption.toOptions("3", "4", "<answer>5", "6", "7"))),
      "R AK MI"),
  ANAGRAM_TRADER_STAN(new NpcClueScrollActon(NpcId.TRADER_STAN, "PortSarimArea"), "RED ART TANS"),
  ANAGRAM_WISE_OLD_MAN(
      new NpcClueScrollActon(
          NpcId.WISE_OLD_MAN,
          "DraynorVillageArea",
          new OptionsDialogue(
              "How many bookcases are in the Wise Old Man's house?",
              DialogueOption.toOptions("22", "24", "26", "<answer>28", "30"))),
      "SLIDE WOMAN"),
  ANAGRAM_TRADER_HANS(new NpcClueScrollActon(NpcId.HANS, "LumbridgeArea"), "SNAH"),
  ANAGRAM_LISSE_ISAAKSON(
      new NpcClueScrollActon(
          NpcId.LISSE_ISAAKSON,
          "NeitiznotArea",
          new OptionsDialogue(
              "How many arctic logs are required to make a large fremennik round shield?",
              DialogueOption.toOptions("1", "<answer>2", "3", "4", "5"))),
      "SNAKES SO I SAIL"),
  ANAGRAM_HICKTON(
      new NpcClueScrollActon(
          NpcId.HICKTON,
          "CatherbyArea",
          new OptionsDialogue(
              "How many ranges are there in Catherby?",
              DialogueOption.toOptions("<answer>2", "3", "4", "5", "6"))),
      "THICKNO"),
  ANAGRAM_SIGLI_THE_HUNTSMAN(
      new NpcClueScrollActon(
          NpcId.SIGLI_THE_HUNTSMAN,
          "RellekkaArea",
          new OptionsDialogue(
              "What is the combined slayer requirement of every monster in the slayer cave?",
              DialogueOption.toOptions("12", "52", "102", "202", "<answer>302"))),
      "UNLEASH NIGHT MIST"),
  ANAGRAM_YSGAWYN(new NpcClueScrollActon(NpcId.YSGAWYN, "LletyaArea"), "YAWNS GY"),
  ANAGRAM_RANAEL(new NpcClueScrollActon(NpcId.RANAEL, "AlKharidArea"), "AN EARL"),
  ANAGRAM_APOTHECARY(new NpcClueScrollActon(NpcId.APOTHECARY, "VarrockArea"), "CARPET AHOY"),
  ANAGRAM_BRIAN(new NpcClueScrollActon(NpcId.BRIAN, "PortSarimArea"), "IN BAR"),
  ANAGRAM_VERONICA(new NpcClueScrollActon(NpcId.VERONICA, "DraynorManorArea"), "RAIN COVE"),
  ANAGRAM_GERTRUDE(new NpcClueScrollActon(NpcId.GERTRUDE, "VarrockArea"), "RUG DETER"),
  ANAGRAM_HAIRDRESSER(new NpcClueScrollActon(NpcId.HAIRDRESSER, "FaladorArea"), "SIR SHARE RED"),
  ANAGRAM_FORTUNATO(new NpcClueScrollActon(NpcId.FORTUNATO, "DraynorVillageArea"), "TAUNT ROOF");

  private static final String ANAGRAM_TEXT = "This anagram reveals who to speak to next: ";
  private final ClueScrollAction action;
  private final String text;

  AnagramClueStageType(ClueScrollAction action, String text) {
    this.action = action;
    this.text = ANAGRAM_TEXT + text;
  }

  @Override
  public int getWidgetId() {
    return -1;
  }
}
