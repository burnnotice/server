package com.palidinodh.playerplugin.gravestone.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.gravestone.GravestonePlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.GRAVESTONE)
class GravestoneWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(GravestonePlugin.class);
    switch (childId) {
      case 3:
        player.getWidgetManager().removeInteractiveWidgets();
        break;
      case 6:
      case 13:
        plugin.takeGravestoneItem(slot, itemId);
        break;
      case 8:
        plugin.takeFreeGravestoneItems();
        break;
      case 15:
        plugin.unlockOrTakeFeeGravestoneItems();
        break;
    }
  }

  @Override
  public boolean widgetOnWidget(
      Player player,
      int useWidgetId,
      int useChildId,
      int onWidgetId,
      int onChildId,
      int useSlot,
      int useItemId,
      int onSlot,
      int onItemId) {
    if (useWidgetId == WidgetId.GRAVESTONE && onWidgetId == WidgetId.GRAVESTONE) {
      if (useChildId == 13 && onChildId == 17) {
        player.getPlugin(GravestonePlugin.class).incinerateGravestoneItem(useSlot, onItemId);
      }
      return true;
    }
    return false;
  }
}
