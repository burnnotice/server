package com.palidinodh.playerplugin.gravestone;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.VarpId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.WidgetManager;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemList;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PTime;
import lombok.Getter;
import lombok.Setter;

public class GravestonePlugin implements PlayerPlugin {

  @Inject private transient Player player;
  private transient Npc grave;

  @Getter private ItemList items = new ItemList();
  @Getter @Setter private int countdown;
  @Getter private Tile tile = new Tile();
  private boolean unlocked;
  private ItemList deathsRetrieval = new ItemList();
  private int deathsCoffer;

  @Override
  public void login() {
    items.player(player).widget(-1, 525).capacity(120);
    deathsRetrieval.player(player).widget(-1, 636).capacity(120);
    spawnGrave();
  }

  @Override
  public void logout() {
    player.getController().removeNpc(grave);
  }

  @Override
  public void tick() {
    if (countdown > 0) {
      countdown--;
      sendGravestoneCountdown();
      if (countdown == 0) {
        moveGravestoneItems();
      }
    }
  }

  @Override
  public boolean npcOptionHook(int option, Npc npc) {
    if (npc.getId() != NpcId.GRAVE) {
      return false;
    }
    openGravestone();
    return true;
  }

  public void openGravestone() {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.GRAVESTONE);
    sortGravestone();
    sendGravestonePrices();
    player
        .getGameEncoder()
        .sendWidgetSettings(
            WidgetId.GRAVESTONE, 6, 0, items.getCapacity() + 1, WidgetSetting.OPTION_0);
    player
        .getGameEncoder()
        .sendWidgetSettings(
            WidgetId.GRAVESTONE,
            13,
            0,
            items.getCapacity() + 1,
            WidgetSetting.OPTION_0,
            WidgetSetting.DEPTH_1);
  }

  public void openDeathsRetrieval() {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.DEATHS_OFFICE_ITEM_RETRIEVAL);
    deathsRetrieval.sendItems();
    player
        .getGameEncoder()
        .sendWidgetSettings(
            WidgetId.DEATHS_OFFICE_ITEM_RETRIEVAL,
            3,
            0,
            items.getCapacity() + 1,
            WidgetSetting.OPTION_0);
    player.getGameEncoder().setVarp(VarpId.DEATHS_RETRIEVAL_ITEM_SLOT, -1);
    player.getGameEncoder().setVarp(VarpId.DEATHS_RETRIEVAL_COFFER, deathsCoffer);
  }

  public boolean takeGravestoneItem(int slot, int itemId) {
    if (slot < 0 || slot >= items.size() || items.getId(slot) != itemId) {
      return false;
    }
    var item = items.getItem(slot);
    var indexFree = player.getAttributeInt("grave_free_index") - 1;
    if ((indexFree == -1 || slot < indexFree) && !unlocked) {
      player
          .getGameEncoder()
          .sendMessage("You need to unlock your items before you can take this.");
      return false;
    }
    if (!player.getInventory().canAddItem(item)) {
      player.getInventory().notEnoughSpace();
      return false;
    }
    player.getInventory().addItem(item);
    items.deleteItem(item, slot);
    sortGravestone();
    if (items.isEmpty()) {
      player
          .getGameEncoder()
          .sendMessage("You successfully retrieved everything from your gravestone.");
      moveGravestoneItems();
    }
    return true;
  }

  public void incinerateGravestoneItem(int slot, int itemId) {
    items.deleteItem(itemId, items.getAmount(slot), slot);
    sortGravestone();
    sendGravestonePrices();
  }

  public void takeFreeGravestoneItems() {
    var indexFree = player.getAttributeInt("grave_free_index") - 1;
    if (indexFree == -1) {
      return;
    }
    for (var i = items.size() - 1; i >= indexFree; i--) {
      if (!takeGravestoneItem(i, items.getId(i))) {
        break;
      }
    }
  }

  public void unlockOrTakeFeeGravestoneItems() {
    if (unlocked) {
      takeFeeGravestoneItems();
    } else {
      unlockGravestone();
    }
  }

  public void moveGravestoneItems() {
    countdown = 0;
    player.getController().removeNpc(grave);
    player.getWidgetManager().removeWidget(WidgetId.GRAVESTONE_COUNTDOWN_1012);
    if (items.isEmpty()) {
      return;
    }
    while (deathsRetrieval.size() + items.size() > deathsRetrieval.getCapacity()) {
      deathsRetrieval.deleteItem(deathsRetrieval.getFirstItem());
    }
    deathsRetrieval.addItems(items.getItems());
    items.clear();
    player.getGameEncoder().sendMessage("Death has collected your gravestone.");
  }

  public void spawnGrave() {
    player.getController().removeNpc(grave);
    if (countdown == 0) {
      return;
    }
    if (items.isEmpty()) {
      return;
    }
    grave = player.getWorld().addNpc(new NpcSpawn(tile, NpcId.GRAVE));
    grave.setOwner(player);
  }

  public void selectDeathsRetrievalItem(int slot, int itemId) {
    if (slot < 0 || slot >= deathsRetrieval.size() || deathsRetrieval.getId(slot) != itemId) {
      return;
    }
    var item = deathsRetrieval.getItem(slot);
    player.getGameEncoder().setVarp(VarpId.DEATHS_RETRIEVAL_ITEM_SLOT, slot);
    if (isFreeItem(item)) {
      takeDeathsRetrievalItem(Integer.MAX_VALUE);
      return;
    }
    player.getGameEncoder().setVarp(VarpId.DEATHS_RETRIEVAL_ITEM_SLOT, slot);
    player
        .getGameEncoder()
        .setVarp(
            VarpId.DEATHS_RETRIEVAL_ITEM_FEE,
            (int) (item.getInfoDef().getConfiguredExchangePrice() * 0.1));
  }

  public void takeDeathsRetrievalItems() {
    var count = deathsRetrieval.size();
    for (var i = 0; i < count; i++) {
      player.getGameEncoder().setVarp(VarpId.DEATHS_RETRIEVAL_ITEM_SLOT, 0);
      if (!takeDeathsRetrievalItem(Integer.MAX_VALUE)) {
        break;
      }
    }
  }

  public boolean takeDeathsRetrievalItem(int quantity) {
    var slot = player.getGameEncoder().getVarp(VarpId.DEATHS_RETRIEVAL_ITEM_SLOT);
    if (quantity <= 0 || slot < 0 || slot >= deathsRetrieval.size()) {
      return false;
    }
    var item = deathsRetrieval.getItem(slot);
    quantity = Math.min(quantity, item.getAmount());
    var fee = (int) Math.min(item.getInfoDef().getConfiguredExchangePrice() * 0.1, Item.MAX_AMOUNT);
    if (isFreeItem(item)) {
      fee = 0;
    }
    if (!PNumber.canMultiplyInt(quantity, fee, Item.MAX_AMOUNT)) {
      quantity = 1;
    }
    fee *= quantity;
    var inventoryCoins = player.getInventory().getCount(ItemId.COINS);
    var bankCoins = player.getBank().getCount(ItemId.COINS);
    if (inventoryCoins < fee && bankCoins < fee) {
      player.getGameEncoder().sendMessage("You don't have enough coins to do this.");
      return false;
    }
    if (!player.getInventory().canAddItem(item)) {
      player.getInventory().notEnoughSpace();
      return false;
    }
    if (inventoryCoins >= fee) {
      player.getInventory().deleteItem(ItemId.COINS, fee);
    } else if (bankCoins >= fee) {
      player.getBank().deleteItem(ItemId.COINS, fee);
    }
    if (quantity == item.getAmount()) {
      player.getInventory().addItem(item);
    } else {
      player.getInventory().addItem(item.getId(), quantity);
    }
    deathsRetrieval.deleteItem(item.getId(), quantity, slot);
    deathsRetrieval.sendItems();
    if (item.getAmount() <= quantity) {
      player.getGameEncoder().setVarp(VarpId.DEATHS_RETRIEVAL_ITEM_SLOT, -1);
    }
    player.getGameEncoder().setVarp(VarpId.DEATHS_RETRIEVAL_COFFER, deathsCoffer);
    return true;
  }

  private void sortGravestone() {
    items
        .getItems()
        .sort(
            (i1, i2) -> {
              if (isFreeItem(i1)) {
                return 1;
              }
              if (isFreeItem(i2)) {
                return 0;
              }
              return Integer.compare(
                  i2.getInfoDef().getConfiguredExchangePrice(),
                  i1.getInfoDef().getConfiguredExchangePrice());
            });
    var indexFree = 0;
    var index1k = 0;
    var index10k = 0;
    for (var i = 0; i < items.size(); i++) {
      var item = items.getItem(i);
      if (isFreeItem(item)) {
        indexFree = i + 1;
        break;
      }
      var exchangePrice = item.getInfoDef().getConfiguredExchangePrice();
      if (index1k == 0 && exchangePrice < 10_000) {
        index1k = i + 1;
      }
      if (index1k == 0 && index10k == 0 && exchangePrice < 100_000) {
        index10k = i + 1;
      }
    }
    player.putAttribute("grave_free_index", indexFree);
    player.putAttribute("grave_1k_index", index1k);
    player.putAttribute("grave_10k_index", index10k);
    player.getGameEncoder().setVarbit(VarbitId.GRAVESTONE_FREE_INDEX, indexFree);
    player.getGameEncoder().setVarbit(VarbitId.GRAVESTONE_1K_INDEX, index1k);
    player.getGameEncoder().setVarbit(VarbitId.GRAVESTONE_10K_INDEX, index10k);
    items.sendItems();
  }

  private void sendGravestonePrices() {
    player
        .getGameEncoder()
        .sendScript(ScriptId.GRAVESTONE_TRANSMIT_DATA, deathsCoffer, getGravestoneTotalPrice());
  }

  private void unlockGravestone() {
    var fee = getGravestoneTotalPrice();
    if (fee == 0) {
      player.getGameEncoder().sendMessage("Your items is already unlocked.");
      return;
    }
    var inventoryCoins = player.getInventory().getCount(ItemId.COINS);
    var bankCoins = player.getBank().getCount(ItemId.COINS);
    if (inventoryCoins < fee && bankCoins < fee) {
      player.getGameEncoder().sendMessage("You don't have enough coins to do this.");
      return;
    }
    unlocked = true;
    if (inventoryCoins >= fee) {
      player.getInventory().deleteItem(ItemId.COINS, fee);
    } else if (bankCoins >= fee) {
      player.getBank().deleteItem(ItemId.COINS, fee);
    }
    sendGravestonePrices();
  }

  private void takeFeeGravestoneItems() {
    if (!unlocked) {
      player
          .getGameEncoder()
          .sendMessage("You need to unlock your items before you can take this.");
      return;
    }
    var indexFree = player.getAttributeInt("grave_free_index") - 1;
    if (indexFree == -1) {
      indexFree = items.size();
    }
    for (var i = 0; i < indexFree; i++) {
      if (!takeGravestoneItem(0, items.getId(0))) {
        break;
      }
    }
  }

  private int getGravestoneTotalPrice() {
    if (unlocked) {
      return 0;
    }
    var total = 0L;
    for (var item : items.getItems()) {
      if (isFreeItem(item)) {
        continue;
      }
      var exchangePrice = item.getInfoDef().getConfiguredExchangePrice();
      if (exchangePrice < 100_000) {
        total += 1_000L * item.getAmount();
      } else if (exchangePrice < 10_000_000) {
        total += 10_000L * item.getAmount();
      } else {
        total += 100_000L * item.getAmount();
      }
    }
    return (int) Math.min(total, Item.MAX_AMOUNT);
  }

  private boolean isFreeItem(Item item) {
    var itemId = item.getId();
    if (itemId == ItemId.COINS) {
      return true;
    }
    if (Skills.isFood(itemId)) {
      return true;
    }
    if (Skills.isDrink(itemId)) {
      return true;
    }
    var exchangePrice = item.getInfoDef().getConfiguredExchangePrice();
    if (exchangePrice < 1_000) {
      return true;
    }
    return exchangePrice == item.getDef().getValue();
  }

  private void sendGravestoneCountdown() {
    player
        .getGameEncoder()
        .sendWidgetText(WidgetId.GRAVESTONE_COUNTDOWN_1012, 2, PTime.ticksToDuration(countdown));
    if (player.getWidgetManager().hasWidget(WidgetId.GRAVESTONE_COUNTDOWN_1012)) {
      return;
    }
    if (player.getWidgetManager().getOverlay() == -1) {
      player.getWidgetManager().sendOverlay(WidgetId.GRAVESTONE_COUNTDOWN_1012);
    } else if (player.getWidgetManager().getViewportType() == WidgetManager.ViewportType.FIXED
        && player.getWidgetManager().getFullOverlay() == -1) {
      player.getWidgetManager().sendFullOverlay(WidgetId.GRAVESTONE_COUNTDOWN_1012);
    }
  }
}
