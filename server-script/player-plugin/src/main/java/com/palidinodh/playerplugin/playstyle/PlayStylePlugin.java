package com.palidinodh.playerplugin.playstyle;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.rs.adaptive.RsDifficultyMode;
import com.palidinodh.rs.adaptive.RsGameMode;
import java.util.Map;
import lombok.Getter;

@Getter
public class PlayStylePlugin implements PlayerPlugin {

  @Inject private transient Player player;

  private RsGameMode gameMode = RsGameMode.UNSET;
  private RsDifficultyMode difficultyMode = RsDifficultyMode.UNSET;

  @Override
  public void loadLegacy(Map<String, Object> map) {
    if (map.containsKey("player.gameMode")) {
      setGameMode((RsGameMode) map.get("player.gameMode"));
    }
    if (map.containsKey("player.difficultyMode")) {
      difficultyMode = (RsDifficultyMode) map.get("player.difficultyMode");
    }
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("play_style_game_mode")) {
      return gameMode;
    }
    if (name.equals("play_style_set_game_mode")) {
      gameMode = (RsGameMode) args[0];
    }
    if (name.equals("play_style_difficulty_mode")) {
      return difficultyMode;
    }
    if (name.equals("play_style_set_difficulty_mode")) {
      difficultyMode = (RsDifficultyMode) args[0];
    }
    return null;
  }

  @Override
  public void login() {
    setGameMode(gameMode);
  }

  public void setGameMode(RsGameMode gameMode) {
    this.gameMode = gameMode;
    switch (gameMode) {
      case IRONMAN:
        player.getGameEncoder().setVarp(499, 8);
        break;
      case HARDCORE_IRONMAN:
        player.getGameEncoder().setVarp(499, 24);
        break;
      case GROUP_IRONMAN:
        player.getGameEncoder().setVarp(499, 16);
        break;
      default:
        player.getGameEncoder().setVarp(499, 0);
        break;
    }
  }
}
