package com.palidinodh.playerplugin.combat.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.COMBAT_LAMP_LEVEL_99_32337)
class CombatLamp99Item implements ItemHandler {

  public static void maxLevel(Player player, int skillId) {
    if (!player.getInventory().hasItem(ItemId.COMBAT_LAMP_LEVEL_99_32337)) {
      return;
    }
    if (player.getController().getLevelForXP(skillId) == 99) {
      player
          .getGameEncoder()
          .sendMessage("Your " + Skills.SKILL_NAMES[skillId] + " level is already 99.");
      return;
    }
    var xp = Skills.XP_TABLE[99] - player.getSkills().getXP(skillId);
    player.getInventory().deleteItem(ItemId.COMBAT_LAMP_LEVEL_99_32337);
    player.getSkills().addXp(skillId, xp, false);
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Attack",
                (c, s) -> {
                  player.openDialogue(new ConfirmationDialogue(player, Skills.ATTACK));
                }),
            new DialogueOption(
                "Strength",
                (c, s) -> {
                  player.openDialogue(new ConfirmationDialogue(player, Skills.STRENGTH));
                }),
            new DialogueOption(
                "Ranged",
                (c, s) -> {
                  player.openDialogue(new ConfirmationDialogue(player, Skills.RANGED));
                }),
            new DialogueOption(
                "Magic",
                (c, s) -> {
                  player.openDialogue(new ConfirmationDialogue(player, Skills.MAGIC));
                }),
            new DialogueOption(
                "Defence",
                (c, s) -> {
                  player.openDialogue(new ConfirmationDialogue(player, Skills.DEFENCE));
                }),
            new DialogueOption(
                "Prayer",
                (c, s) -> {
                  player.openDialogue(new ConfirmationDialogue(player, Skills.PRAYER));
                }),
            new DialogueOption(
                "Hitpoints",
                (c, s) -> {
                  player.openDialogue(new ConfirmationDialogue(player, Skills.HITPOINTS));
                })));
  }

  private static class ConfirmationDialogue extends OptionsDialogue {

    ConfirmationDialogue(Player player, int skillId) {
      addOption(
          "Confirm selection.",
          (c, s) -> {
            maxLevel(player, skillId);
          });
      addOption("Nevermind.");
    }
  }
}
