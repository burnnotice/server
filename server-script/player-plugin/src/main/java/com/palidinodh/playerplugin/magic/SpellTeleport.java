package com.palidinodh.playerplugin.magic;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PString;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SpellTeleport {
  LUMBRIDGE_HOME_TELEPORT(
      SpellbookChild.LUMBRIDGE_HOME_TELEPORT, TeleportStyle.HOME, 1, 0, null, null, 0, 0),
  VARROCK_TELEPORT(
      SpellbookChild.VARROCK_TELEPORT,
      TeleportStyle.NORMAL,
      25,
      35,
      PCollection.toImmutableList(
          new Item(ItemId.FIRE_RUNE), new Item(ItemId.AIR_RUNE, 3), new Item(ItemId.LAW_RUNE)),
      new Tile(3212, 3428),
      0,
      0),
  LUMBRIDGE_TELEPORT(
      SpellbookChild.LUMBRIDGE_TELEPORT,
      TeleportStyle.NORMAL,
      31,
      41,
      PCollection.toImmutableList(
          new Item(ItemId.EARTH_RUNE), new Item(ItemId.AIR_RUNE, 3), new Item(ItemId.LAW_RUNE)),
      new Tile(3221, 3218),
      0,
      0),
  FALADOR_TELEPORT(
      SpellbookChild.FALADOR_TELEPORT,
      TeleportStyle.NORMAL,
      37,
      48,
      PCollection.toImmutableList(
          new Item(ItemId.WATER_RUNE), new Item(ItemId.AIR_RUNE, 3), new Item(ItemId.LAW_RUNE)),
      new Tile(2965, 3379),
      0,
      0),
  TELEPORT_TO_HOUSE(
      SpellbookChild.TELEPORT_TO_HOUSE,
      TeleportStyle.NORMAL,
      40,
      30,
      PCollection.toImmutableList(
          new Item(ItemId.LAW_RUNE), new Item(ItemId.AIR_RUNE), new Item(ItemId.EARTH_RUNE)),
      null,
      0,
      0),
  CAMELOT_TELEPORT(
      SpellbookChild.CAMELOT_TELEPORT,
      TeleportStyle.NORMAL,
      45,
      56,
      PCollection.toImmutableList(new Item(ItemId.AIR_RUNE, 5), new Item(ItemId.LAW_RUNE)),
      new Tile(2757, 3478),
      0,
      0),
  ARDOUGNE_TELEPORT(
      SpellbookChild.ARDOUGNE_TELEPORT,
      TeleportStyle.NORMAL,
      51,
      61,
      PCollection.toImmutableList(new Item(ItemId.WATER_RUNE, 2), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(2661, 3305),
      0,
      0),
  TROLLHEIM_TELEPORT(
      SpellbookChild.TROLLHEIM_TELEPORT,
      TeleportStyle.NORMAL,
      61,
      68,
      PCollection.toImmutableList(new Item(ItemId.FIRE_RUNE, 2), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(2890, 3669),
      0,
      0),

  EDGEVILLE_HOME_TELEPORT(
      SpellbookChild.EDGEVILLE_HOME_TELEPORT, TeleportStyle.HOME, 1, 0, null, null, 0, 0),
  PADDEWWA_TELEPORT(
      SpellbookChild.PADDEWWA_TELEPORT,
      TeleportStyle.ANCIENT,
      54,
      64,
      PCollection.toImmutableList(
          new Item(ItemId.FIRE_RUNE), new Item(ItemId.AIR_RUNE), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(3094, 3470),
      0,
      0),
  KHARYRLL_TELEPORT(
      SpellbookChild.KHARYRLL_TELEPORT,
      TeleportStyle.ANCIENT,
      66,
      76,
      PCollection.toImmutableList(new Item(ItemId.BLOOD_RUNE), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(3499, 3485),
      0,
      0),
  DAREEYAK_TELEPORT(
      SpellbookChild.DAREEYAK_TELEPORT,
      TeleportStyle.ANCIENT,
      78,
      88,
      PCollection.toImmutableList(
          new Item(ItemId.FIRE_RUNE, 3),
          new Item(ItemId.AIR_RUNE, 2),
          new Item(ItemId.LAW_RUNE, 2)),
      new Tile(2968, 3695),
      0,
      0),
  CARRALLANGER_TELEPORT(
      SpellbookChild.CARRALLANGER_TELEPORT,
      TeleportStyle.ANCIENT,
      84,
      94,
      PCollection.toImmutableList(new Item(ItemId.SOUL_RUNE, 2), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(3175, 3669),
      0,
      0),
  ANNAKARL_TELEPORT(
      SpellbookChild.ANNAKARL_TELEPORT,
      TeleportStyle.ANCIENT,
      90,
      100,
      PCollection.toImmutableList(new Item(ItemId.BLOOD_RUNE, 2), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(3290, 3886),
      0,
      0),
  GHORROCK_TELEPORT(
      SpellbookChild.GHORROCK_TELEPORT,
      TeleportStyle.ANCIENT,
      96,
      106,
      PCollection.toImmutableList(new Item(ItemId.WATER_RUNE, 8), new Item(ItemId.LAW_RUNE, 2)),
      new Tile(2974, 3873),
      0,
      0),

  LUNAR_HOME_TELEPORT(
      SpellbookChild.LUNAR_HOME_TELEPORT, TeleportStyle.HOME, 1, 0, null, null, 0, 0),
  OURANIA_TELEPORT(
      SpellbookChild.OURANIA_TELEPORT,
      TeleportStyle.LUNAR,
      71,
      69,
      PCollection.toImmutableList(
          new Item(ItemId.EARTH_RUNE, 6),
          new Item(ItemId.ASTRAL_RUNE, 2),
          new Item(ItemId.LAW_RUNE)),
      new Tile(3015, 5628),
      0,
      0),
  WATERBIRTH_TELEPORT(
      SpellbookChild.WATERBIRTH_TELEPORT,
      TeleportStyle.LUNAR,
      72,
      71,
      PCollection.toImmutableList(
          new Item(ItemId.WATER_RUNE), new Item(ItemId.ASTRAL_RUNE, 2), new Item(ItemId.LAW_RUNE)),
      new Tile(2549, 3757),
      0,
      0);

  private final SpellbookChild widgetChild;
  private final TeleportStyle teleportStyle;
  private final int level;
  private final int experience;
  private final List<Item> runes;
  private final Tile tile;
  private final int tileRangeX;
  private final int tileRangeY;

  public static void normalTeleport(Player player, Tile tile) {
    teleport(player, TeleportStyle.NORMAL, tile, 0, 0);
  }

  public static void teleport(Player player, TeleportStyle teleportStyle, Tile tile) {
    teleport(player, teleportStyle, tile, 0, 0);
  }

  public static void teleport(
      Player player, TeleportStyle teleportStyle, Tile tile, int tileRangeX, int tileRangeY) {
    if (tile == null) {
      tile = player.getWidgetManager().getHomeTile();
    }
    var height = tile.getHeight();
    if (player.getClientHeight() == 0
        && Area.inWilderness(tile)
        && (player.inEdgeville() || player.getArea().inWilderness())) {
      height = player.getHeight();
    }
    tile = new Tile(tile.getX(), tile.getY(), height);
    switch (teleportStyle) {
      case ANCIENT:
        player.getMovement().animatedTeleport(tile, 1979, new Graphic(392), 2);
        break;
      case TABLET:
        player
            .getMovement()
            .animatedTeleport(tile, 4069, 4071, -1, null, new Graphic(678), null, 0, 2);
        break;
      default:
        player.getMovement().animatedTeleport(tile, 714, 715, new Graphic(308, 100, 45), null, 2);
        break;
    }
    player.getController().sendMapSound(teleportStyle.getSound());
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
  }

  public static SpellTeleport get(SpellbookChild widgetChild) {
    for (var spell : values()) {
      if (spell.widgetChild != widgetChild) {
        continue;
      }
      return spell;
    }
    return null;
  }

  public void teleport(Player player) {
    if (teleportStyle == TeleportStyle.HOME && player.getCombat().inRecentCombat()) {
      player.getGameEncoder().sendMessage("You can't use this while in combat.");
      return;
    }
    if (!player.getController().canTeleport(true)) {
      return;
    }
    if (player.getSkills().getLevel(Skills.MAGIC) < level) {
      player
          .getGameEncoder()
          .sendMessage("You need a Magic level of " + level + " to cast this spell.");
      return;
    }
    if (runes != null && !runes.isEmpty()) {
      for (var rune : runes) {
        if (player.getMagic().hasRune(rune)) {
          continue;
        }
        player.getGameEncoder().sendMessage("You don't have enough runes to cast this spell.");
        return;
      }
      for (var rune : runes) {
        player.getMagic().deleteRune(rune);
      }
    }
    if (experience > 0) {
      player.getSkills().addXp(Skills.MAGIC, experience);
    }
    teleport(player, teleportStyle, tile, tileRangeX, tileRangeY);
    AchievementDiary.castSpellUpdate(player, widgetChild, null, null, null);
    player.log(
        PlayerLogType.TELEPORT,
        PString.formatName(widgetChild.name().toLowerCase().replace("_", "")));
  }

  @AllArgsConstructor
  @Getter
  public enum TeleportStyle {
    HOME(new Sound(200)),
    NORMAL(new Sound(200)),
    ANCIENT(new Sound(197)),
    LUNAR(new Sound(200)),
    TABLET(new Sound(965));

    private final Sound sound;
  }
}
