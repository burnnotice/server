package com.palidinodh.playerplugin.slayer;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.VarpId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.chat.NormalChatDialogue;
import com.palidinodh.osrscore.model.entity.player.slayer.AssignedSlayerTask;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerMaster;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerTask;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerTaskIdentifier;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.playerplugin.bountyhunter.MysteriousEmblem;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class SlayerPlugin implements PlayerPlugin {

  private static final int[] BLOCKED_TASK_VARBITS = {
    VarbitId.SLAYER_BLOCKED_TASK_1,
    VarbitId.SLAYER_BLOCKED_TASK_2,
    VarbitId.SLAYER_BLOCKED_TASK_3,
    VarbitId.SLAYER_BLOCKED_TASK_4,
    VarbitId.SLAYER_BLOCKED_TASK_5,
    VarbitId.SLAYER_BLOCKED_TASK_6
  };

  @Setter private static boolean wildernessTasksEnabled = true;

  @Inject private transient Player player;

  @Getter private AssignedSlayerTask task = new AssignedSlayerTask();
  private AssignedSlayerTask wildernessTask = new AssignedSlayerTask();
  private int consecutiveTasks;
  private int consecutiveWildernessTasks;
  private int totalTasks;
  private int wildernessTaskEmblemId = -1;
  private List<SlayerTaskIdentifier> blockedTasks;
  private List<SlayerUnlock> unlocks;
  @Getter private int crystalKeys;
  @Getter private int brimstoneKeys;
  @Getter private int larransKeys;
  @Getter private int supplyBoxes;
  @Getter private int gemBags;
  @Getter private int herbBoxes;
  @Getter private int barBoxes;
  @Getter @Setter private int points;

  @Override
  public Object script(String name, Object... args) {
    switch (name) {
      case "slayer_is_any_task":
        {
          var npcId = args[0] instanceof Npc ? ((Entity) args[0]).getId() : (int) args[0];
          return isAnyTask(npcId, NpcDefinition.getLowerCaseName(npcId));
        }
      case "slayer_is_task":
        {
          var npc = (Npc) args[0];
          return isTask(task, npc.getId(), npc.getDef().getLowerCaseName());
        }
      case "slayer_is_wilderness_task":
        {
          var npc = (Npc) args[0];
          return isTask(wildernessTask, npc.getId(), npc.getDef().getLowerCaseName());
        }
      case "slayer_is_unlocked":
        return isUnlocked((SlayerUnlock) args[0]);
      case "slayer_get_task":
        {
          var plural =
              task.getQuantity() > 1
                  ? (task.getName().endsWith("ss")
                      ? "es"
                      : (task.getName().endsWith("s") ? "'" : "s"))
                  : "";
          return task.isComplete() ? "none" : task.getQuantity() + " " + task.getName() + plural;
        }
      case "slayer_get_wilderness_task":
        {
          var plural =
              wildernessTask.getQuantity() > 1
                  ? (wildernessTask.getName().endsWith("ss")
                      ? "es"
                      : (wildernessTask.getName().endsWith("s") ? "'" : "s"))
                  : "";
          return wildernessTask.isComplete()
              ? "none"
              : wildernessTask.getQuantity() + " " + wildernessTask.getName() + plural;
        }
      case "slayer_send_information":
        {
          player
              .getGameEncoder()
              .sendMessage(
                  "Slayer Task Streaks: " + consecutiveTasks + " / " + consecutiveWildernessTasks);
          player.getGameEncoder().sendMessage("Slayer Points: " + PNumber.formatNumber(points));
          player
              .getGameEncoder()
              .sendMessage("Total Slayer Tasks: " + PNumber.formatNumber(totalTasks));
          player.getGameEncoder().sendMessage("Brimstone keys: " + brimstoneKeys);
          player.getGameEncoder().sendMessage("Larran's keys: " + larransKeys);
          sendTask();
          break;
        }
      case "slayer_reset_task":
        task.cancel();
        break;
      case "slayer_reset_wilderness_task":
        wildernessTask.cancel();
        break;
    }
    return null;
  }

  @Override
  public void login() {
    sendVarps();
  }

  @Override
  public int getCurrency(String identifier) {
    if (identifier.equals("slayer points")) {
      return points;
    }
    return 0;
  }

  @Override
  public void changeCurrency(String identifier, int amount) {
    if (identifier.equals("slayer points")) {
      points += amount;
    }
  }

  @Override
  public void npcKilled(Npc npc) {
    taskKillCheck(task, npc);
    taskKillCheck(wildernessTask, npc);
  }

  public void getAssignment(String name) {
    var master = SlayerMaster.get(name);
    if (master == null) {
      return;
    }
    var isWilderness = name.equals(SlayerMaster.WILDERNESS_MASTER);
    if (isWilderness && !wildernessTasksEnabled) {
      player.getGameEncoder().sendMessage("Wilderness tasks can't currently be assigned.");
      return;
    }
    var assignedTask = isWilderness ? wildernessTask : task;
    var assignedSlayerTask = assignedTask.getSlayerTask();
    if (!assignedTask.isComplete()) {
      if (assignedTask == task && !name.equals(SlayerMaster.RESET_MASTER)) {
        if (!player.isUsergroup(UserRank.SAPPHIRE_MEMBER)
            || PTime.getDate().equals(assignedTask.getDate())) {
          player.getGameEncoder().sendMessage("You already have a task.");
          return;
        }
      }
      if (assignedTask == wildernessTask) {
        player.getGameEncoder().sendMessage("You already have a task.");
        return;
      }
    }
    if (player.getSkills().getCombatLevel() < master.getCombatLevel()) {
      player
          .getGameEncoder()
          .sendMessage(
              "You need a combat level of "
                  + master.getCombatLevel()
                  + " to use this Slayer master.");
      return;
    }
    if (player.getController().getLevelForXP(Skills.SLAYER) < master.getSlayerLevel()) {
      player
          .getGameEncoder()
          .sendMessage(
              "You need a Slayer level of "
                  + master.getSlayerLevel()
                  + " to get one of these tasks.");
      return;
    }
    SlayerTask selectedTask = null;
    if (assignedSlayerTask != null
        && player.getEquipment().wearingAccomplishmentCape(Skills.SLAYER)
        && PRandom.randomE(10) == 0) {
      selectedTask = assignedSlayerTask;
    }
    var isBoss = false;
    for (var i = 0; i < 128 && selectedTask == null; i++) {
      var aTask = PRandom.listRandom(master.getTasks());
      isBoss = aTask.getIdentifier() == SlayerTaskIdentifier.BOSS;
      if (isBoss) {
        if (!isUnlocked(SlayerUnlock.LIKE_A_BOSS)) {
          continue;
        }
        if (aTask.getMaximumQuantity() == 0) {
          aTask = PRandom.listRandom(SlayerMaster.get(SlayerMaster.BOSS_MASTER).getTasks());
        }
        if (aTask.getName().equals("Grotesque Guardians")
            && !isUnlocked(SlayerUnlock.GROTESQUE_GUARDIANS)) {
          continue;
        }
        if (aTask.isWilderness() && isUnlocked(SlayerUnlock.WILDERNESS_BOSS)) {
          continue;
        }
        if (aTask.getName().equals("Corporeal Beast")
            && !isUnlocked(SlayerUnlock.CORPOREAL_BEAST)) {
          continue;
        }
        if (aTask.getName().equals("Raids Boss") && !isUnlocked(SlayerUnlock.RAIDS)) {
          continue;
        }
        if (assignedSlayerTask != null
            && assignedSlayerTask.isWilderness()
            && aTask.isWilderness()) {
          continue;
        }
      }
      if (player.getController().getLevelForXP(Skills.SLAYER) < aTask.getSlayerLevel()) {
        continue;
      }
      if (!isWilderness && isBlockedTask(aTask.getIdentifier())) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.RED_DRAGON
          && !isUnlocked(SlayerUnlock.SEEING_RED)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.MITHRIL_DRAGON
          && !isUnlocked(SlayerUnlock.I_HOPE_YOU_MITH_ME)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.AVIANSIE
          && !isUnlocked(SlayerUnlock.WATCH_THE_BIRDIE)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.TZHAAR
          && !isUnlocked(SlayerUnlock.HOT_STUFF)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.LIZARDMAN
          && !isUnlocked(SlayerUnlock.REPTILE_GOT_RIPPED)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.FOSSIL_ISLAND_WYVERN
          && isUnlocked(SlayerUnlock.STOP_THE_WYVERN)) {
        continue;
      }
      if (assignedSlayerTask != null
          && assignedSlayerTask.getIdentifier() != null
          && assignedSlayerTask.getIdentifier() == aTask.getIdentifier()) {
        continue;
      }
      if (assignedSlayerTask != null
          && assignedSlayerTask.getName() != null
          && assignedSlayerTask.getName().equals(aTask.getName())) {
        continue;
      }
      selectedTask = aTask;
      break;
    }
    if (selectedTask == null) {
      player.getGameEncoder().sendMessage("Failed to assign a task!");
      return;
    }
    int quantity = selectedTask.getRandomQuantity();
    if (selectedTask.getIdentifier() != null
        && selectedTask.getIdentifier().getExtendedUnlock() != null
        && isUnlocked(selectedTask.getIdentifier().getExtendedUnlock())) {
      quantity *= 1.5;
    }
    if (isBoss) {
      quantity = selectedTask.getMinimumQuantity();
    }
    if (assignedTask != null && !assignedTask.isComplete() && assignedTask == task) {
      if (name.equalsIgnoreCase(SlayerMaster.RESET_MASTER)
          && !assignedTask.getName().equals(SlayerMaster.RESET_MASTER)) {
        consecutiveTasks = 0;
      }
    }
    var newAssignedTask = new AssignedSlayerTask(master.getName(), selectedTask, isBoss, quantity);
    var newSlayerTask = newAssignedTask.getSlayerTask();
    if (isWilderness) {
      wildernessTask = newAssignedTask;
    } else {
      task = newAssignedTask;
    }
    player.openDialogue(
        new NormalChatDialogue(
            master.getNpcId(),
            "Your new task is to kill "
                + (newAssignedTask.isBoss() ? "" : newAssignedTask.getQuantity() + " ")
                + newAssignedTask.getPluralName()
                + ".",
            (c, s) -> {
              if (newAssignedTask.isBoss()) {
                setBossTaskQuantity(newAssignedTask);
              } else if (isWilderness) {
                sendAssignedTaskMessages(newAssignedTask);
              } else {
                setTaskQuantity(newAssignedTask);
              }
            }));
  }

  public void sendTask() {
    if (task.isComplete() && wildernessTask.isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    if (!task.isComplete()) {
      player
          .getGameEncoder()
          .sendMessage(
              "You're assigned to kill "
                  + task.getName()
                  + "s; only "
                  + task.getQuantity()
                  + " more to go.");
      if (task.getSlayerTask().getLocation() != null) {
        player
            .getGameEncoder()
            .sendMessage("They are located at " + task.getSlayerTask().getLocation() + ".");
      }
    }
    if (!wildernessTask.isComplete()) {
      player
          .getGameEncoder()
          .sendMessage(
              "You're assigned to kill "
                  + wildernessTask.getName()
                  + "s in the wilderness; only "
                  + wildernessTask.getQuantity()
                  + " more to go.");
      if (wildernessTask.getSlayerTask().getLocation() != null) {
        player
            .getGameEncoder()
            .sendMessage(
                "They are located at " + wildernessTask.getSlayerTask().getLocation() + ".");
      }
    }
  }

  public void cancelWildernessTask() {
    if (wildernessTask.isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    var cost = 30;
    switch (player.getPlugin(BondPlugin.class).getDonatorRank()) {
      case ONYX:
        cost -= 24;
        break;
      case DRAGONSTONE:
        cost -= 20;
        break;
      case DIAMOND:
        cost -= 16;
        break;
      case RUBY:
        cost -= 12;
        break;
      case EMERALD:
        cost -= 8;
        break;
      case SAPPHIRE:
        cost -= 4;
        break;
    }
    cost = Math.max(0, cost);
    if (points < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to cancel a task.");
      return;
    }
    player.getGameEncoder().sendMessage("Your wilderness task has been cancelled.");
    wildernessTask.cancel();
    points -= cost;
  }

  public void openRewards() {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.SLAYER_REWARDS);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 8, 0, 100, 2);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 12, 0, 10, 2);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 23, 0, 20, 1086);
    sendRewardsVarbits();
  }

  public boolean isUnlocked(SlayerUnlock unlock) {
    return unlocks != null && unlocks.contains(unlock);
  }

  public void unlock(SlayerUnlock unlock) {
    if (isUnlocked(unlock)) {
      lock(unlock);
      return;
    }
    var cost = unlock.getPrice();
    if (points < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to unlock this.");
      return;
    }
    if (unlocks == null) {
      unlocks = new ArrayList<>();
    }
    if (!unlocks.contains(unlock)) {
      unlocks.add(unlock);
    }
    points -= cost;
    sendRewardsVarbits();
  }

  public void lock(SlayerUnlock unlock) {
    if (unlocks == null || !unlocks.contains(unlock)) {
      return;
    }
    unlocks.remove(unlock);
    sendRewardsVarbits();
  }

  public void incrimentBrimstoneKeys() {
    brimstoneKeys++;
  }

  public void incrimentCrystalKeys() {
    crystalKeys++;
  }

  public void incrimentSupplyBoxes() {
    supplyBoxes++;
  }

  public void incrimentLarransKeys() {
    larransKeys++;
  }

  public void incrimentGemBags() {
    gemBags++;
  }

  public void incrimentBarBoxes() {
    barBoxes++;
  }

  public void incrimentHerbBoxes() {
    herbBoxes++;
  }

  public void cancelTask() {
    if (task.isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    var cost = 30;
    switch (player.getPlugin(BondPlugin.class).getDonatorRank()) {
      case ONYX:
        cost -= 24;
        break;
      case DRAGONSTONE:
        cost -= 20;
        break;
      case DIAMOND:
        cost -= 16;
        break;
      case RUBY:
        cost -= 12;
        break;
      case EMERALD:
        cost -= 8;
        break;
      case SAPPHIRE:
        cost -= 4;
        break;
    }
    cost = Math.max(0, cost);
    if (points < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to cancel a task.");
      return;
    }
    player.getGameEncoder().sendMessage("Your task has been cancelled.");
    task.cancel();
    points -= cost;
    sendRewardsVarbits();
  }

  public void blockTask() {
    if (task.isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    if (task.getIdentifier() == null) {
      player.getGameEncoder().sendMessage("This task can't be blocked.");
      return;
    }
    if (blockedTasks != null && blockedTasks.contains(task.getIdentifier())) {
      player.getGameEncoder().sendMessage("This task is already blocked.");
      return;
    }
    if (points < 100) {
      player.getGameEncoder().sendMessage("You need 100 points to block a task.");
      return;
    }
    if (blockedTasks != null && blockedTasks.size() >= 6) {
      player.getGameEncoder().sendMessage("You can't block any more tasks.");
      return;
    }
    if (blockedTasks == null) {
      blockedTasks = new ArrayList<>();
    }
    blockedTasks.add(task.getIdentifier());
    points -= 100;
    task.cancel();
    sendRewardsVarbits();
  }

  public void unblockTask(int option) {
    if (blockedTasks == null || option >= blockedTasks.size()) {
      return;
    }
    blockedTasks.remove(option);
    if (blockedTasks.isEmpty()) {
      blockedTasks = null;
    }
    sendRewardsVarbits();
  }

  public void buy(Item item, int cost) {
    if (points < cost) {
      player
          .getGameEncoder()
          .sendMessage("You need " + PNumber.formatNumber(cost) + " points to buy this.");
      return;
    }
    if (!player.getInventory().canAddItem(item)) {
      player.getInventory().notEnoughSpace();
      return;
    }
    player.getInventory().addItem(item);
    points -= cost;
    sendRewardsVarbits();
  }

  public void openMasterMenuDialogue() {
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Get task",
                (c, s) -> {
                  openChooseMasterDialogue();
                }),
            new DialogueOption(
                "Current task",
                (c, s) -> {
                  sendTask();
                })));
  }

  public void openChooseMasterDialogue() {
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Turael - level 1",
                (c, s) -> {
                  getAssignment("Turael");
                }),
            new DialogueOption(
                "Mazchna - level 20",
                (c, s) -> {
                  getAssignment("Mazchna");
                }),
            new DialogueOption(
                "Chaeldar - level 70",
                (c, s) -> {
                  getAssignment("Chaeldar");
                }),
            new DialogueOption(
                "Nieve - level 85",
                (c, s) -> {
                  getAssignment("Nieve");
                }),
            new DialogueOption(
                "Duradel - level 100",
                (c, s) -> {
                  getAssignment("Duradel");
                })));
  }

  private void setBossTaskQuantity(AssignedSlayerTask assignedTask) {
    if (!assignedTask.isBoss()) {
      return;
    }
    if (assignedTask.getQuantity() != assignedTask.getTotal()) {
      return;
    }
    var slayerTask = assignedTask.getSlayerTask();
    player
        .getGameEncoder()
        .sendEnterAmount(
            "Quantity between "
                + slayerTask.getMinimumQuantity()
                + " and "
                + slayerTask.getMaximumQuantity()
                + ":",
            ie -> {
              if (ie >= slayerTask.getMinimumQuantity() && ie <= slayerTask.getMaximumQuantity()) {
                assignedTask.resetQuantity(ie);
              }
              sendAssignedTaskMessages(assignedTask);
            });
  }

  private void setTaskQuantity(AssignedSlayerTask assignedTask) {
    var slayerTask = assignedTask.getSlayerTask();
    if (slayerTask == null) {
      return;
    }
    if (slayerTask.isWilderness()) {
      return;
    }
    if (assignedTask.getQuantity() != assignedTask.getTotal()) {
      return;
    }
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Keep quantity at " + assignedTask.getTotal() + ".",
                (c, s) -> {
                  sendAssignedTaskMessages(assignedTask);
                }),
            new DialogueOption(
                "Reduce quantity to " + (assignedTask.getTotal() / 2) + ".",
                (c, s) -> {
                  assignedTask.resetQuantity(assignedTask.getTotal() / 2);
                  sendAssignedTaskMessages(assignedTask);
                })));
  }

  private void sendAssignedTaskMessages(AssignedSlayerTask assignedTask) {
    if (assignedTask.getMaster().equals(SlayerMaster.WILDERNESS_MASTER)) {
      player
          .getGameEncoder()
          .sendMessage(
              "Your new wilderness task is to kill "
                  + assignedTask.getQuantity()
                  + " "
                  + assignedTask.getPluralName()
                  + ".");
      if (player.getGameMode().isIronType()) {
        player
            .getGameEncoder()
            .sendMessage("Carrying the same emblem for the whole task will upgrade its tier.");
      } else {
        player
            .getGameEncoder()
            .sendMessage(
                "Task kills will reward blood money if you're carrying an emblem, and carrying the same emblem for the whole task will upgrade its tier.");
      }
      wildernessTaskEmblemId = -1;
    } else {
      player
          .getGameEncoder()
          .sendMessage(
              "Your new task is to kill "
                  + assignedTask.getQuantity()
                  + " "
                  + assignedTask.getPluralName()
                  + ".");
    }
    if (assignedTask.getSlayerTask().getLocation() != null) {
      player
          .getGameEncoder()
          .sendMessage("They are located at " + assignedTask.getSlayerTask().getLocation() + ".");
    }
    AchievementDiary.slayerAssignmentUpdate(
        player,
        assignedTask.getSlayerMaster(),
        assignedTask.getSlayerTask(),
        assignedTask.getQuantity());
  }

  private void sendVarps() {
    player.getGameEncoder().setVarp(VarpId.SLAYER_QUANTITY, task.getQuantity());
    player
        .getGameEncoder()
        .setVarp(
            VarpId.SLAYER_TASK_IDENTIFIER,
            task.isComplete()
                ? 0
                : task.getIdentifier() != null ? task.getIdentifier().ordinal() : 156);
    player
        .getGameEncoder()
        .setVarbit(
            VarbitId.SLAYER_GROTESQUE_GUARDIANS_DOOR,
            isUnlocked(SlayerUnlock.GROTESQUE_GUARDIANS) ? 1 : 0);
  }

  private void sendRewardsVarbits() {
    sendVarps();
    player.getGameEncoder().setVarbit(VarbitId.SLAYER_POINTS, points);
    if (unlocks != null) {
      for (SlayerUnlock unlock : unlocks) {
        if (unlock.getVarbit() == -1) {
          continue;
        }
        player.getGameEncoder().setVarbit(unlock.getVarbit(), 1);
      }
    }
    if (blockedTasks != null) {
      for (int i = 0; i < blockedTasks.size(); i++) {
        player.getGameEncoder().setVarbit(BLOCKED_TASK_VARBITS[i], blockedTasks.get(i).ordinal());
      }
    }
  }

  private boolean isBlockedTask(SlayerTaskIdentifier identifier) {
    return blockedTasks != null && blockedTasks.contains(identifier);
  }

  private void taskKillCheck(AssignedSlayerTask assignedTask, Npc npc) {
    if (assignedTask == wildernessTask && player.getSkills().getCombatLevel() < 100) {
      return;
    }
    if (!countsTowardTaskQuantity(assignedTask, npc.getId())) {
      return;
    }
    if (assignedTask.isComplete()
        || !isTask(assignedTask, npc.getId(), npc.getDef().getLowerCaseName())) {
      return;
    }
    var experience = npc.getCombat().getMaxHitpoints();
    if (npc.getCombatDef().getSlayer().getExperience() > 0) {
      experience = npc.getCombatDef().getSlayer().getExperience();
    }
    player.getSkills().addXp(Skills.SLAYER, experience);
    if (npc.getCombatDef().getSlayer().getSuperiorId() != -1
        && isUnlocked(SlayerUnlock.BIGGER_BADDER)
        && PRandom.randomE(100) == 0) {
      player.getGameEncoder().sendMessage("<col=ff0000>A superior foe has appeared...");
      var superiorNpc =
          player
              .getController()
              .addNpc(new NpcSpawn(npc, npc.getCombatDef().getSlayer().getSuperiorId()));
      superiorNpc.getCombat().setTarget(player);
    }
    int brimstoneKeyChance;
    if (npc.getDef().getCombatLevel() >= 100) {
      brimstoneKeyChance = (int) (-0.2 * Math.min(npc.getDef().getCombatLevel(), 350) + 120);
    } else {
      brimstoneKeyChance =
          (int) (0.2 * StrictMath.pow(npc.getDef().getCombatLevel() - 100, 2) + 100);
    }
    if (brimstoneKeyChance > 0 && assignedTask == task && PRandom.inRange(1, brimstoneKeyChance)) {
      Tile tile = npc;
      if (npc.getCombatDef().getDrop().isUnderKiller()) {
        tile = player;
      }
      if ((player.canDonatorPickupItem(ItemId.BRIMSTONE_KEY))
          && player.getInventory().canAddItem(ItemId.BRIMSTONE_KEY)) {
        player.getInventory().addItem(ItemId.BRIMSTONE_KEY);
      } else {
        player.getController().addMapItem(new Item(ItemId.BRIMSTONE_KEY), tile, player);
      }
    }
    int larransKeyChance;
    if (npc.getDef().getCombatLevel() >= 81) {
      larransKeyChance = (int) (-0.18 * Math.min(npc.getDef().getCombatLevel(), 350) + 113);
    } else {
      larransKeyChance = (int) (0.3 * StrictMath.pow(80 - npc.getDef().getCombatLevel(), 2) + 100);
    }
    if (npc.getCombatDef().getSlayer().getLevel() > 1) {
      larransKeyChance /= 1.25;
    }
    if (larransKeyChance > 0
        && assignedTask == wildernessTask
        && PRandom.inRange(1, larransKeyChance)) {
      Tile tile = npc;
      if (npc.getCombatDef().getDrop().isUnderKiller()) {
        tile = player;
      }
      if ((player.canDonatorPickupItem(ItemId.LARRANS_KEY))
          && player.getInventory().canAddItem(ItemId.LARRANS_KEY)) {
        player.getInventory().addItem(ItemId.LARRANS_KEY);
      } else {
        player.getController().addMapItem(new Item(ItemId.LARRANS_KEY), tile, player);
      }
    }
    if (assignedTask == wildernessTask) {
      if (assignedTask.getQuantity() == assignedTask.getTotal()) {
        wildernessTaskEmblemId = player.getPlugin(BountyHunterPlugin.class).getEmblemId();
      } else if (wildernessTaskEmblemId > -1
          && !player.getInventory().hasItem(wildernessTaskEmblemId)) {
        player
            .getGameEncoder()
            .sendMessage("Your task is no longer eligible to upgrade an emblem.");
        player
            .getGameEncoder()
            .sendMessage(
                "You're missing an " + ItemDefinition.getName(wildernessTaskEmblemId) + "!");
        wildernessTaskEmblemId = -1;
      }
    }
    assignedTask.decreaseQuantity();
    if (!assignedTask.isComplete()) {
      return;
    }
    var tasksCompleted = 0;
    if (assignedTask == task) {
      totalTasks++;
      if (!assignedTask.getMaster().equals(SlayerMaster.RESET_MASTER)) {
        tasksCompleted = ++consecutiveTasks;
        if (Main.getHolidayToken() != -1) {
          for (var i = 1; i < SlayerMaster.STANDARD_MASTERS.length; i++) {
            if (!assignedTask.getMaster().equals(SlayerMaster.STANDARD_MASTERS[i])) {
              continue;
            }
            player.getInventory().addOrDropItem(Main.getHolidayToken(), i);
            break;
          }
        }
      }
    } else if (assignedTask == wildernessTask) {
      totalTasks++;
      tasksCompleted = ++consecutiveWildernessTasks;
      if (Main.getHolidayToken() != -1) {
        player.getInventory().addOrDropItem(Main.getHolidayToken(), 5);
      }
    }
    var slayerMaster = SlayerMaster.get(assignedTask.getMaster());
    var slayerTask = assignedTask.getSlayerTask();
    var rewardPoints = slayerMaster.getPoints();
    rewardPoints += player.getPlugin(BondPlugin.class).getDonatorRank().getMultiplier() - 1;
    if (assignedTask == wildernessTask) {
      if (wildernessTaskEmblemId > -1 && player.getInventory().hasItem(wildernessTaskEmblemId)) {
        var nextEmblemId = MysteriousEmblem.getNextId(wildernessTaskEmblemId);
        if (nextEmblemId != -1) {
          player.getInventory().deleteItem(wildernessTaskEmblemId);
          player.getInventory().addItem(nextEmblemId);
        }
      }
      if (PRandom.randomE(20) == 0) {
        Tile tile = npc;
        if (npc.getCombatDef().getDrop().isUnderKiller()) {
          tile = player;
        }
        player.getController().addMapItem(new Item(ItemId.BLOODY_KEY), tile, player);
        player
            .getWorld()
            .sendMessage(
                "<col=ff0000>A bloody key has been dropped near "
                    + assignedTask.getName()
                    + "s at Level "
                    + Area.getWildernessLevel(tile)
                    + " for "
                    + player.getUsername()
                    + "!");
      }
    }
    if (tasksCompleted % 1000 == 0) {
      rewardPoints *= 50;
    } else if (tasksCompleted % 250 == 0) {
      rewardPoints *= 35;
    } else if (tasksCompleted % 100 == 0) {
      rewardPoints *= 25;
    } else if (tasksCompleted % 50 == 0) {
      rewardPoints *= 15;
    } else if (tasksCompleted % 10 == 0) {
      rewardPoints *= 5;
    }
    points += rewardPoints;
    if (rewardPoints > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "<col=8F4808>You've completed "
                  + tasksCompleted
                  + " tasks in a row and received "
                  + rewardPoints
                  + " points; return to a Slayer master.");
    } else {
      player
          .getGameEncoder()
          .sendMessage(
              "<col=8F4808>You've completed "
                  + tasksCompleted
                  + " tasks; return to a Slayer master.");
    }
    AchievementDiary.slayerAssignmentCompleteUpdate(player, slayerMaster, slayerTask);
    if (assignedTask == task) {
      player.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.SLAYER_TASKS, 1);
    } else if (assignedTask == wildernessTask) {
      player
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.WILD_SLAYER_TASKS, 1);
    }
  }

  private boolean countsTowardTaskQuantity(AssignedSlayerTask assignedTask, int id) {
    if (id == NpcId.RESPIRATORY_SYSTEM) {
      return false;
    }
    if ((id == NpcId.DAWN_228 || id == NpcId.DAWN_228_7885)
        && !isUnlocked(SlayerUnlock.DOUBLE_TROUBLE)) {
      return false;
    }
    if (assignedTask.isBoss()) {
      switch (id) {
        case NpcId.WINGMAN_SKREE_143:
        case NpcId.FLOCKLEADER_GEERIN_149:
        case NpcId.FLIGHT_KILISA_159:
        case NpcId.STARLIGHT_149:
        case NpcId.GROWLER_139:
        case NpcId.BREE_146:
        case NpcId.SERGEANT_STRONGSTACK_141:
        case NpcId.SERGEANT_STEELWILL_142:
        case NpcId.SERGEANT_GRIMSPIKE_142:
        case NpcId.TSTANON_KARLAK_145:
        case NpcId.ZAKLN_GRITCH_142:
        case NpcId.BALFRUG_KREEYATH_151:
        case NpcId.GREAT_OLM_RIGHT_CLAW:
        case NpcId.GREAT_OLM_LEFT_CLAW:
        case NpcId.GREAT_OLM_RIGHT_CLAW_549:
        case NpcId.GREAT_OLM_LEFT_CLAW_750:
        case NpcId.SCORPIAS_OFFSPRING_15:
          return false;
      }
    }
    return true;
  }

  private boolean isAnyTask(int id, String name) {
    return isTask(task, id, name) || isTask(wildernessTask, id, name);
  }

  private boolean isTask(AssignedSlayerTask assignedTask, int id, String name) {
    if (assignedTask.isComplete()) {
      return false;
    }
    if (assignedTask == wildernessTask && !player.getArea().inWilderness()) {
      return false;
    }
    return assignedTask.containsId(id)
        || name != null
            && name.matches(".*(?i)(" + assignedTask.getName().toLowerCase() + ")\\b.*");
  }
}
