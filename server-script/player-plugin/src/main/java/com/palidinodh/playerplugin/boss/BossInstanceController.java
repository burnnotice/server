package com.palidinodh.playerplugin.boss;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.osrscore.model.Controller;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.controller.PController;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

public class BossInstanceController extends PController {

  @Inject private Player player;
  @Getter private int npcId;
  private boolean isPrivate;
  @Getter private long startTime = PTime.nanoTime();
  @Getter private List<Npc> bossNpcs = new ArrayList<>();

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("boss_instance_controller")) {
      return Boolean.TRUE;
    }
    if (name.equals("boss_instance_controller_name")) {
      return NpcDefinition.getName(npcId);
    }
    if (name.equals("boss_instance_controller_time")) {
      return Math.max(0, 60 - PTime.betweenNanoToMin(startTime));
    }
    if (name.equals("boss_instance_controller_check_expiration")) {
      return checkExpiration();
    }
    return null;
  }

  @Override
  public void startHook() {
    player.setLargeVisibility();
    player.getCombat().setDamageInflicted(0);
    setExitTile(new Tile(player));
  }

  @Override
  public void stopHook() {
    if (player == null) {
      return;
    }
    player.setDefaultVisibility();
    player.getWidgetManager().removeOverlay();
    player.getWidgetManager().removeFullOverlay();
    player.getPlugin(BossPlugin.class).restore();
    if (isPrivate) {
      removeNpcs(bossNpcs);
    }
  }

  @Override
  public void instanceChangedHook(Controller controller) {
    if (isInstanced() && player != null) {
      player.setLargeVisibility();
    }
    if (controller == null || controller == this) {
      return;
    }
    var bc = controller.as(BossInstanceController.class);
    startTime = bc.getStartTime();
    npcId = bc.getNpcId();
    bossNpcs = bc.getBossNpcs();
  }

  @Override
  public void tickHook() {
    checkExpiration();
    if (player != null && player.isLocked() && !player.isVisible()) {
      setEntity(null);
      player = null;
    }
  }

  @Override
  public void applyDeadHook() {
    setExitTile(null);
    stopWithTeleport();
  }

  @Override
  public MapItem addMapItemHook(MapItem mapItem) {
    if (!mapItem.getDef().getUntradable()
        && (isFood(mapItem.getId()) || isDrink(mapItem.getId()))) {
      mapItem.setAlwaysAppear();
    }
    return mapItem;
  }

  @Override
  public void logoutHook() {
    if (player != null && getExitTile() != null) {
      player.getMovement().teleport(getExitTile());
    }
  }

  public void setBossInstance(int bossId, BossInstance instance, boolean makePrivate) {
    npcId = bossId;
    isPrivate = makePrivate;
    for (var spawn : instance.getSpawns()) {
      var npc = addNpc(spawn);
      npc.setLargeVisibility();
      bossNpcs.add(npc);
    }
  }

  private boolean checkExpiration() {
    boolean isExpired = startTime == 0 || PTime.betweenNanoToMin(startTime) > 60;
    if (isExpired && bossNpcs != null) {
      if (!isPrivate) {
        for (Player player2 : getPlayers()) {
          player2.getGameEncoder().sendMessage("<col=ff0000>The instance has expired!");
        }
      } else if (player != null) {
        player.getGameEncoder().sendMessage("<col=ff0000>The instance has expired!");
      }
      for (Npc npc : bossNpcs) {
        if (npc == null) {
          continue;
        }
        npc.getSpawn().respawnable(false);
      }
      bossNpcs = null;
    }
    return isExpired;
  }

  public void expire() {
    startTime = 0;
  }
}
