package com.palidinodh.playerplugin.woodcutting;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;

public class WoodcuttingPlugin implements PlayerPlugin {

  private static final Item[] EVIL_CHICKEN_OUTFIT = {
    new Item(ItemId.EVIL_CHICKEN_FEET),
    new Item(ItemId.EVIL_CHICKEN_WINGS),
    new Item(ItemId.EVIL_CHICKEN_HEAD),
    new Item(ItemId.EVIL_CHICKEN_LEGS)
  };

  @Inject private transient Player player;

  public void checkShrine() {
    var eggCount =
        player.getInventory().getCount(ItemId.BIRDS_EGG)
            + player.getInventory().getCount(ItemId.BIRDS_EGG_5077)
            + player.getInventory().getCount(ItemId.BIRDS_EGG_5078);
    if (eggCount == 0) {
      player.getGameEncoder().sendMessage("You have no eggs to exchange.");
      return;
    }
    player.getInventory().deleteItem(ItemId.BIRDS_EGG, Item.MAX_AMOUNT);
    player.getInventory().deleteItem(ItemId.BIRDS_EGG_5077, Item.MAX_AMOUNT);
    player.getInventory().deleteItem(ItemId.BIRDS_EGG_5078, Item.MAX_AMOUNT);
    var value = 100_000;
    if (player.getGameMode().isIronType()) {
      value /= 4;
    }
    player.getInventory().addOrDropItem(ItemId.COINS, value * eggCount);
    player.getInventory().addOrDropItem(ItemId.BIRD_NEST_5073, eggCount);
    if (PRandom.randomE(132 / eggCount) == 0) {
      if (!player.hasItem(ItemId.EVIL_CHICKEN_FEET)) {
        player.getInventory().addOrDropItem(ItemId.EVIL_CHICKEN_FEET);
      } else if (!player.hasItem(ItemId.EVIL_CHICKEN_WINGS)) {
        player.getInventory().addOrDropItem(ItemId.EVIL_CHICKEN_WINGS);
      } else if (!player.hasItem(ItemId.EVIL_CHICKEN_HEAD)) {
        player.getInventory().addOrDropItem(ItemId.EVIL_CHICKEN_HEAD);
      } else if (!player.hasItem(ItemId.EVIL_CHICKEN_LEGS)) {
        player.getInventory().addOrDropItem(ItemId.EVIL_CHICKEN_LEGS);
      } else {
        player.getInventory().addOrDropItem(PRandom.arrayRandom(EVIL_CHICKEN_OUTFIT));
      }
    }
  }
}
