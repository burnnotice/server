package com.palidinodh.playerplugin.holiday.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.CHRISTMAS_CRACKER_32391)
class ChristmasCrackerItem implements ItemHandler {

  private static final int[] PARTYHATS = {
    ItemId.RED_PARTYHAT,
    ItemId.YELLOW_PARTYHAT,
    ItemId.BLUE_PARTYHAT,
    ItemId.GREEN_PARTYHAT,
    ItemId.PURPLE_PARTYHAT,
    ItemId.WHITE_PARTYHAT,
    ItemId.BLACK_PARTYHAT,
    ItemId.RAINBOW_PARTYHAT
  };

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.replace(new Item(PRandom.arrayRandom(PARTYHATS)));
  }
}
