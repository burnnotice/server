package com.palidinodh.playerplugin.magic.handler.widget;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.io.cache.widget.ViewportContainer;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.combat.CombatSpellDef;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.SPELL_SELECT)
class SpellSelectWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (player.getEquipment().getWeaponId() == ItemId.TRIDENT_OF_THE_SEAS_FULL
        || player.getEquipment().getWeaponId() == ItemId.TRIDENT_OF_THE_SEAS
        || player.getEquipment().getWeaponId() == ItemId.UNCHARGED_TRIDENT
        || player.getEquipment().getWeaponId() == ItemId.TRIDENT_OF_THE_SEAS_E
        || player.getEquipment().getWeaponId() == ItemId.UNCHARGED_TRIDENT_E
        || player.getEquipment().getWeaponId() == ItemId.TRIDENT_OF_THE_SWAMP
        || player.getEquipment().getWeaponId() == ItemId.UNCHARGED_TOXIC_TRIDENT
        || player.getEquipment().getWeaponId() == ItemId.TRIDENT_OF_THE_SWAMP_E
        || player.getEquipment().getWeaponId() == ItemId.UNCHARGED_TOXIC_TRIDENT_E
        || player.getEquipment().getWeaponId() == ItemId.SANGUINESTI_STAFF
        || player.getEquipment().getWeaponId() == ItemId.SANGUINESTI_STAFF_UNCHARGED
        || player.getEquipment().getWeaponId() == ItemId.DAWNBRINGER
        || player.getEquipment().getWeaponId() == ItemId.CORRUPTED_STAFF_BASIC_32384
        || player.getEquipment().getWeaponId() == ItemId.CORRUPTED_STAFF_ATTUNED_32385
        || player.getEquipment().getWeaponId() == ItemId.CORRUPTED_STAFF_PERFECTED_32386) {
      return;
    }
    CombatSpellDef spell = null;
    if (player.getMagic().getSpellbook() == Magic.STANDARD_MAGIC) {
      if (slot == 1) {
        spell = CombatSpellDef.getDef(SpellbookChild.WIND_STRIKE);
      } else if (slot == 2) {
        spell = CombatSpellDef.getDef(SpellbookChild.WATER_STRIKE);
      } else if (slot == 3) {
        spell = CombatSpellDef.getDef(SpellbookChild.EARTH_STRIKE);
      } else if (slot == 4) {
        spell = CombatSpellDef.getDef(SpellbookChild.FIRE_STRIKE);
      } else if (slot == 5) {
        spell = CombatSpellDef.getDef(SpellbookChild.WIND_BOLT);
      } else if (slot == 6) {
        spell = CombatSpellDef.getDef(SpellbookChild.WATER_BOLT);
      } else if (slot == 7) {
        spell = CombatSpellDef.getDef(SpellbookChild.EARTH_BOLT);
      } else if (slot == 8) {
        spell = CombatSpellDef.getDef(SpellbookChild.FIRE_BOLT);
      } else if (slot == 9) {
        spell = CombatSpellDef.getDef(SpellbookChild.WIND_BLAST);
      } else if (slot == 10) {
        spell = CombatSpellDef.getDef(SpellbookChild.WATER_BLAST);
      } else if (slot == 11) {
        spell = CombatSpellDef.getDef(SpellbookChild.EARTH_BLAST);
      } else if (slot == 12) {
        spell = CombatSpellDef.getDef(SpellbookChild.FIRE_BLAST);
      } else if (slot == 13) {
        spell = CombatSpellDef.getDef(SpellbookChild.WIND_WAVE);
      } else if (slot == 14) {
        spell = CombatSpellDef.getDef(SpellbookChild.WATER_WAVE);
      } else if (slot == 15) {
        spell = CombatSpellDef.getDef(SpellbookChild.EARTH_WAVE);
      } else if (slot == 16) {
        spell = CombatSpellDef.getDef(SpellbookChild.FIRE_WAVE);
      } else if (slot == 48) {
        spell = CombatSpellDef.getDef(SpellbookChild.WIND_SURGE);
      } else if (slot == 49) {
        spell = CombatSpellDef.getDef(SpellbookChild.WATER_SURGE);
      } else if (slot == 50) {
        spell = CombatSpellDef.getDef(SpellbookChild.EARTH_SURGE);
      } else if (slot == 51) {
        spell = CombatSpellDef.getDef(SpellbookChild.FIRE_SURGE);
      }
      if (player.getEquipment().getWeaponId() == ItemId.IBANS_STAFF) {
        if (slot == 47) {
          spell = CombatSpellDef.getDef(SpellbookChild.IBAN_BLAST);
        }
      }
      if (player.getEquipment().getWeaponId() == ItemId.SLAYERS_STAFF
          || player.getEquipment().getWeaponId() == ItemId.STAFF_OF_THE_DEAD
          || player.getEquipment().getWeaponId() == ItemId.TOXIC_STAFF_UNCHARGED
          || player.getEquipment().getWeaponId() == ItemId.TOXIC_STAFF_OF_THE_DEAD
          || player.getEquipment().getWeaponId() == ItemId.SLAYERS_STAFF_E) {
        if (slot == 18) {
          spell = CombatSpellDef.getDef(SpellbookChild.MAGIC_DART);
        }
      }
      if (player.getEquipment().getWeaponId() == ItemId.STAFF_OF_THE_DEAD
          || player.getEquipment().getWeaponId() == ItemId.TOXIC_STAFF_UNCHARGED
          || player.getEquipment().getWeaponId() == ItemId.TOXIC_STAFF_OF_THE_DEAD) {
        if (slot == 20) {
          spell = CombatSpellDef.getDef(SpellbookChild.FLAMES_OF_ZAMORAK);
        }
      }
      if (player.getEquipment().getWeaponId() == ItemId.STAFF_OF_LIGHT) {
        if (slot == 52) {
          spell = CombatSpellDef.getDef(SpellbookChild.SARADOMIN_STRIKE);
        }
      }
      if (player.getEquipment().getWeaponId() == ItemId.STAFF_OF_BALANCE) {
        if (slot == 19) {
          spell = CombatSpellDef.getDef(SpellbookChild.CLAWS_OF_GUTHIX);
        }
      }
    } else if (player.getMagic().getSpellbook() == Magic.ANCIENT_MAGIC) {
      if (slot == 31) {
        spell = CombatSpellDef.getDef(SpellbookChild.SMOKE_RUSH);
      } else if (slot == 32) {
        spell = CombatSpellDef.getDef(SpellbookChild.SHADOW_RUSH);
      } else if (slot == 33) {
        spell = CombatSpellDef.getDef(SpellbookChild.BLOOD_RUSH);
      } else if (slot == 34) {
        spell = CombatSpellDef.getDef(SpellbookChild.ICE_RUSH);
      } else if (slot == 35) {
        spell = CombatSpellDef.getDef(SpellbookChild.SMOKE_BURST);
      } else if (slot == 36) {
        spell = CombatSpellDef.getDef(SpellbookChild.SHADOW_BURST);
      } else if (slot == 37) {
        spell = CombatSpellDef.getDef(SpellbookChild.BLOOD_BURST);
      } else if (slot == 38) {
        spell = CombatSpellDef.getDef(SpellbookChild.ICE_BURST);
      } else if (slot == 39) {
        spell = CombatSpellDef.getDef(SpellbookChild.SMOKE_BLITZ);
      } else if (slot == 40) {
        spell = CombatSpellDef.getDef(SpellbookChild.SHADOW_BLITZ);
      } else if (slot == 41) {
        spell = CombatSpellDef.getDef(SpellbookChild.BLOOD_BLITZ);
      } else if (slot == 42) {
        spell = CombatSpellDef.getDef(SpellbookChild.ICE_BLITZ);
      } else if (slot == 43) {
        spell = CombatSpellDef.getDef(SpellbookChild.SMOKE_BARRAGE);
      } else if (slot == 44) {
        spell = CombatSpellDef.getDef(SpellbookChild.SHADOW_BARRAGE);
      } else if (slot == 45) {
        spell = CombatSpellDef.getDef(SpellbookChild.BLOOD_BARRAGE);
      } else if (slot == 46) {
        spell = CombatSpellDef.getDef(SpellbookChild.ICE_BARRAGE);
      }
    }
    if (slot != 0 && spell == null) {
      player.getGameEncoder().sendMessage("Unable to find the spell you selected.");
      return;
    }
    player.getMagic().setAutoSpellId(spell != null ? spell.getChildId() : 0);
    player.getMagic().setDefensive(player.getAttributeBool("magic_defensive"));
    player.getWidgetManager().sendWidget(ViewportContainer.COMBAT, WidgetId.COMBAT);
    player.getEquipment().sendCombatTabText();
  }
}
