package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class BabyChinchompaPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(ItemId.BABY_CHINCHOMPA, NpcId.BABY_CHINCHOMPA, NpcId.BABY_CHINCHOMPA_6756));
    builder.entry(
        new Pet.Entry(
            ItemId.BABY_CHINCHOMPA_13324, NpcId.BABY_CHINCHOMPA_6719, NpcId.BABY_CHINCHOMPA_6757));
    builder.entry(
        new Pet.Entry(
            ItemId.BABY_CHINCHOMPA_13325, NpcId.BABY_CHINCHOMPA_6720, NpcId.BABY_CHINCHOMPA_6758));
    builder.entry(
        new Pet.Entry(
            ItemId.BABY_CHINCHOMPA_13326, NpcId.BABY_CHINCHOMPA_6721, NpcId.BABY_CHINCHOMPA_6759));
    return builder;
  }
}
