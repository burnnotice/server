package com.palidinodh.playerplugin.crafting.handler.widget;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.playerplugin.crafting.CraftingPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.TANNING)
class TanningWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var amount = 1;
    if (childId >= 124 && childId <= 130) {
      amount = -1;
    } else if (childId >= 132 && childId <= 138) {
      amount = -2;
    } else if (childId >= 140 && childId <= 146) {
      amount = 5;
    }
    int makeItemId;
    if (childId == 124 || childId == 132 || childId == 140 || childId == 148) {
      makeItemId = ItemId.LEATHER;
    } else if (childId == 125 || childId == 133 || childId == 141 || childId == 149) {
      makeItemId = ItemId.HARD_LEATHER;
    } else if (childId == 126 || childId == 134 || childId == 142 || childId == 150) {
      makeItemId = ItemId.GREEN_DRAGON_LEATHER;
    } else if (childId == 127 || childId == 135 || childId == 143 || childId == 151) {
      makeItemId = ItemId.BLUE_DRAGON_LEATHER;
    } else if (childId == 128 || childId == 136 || childId == 144 || childId == 152) {
      makeItemId = ItemId.RED_DRAGON_LEATHER;
    } else if (childId == 129 || childId == 137 || childId == 145 || childId == 153) {
      makeItemId = ItemId.BLACK_DRAGON_LEATHER;
    } else if (childId == 130 || childId == 138 || childId == 146 || childId == 154) {
      makeItemId = ItemId.SNAKESKIN;
    } else {
      makeItemId = -1;
    }
    if (makeItemId == -1) {
      return;
    }
    if (amount == -2) {
      player
          .getGameEncoder()
          .sendEnterAmount(
              value -> {
                tanHide(player, makeItemId, value);
              });
    } else {
      tanHide(player, makeItemId, amount);
    }
  }

  private void tanHide(Player player, int craftItemId, int amount) {
    var baseItemId = -1;
    var cost = -1;
    if (craftItemId == ItemId.LEATHER) {
      baseItemId = ItemId.COWHIDE;
      cost = CraftingPlugin.LEATHER_COST;
    } else if (craftItemId == ItemId.HARD_LEATHER) {
      baseItemId = ItemId.COWHIDE;
      cost = CraftingPlugin.HARD_LEATHER_COST;
    } else if (craftItemId == ItemId.GREEN_DRAGON_LEATHER) {
      baseItemId = ItemId.GREEN_DRAGONHIDE;
      cost = CraftingPlugin.GREEN_DRAGON_LEATHER_COST;
    } else if (craftItemId == ItemId.BLUE_DRAGON_LEATHER) {
      baseItemId = ItemId.BLUE_DRAGONHIDE;
      cost = CraftingPlugin.BLUE_DRAGON_LEATHER_COST;
    } else if (craftItemId == ItemId.RED_DRAGON_LEATHER) {
      baseItemId = ItemId.RED_DRAGONHIDE;
      cost = CraftingPlugin.RED_DRAGON_LEATHER_COST;
    } else if (craftItemId == ItemId.BLACK_DRAGON_LEATHER) {
      baseItemId = ItemId.BLACK_DRAGONHIDE;
      cost = CraftingPlugin.BLACK_DRAGON_LEATHER_COST;
    } else if (craftItemId == ItemId.SNAKESKIN) {
      baseItemId = ItemId.SNAKE_HIDE;
      cost = CraftingPlugin.SNAKESKIN_LEATHER_COST;
    }
    if (baseItemId == -1 || cost == -1) {
      return;
    }
    if (player.getInventory().getCount(baseItemId) == 0) {
      baseItemId = ItemDef.getNotedId(baseItemId);
      craftItemId = ItemDef.getNotedId(craftItemId);
    }
    if (amount == -1) {
      amount = player.getInventory().getCount(baseItemId);
    }
    if (amount > player.getInventory().getCount(baseItemId)) {
      amount = player.getInventory().getCount(baseItemId);
    }
    if (baseItemId == -1 || craftItemId == -1 || amount == 0) {
      player.getGameEncoder().sendMessage("You don't have raw hide to do this.");
      return;
    }
    if (player.getInventory().getCount(ItemId.COINS) < amount * cost) {
      player.getGameEncoder().sendMessage("You don't have enough coins to do this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.COINS, amount * cost);
    player.getInventory().deleteItem(baseItemId, amount);
    player.getInventory().addItem(craftItemId, amount);
    AchievementDiary.makeItemUpdate(
        player, Skills.CRAFTING, new RandomItem(craftItemId, amount), null, null);
  }
}
