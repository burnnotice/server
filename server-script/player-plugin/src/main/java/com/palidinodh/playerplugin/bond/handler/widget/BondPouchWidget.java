package com.palidinodh.playerplugin.bond.handler.widget;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.bond.DonatorEffectType;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;
import java.util.ArrayList;

@ReferenceId({WidgetId.BOND_POUCH_1017, WidgetId.BOND_INFORMATION_1018})
class BondPouchWidget implements WidgetHandler {

  private static void depositBonds(Player player, int quantity) {
    var inventoryCount = player.getInventory().getCount(ItemId.BOND_32318);
    quantity = Math.min(quantity, inventoryCount);
    if (inventoryCount == 0 || quantity <= 0) {
      player.getGameEncoder().sendMessage("You have no bonds in your inventory.");
      return;
    }
    player.getInventory().deleteItem(ItemId.BOND_32318, quantity);
    var plugin = player.getPlugin(BondPlugin.class);
    plugin.setPouch(plugin.getPouch() + quantity);
    plugin.sendPouchCounts();
  }

  private static void withdrawBonds(Player player, int quantity) {
    var plugin = player.getPlugin(BondPlugin.class);
    if (plugin.getPouch() == 0) {
      player.getGameEncoder().sendMessage("You have no bonds in your pouch.");
      return;
    }
    quantity = (int) Math.min(quantity, plugin.getPouch());
    quantity = Math.min(quantity, Item.MAX_AMOUNT);
    quantity = player.getInventory().canAddAmount(ItemId.BOND_32318, quantity);
    if (quantity <= 0) {
      player.getInventory().notEnoughSpace();
      return;
    }
    plugin.setPouch(plugin.getPouch() - quantity);
    player.getInventory().addItem(ItemId.BOND_32318, quantity);
  }

  private static void skinColorDialogue(Player player) {
    player.openDialogue(
        new LargeOptionsDialogue(
            new DialogueOption(
                "Default",
                (c, s) -> {
                  player.getAppearance().setColor(4, 1);
                }),
            new DialogueOption(
                "Sapphire",
                (c, s) -> {
                  if (!player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
                    player
                        .getGameEncoder()
                        .sendMessage("Only sapphire members and above can use this.");
                    return;
                  }
                  player.getAppearance().setColor(4, 19);
                }),
            new DialogueOption(
                "Emerald",
                (c, s) -> {
                  if (!player.isUsergroup(UserRank.EMERALD_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only emerald members can use this.");
                    return;
                  }
                  player.getAppearance().setColor(4, 20);
                }),
            new DialogueOption(
                "Ruby",
                (c, s) -> {
                  if (!player.isUsergroup(UserRank.RUBY_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only ruby members can use this.");
                    return;
                  }
                  player.getAppearance().setColor(4, 21);
                }),
            new DialogueOption(
                "Diamond",
                (c, s) -> {
                  if (!player.isUsergroup(UserRank.DIAMOND_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only diamond members can use this.");
                    return;
                  }
                  player.getAppearance().setColor(4, 22);
                }),
            new DialogueOption(
                "Dragonstone",
                (c, s) -> {
                  if (!player.isUsergroup(UserRank.DRAGONSTONE_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only dragonstone members can use this.");
                    return;
                  }
                  player.getAppearance().setColor(4, 23);
                }),
            new DialogueOption(
                "Onyx",
                (c, s) -> {
                  if (!player.isUsergroup(UserRank.ONYX_MEMBER)) {
                    player.getGameEncoder().sendMessage("Only onyx members can use this.");
                    return;
                  }
                  player.getAppearance().setColor(4, 24);
                })));
  }

  private static void effectsDialogue(Player player) {
    var plugin = player.getPlugin(BondPlugin.class);
    var effects = DonatorEffectType.values();
    var effectOptions = new ArrayList<String>();
    for (var effect : effects) {
      effectOptions.add(effect.getFormattedName() + ": " + plugin.isDonatorEffectEnabled(effect));
    }
    player.openDialogue(
        new LargeOptionsDialogue(DialogueOption.toOptions(effectOptions))
            .action(
                (c, s) -> {
                  if (s >= effects.length) {
                    return;
                  }
                  plugin.toggleDonatorEffect(effects[s]);
                  effectsDialogue(player);
                }));
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(BondPlugin.class);
    if (widgetId == WidgetId.BOND_POUCH_1017) {
      switch (childId) {
        case 17:
          plugin.openInformation();
          break;
        case 28:
          {
            if (option == 0) {
              depositBonds(player, 1);
            } else if (option == 1) {
              player.getGameEncoder().sendEnterAmount(v -> depositBonds(player, v));
            } else if (option == 2) {
              depositBonds(player, Integer.MAX_VALUE);
            }
            break;
          }
        case 29:
          {
            if (option == 0) {
              withdrawBonds(player, 1);
            } else if (option == 1) {
              player.getGameEncoder().sendEnterAmount(v -> withdrawBonds(player, v));
            } else if (option == 2) {
              withdrawBonds(player, Integer.MAX_VALUE);
            }
            break;
          }
        case 70:
          player.getGameEncoder().sendOpenUrl(Settings.getInstance().getStoreUrl());
          break;
        case 72:
          player.openShop("bond");
          break;
        case 74:
          effectsDialogue(player);
          break;
        case 76:
          skinColorDialogue(player);
          break;
        case 58:
          plugin.setHideRankIcon(!plugin.isHideRankIcon());
          player.getGameEncoder().sendMessage("Hide rank: " + plugin.isHideRankIcon());
          break;
      }
      plugin.sendPouchCounts();
    } else if (widgetId == WidgetId.BOND_INFORMATION_1018) {
      if (childId >= 27 && childId <= 86) {
        plugin.sendInformation((childId - 27) / 3);
        return;
      }
      switch (childId) {
        case 17:
          plugin.openPouch();
          break;
      }
    }
  }
}
