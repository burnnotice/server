package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class VetionJrPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.VETION_JR, NpcId.VETION_JR, NpcId.VETION_JR_5559));
    builder.entry(
        new Pet.Entry(ItemId.VETION_JR_13180, NpcId.VETION_JR_5537, NpcId.VETION_JR_5560));
    return builder;
  }
}
