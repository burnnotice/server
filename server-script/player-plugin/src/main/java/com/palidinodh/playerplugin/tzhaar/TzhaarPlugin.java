package com.palidinodh.playerplugin.tzhaar;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.player.Appearance;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;

public class TzhaarPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  @Override
  public void login() {
    if (player.getId() == TzhaarFightPitMinigame.getWinnerUserId()
        && player.getAppearance().getSkullIcon() == -1) {
      player.getAppearance().setSkullIcon(Appearance.PK_ICON_RED_SKULL);
    }
  }

  public void joinFightPit() {
    if (!TzhaarFightPitMinigame.isOpen()) {
      player.openDialogue(
          new OptionsDialogue(
              new DialogueOption(
                  "Open the lobby?",
                  (c, s) -> {
                    if (TzhaarFightPitMinigame.isOpen()) {
                      player.getGameEncoder().sendMessage("The lobby is already open.");
                      return;
                    }
                    TzhaarFightPitMinigame.start(false, false);
                    joinFightPit();
                  }),
              new DialogueOption("Nevermind.")));
      return;
    }
    player.setController(new TzhaarFightPitController());
  }
}
