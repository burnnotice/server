package com.palidinodh.playerplugin.treasuretrail.reward;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.random.PRandom;
import java.util.List;

class MasterReward implements TreasureTrailReward {

  private List<RandomItem> items =
      RandomItem.buildList(
          new RandomItem(ItemId.DRAGON_PLATEBODY_ORNAMENT_KIT),
          new RandomItem(ItemId.DRAGON_KITESHIELD_ORNAMENT_KIT),
          new RandomItem(ItemId.DRAGON_DEFENDER_ORNAMENT_KIT),
          new RandomItem(ItemId.ANGUISH_ORNAMENT_KIT),
          new RandomItem(ItemId.TORTURE_ORNAMENT_KIT),
          new RandomItem(ItemId.OCCULT_ORNAMENT_KIT),
          new RandomItem(ItemId.ARMADYL_GODSWORD_ORNAMENT_KIT),
          new RandomItem(ItemId.BANDOS_GODSWORD_ORNAMENT_KIT),
          new RandomItem(ItemId.SARADOMIN_GODSWORD_ORNAMENT_KIT),
          new RandomItem(ItemId.ZAMORAK_GODSWORD_ORNAMENT_KIT),
          new RandomItem(ItemId.TORMENTED_ORNAMENT_KIT),
          new RandomItem(ItemId.LESSER_DEMON_MASK),
          new RandomItem(ItemId.GREATER_DEMON_MASK),
          new RandomItem(ItemId.BLACK_DEMON_MASK),
          new RandomItem(ItemId.JUNGLE_DEMON_MASK),
          new RandomItem(ItemId.OLD_DEMON_MASK),
          new RandomItem(ItemId.ARCEUUS_HOOD),
          new RandomItem(ItemId.HOSIDIUS_HOOD),
          new RandomItem(ItemId.LOVAKENGJ_HOOD),
          new RandomItem(ItemId.PISCARILIUS_HOOD),
          new RandomItem(ItemId.SHAYZIEN_HOOD),
          new RandomItem(ItemId.SAMURAI_KASA),
          new RandomItem(ItemId.SAMURAI_SHIRT),
          new RandomItem(ItemId.SAMURAI_GLOVES),
          new RandomItem(ItemId.SAMURAI_GREAVES),
          new RandomItem(ItemId.SAMURAI_BOOTS),
          new RandomItem(ItemId.MUMMYS_HEAD),
          new RandomItem(ItemId.MUMMYS_BODY),
          new RandomItem(ItemId.MUMMYS_HANDS),
          new RandomItem(ItemId.MUMMYS_LEGS),
          new RandomItem(ItemId.MUMMYS_FEET),
          new RandomItem(ItemId.ANKOU_MASK),
          new RandomItem(ItemId.ANKOU_TOP),
          new RandomItem(ItemId.ANKOU_GLOVES),
          new RandomItem(ItemId.ANKOUS_LEGGINGS),
          new RandomItem(ItemId.ANKOU_SOCKS),
          new RandomItem(ItemId.HOOD_OF_DARKNESS),
          new RandomItem(ItemId.ROBE_TOP_OF_DARKNESS),
          new RandomItem(ItemId.GLOVES_OF_DARKNESS),
          new RandomItem(ItemId.ROBE_BOTTOM_OF_DARKNESS),
          new RandomItem(ItemId.BOOTS_OF_DARKNESS),
          new RandomItem(ItemId.RING_OF_COINS),
          new RandomItem(ItemId.LEFT_EYE_PATCH),
          new RandomItem(ItemId.OBSIDIAN_CAPE_R),
          new RandomItem(ItemId.FANCY_TIARA),
          new RandomItem(ItemId.HALF_MOON_SPECTACLES),
          new RandomItem(ItemId.ALE_OF_THE_GODS),
          new RandomItem(ItemId.BUCKET_HELM_G),
          new RandomItem(ItemId.BOWL_WIG),
          new RandomItem(ItemId.SCROLL_SACK));
  private List<RandomItem> gildedItems =
      RandomItem.buildList(
          new RandomItem(ItemId.GILDED_FULL_HELM),
          new RandomItem(ItemId.GILDED_PLATEBODY),
          new RandomItem(ItemId.GILDED_PLATELEGS),
          new RandomItem(ItemId.GILDED_PLATESKIRT),
          new RandomItem(ItemId.GILDED_KITESHIELD),
          new RandomItem(ItemId.GILDED_MED_HELM),
          new RandomItem(ItemId.GILDED_CHAINBODY),
          new RandomItem(ItemId.GILDED_SQ_SHIELD),
          new RandomItem(ItemId.GILDED_2H_SWORD),
          new RandomItem(ItemId.GILDED_SPEAR),
          new RandomItem(ItemId.GILDED_HASTA),
          new RandomItem(ItemId.GILDED_BOOTS),
          new RandomItem(ItemId.GILDED_SCIMITAR),
          new RandomItem(ItemId.GILDED_DHIDE_VAMBS),
          new RandomItem(ItemId.GILDED_DHIDE_BODY),
          new RandomItem(ItemId.GILDED_DHIDE_CHAPS),
          new RandomItem(ItemId.GILDED_COIF),
          new RandomItem(ItemId.GILDED_AXE),
          new RandomItem(ItemId.GILDED_PICKAXE),
          new RandomItem(ItemId.GILDED_SPADE));
  private List<RandomItem> thirdAgeItems =
      RandomItem.buildList(
          new RandomItem(ItemId._3RD_AGE_FULL_HELMET),
          new RandomItem(ItemId._3RD_AGE_PLATEBODY),
          new RandomItem(ItemId._3RD_AGE_PLATELEGS),
          new RandomItem(ItemId._3RD_AGE_KITESHIELD),
          new RandomItem(ItemId._3RD_AGE_RANGE_COIF),
          new RandomItem(ItemId._3RD_AGE_RANGE_TOP),
          new RandomItem(ItemId._3RD_AGE_RANGE_LEGS),
          new RandomItem(ItemId._3RD_AGE_VAMBRACES),
          new RandomItem(ItemId._3RD_AGE_MAGE_HAT),
          new RandomItem(ItemId._3RD_AGE_ROBE_TOP),
          new RandomItem(ItemId._3RD_AGE_ROBE),
          new RandomItem(ItemId._3RD_AGE_AMULET),
          new RandomItem(ItemId._3RD_AGE_CLOAK),
          new RandomItem(ItemId._3RD_AGE_WAND),
          new RandomItem(ItemId._3RD_AGE_BOW),
          new RandomItem(ItemId._3RD_AGE_LONGSWORD),
          new RandomItem(ItemId._3RD_AGE_AXE),
          new RandomItem(ItemId._3RD_AGE_PICKAXE),
          new RandomItem(ItemId._3RD_AGE_DRUIDIC_ROBE_TOP),
          new RandomItem(ItemId._3RD_AGE_DRUIDIC_ROBE_BOTTOMS),
          new RandomItem(ItemId._3RD_AGE_DRUIDIC_CLOAK),
          new RandomItem(ItemId._3RD_AGE_DRUIDIC_STAFF),
          new RandomItem(ItemId.RING_OF_3RD_AGE),
          new RandomItem(ItemId._3RD_AGE_PLATESKIRT));
  private List<RandomItem> commonItems =
      RandomItem.combine(
          BASE_COMMON,
          RandomItem.buildList(
              new RandomItem(ItemId.DRAGON_DAGGER),
              new RandomItem(ItemId.DRAGON_MACE),
              new RandomItem(ItemId.DRAGON_LONGSWORD),
              new RandomItem(ItemId.DRAGON_SCIMITAR),
              new RandomItem(ItemId.DRAGON_BATTLEAXE),
              new RandomItem(ItemId.DRAGON_HALBERD),
              new RandomItem(ItemId.NATURE_RUNE, 100, 200),
              new RandomItem(ItemId.DEATH_RUNE, 100, 200),
              new RandomItem(ItemId.BLOOD_RUNE, 100, 200),
              new RandomItem(ItemId.SOUL_RUNE, 100, 200),
              new RandomItem(ItemId.ONYX_BOLTS_E, 15, 25),
              new RandomItem(ItemId.MANTA_RAY_NOTED, 15, 25),
              new RandomItem(ItemId.WINE_OF_ZAMORAK_NOTED, 35, 50),
              new RandomItem(ItemId.LIMPWURT_ROOT_NOTED, 40, 60),
              new RandomItem(ItemId.GRIMY_RANARR_WEED_NOTED, 5, 10),
              new RandomItem(ItemId.GRIMY_TOADFLAX_NOTED, 25, 35),
              new RandomItem(ItemId.GRIMY_SNAPDRAGON_NOTED, 5, 10),
              new RandomItem(ItemId.RUNITE_ORE_NOTED, 5, 8),
              new RandomItem(ItemId.RUNITE_BAR_NOTED, 5, 7),
              new RandomItem(ItemId.TOOTH_HALF_OF_KEY),
              new RandomItem(ItemId.LOOP_HALF_OF_KEY),
              new RandomItem(ItemId.PALM_TREE_SEED, 1, 2),
              new RandomItem(ItemId.YEW_SEED, 1, 2),
              new RandomItem(ItemId.MAGIC_SEED, 1, 2)));

  @Override
  public Item getCommon(Player player) {
    return RandomItem.getItem(commonItems);
  }

  @Override
  public Item getUnique(Player player) {
    player.getPlugin(FamiliarPlugin.class).rollPet(ItemId.BLOODHOUND, 0.1);
    if (PRandom.inRange(player.getCombat().getDropRate(0.1))) {
      return RandomItem.getItem(thirdAgeItems);
    } else if (PRandom.inRange(player.getCombat().getDropRate(0.5))) {
      return RandomItem.getItem(gildedItems);
    }
    return RandomItem.getItem(items);
  }

  @Override
  public int getRandomRolls() {
    return PRandom.randomI(5, 7);
  }

  @Override
  public int getRandomCoinQuantity() {
    return PRandom.randomI(400_000, 800_000);
  }

  @Override
  public int getMasterScrollRate() {
    return 0;
  }
}
