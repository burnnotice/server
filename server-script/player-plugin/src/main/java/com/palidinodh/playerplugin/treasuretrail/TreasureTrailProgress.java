package com.palidinodh.playerplugin.treasuretrail;

import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.treasuretrail.stage.ClueStage;
import com.palidinodh.random.PRandom;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
class TreasureTrailProgress {

  private ClueScrollType clueType;
  private int remainingSteps = 1;
  private String type;
  private List<String> previous = new ArrayList<>();

  TreasureTrailProgress(ClueScrollType clueType) {
    this.clueType = clueType;
    remainingSteps = clueType.getSteps();
    setType();
  }

  public ClueStage getType() {
    return ClueStage.TYPES.get(type);
  }

  public void setType() {
    ClueStage stage;
    while (true) {
      stage = PRandom.collectionRandom(ClueStage.TYPES.values());
      if (clueType == null) {
        break;
      }
      if (!stage.isWilderness()) {
        break;
      }
      if (clueType == ClueScrollType.HARD) {
        break;
      }
      if (clueType == ClueScrollType.ELITE) {
        break;
      }
      if (clueType == ClueScrollType.MASTER) {
        break;
      }
    }
    type = stage.name();
  }

  public void stepComplete() {
    remainingSteps--;
    if (remainingSteps > 0) {
      setType();
    }
  }
}
