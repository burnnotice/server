package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class PenanceQueenPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(ItemId.PET_PENANCE_QUEEN, NpcId.PENANCE_PET, NpcId.PENANCE_PET_6674));
    return builder;
  }
}
