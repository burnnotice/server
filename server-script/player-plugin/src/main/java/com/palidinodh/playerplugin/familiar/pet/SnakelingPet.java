package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class SnakelingPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.PET_SNAKELING, NpcId.SNAKELING_2127, NpcId.SNAKELING_2130));
    builder.entry(
        new Pet.Entry(ItemId.PET_SNAKELING_12939, NpcId.SNAKELING_2128, NpcId.SNAKELING_2131));
    builder.entry(
        new Pet.Entry(ItemId.PET_SNAKELING_12940, NpcId.SNAKELING_2129, NpcId.SNAKELING_2132));
    return builder;
  }
}
