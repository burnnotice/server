package com.palidinodh.playerplugin.treasuretrail;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.dialogue.Dialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.ItemMessageDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.treasuretrail.reward.TreasureTrailReward;
import com.palidinodh.random.PRandom;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.Map;
import lombok.Getter;

public class TreasureTrailPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  @Getter private TreasureChest chest = new TreasureChest();
  private Map<ClueScrollType, TreasureTrailProgress> trails = new EnumMap<>(ClueScrollType.class);
  private Map<ClueScrollType, Integer> totalCompleted = new EnumMap<>(ClueScrollType.class);

  @Override
  public void login() {
    chest.setPlayer(player);
  }

  @Override
  public boolean digHook() {
    for (var entry : trails.entrySet()) {
      if (entry.getValue().getType() == null) {
        continue;
      }
      if (!player.getInventory().hasItem(entry.getKey().getScrollId())) {
        continue;
      }
      if (!entry.getValue().getType().getAction().digHook(player, entry.getKey())) {
        continue;
      }
      return true;
    }
    return false;
  }

  @Override
  public boolean mapObjectOptionHook(int option, MapObject mapObject) {
    for (var entry : trails.entrySet()) {
      if (entry.getValue().getType() == null) {
        continue;
      }
      if (!player.getInventory().hasItem(entry.getKey().getScrollId())) {
        continue;
      }
      if (!entry
          .getValue()
          .getType()
          .getAction()
          .mapObjectOptionHook(player, entry.getKey(), mapObject)) {
        continue;
      }
      return true;
    }
    return false;
  }

  @Override
  public boolean npcOptionHook(int option, Npc npc) {
    for (var entry : trails.entrySet()) {
      if (entry.getValue().getType() == null) {
        continue;
      }
      if (!player.getInventory().hasItem(entry.getKey().getScrollId())) {
        continue;
      }
      if (!entry.getValue().getType().getAction().npcOptionHook(player, entry.getKey(), npc)) {
        continue;
      }
      return true;
    }
    return false;
  }

  public boolean dialogueHook(Dialogue dialogue, int slot) {
    for (var entry : trails.entrySet()) {
      if (entry.getValue().getType() == null) {
        continue;
      }
      if (!player.getInventory().hasItem(entry.getKey().getScrollId())) {
        continue;
      }
      if (!entry
          .getValue()
          .getType()
          .getAction()
          .dialogueHook(player, entry.getKey(), dialogue, slot)) {
        continue;
      }
      return true;
    }
    return false;
  }

  public void stageComplete(ClueScrollType type) {
    if (!player.getInventory().hasItem(type.getScrollId())) {
      return;
    }
    var progress = trails.get(type);
    if (progress == null) {
      return;
    }
    progress.stepComplete();
    if (progress.getRemainingSteps() <= 0) {
      player.openDialogue(
          new ItemMessageDialogue(type.getRewardCasketId(), "You've obtained a casket!"));
      trails.remove(type);
      player.getInventory().deleteItem(type.getScrollId());
      player.getInventory().addOrDropItem(type.getRewardCasketId());
      totalCompleted.put(type, totalCompleted.getOrDefault(type, 0) + 1);
    } else {
      player.openDialogue(new ItemMessageDialogue(type.getScrollId(), "You've found a new clue!"));
    }
  }

  public TreasureTrailProgress resetProgress(ClueScrollType type) {
    trails.put(type, new TreasureTrailProgress(type));
    return trails.get(type);
  }

  public void openClue(ClueScrollType type) {
    if (type == null) {
      return;
    }
    if (!player.getInventory().hasItem(type.getScrollId())) {
      return;
    }
    var progress = trails.get(type);
    if (progress == null) {
      progress = resetProgress(type);
    }
    if (type.getSteps() == -1) {
      stageComplete(type);
      return;
    }
    if (progress.getType() == null) {
      progress.setType();
    }
    if (progress.getType().getWidgetId() == -1) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.CLUE_TEXT);
      player.getGameEncoder().sendWidgetText(WidgetId.CLUE_TEXT, 2, progress.getType().getText());
    } else {
      player.getWidgetManager().sendInteractiveOverlay(progress.getType().getWidgetId());
    }
  }

  public void openCasket(ClueScrollType type) {
    if (type == null) {
      return;
    }
    if (!player.getInventory().hasItem(type.getRewardCasketId())) {
      return;
    }
    player.getInventory().deleteItem(type.getRewardCasketId());
    var rewards = TreasureTrailReward.REWARDS.get(type);
    var items = new ArrayList<Item>();
    items.add(rewards.getUnique(player));
    if (PRandom.randomE(20) == 0) {
      items.add(rewards.getUnique(player));
    }
    if (rewards.getMasterScrollRate() > 0 && PRandom.randomE(rewards.getMasterScrollRate()) == 0) {
      items.add(new Item(ClueScrollType.MASTER.getScrollId()));
      resetProgress(ClueScrollType.MASTER);
    }
    var coinQuantity = rewards.getRandomCoinQuantity();
    if (player.getGameMode().isIronType()) {
      coinQuantity /= 4;
    }
    if (coinQuantity > 0) {
      items.add(new Item(ItemId.COINS, coinQuantity));
    }
    var rolls = rewards.getRandomRolls();
    for (var i = 0; i < rolls; i++) {
      var item = rewards.getCommon(player);
      item.setAmount(
          (int)
              (item.getAmount()
                  * player.getPlugin(BondPlugin.class).getDonatorRank().getMultiplier()));
      items.add(item);
    }
    items.forEach(i -> player.getInventory().addOrDropItem(i));
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.CLUE_REWARD);
    player.getGameEncoder().sendItems(-1, 141, items);
  }
}
