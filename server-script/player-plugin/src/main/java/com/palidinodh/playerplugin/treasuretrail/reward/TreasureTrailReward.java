package com.palidinodh.playerplugin.treasuretrail.reward;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import java.util.List;
import java.util.Map;

public interface TreasureTrailReward {

  List<RandomItem> BASE_COMMON =
      RandomItem.buildList(
          new RandomItem(ItemId.COINS, 20_000, 35_000),
          new RandomItem(ItemId.COINS, 15_000, 30_000),
          new RandomItem(ItemId.COINS, 10_000, 15_000),
          new RandomItem(ItemId.PURPLE_SWEETS, 14, 33),
          new RandomItem(ItemId.PURPLE_SWEETS, 8, 12),
          new RandomItem(ItemId.HOLY_BLESSING),
          new RandomItem(ItemId.UNHOLY_BLESSING),
          new RandomItem(ItemId.PEACEFUL_BLESSING),
          new RandomItem(ItemId.HONOURABLE_BLESSING),
          new RandomItem(ItemId.WAR_BLESSING),
          new RandomItem(ItemId.ANCIENT_BLESSING),
          new RandomItem(ItemId.BEAR_FEET),
          new RandomItem(ItemId.MOLE_SLIPPERS),
          new RandomItem(ItemId.FROG_SLIPPERS),
          new RandomItem(ItemId.DEMON_FEET),
          new RandomItem(ItemId.SANDWICH_LADY_HAT),
          new RandomItem(ItemId.SANDWICH_LADY_TOP),
          new RandomItem(ItemId.SANDWICH_LADY_BOTTOM),
          new RandomItem(ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_GUTHIX),
          new RandomItem(ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_SARADOMIN),
          new RandomItem(ItemId.RUNE_SCIMITAR_ORNAMENT_KIT_ZAMORAK),
          new RandomItem(ItemId.MONKS_ROBE_TOP_T),
          new RandomItem(ItemId.MONKS_ROBE_T),
          new RandomItem(ItemId.AMULET_OF_DEFENCE_T),
          new RandomItem(ItemId.JESTER_CAPE),
          new RandomItem(ItemId.SHOULDER_PARROT));

  Map<ClueScrollType, TreasureTrailReward> REWARDS =
      Map.of(
          ClueScrollType.EASY,
          new EasyReward(),
          ClueScrollType.MEDIUM,
          new MediumReward(),
          ClueScrollType.HARD,
          new HardReward(),
          ClueScrollType.ELITE,
          new EliteReward(),
          ClueScrollType.MASTER,
          new MasterReward());

  Item getCommon(Player player);

  Item getUnique(Player player);

  int getRandomRolls();

  int getRandomCoinQuantity();

  int getMasterScrollRate();
}
