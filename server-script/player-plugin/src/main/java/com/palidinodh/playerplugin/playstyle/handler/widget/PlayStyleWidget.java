package com.palidinodh.playerplugin.playstyle.handler.widget;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.Messaging;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.adaptive.RsDifficultyMode;
import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.EnumMap;
import java.util.Map;

@ReferenceId(WidgetId.PLAY_STYLE_1011)
class PlayStyleWidget implements WidgetHandler {

  private static final Map<RsGameMode, String> GAME_MODE_DESCRIPTIONS =
      new EnumMap<>(RsGameMode.class);
  private static final Map<RsDifficultyMode, String> DIFFICULTY_MODE_DESCRIPTIONS =
      new EnumMap<>(RsDifficultyMode.class);

  static {
    GAME_MODE_DESCRIPTIONS.put(RsGameMode.UNSET, "");
    GAME_MODE_DESCRIPTIONS.put(RsGameMode.REGULAR, "Play without any restrictions.");
    GAME_MODE_DESCRIPTIONS.put(
        RsGameMode.IRONMAN,
        "  * No trading, grand exchange, or staking.<br>  * Less items can be purchased from shops.");
    GAME_MODE_DESCRIPTIONS.put(
        RsGameMode.HARDCORE_IRONMAN,
        "  * No trading, grand exchange, or staking.<br>  * Less items can be purchased from shops.<br>  * On 'unsafe' death, your mode is reverted to ironman.");

    DIFFICULTY_MODE_DESCRIPTIONS.put(RsDifficultyMode.UNSET, "");
    DIFFICULTY_MODE_DESCRIPTIONS.put(
        RsDifficultyMode.NORMAL,
        "  * Combat experience multiplier: x100.<br>  * Skill experience multiplier: x20.  * Drop rate multiplier: x1.2.");
    DIFFICULTY_MODE_DESCRIPTIONS.put(
        RsDifficultyMode.HARD,
        "  * Combat experience multiplier: x20.<br>  * Skill experience multiplier: x20.<br>  * Drop rate multiplier: x1.25.");
    DIFFICULTY_MODE_DESCRIPTIONS.put(
        RsDifficultyMode.ELITE,
        "  * Combat experience multiplier: x5.<br>  * Skill experience multiplier: x5.<br>  * Drop rate multiplier: x1.3.");
  }

  private static boolean canPair(RsGameMode gameMode, RsDifficultyMode difficultyMode) {
    if (gameMode.isUnset() || difficultyMode.isUnset()) {
      return true;
    }
    if (gameMode.isIronType()) {
      return difficultyMode.isHard();
    }
    return true;
  }

  private static boolean canChangeGameMode(RsGameMode currentGameMode, RsGameMode newGameMode) {
    if (newGameMode.isUnset()) {
      return false;
    }
    if (currentGameMode.isUnset()) {
      return true;
    }
    if (currentGameMode == newGameMode) {
      return true;
    }
    switch (currentGameMode) {
      case IRONMAN:
        return newGameMode.isRegular();
      case ULTIMATE_IRONMAN:
      case HARDCORE_IRONMAN:
      case GROUP_IRONMAN:
        return newGameMode.isIronman();
      default:
        return false;
    }
  }

  private static boolean setGameMode(Player player, RsGameMode gameMode) {
    if (gameMode.isUnset()) {
      player.getGameEncoder().sendMessage("Please select a game mode.");
      return false;
    }
    if (!canChangeGameMode(player.getGameMode(), gameMode)) {
      player.getGameEncoder().sendMessage("You can't select this game mode.");
      return false;
    }
    player.putAttribute("game_mode_selected", gameMode);
    if (gameMode.isIronType()) {
      player.putAttribute("difficulty_mode_selected", RsDifficultyMode.HARD);
    }
    return true;
  }

  private static boolean canChangeDifficultyMode(
      RsDifficultyMode currentDifficultyMode, RsDifficultyMode newDifficultyMode) {
    if (currentDifficultyMode.isUnset()) {
      return true;
    }
    if (currentDifficultyMode == newDifficultyMode) {
      return true;
    }
    return currentDifficultyMode.ordinal() > newDifficultyMode.ordinal();
  }

  private static boolean setDifficultyMode(
      Player player, RsGameMode gameMode, RsDifficultyMode difficultyMode) {
    if (difficultyMode.isUnset()) {
      player.getGameEncoder().sendMessage("Please select a difficulty mode.");
      return false;
    }
    if (!canChangeDifficultyMode(player.getDifficultyMode(), difficultyMode)) {
      player
          .getGameEncoder()
          .sendMessage("You can't select a difficulty mode harder than your current mode.");
      return false;
    }
    if (!canPair(gameMode, difficultyMode)) {
      player
          .getGameEncoder()
          .sendMessage("This game mode and difficulty mode can't be paired together.");
      return false;
    }
    if (!player.getGameMode().isUnset()
        && player.getGameMode() != gameMode
        && player.getDifficultyMode() != difficultyMode) {
      player
          .getGameEncoder()
          .sendMessage("You can't change your game mode and difficulty mode at the same time.");
      return false;
    }
    player.putAttribute("difficulty_mode_selected", difficultyMode);
    return true;
  }

  private static void sendModes(Player player) {
    var gameMode = (RsGameMode) player.getAttribute("game_mode_selected");
    if (gameMode != RsGameMode.UNSET) {
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.PLAY_STYLE_1011, 22, gameMode.getFormattedName());
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.PLAY_STYLE_1011, 23, GAME_MODE_DESCRIPTIONS.get(gameMode));
    }
    var difficultyMode = (RsDifficultyMode) player.getAttribute("difficulty_mode_selected");
    if (difficultyMode != RsDifficultyMode.UNSET) {
      player
          .getGameEncoder()
          .sendWidgetText(WidgetId.PLAY_STYLE_1011, 24, difficultyMode.getFormattedName());
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.PLAY_STYLE_1011, 25, DIFFICULTY_MODE_DESCRIPTIONS.get(difficultyMode));
    }
  }

  private static void accept(Player player) {
    var gameMode = (RsGameMode) player.getAttribute("game_mode_selected");
    var difficultyMode = (RsDifficultyMode) player.getAttribute("difficulty_mode_selected");
    if (player.getGameMode() == gameMode && player.getDifficultyMode() == difficultyMode) {
      player.getGameEncoder().sendMessage("No changes have been selected.");
      return;
    }
    if (!setGameMode(player, gameMode)) {
      return;
    }
    if (!setDifficultyMode(player, gameMode, difficultyMode)) {
      return;
    }
    var isFirstTime = player.getGameMode().isUnset() || player.getDifficultyMode().isUnset();
    if (!isFirstTime && player.getBank().needsPinInput(false)) {
      player.getGameEncoder().sendMessage("You need to enter your bank pin to do this.");
      return;
    }
    player.getWidgetManager().removeInteractiveWidgets();
    player.unlock();
    player.setDifficultyMode(difficultyMode);
    player.setGameMode(gameMode);
    if (!isFirstTime) {
      return;
    }
    player.getInventory().addItem(ItemId.STARTER_PACK_32288);
    player.getInventory().addItem(ItemId.COINS, 50_000);
    player.getInventory().addItem(ItemId.MONKFISH_NOTED, 100);
    player.getEquipment().setItem(Equipment.Slot.WEAPON, ItemId.IRON_SCIMITAR, 1);
    player.getEquipment().setItem(Equipment.Slot.NECK, ItemId.AMULET_OF_POWER, 1);
    player.getEquipment().setItem(Equipment.Slot.CAPE, ItemId.AVAS_ATTRACTOR, 1);
    if (player.getGameMode().isIronman() || player.getGameMode().isGroupIronman()) {
      player.getEquipment().setItem(Equipment.Slot.HEAD, ItemId.IRONMAN_HELM, 1);
      player.getEquipment().setItem(Equipment.Slot.CHEST, ItemId.IRONMAN_PLATEBODY, 1);
      player.getEquipment().setItem(Equipment.Slot.LEG, ItemId.IRONMAN_PLATELEGS, 1);
    } else if (player.getGameMode().isHardcoreIronman()) {
      player.getEquipment().setItem(Equipment.Slot.HEAD, ItemId.HARDCORE_IRONMAN_HELM, 1);
      player.getEquipment().setItem(Equipment.Slot.CHEST, ItemId.HARDCORE_IRONMAN_PLATEBODY, 1);
      player.getEquipment().setItem(Equipment.Slot.LEG, ItemId.HARDCORE_IRONMAN_PLATELEGS, 1);
    } else {
      player.getEquipment().setItem(Equipment.Slot.CHEST, ItemId.MONKS_ROBE_TOP, 1);
      player.getEquipment().setItem(Equipment.Slot.LEG, ItemId.MONKS_ROBE, 1);
      player.getEquipment().setItem(Equipment.Slot.NECK, ItemId.AMULET_OF_GLORY_4, 1);
    }
    player.getEquipment().setItem(Equipment.Slot.HAND, ItemId.MITHRIL_GLOVES, 1);
    player.getEquipment().setItem(Equipment.Slot.FOOT, ItemId.CLIMBING_BOOTS, 1);
    player.getEquipment().setItem(Equipment.Slot.SHIELD, ItemId.UNHOLY_BOOK, 1);
    player.getEquipment().weaponUpdate(true);
    player.getEquipment().setUpdate(true);
    player.getAppearance().setUpdate(true);
    player
        .getGameEncoder()
        .sendMessage(
            "If you're not sure where to start, begin with the Training Day achievement diary!",
            Messaging.CHAT_TYPE_BROADCAST);
  }

  @Override
  public boolean isLockedUsable() {
    return true;
  }

  @Override
  public void onOpen(Player player) {
    player.putAttribute("game_mode_selected", player.getGameMode());
    player.putAttribute("difficulty_mode_selected", player.getDifficultyMode());
    sendModes(player);
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var gameMode = (RsGameMode) player.getAttribute("game_mode_selected");
    switch (childId) {
      case 29:
        setGameMode(player, RsGameMode.REGULAR);
        break;
      case 32:
        setGameMode(player, RsGameMode.IRONMAN);
        break;
      case 35:
        setGameMode(player, RsGameMode.HARDCORE_IRONMAN);
        break;
      case 44:
        setDifficultyMode(player, gameMode, RsDifficultyMode.NORMAL);
        break;
      case 47:
        setDifficultyMode(player, gameMode, RsDifficultyMode.HARD);
        break;
      case 50:
        setDifficultyMode(player, gameMode, RsDifficultyMode.ELITE);
        break;
      case 18:
        accept(player);
        return;
    }
    sendModes(player);
  }
}
