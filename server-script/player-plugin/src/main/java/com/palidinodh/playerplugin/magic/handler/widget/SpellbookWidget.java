package com.palidinodh.playerplugin.magic.handler.widget;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.playerplugin.magic.SpellOnItem;
import com.palidinodh.playerplugin.magic.SpellOnMapObject;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PTime;

@ReferenceId(WidgetId.SPELLBOOK)
class SpellbookWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var spellbookChild = SpellbookChild.get(childId);
    if (spellbookChild == null) {
      return;
    }
    var spellTeleport = SpellTeleport.get(spellbookChild);
    if (spellTeleport != null) {
      spellTeleport.teleport(player);
      return;
    }
    switch (spellbookChild) {
      case SPELL_FILTERS:
        if (slot == 0) {
          player.getMagic().setShowCombatSpells(!player.getMagic().getShowCombatSpells());
        } else if (slot == 1) {
          player.getMagic().setShowTeleportSpells(!player.getMagic().getShowTeleportSpells());
        } else if (slot == 2) {
          player.getMagic().setShowUtilitySpells(!player.getMagic().getShowUtilitySpells());
        } else if (slot == 3) {
          player.getMagic().setShowLackLevelSpells(!player.getMagic().getShowLackLevelSpells());
        } else if (slot == 4) {
          player.getMagic().setShowLackRunesSpells(!player.getMagic().getShowLackRunesSpells());
        }
        break;
      case TELEPORT_TO_BOUNTY_TARGET:
        var plugin = player.getPlugin(BountyHunterPlugin.class);
        if (option == 0) {
          plugin.selectTeleportToTarget(false);
        } else if (option == 9) {
          plugin.setTeleportWarning(!plugin.isTeleportWarning());
          player.getGameEncoder().sendMessage("Teleport warning: " + plugin.isTeleportWarning());
        }
        break;
      case CHARGE:
        if (player.getSkills().getLevel(Skills.MAGIC) < 80) {
          player
              .getGameEncoder()
              .sendMessage("Your Magic level is not high enough for this spell.");
          break;
        } else if (!player.getMagic().hasRunes(ItemId.FIRE_RUNE, 3)
            || !player.getMagic().hasRunes(ItemId.BLOOD_RUNE, 3)
            || !player.getMagic().hasRunes(ItemId.AIR_RUNE, 3)) {
          player.getMagic().notEnoughRunes();
          break;
        } else if (player.getMagic().getChargeDelay() > 500) {
          player
              .getGameEncoder()
              .sendMessage("You can't recast that yet, your current Charge is too strong.");
          break;
        }
        player.getMagic().deleteRunes(ItemId.FIRE_RUNE, 3);
        player.getMagic().deleteRunes(ItemId.BLOOD_RUNE, 3);
        player.getMagic().deleteRunes(ItemId.AIR_RUNE, 3);
        player.setAnimation(811);
        player.setGraphic(308, 124, 45);
        player.getMagic().setChargeDelay(600);
        player.getGameEncoder().sendMessage("<col=ef1020>You feel charged with magic power.</col>");
        AchievementDiary.castSpellUpdate(player, spellbookChild, null, null, null);
        break;
      case MAGIC_IMBUE:
        if (player.getSkills().getLevel(Skills.MAGIC) < 82) {
          player.getGameEncoder().sendMessage("You need a Magic level of 82 to cast Magic Imbue.");
          break;
        } else if (!player.getMagic().hasRunes(ItemId.FIRE_RUNE, 7)
            || !player.getMagic().hasRunes(ItemId.WATER_RUNE, 7)
            || !player.getMagic().hasRunes(ItemId.ASTRAL_RUNE, 2)) {
          player.getMagic().notEnoughRunes();
          break;
        } else if (player.getMagic().getMagicImbueTime() > 0) {
          player
              .getGameEncoder()
              .sendMessage(
                  "You can cast Magic Imbue in "
                      + PTime.tickToSec(player.getMagic().getMagicImbueTime())
                      + " seconds.");
          break;
        }
        player.getGameEncoder().sendMessage("You are charged to combine runes!");
        player.getMagic().deleteRunes(ItemId.FIRE_RUNE, 7);
        player.getMagic().deleteRunes(ItemId.WATER_RUNE, 7);
        player.getMagic().deleteRunes(ItemId.ASTRAL_RUNE, 2);
        player.getSkills().addXp(Skills.MAGIC, 86);
        player.getMagic().setMagicImbueTime(20);
        player.setGraphic(141, 100);
        player.setAnimation(722);
        AchievementDiary.castSpellUpdate(player, spellbookChild, null, null, null);
        break;
      case VENGEANCE:
        if (player.getSkills().getLevel(Skills.MAGIC) < 94
            || player.getController().getLevelForXP(Skills.DEFENCE) < 40) {
          player
              .getGameEncoder()
              .sendMessage(
                  "You need a Magic level of 94 and Defence level of 40 to cast Vengeance.");
          break;
        } else if (!player.getMagic().hasRunes(ItemId.EARTH_RUNE, 10)
            || !player.getMagic().hasRunes(ItemId.DEATH_RUNE, 2)
            || !player.getMagic().hasRunes(ItemId.ASTRAL_RUNE, 4)) {
          player.getMagic().notEnoughRunes();
          break;
        } else if (player.getMagic().getVengeanceDelay() > 0) {
          player
              .getGameEncoder()
              .sendMessage(
                  "You can cast Vengeance in "
                      + PTime.tickToSec(player.getMagic().getVengeanceDelay())
                      + " seconds.");
          break;
        } else if (player.getMagic().getVengeanceCast()) {
          player.getGameEncoder().sendMessage("You already have Vengeance cast.");
          break;
        }
        player.getMagic().deleteRunes(ItemId.EARTH_RUNE, 10);
        player.getMagic().deleteRunes(ItemId.DEATH_RUNE, 2);
        player.getMagic().deleteRunes(ItemId.ASTRAL_RUNE, 4);
        player.getSkills().addXp(Skills.MAGIC, 112);
        player.getMagic().setVengeanceCast(true);
        player.getMagic().setVengeanceDelay(Magic.VENGEANCE_DELAY);
        player.setGraphic(726, 100);
        player.setAnimation(8316);
        player.getController().sendMapSound(2907);
        AchievementDiary.castSpellUpdate(player, spellbookChild, null, null, null);
        break;
      case GEOMANCY:
        if (player.getSkills().getLevel(Skills.MAGIC) < 65) {
          player.getGameEncoder().sendMessage("You need a Magic level of 65 to cast this spell.");
          break;
        } else if (!player.getMagic().hasRunes(ItemId.ASTRAL_RUNE, 3)
            || !player.getMagic().hasRunes(ItemId.NATURE_RUNE, 3)
            || !player.getMagic().hasRunes(ItemId.EARTH_RUNE, 8)) {
          player.getMagic().notEnoughRunes();
          break;
        } else if (player.getMagic().getGeomancyTime() > 0) {
          player
              .getGameEncoder()
              .sendMessage(
                  "You can cast Geomancy in "
                      + PTime.tickToSec(player.getMagic().getGeomancyTime())
                      + " seconds.");
          break;
        }
        player.getMagic().deleteRunes(ItemId.ASTRAL_RUNE, 3);
        player.getMagic().deleteRunes(ItemId.NATURE_RUNE, 3);
        player.getMagic().deleteRunes(ItemId.EARTH_RUNE, 8);
        player.getSkills().addXp(Skills.MAGIC, 60);
        player.getMagic().setGeomancyTime(20);
        player.getFarming().openGeomancy();
        AchievementDiary.castSpellUpdate(player, spellbookChild, null, null, null);
        break;
      default:
        break;
    }
  }

  @Override
  public boolean widgetOnWidget(
      Player player,
      int useWidgetId,
      int useChildId,
      int onWidgetId,
      int onChildId,
      int useSlot,
      int useItemId,
      int onSlot,
      int onItemId) {
    if (onWidgetId == WidgetId.INVENTORY) {
      var spellbookChild = SpellbookChild.get(useChildId);
      if (spellbookChild == null) {
        return true;
      }
      var spellOnItem = SpellOnItem.get(spellbookChild);
      if (spellOnItem != null) {
        return spellOnItem.onItem(player, onSlot, onItemId);
      }
      return true;
    }
    return false;
  }

  @Override
  public void widgetOnMapObject(
      Player player, int widgetId, int childId, int slot, MapObject mapObject) {
    var spellbookChild = SpellbookChild.get(childId);
    if (spellbookChild == null) {
      return;
    }
    var spellOnMapObject = SpellOnMapObject.get(spellbookChild);
    if (spellOnMapObject != null) {
      spellOnMapObject.onMapObject(player, mapObject);
    }
  }
}
