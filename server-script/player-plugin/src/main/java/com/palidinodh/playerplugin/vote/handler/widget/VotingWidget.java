package com.palidinodh.playerplugin.vote.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId(WidgetId.VOTING_1020)
class VotingWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (childId) {
      case 24:
        player.getGameEncoder().sendOpenUrl(Settings.getInstance().getVoteUrl());
        break;
      case 26:
        player.openShop("vote");
        break;
    }
  }
}
