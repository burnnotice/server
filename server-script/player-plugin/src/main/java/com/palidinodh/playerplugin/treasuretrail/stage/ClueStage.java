package com.palidinodh.playerplugin.treasuretrail.stage;

import com.palidinodh.playerplugin.treasuretrail.action.ClueScrollAction;
import com.palidinodh.util.PLogger;
import java.util.HashMap;
import java.util.Map;

public interface ClueStage {

  Map<String, ClueStage> TYPES = new HashMap<>();
  InitializeTreasureTrailStage INITIALIZE = new InitializeTreasureTrailStage();

  String name();

  default boolean isWilderness() {
    return name().contains("WILDERNESS");
  }

  static void addStage(ClueStage stage) {
    if (TYPES.containsKey(stage.name())) {
      throw new RuntimeException(
          TYPES.getClass().getName() + " - " + stage.name() + ": clue scroll stage already used.");
    }
    TYPES.put(stage.name(), stage);
  }

  static void load(boolean force) {
    if (!force && !TYPES.isEmpty()) {
      return;
    }
    TYPES.clear();
    try {
      for (var stage : AnagramClueStageType.values()) {
        addStage(stage);
      }
      for (var stage : CoordinateClueStageType.values()) {
        addStage(stage);
      }
      for (var stage : CrypticClueStageType.values()) {
        addStage(stage);
      }
      for (var stage : MapClueStageType.values()) {
        addStage(stage);
      }
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  int getWidgetId();

  String getText();

  ClueScrollAction getAction();

  class InitializeTreasureTrailStage {

    static {
      load(true);
    }
  }
}
