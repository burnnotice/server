package com.palidinodh.playerplugin.clanwars;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.ObjectDefinition;
import com.palidinodh.cache.id.NullObjectId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Messaging;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class ClanWarsPlugin implements PlayerPlugin {

  public static final int COUNT_DOWN = 200;

  @Inject private transient Player player;
  @Getter @Setter private transient Player opponent;
  @Getter @Setter private transient Player teammate;
  @Getter @Setter private transient ClanWarsPlayerState state = ClanWarsPlayerState.NONE;
  @Getter @Setter private transient CompletedState completed;
  @Getter @Setter private transient int countdown;
  @Getter private transient int[] rules;
  @Getter private transient boolean isTop;
  @Getter private transient int time;
  @Getter private transient int totalKills;
  private transient boolean inClanWarsChallengeArea;
  private transient boolean inTournamentPlayerStateArea;
  @Setter private transient int tournamentFightDelay;

  @Getter private int tournamentWins;
  @Getter @Setter private int points;

  @Override
  public int getCurrency(String identifier) {
    if (identifier.equals("tournament points")) {
      return points;
    }
    return 0;
  }

  @Override
  public void changeCurrency(String identifier, int amount) {
    if (identifier.equals("tournament points")) {
      points += amount;
    }
  }

  @Override
  public void tick() {
    if (inClanWarsChallengeArea != player.inClanWarsChallengeArea()) {
      inClanWarsChallengeArea = player.inClanWarsChallengeArea();
      player
          .getGameEncoder()
          .sendPlayerOption(inClanWarsChallengeArea ? "Challenge" : "null", 1, false);
    }
    var inClanWarsTournamentStatusArea = player.inClanWarsTournamentStatusArea();
    if (inTournamentPlayerStateArea
        || inTournamentPlayerStateArea != inClanWarsTournamentStatusArea) {
      inTournamentPlayerStateArea = inClanWarsTournamentStatusArea;
      if (inTournamentPlayerStateArea) {
        if (player.getWidgetManager().getOverlay() != WidgetId.LMS_LOBBY_OVERLAY) {
          player.getWidgetManager().sendOverlay(WidgetId.LMS_LOBBY_OVERLAY);
          player.getGameEncoder().sendWidgetText(WidgetId.LMS_LOBBY_OVERLAY, 7, "");
          player.getGameEncoder().sendWidgetText(WidgetId.LMS_LOBBY_OVERLAY, 9, "");
          player.getGameEncoder().sendWidgetText(WidgetId.LMS_LOBBY_OVERLAY, 11, "");
        }
      } else if (!inTournamentPlayerStateArea
          && player.getWidgetManager().getOverlay() == WidgetId.LMS_LOBBY_OVERLAY) {
        player.getWidgetManager().removeOverlay();
      }
    }
    if (countdown > 0) {
      countdown--;
      if (state == ClanWarsPlayerState.BATTLE) {
        if (countdown == 4) {
          loadBarrier(BarrierState.DROP);
        } else if (countdown == 0) {
          loadBarrier(BarrierState.DELETE);
          player.getGameEncoder().sendMessage("<col=ff0000>The war has begun!");
        }
      }
    }
    if (state == ClanWarsPlayerState.BATTLE) {
      time++;
    }
    if (tournamentFightDelay > 0) {
      tournamentFightDelay--;
      if (tournamentFightDelay == 0) {
        player.setForceMessage("FIGHT!");
      } else if (tournamentFightDelay % 2 == 0) {
        player.setForceMessage(String.valueOf(PTime.tickToSec(tournamentFightDelay)));
      }
    }
  }

  @Override
  public boolean playerOptionHook(int option, Player player2) {
    if (option == 0 && player.inClanWarsChallengeArea() && player2.inClanWarsChallengeArea()) {
      if (state != ClanWarsPlayerState.NONE
          || player2.getPlugin(ClanWarsPlugin.class).getState() != ClanWarsPlayerState.NONE) {
        return true;
      }
      if (!player.getMessaging().canClanChatEvent()) {
        player
            .getGameEncoder()
            .sendMessage("Your Clan Chat privledges aren't high enough to do that.");
        return true;
      } else if (!player2.getMessaging().canClanChatEvent()) {
        player
            .getGameEncoder()
            .sendMessage("Their Clan Chat privledges aren't high enough to do that.");
        return true;
      }
      opponent = player2;
      if (player == player2.getPlugin(ClanWarsPlugin.class).getOpponent()) {
        ClanWarsStages.openRuleSelection(player, this);
        ClanWarsStages.openRuleSelection(player2, player2.getPlugin(ClanWarsPlugin.class));
      } else {
        player.getGameEncoder().sendMessage("Sending Clan Wars challenge...");
        player2
            .getGameEncoder()
            .sendMessage(
                player.getUsername() + " wishes to challenge your clan to a Clan War.",
                Messaging.CHAT_TYPE_DUEL,
                player.getUsername());
      }
      return true;
    }
    return false;
  }

  public void startWar(boolean startInstance, boolean isTop) {
    if (opponent == null) {
      return;
    }
    state = ClanWarsPlayerState.BATTLE;
    countdown = COUNT_DOWN;
    time = 0;
    this.isTop = isTop;
    player.rejuvenate();
    player.setController(new ClanWarsPC());
    if (startInstance) {
      player.getController().startInstance();
    } else {
      player.getController().joinInstance(opponent.getController());
    }
    loadBarrier(BarrierState.LOAD);
    teleport();
    player.getWidgetManager().removeInteractiveWidgets();
  }

  public void joinWar() {
    if (teammate == null) {
      return;
    }
    var plugin = teammate.getPlugin(ClanWarsPlugin.class);
    isTop = plugin.isTop();
    countdown = plugin.getCountdown();
    time = plugin.getTime();
    totalKills = plugin.getTotalKills();
    player.setController(new ClanWarsPC());
    player.getController().joinInstance(teammate.getController());
    player.getWidgetManager().removeInteractiveWidgets();
  }

  public void cancel() {
    opponent = null;
    teammate = null;
    state = ClanWarsPlayerState.NONE;
    rules = null;
    isTop = false;
    time = 0;
    countdown = 0;
    completed = CompletedState.NONE;
    totalKills = 0;
  }

  public void teleport() {
    if (rules == null) {
      return;
    }
    player.getMovement().teleport(getArena().getArenaTile(isTop));
  }

  public void teleportViewing() {
    if (rules == null) {
      return;
    }
    player.getMovement().teleport(getArena().getViewTile(isTop));
  }

  public void loadBarrier(BarrierState stage) {
    List<MapObject> barriers = null;
    var alreadyLoaded = false;
    if (getArena().getBarrierObjectId() == -1) {
      return;
    }
    for (var i = getArena().getBarrierStartX(); i <= getArena().getBarrierEndX(); i++) {
      if (ruleSelected(ClanWarsRule.ARENA, ClanWarsRuleOption.SYLVAN_GLADE)
          && i > 3419
          && i < 3428) {
        continue;
      } else if (ruleSelected(ClanWarsRule.ARENA, ClanWarsRuleOption.FORSAKEN_QUARRY)
          && i > 3415
          && i < 3432) {
        continue;
      }
      var barrier =
          new MapObject(
              getArena().getBarrierObjectId(),
              10,
              MapObject.getRandomDirection(),
              new Tile(i, getArena().getBarrierY()));
      if (stage == BarrierState.DROP) {
        barrier.setId(getArena().getBarrierObjectId() + 1);
      } else if (stage == BarrierState.DELETE) {
        barrier.setId(-1);
      }
      var region = player.getController().getRegion(barrier.getRegionId(), true);
      var current = region.getSolidMapObject(i, getArena().getBarrierY(), 0);
      if (current != null && current.getId() == barrier.getId()) {
        alreadyLoaded = true;
        break;
      }
      if (stage != BarrierState.DELETE
          && current != null
          && current.getId() != getArena().getBarrierObjectId()
          && current.getId() + 1 != getArena().getBarrierObjectId()) {
        continue;
      }
      if (current != null) {
        barrier.setDirection(current.getDirection());
      }
      if (current != null) {
        region.addMapObject(new MapObject(-1, current));
      }
      if (stage != BarrierState.DELETE) {
        region.addMapObject(barrier);
      }
      if (barriers == null) {
        barriers = new ArrayList<>();
      }
      barriers.add(barrier);
    }
    if (alreadyLoaded) {
      return;
    }
    var players = player.getController().getPlayers();
    if (players.isEmpty() || barriers == null) {
      return;
    }
    for (var nearbyPlayer : players) {
      for (var barrier : barriers) {
        if (!nearbyPlayer.withinMapDistance(barrier)) {
          continue;
        }
        nearbyPlayer.getGameEncoder().sendMapObject(barrier);
      }
    }
  }

  public boolean ruleSelected(ClanWarsRule rule, ClanWarsRuleOption option) {
    if (state == ClanWarsPlayerState.NONE || rules == null || rule == null || option == null) {
      return false;
    }
    return rules[rule.ordinal()] == option.getIndex();
  }

  public void changeRule(ClanWarsRule rule, ClanWarsRuleOption option) {
    if (rule == null || option == null || !rule.hasOption(option)) {
      return;
    }
    changeRule(rule, option.getIndex());
  }

  public void changeRule(ClanWarsRule rule, int slot) {
    if (state != ClanWarsPlayerState.RULE_SELECTION
        && state != ClanWarsPlayerState.ACCEPT_RULE_SELECTION) {
      return;
    }
    if (opponent == null || opponent.isLocked()) {
      return;
    }
    if (!player.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_OPTIONS)
        || !opponent.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_OPTIONS)) {
      return;
    }
    if (opponent.getPlugin(ClanWarsPlugin.class).getState() != ClanWarsPlayerState.RULE_SELECTION
        && opponent.getPlugin(ClanWarsPlugin.class).getState()
            != ClanWarsPlayerState.ACCEPT_RULE_SELECTION) {
      return;
    }
    if (rule == null || slot < 0 || slot >= rule.getOptions().size()) {
      return;
    }
    rules[rule.ordinal()] = slot;
    opponent.getPlugin(ClanWarsPlugin.class).setRule(rule.ordinal(), slot);
    player.getGameEncoder().setVarbit(rule.getVarbit(), slot);
    opponent.getGameEncoder().setVarbit(rule.getVarbit(), slot);
    state = ClanWarsPlayerState.RULE_SELECTION;
    opponent.getPlugin(ClanWarsPlugin.class).setState(ClanWarsPlayerState.RULE_SELECTION);
  }

  public void sendBattleVarbits(int myTeamCount, int otherTeamCount, Player opponent) {
    player.getGameEncoder().setVarbit(VarbitId.CLAN_WARS_COUNTDOWN, countdown);
    player.getGameEncoder().setVarbit(VarbitId.CLAN_WARS_TEAMMATES, myTeamCount);
    player.getGameEncoder().setVarbit(VarbitId.CLAN_WARS_OPPONENTS, otherTeamCount);
    player.getGameEncoder().setVarbit(VarbitId.CLAN_WARS_TEAM_KILLS, totalKills);
    player
        .getGameEncoder()
        .setVarbit(
            VarbitId.CLAN_WARS_OPPONENT_KILLS,
            opponent.getPlugin(ClanWarsPlugin.class).getTotalKills());
  }

  public void teleportViewing(int option) {
    if (player.isLocked() || !player.getMovement().isTeleportStateNone()) {
      return;
    }
    Tile tile;
    if (state == ClanWarsPlayerState.NONE
        || state != ClanWarsPlayerState.VIEW
        || option < 0
        || getArena().getOrbs().length > 4) {
      return;
    }
    if (!player.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_ORBS)) {
      player.getWidgetManager().sendInventoryOverlay(WidgetId.CLAN_WARS_ORBS);
    }
    player
        .getGameEncoder()
        .sendScript(
            ScriptId.CLANWARS_VIEW_SETUP,
            ObjectDefinition.getDefinition(NullObjectId.NULL_26742).getFirstModelId(),
            option);
    tile = getArena().getOrbs()[option];
    if (tile == null) {
      return;
    }
    player.getMovement().setViewing(tile.getX(), tile.getY(), tile.getHeight());
  }

  public Arena getArena() {
    return Arena.get(rules[ClanWarsRule.ARENA.ordinal()]);
  }

  public ClanWarsRuleOption getRule(ClanWarsRule rule) {
    if (state == ClanWarsPlayerState.NONE || rules == null || rule == null) {
      return null;
    }
    return rule.getOptions().get(rules[rule.ordinal()]);
  }

  public void setRule(int option, int slot) {
    rules[option] = slot;
  }

  public void setRules(int[] rules) {
    if (this.rules == null) {
      this.rules = new int[ClanWarsRule.TOTAL];
    }
    System.arraycopy(rules, 0, this.rules, 0, ClanWarsRule.TOTAL);
  }

  public void incrimentTournamentWins() {
    tournamentWins++;
  }

  public void incrimentTotalKills() {
    totalKills++;
  }
}
