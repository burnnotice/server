package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class IkkleHydraPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.IKKLE_HYDRA, NpcId.IKKLE_HYDRA_8517, NpcId.IKKLE_HYDRA));
    builder.entry(
        new Pet.Entry(ItemId.IKKLE_HYDRA_22748, NpcId.IKKLE_HYDRA_8518, NpcId.IKKLE_HYDRA_8493));
    builder.entry(
        new Pet.Entry(ItemId.IKKLE_HYDRA_22750, NpcId.IKKLE_HYDRA_8519, NpcId.IKKLE_HYDRA_8494));
    builder.entry(
        new Pet.Entry(ItemId.IKKLE_HYDRA_22752, NpcId.IKKLE_HYDRA_8520, NpcId.IKKLE_HYDRA_8495));
    return builder;
  }
}
