package com.palidinodh.skill.cooking;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.RandomItem;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

class CookingEntries {

  @Getter private static List<SkillEntry> entries = load();

  private static List<SkillEntry> load() {
    var list = new ArrayList<SkillEntry>();

    var entry = SkillEntry.builder();
    entry
        .level(1)
        .failFactor(33)
        .experience(30)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_SHRIMPS))
        .create(new RandomItem(ItemId.SHRIMPS))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(1)
        .failFactor(33)
        .experience(30)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_ANCHOVIES))
        .create(new RandomItem(ItemId.ANCHOVIES))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(1)
        .failFactor(40)
        .experience(35)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_SARDINE))
        .create(new RandomItem(ItemId.SARDINE))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_369));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(15)
        .failFactor(50)
        .experience(70)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_TROUT))
        .create(new RandomItem(ItemId.TROUT))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_343));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(20)
        .failFactor(59)
        .experience(80)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_PIKE))
        .create(new RandomItem(ItemId.PIKE))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_343));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(25)
        .failFactor(58)
        .experience(70)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_SALMON))
        .create(new RandomItem(ItemId.SALMON))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_343));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(30)
        .failFactor(99)
        .experience(190)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_KARAMBWAN))
        .create(new RandomItem(ItemId.COOKED_KARAMBWAN))
        .failedCreate(new RandomItem(ItemId.BURNT_KARAMBWAN));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(30)
        .failFactor(64)
        .experience(80)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_TUNA))
        .create(new RandomItem(ItemId.TUNA))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_367));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(40)
        .failFactor(74)
        .experience(90)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_LOBSTER))
        .create(new RandomItem(ItemId.LOBSTER))
        .failedCreate(new RandomItem(ItemId.BURNT_LOBSTER));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(43)
        .failFactor(80)
        .experience(130)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_BASS))
        .create(new RandomItem(ItemId.BASS))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_367));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(45)
        .failFactor(86)
        .experience(140)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_SWORDFISH))
        .create(new RandomItem(ItemId.SWORDFISH))
        .failedCreate(new RandomItem(ItemId.BURNT_SWORDFISH));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(62)
        .failFactor(90)
        .experience(150)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_MONKFISH))
        .create(new RandomItem(ItemId.MONKFISH))
        .failedCreate(new RandomItem(ItemId.BURNT_MONKFISH));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(82)
        .failFactor(104)
        .experience(211)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_SEA_TURTLE))
        .create(new RandomItem(ItemId.SEA_TURTLE))
        .failedCreate(new RandomItem(ItemId.BURNT_SEA_TURTLE));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(80)
        .failFactor(104)
        .experience(210)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_SHARK))
        .create(new RandomItem(ItemId.SHARK))
        .failedCreate(new RandomItem(ItemId.BURNT_SHARK));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(84)
        .failFactor(108)
        .experience(230)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_ANGLERFISH))
        .create(new RandomItem(ItemId.ANGLERFISH))
        .failedCreate(new RandomItem(ItemId.BURNT_ANGLERFISH));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(90)
        .failFactor(114)
        .experience(215)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_DARK_CRAB))
        .create(new RandomItem(ItemId.DARK_CRAB))
        .failedCreate(new RandomItem(ItemId.BURNT_DARK_CRAB));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(91)
        .failFactor(114)
        .experience(216)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_MANTA_RAY))
        .create(new RandomItem(ItemId.MANTA_RAY))
        .failedCreate(new RandomItem(ItemId.BURNT_MANTA_RAY));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(35)
        .failFactor(68)
        .experience(200)
        .animation(3283)
        .consume(new RandomItem(ItemId.GRAPES))
        .consume(new RandomItem(ItemId.JUG_OF_WATER))
        .create(new RandomItem(ItemId.JUG_OF_WINE))
        .failedCreate(new RandomItem(ItemId.JUG_OF_BAD_WINE));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(70)
        .experience(80)
        .animation(6702)
        .tool(new RandomItem(ItemId.KNIFE))
        .consume(new RandomItem(ItemId.LEAPING_STURGEON));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(72)
        .experience(100)
        .animation(1248)
        .tool(new RandomItem(ItemId.KNIFE))
        .consume(new RandomItem(ItemId.SACRED_EEL));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .animation(1248)
        .tool(new RandomItem(ItemId.HAMMER))
        .consume(new RandomItem(ItemId.INFERNAL_EEL));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.SEAWEED))
        .create(new RandomItem(ItemId.MOLTEN_GLASS));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(10)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_PYSK_FISH_0))
        .create(new RandomItem(ItemId.PYSK_FISH_0))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_20854));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(15)
        .experience(15)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_SUPHI_FISH_1))
        .create(new RandomItem(ItemId.SUPHI_FISH_1))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_20854));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(30)
        .experience(20)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_LECKISH_FISH_2))
        .create(new RandomItem(ItemId.LECKISH_FISH_2))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_20854));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(45)
        .experience(25)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_BRAWK_FISH_3))
        .create(new RandomItem(ItemId.BRAWK_FISH_3))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_20854));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(60)
        .experience(30)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_MYCIL_FISH_4))
        .create(new RandomItem(ItemId.MYCIL_FISH_4))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_20854));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(75)
        .experience(35)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_ROQED_FISH_5))
        .create(new RandomItem(ItemId.ROQED_FISH_5))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_20854));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(90)
        .experience(38)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_KYREN_FISH_6))
        .create(new RandomItem(ItemId.KYREN_FISH_6))
        .failedCreate(new RandomItem(ItemId.BURNT_FISH_20854));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(10)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_GUANIC_BAT_0))
        .create(new RandomItem(ItemId.GUANIC_BAT_0))
        .failedCreate(new RandomItem(ItemId.BURNT_BAT));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(15)
        .experience(15)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_PRAEL_BAT_1))
        .create(new RandomItem(ItemId.PRAEL_BAT_1))
        .failedCreate(new RandomItem(ItemId.BURNT_BAT));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(30)
        .experience(20)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_GIRAL_BAT_2))
        .create(new RandomItem(ItemId.GIRAL_BAT_2))
        .failedCreate(new RandomItem(ItemId.BURNT_BAT));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(45)
        .experience(25)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_PHLUXIA_BAT_3))
        .create(new RandomItem(ItemId.PHLUXIA_BAT_3))
        .failedCreate(new RandomItem(ItemId.BURNT_BAT));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(60)
        .experience(30)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_KRYKET_BAT_4))
        .create(new RandomItem(ItemId.KRYKET_BAT_4))
        .failedCreate(new RandomItem(ItemId.BURNT_BAT));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(75)
        .experience(35)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_MURNG_BAT_5))
        .create(new RandomItem(ItemId.MURNG_BAT_5))
        .failedCreate(new RandomItem(ItemId.BURNT_BAT));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(90)
        .experience(38)
        .animation(883)
        .itemOnMapObject(ObjectId.COOKING_RANGE)
        .consume(new RandomItem(ItemId.RAW_PSYKK_BAT_6))
        .create(new RandomItem(ItemId.PSYKK_BAT_6))
        .failedCreate(new RandomItem(ItemId.BURNT_BAT));
    list.add(entry.build());

    return list;
  }
}
