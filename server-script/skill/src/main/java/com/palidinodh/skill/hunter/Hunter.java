package com.palidinodh.skill.hunter;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.hunter.HunterPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import java.util.List;

public class Hunter extends SkillContainer {

  private static final Item MAGIC_BUTTERFLY_NET = new Item(ItemId.MAGIC_BUTTERFLY_NET);

  @Override
  public int getSkillId() {
    return Skills.HUNTER;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return HunterEntries.getEntries();
  }

  @Override
  public int getDefaultMakeAmount() {
    return 1;
  }

  @Override
  public void actionSuccess(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null) {
      npc.getCombat().startDeath(2);
    }
  }

  @Override
  public Item createHook(Player player, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (entry.getConsume() != null && entry.getConsume().getId() == ItemId.IMPLING_JAR) {
      player
          .getGameEncoder()
          .sendFilteredMessage("You manage to catch the impling and squeeze it into a jar.");
    } else if (entry.getConsume() != null && entry.getConsume().getId() == ItemId.BUTTERFLY_JAR) {
      player
          .getGameEncoder()
          .sendFilteredMessage("You manage to catch the butterfly and squeeze it into a jar.");
    }
    return item;
  }

  @Override
  public int experienceHook(
      Player player, int experience, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().wearingLarupiaOutfit()) {
      experience *= 1.1;
    }
    return experience;
  }

  @Override
  public Item toolHook(Player player, Item tool, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (tool.getId() == ItemId.BUTTERFLY_NET
        && player.getEquipment().getWeaponId() == ItemId.MAGIC_BUTTERFLY_NET) {
      return MAGIC_BUTTERFLY_NET;
    }
    return tool;
  }

  @Override
  public boolean skipActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    var power = player.getSkills().getLevel(getSkillId()) + 8;
    var failure = entry.getLevel() + 2;
    if (player.inWildernessResourceArea()) {
      failure /= 2;
    }
    var chance = 0;
    if (power > failure) {
      chance = (int) ((1 - (failure + 2) / ((power + 1) * 2.0)) * 100);
    } else {
      chance = (int) ((power / ((failure + 1) * 2.0)) * 100);
    }
    chance *= 1 + getMembershipBoost(player);
    if (npc != null && player.getEquipment().getWeaponId() == ItemId.MAGIC_BUTTERFLY_NET) {
      chance += 10;
    }
    if (player.getEquipment().wearingLarupiaOutfit()) {
      chance += 10;
    }
    if (player.hasVoted()) {
      chance += 5;
    }
    chance = Math.min(chance, 100);
    return PRandom.randomE(100) > chance;
  }

  @Override
  public boolean mapObjectOptionHook(Player player, int option, MapObject mapObject) {
    switch (mapObject.getName().toLowerCase()) {
      case "bird snare":
      case "net trap":
      case "shaking box":
      case "box trap":
      case "young tree":
        player.getPlugin(HunterPlugin.class).pickupTrap(mapObject);
        return true;
    }
    switch (mapObject.getId()) {
      case ObjectId.YOUNG_TREE_8732:
      case ObjectId.YOUNG_TREE_8990:
      case ObjectId.YOUNG_TREE_8999:
      case ObjectId.YOUNG_TREE_9000:
      case ObjectId.YOUNG_TREE_9341:
        player.getPlugin(HunterPlugin.class).layTrap(-1, mapObject);
        return true;
    }
    return false;
  }
}
