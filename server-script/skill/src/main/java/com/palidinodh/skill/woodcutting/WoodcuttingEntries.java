package com.palidinodh.skill.woodcutting;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.entity.player.skill.SkillModel;
import com.palidinodh.osrscore.model.entity.player.skill.SkillPet;
import com.palidinodh.osrscore.model.entity.player.skill.SkillTemporaryMapObject;
import com.palidinodh.osrscore.model.item.RandomItem;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

class WoodcuttingEntries {

  @Getter private static List<SkillEntry> entries = load();

  private static List<SkillEntry> load() {
    var list = new ArrayList<SkillEntry>();

    var entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(25)
        .mapObject(new SkillModel(ObjectId.TREE, 0))
        .mapObject(new SkillModel(ObjectId.TREE_1277, 0))
        .mapObject(new SkillModel(ObjectId.TREE_1278, 0))
        .mapObject(new SkillModel(ObjectId.TREE_1279, 0))
        .mapObject(new SkillModel(ObjectId.TREE_1280, 0))
        .mapObject(new SkillModel(ObjectId.DEAD_TREE, 0))
        .mapObject(new SkillModel(ObjectId.DEAD_TREE_1283, 0))
        .mapObject(new SkillModel(ObjectId.DEAD_TREE_1284, 0))
        .mapObject(new SkillModel(ObjectId.DEAD_TREE_1285, 0))
        .mapObject(new SkillModel(ObjectId.DEAD_TREE_1286, 0))
        .mapObject(new SkillModel(ObjectId.DEAD_TREE_1287, 0))
        .mapObject(new SkillModel(ObjectId.DEAD_TREE_1288, 0))
        .mapObject(new SkillModel(ObjectId.DEAD_TREE_1289, 0))
        .mapObject(new SkillModel(ObjectId.DEAD_TREE_1290, 0))
        .mapObject(new SkillModel(ObjectId.DEAD_TREE_1291, 0))
        .create(new RandomItem(ItemId.LOGS))
        .pet(new SkillPet(ItemId.BEAVER, 317647))
        .clueChance(317647)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.TREE_STUMP_1342, 1, 8));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(15)
        .experience(38)
        .mapObject(new SkillModel(ObjectId.OAK, 0))
        .create(new RandomItem(ItemId.OAK_LOGS))
        .pet(new SkillPet(ItemId.BEAVER, 361146))
        .clueChance(361146)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.TREE_STUMP_1342, 3, 8));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(30)
        .experience(68)
        .mapObject(new SkillModel(ObjectId.WILLOW, 0))
        .mapObject(new SkillModel(ObjectId.WILLOW_1756, 0))
        .mapObject(new SkillModel(ObjectId.WILLOW_1758, 0))
        .mapObject(new SkillModel(ObjectId.WILLOW_1760, 0))
        .create(new RandomItem(ItemId.WILLOW_LOGS))
        .pet(new SkillPet(ItemId.BEAVER, 289286))
        .clueChance(289286)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.TREE_STUMP_1342, 7, 8));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(45)
        .experience(100)
        .mapObject(new SkillModel(ObjectId.MAPLE_TREE, 0))
        .create(new RandomItem(ItemId.MAPLE_LOGS))
        .pet(new SkillPet(ItemId.BEAVER, 221918))
        .clueChance(221918)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.TREE_STUMP_1342, 11, 8));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(60)
        .experience(175)
        .mapObject(new SkillModel(ObjectId.YEW, 0))
        .mapObject(new SkillModel(ObjectId.YEW_1754, 0))
        .mapObject(new SkillModel(ObjectId.YEW_1755, 0))
        .create(new RandomItem(ItemId.YEW_LOGS))
        .pet(new SkillPet(ItemId.BEAVER, 145013))
        .clueChance(145013)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.TREE_STUMP_1342, 15, 8));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(75)
        .experience(250)
        .mapObject(new SkillModel(ObjectId.MAGIC_TREE, 0))
        .mapObject(new SkillModel(ObjectId.MAGIC_TREE_1762, 0))
        .create(new RandomItem(ItemId.MAGIC_LOGS))
        .pet(new SkillPet(ItemId.BEAVER, 72321))
        .clueChance(72321)
        .temporaryMapObject(new SkillTemporaryMapObject(ObjectId.TREE_STUMP_1342, 18, 8));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(90)
        .experience(380)
        .mapObject(new SkillModel(ObjectId.REDWOOD, 0))
        .mapObject(new SkillModel(ObjectId.REDWOOD_29670, 0))
        .create(new RandomItem(ItemId.REDWOOD_LOGS))
        .pet(new SkillPet(ItemId.BEAVER, 72321))
        .clueChance(72321);
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(45)
        .mapObject(new SkillModel(ObjectId.SAPLING, 0))
        .create(new RandomItem(ItemId.KINDLING_20799));
    list.add(entry.build());

    return list;
  }
}
