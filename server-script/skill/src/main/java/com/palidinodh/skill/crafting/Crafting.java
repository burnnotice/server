package com.palidinodh.skill.crafting;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.map.MapObject;
import java.util.Arrays;
import java.util.List;

class Crafting extends SkillContainer {

  private static final List<Integer> JEWELRY_RELATED =
      Arrays.asList(
          ItemId.RING_MOULD,
          ItemId.NECKLACE_MOULD,
          ItemId.BRACELET_MOULD,
          ItemId.AMULET_MOULD,
          ItemId.GOLD_BAR,
          ItemId.SAPPHIRE,
          ItemId.EMERALD,
          ItemId.RUBY,
          ItemId.DIAMOND,
          ItemId.DRAGONSTONE,
          ItemId.ONYX,
          ItemId.ZENYTE);

  @Override
  public int getSkillId() {
    return Skills.CRAFTING;
  }

  public List<SkillEntry> getEntries() {
    return CraftingEntries.getEntries();
  }

  @Override
  public boolean widgetHook(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId == WidgetId.MAKE_JEWELRY) {
      switch (itemId) {
        case ItemId.GOLD_BRACELET_11068:
          itemId = ItemId.GOLD_BRACELET;
          break;
        case ItemId.SAPPHIRE_BRACELET_11071:
          itemId = ItemId.SAPPHIRE_BRACELET;
          break;
        case ItemId.EMERALD_BRACELET_11078:
          itemId = ItemId.EMERALD_BRACELET;
          break;
        case ItemId.RUBY_BRACELET_11087:
          itemId = ItemId.RUBY_BRACELET;
          break;
        case ItemId.DIAMOND_BRACELET_11094:
          itemId = ItemId.DIAMOND_BRACELET;
          break;
        case ItemId.DRAGON_BRACELET:
          itemId = ItemId.DRAGONSTONE_BRACELET;
          break;
        case ItemId.ONYX_BRACELET_11132:
          itemId = ItemId.ONYX_BRACELET;
          break;
        case ItemId.ZENYTE_BRACELET_19492:
          itemId = ItemId.ZENYTE_BRACELET;
          break;
      }
      var entry = findEntryFromCreate(itemId);
      if (entry == null) {
        return true;
      }
      switch (option) {
        case 0:
          startEvent(player, entry, 1);
          break;
        case 1:
          startEvent(player, entry, 5);
          break;
        case 2:
          startEvent(player, entry, 10);
          break;
        case 3:
          player
              .getGameEncoder()
              .sendEnterAmount(
                  value -> {
                    startEvent(player, entry, value);
                  });
          break;
        case 4:
          startEvent(player, entry, 28);
          break;
      }
      return true;
    }
    return false;
  }

  @Override
  public boolean widgetOnMapObjectHook(
      Player player, int widgetId, int childId, int slot, int itemId, MapObject mapObject) {
    if (widgetId == WidgetId.INVENTORY && mapObject.getDef().hasOption("smelt")) {
      if (JEWELRY_RELATED.contains(itemId)) {
        player.getWidgetManager().sendInteractiveOverlay(WidgetId.MAKE_JEWELRY);
        return true;
      }
    }
    return false;
  }
}
