package com.palidinodh.skill.hunter;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.entity.player.skill.SkillModel;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

class HunterEntries {

  @Getter private static List<SkillEntry> entries = load();

  private static List<SkillEntry> load() {
    var list = new ArrayList<SkillEntry>();

    var entry = SkillEntry.builder();
    entry
        .level(17)
        .experience(18)
        .animation(6606)
        .npc(new SkillModel(NpcId.BABY_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.BABY_IMPLING_JAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(22)
        .experience(20)
        .animation(6606)
        .npc(new SkillModel(NpcId.YOUNG_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.YOUNG_IMPLING_JAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(28)
        .experience(22)
        .animation(6606)
        .npc(new SkillModel(NpcId.GOURMET_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.GOURMET_IMPLING_JAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(36)
        .experience(25)
        .animation(6606)
        .npc(new SkillModel(NpcId.EARTH_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.EARTH_IMPLING_JAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(42)
        .experience(27)
        .animation(6606)
        .npc(new SkillModel(NpcId.ESSENCE_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.ESSENCE_IMPLING_JAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(50)
        .experience(30)
        .animation(6606)
        .npc(new SkillModel(NpcId.ECLECTIC_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.ECLECTIC_IMPLING_JAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(58)
        .experience(34)
        .animation(6606)
        .npc(new SkillModel(NpcId.NATURE_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.NATURE_IMPLING_JAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(65)
        .experience(44)
        .animation(6606)
        .npc(new SkillModel(NpcId.MAGPIE_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.MAGPIE_IMPLING_JAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(74)
        .experience(50)
        .animation(6606)
        .npc(new SkillModel(NpcId.NINJA_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.NINJA_IMPLING_JAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(83)
        .experience(65)
        .animation(6606)
        .npc(new SkillModel(NpcId.DRAGON_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.DRAGON_IMPLING_JAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(89)
        .experience(80)
        .animation(6606)
        .npc(new SkillModel(NpcId.LUCKY_IMPLING, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.IMPLING_JAR))
        .create(new RandomItem(ItemId.LUCKY_IMPLING_JAR));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(15)
        .experience(24)
        .animation(6606)
        .npc(new SkillModel(NpcId.RUBY_HARVEST, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.BUTTERFLY_JAR))
        .create(new RandomItem(ItemId.RUBY_HARVEST));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(25)
        .experience(34)
        .animation(6606)
        .npc(new SkillModel(NpcId.SAPPHIRE_GLACIALIS, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.BUTTERFLY_JAR))
        .create(new RandomItem(ItemId.SAPPHIRE_GLACIALIS));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(35)
        .experience(44)
        .animation(6606)
        .npc(new SkillModel(NpcId.SNOWY_KNIGHT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.BUTTERFLY_JAR))
        .create(new RandomItem(ItemId.SNOWY_KNIGHT));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(45)
        .experience(54)
        .animation(6606)
        .npc(new SkillModel(NpcId.BLACK_WARLOCK, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .consume(new RandomItem(ItemId.BUTTERFLY_JAR))
        .create(new RandomItem(ItemId.BLACK_WARLOCK));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(5)
        .animation(6606)
        .npc(new SkillModel(NpcId.GUANIC_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .create(new RandomItem(ItemId.RAW_GUANIC_BAT_0));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(15)
        .experience(15)
        .animation(6606)
        .npc(new SkillModel(NpcId.PRAEL_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .create(new RandomItem(ItemId.RAW_PRAEL_BAT_1));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(30)
        .experience(20)
        .animation(6606)
        .npc(new SkillModel(NpcId.GIRAL_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .create(new RandomItem(ItemId.RAW_GIRAL_BAT_2));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(45)
        .experience(25)
        .animation(6606)
        .npc(new SkillModel(NpcId.PHLUXIA_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .create(new RandomItem(ItemId.RAW_PHLUXIA_BAT_3));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(60)
        .experience(30)
        .animation(6606)
        .npc(new SkillModel(NpcId.KRYKET_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .create(new RandomItem(ItemId.RAW_KRYKET_BAT_4));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(75)
        .experience(35)
        .animation(6606)
        .npc(new SkillModel(NpcId.MURNG_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .create(new RandomItem(ItemId.RAW_MURNG_BAT_5));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(90)
        .experience(38)
        .animation(6606)
        .npc(new SkillModel(NpcId.PSYKK_BAT, 0))
        .tool(new Item(ItemId.BUTTERFLY_NET))
        .create(new RandomItem(ItemId.RAW_PSYKK_BAT_6));
    list.add(entry.build());

    return list;
  }
}
