package com.palidinodh.skill.firemaking;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.entity.player.skill.SkillPet;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

class FiremakingEntries {

  @Getter private static List<SkillEntry> entries = load();

  private static List<SkillEntry> load() {
    var list = new ArrayList<SkillEntry>();

    var entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(40)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.LOGS))
        .pet(new SkillPet(ItemId.PHOENIX, 461808));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(40)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.ACHEY_TREE_LOGS));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(1)
        .experience(40)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.KINDLING_20799))
        .pet(new SkillPet(ItemId.PHOENIX, 461808));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(15)
        .experience(60)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.OAK_LOGS))
        .pet(new SkillPet(ItemId.PHOENIX, 443697));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(30)
        .experience(90)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.WILLOW_LOGS))
        .pet(new SkillPet(ItemId.PHOENIX, 435165));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(35)
        .experience(105)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.TEAK_LOGS))
        .pet(new SkillPet(ItemId.PHOENIX, 426954));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(42)
        .experience(125)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.ARCTIC_PINE_LOGS))
        .pet(new SkillPet(ItemId.PHOENIX, 382609));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(45)
        .experience(135)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.MAPLE_LOGS))
        .pet(new SkillPet(ItemId.PHOENIX, 305792));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(50)
        .experience(157)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.MAHOGANY_LOGS))
        .pet(new SkillPet(ItemId.PHOENIX, 170874));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(60)
        .experience(202)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.YEW_LOGS))
        .pet(new SkillPet(ItemId.PHOENIX, 149434));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(75)
        .experience(303)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.MAGIC_LOGS))
        .pet(new SkillPet(ItemId.PHOENIX, 138583));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(90)
        .experience(350)
        .animation(733)
        .alwaysMake1(true)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.REDWOOD_LOGS))
        .pet(new SkillPet(ItemId.PHOENIX, 128885));
    list.add(entry.build());

    entry = SkillEntry.builder();
    entry
        .level(49)
        .tool(new Item(ItemId.TINDERBOX))
        .consume(new RandomItem(ItemId.BULLSEYE_LANTERN))
        .create(new RandomItem(ItemId.BULLSEYE_LANTERN_4550));
    list.add(entry.build());

    return list;
  }
}
