package com.palidinodh.skill.firemaking;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.WidgetManager;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.osrscore.model.map.route.Route;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PEvent;
import java.util.Arrays;
import java.util.List;

class Firemaking extends SkillContainer {

  private static final List<Integer> LOGS =
      Arrays.asList(
          ItemId.LOGS,
          ItemId.OAK_LOGS,
          ItemId.WILLOW_LOGS,
          ItemId.MAPLE_LOGS,
          ItemId.YEW_LOGS,
          ItemId.MAGIC_LOGS,
          ItemId.ACHEY_TREE_LOGS,
          ItemId.TEAK_LOGS,
          ItemId.ARCTIC_PINE_LOGS,
          ItemId.MAHOGANY_LOGS,
          ItemId.REDWOOD_LOGS,
          ItemId.KINDLING_20799);

  @Override
  public int getSkillId() {
    return Skills.FIREMAKING;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return FiremakingEntries.getEntries();
  }

  @Override
  public void actionSuccess(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (LOGS.contains(entry.getConsume().getId())) {
      if (PRandom.randomE(160 - entry.getLevel()) == 0) {
        player.getInventory().addOrDropItem(ItemId.SUPPLY_CRATE);
      }
      if (entry.getAnimation() == 733 && mapObject != null) {
        player.setAnimation(897);
      }
      if (mapObject != null) {
        player.setFaceTile(mapObject);
        if (player.withinDistance(mapObject, 0)) {
          Route.moveOffTile(player);
        }
      } else {
        player.lock();
        var logMapItem = MapItem.getForPacket(entry.getConsume(), player);
        player.getGameEncoder().sendMapItem(logMapItem);
        player
            .getWorld()
            .addEvent(
                PEvent.singleEvent(
                    2,
                    e -> {
                      var fire = new MapObject(ObjectId.FIRE_5249, 10, 0, player);
                      player
                          .getWorld()
                          .addEvent(new TempMapObject(100, player.getController(), fire));
                      AchievementDiary.makeFireUpdate(player, entry.getConsume());
                      Route.moveOffTile(player);
                      player.getGameEncoder().sendRemoveMapItem(logMapItem);
                      player.unlock();
                    }));
      }
    }
  }

  @Override
  public int experienceHook(
      Player player, int experience, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().wearingPyromancerOutfit()) {
      experience *= 1.1;
    }
    return experience;
  }

  @Override
  public boolean canDoActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (mapObject == null && LOGS.contains(entry.getConsume().getId())) {
      if (player.getHeight() != player.getClientHeight()
          || player.getController().hasSolidMapObject(player)) {
        player.getGameEncoder().sendMessage("You can't do this here.");
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean widgetOnMapObjectHook(
      Player player, int widgetId, int childId, int slot, int itemId, MapObject mapObject) {
    if (widgetId != WidgetId.INVENTORY || mapObject.getId() != ObjectId.FIRE_5249) {
      return false;
    }
    if (!LOGS.contains(itemId)) {
      return false;
    }
    openMakeX(
        player,
        mapObject,
        WidgetManager.MakeXType.FIRE,
        player.getInventory().getCount(itemId),
        PCollection.toList(findEntryFromConsume(itemId)));
    return true;
  }
}
