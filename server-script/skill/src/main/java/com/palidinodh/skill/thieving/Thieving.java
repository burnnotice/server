package com.palidinodh.skill.thieving;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import java.util.Arrays;
import java.util.List;

class Thieving extends SkillContainer {

  private static final List<Integer> FARMERS =
      Arrays.asList(
          NpcId.MASTER_FARMER, NpcId.MASTER_FARMER_3258, NpcId.MARTIN_THE_MASTER_GARDENER);

  private static boolean isCarryingArdougneCloak(Player player) {
    if (player.hasItem(ItemId.ARDOUGNE_CLOAK_2)) {
      return true;
    }
    if (player.hasItem(ItemId.ARDOUGNE_CLOAK_3)) {
      return true;
    }
    if (player.hasItem(ItemId.ARDOUGNE_CLOAK_4)) {
      return true;
    }
    return player.hasItem(ItemId.ARDOUGNE_MAX_CAPE);
  }

  @Override
  public int getSkillId() {
    return Skills.THIEVING;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return ThievingEntries.getEntries();
  }

  @Override
  public int getDefaultMakeAmount() {
    return 1;
  }

  @Override
  public void actionSuccess(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null) {
      player.getGameEncoder().sendMessage("You pick the " + npc.getName() + "'s pocket.");
    } else if (mapObject != null) {
      player.getGameEncoder().sendMessage("You steal from the " + mapObject.getName() + ".");
      setTemporaryMapObject(player, mapObject, entry);
    }
  }

  @Override
  public Item createHook(Player player, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    var farmer = npc != null && FARMERS.contains(npc.getId());
    if (player.getEquipment().wearingRogueOutfit() && (PRandom.randomE(10) == 0 || farmer)) {
      item = new Item(item.getId(), item.getAmount() * 2);
    }
    return item;
  }

  @Override
  public int experienceHook(
      Player player, int experience, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().wearingRogueOutfit()) {
      experience *= 1.1;
    }
    // if (player.getArea().is(DonatorArea.class) && player.isUsergroup(UserRank.OPAL_MEMBER)) {
    // experience *= 1.1;
    // }
    return experience;
  }

  @Override
  public boolean failedActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    var chance =
        Math.min(128.0 - entry.getLevel() + player.getSkills().getLevel(getSkillId()) * 1.4, 240.0)
            / 255.0
            * 100.0;
    chance *= 1 + getMembershipBoost(player);
    if (player.inArdougne() && player.getEquipment().wearingRogueOutfit()) {
      chance += 20;
    }
    if (isCarryingArdougneCloak(player)) {
      chance += 10;
    }
    // if (player.getArea().is(DonatorArea.class) && player.isUsergroup(UserRank.OPAL_MEMBER)) {
    // chance += 10;
    // }
    if (player.getEquipment().wearingAccomplishmentCape(getSkillId())) {
      chance += 10;
    }
    if (player.hasVoted()) {
      chance += 5;
    }
    chance = Math.min(chance, 100);
    if (npc != null && PRandom.randomE(100) > chance) {
      npc.setForceMessage("What do you think you're doing?");
      npc.setAnimation(npc.getCombatDef().getAttackAnimation());
      npc.setFaceTile(player);
      player.setAnimation(player.getCombat().getBlockAnimation());
      player.setGraphic(80, 100);
      player.getGameEncoder().sendMessage("You've been stunned!");
      player.getController().setMagicBind(8);
      player.getSkills().setSkillDelay(8);
      return true;
    }
    return false;
  }

  @Override
  public String deathReasonHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null) {
      return "pickpocketing a " + npc.getName();
    }
    return "thieving";
  }
}
