package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DAWNBRINGER)
class DawnbringerSpecialAttack extends SpecialAttack {

  DawnbringerSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(35);
    entry.animation(1167);
    entry.castGraphic(new Graphic(1546, 92));
    entry.impactGraphic(new Graphic(1548, 46));
    entry.projectileId(1547);
    entry.magic(true);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var opponent = hooks.getOpponent();
    if (!opponent.isNpc()) {
      hooks.getPlayer().getEquipment().deleteItem(ItemId.DAWNBRINGER);
      return;
    }
    if (opponent.getId() != NpcId.VERZIK_VITUR_1040_8370) {
      hooks.getPlayer().getEquipment().deleteItem(ItemId.DAWNBRINGER);
      return;
    }
    hooks.setDamage(PRandom.randomI(75, 150));
  }
}
