package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ARMADYL_CROSSBOW)
class ArmadylCrossbowSpecialAttack extends SpecialAttack {

  ArmadylCrossbowSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(40);
    entry.animation(4230);
    entry.projectileId(301);
    entry.castSound(new Sound(3892));
    entry.accuracyModifier(2);
    addEntry(entry);
  }
}
