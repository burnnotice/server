package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DRAGON_MACE)
class DragonMaceSpecialAttack extends SpecialAttack {

  DragonMaceSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(25);
    entry.animation(1060);
    entry.castGraphic(new Graphic(251, 100));
    entry.castSound(new Sound(2541));
    entry.accuracyModifier(1.25);
    entry.damageModifier(1.5);
    addEntry(entry);
  }
}
