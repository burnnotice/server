package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.ABYSSAL_DAGGER,
  ItemId.ABYSSAL_DAGGER_P,
  ItemId.ABYSSAL_DAGGER_P_PLUS,
  ItemId.ABYSSAL_DAGGER_P_PLUS_PLUS
})
class AbyssalDaggerSpecialAttack extends SpecialAttack {

  AbyssalDaggerSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(3300);
    entry.castGraphic(new Graphic(1283));
    entry.castSound(new Sound(2537));
    entry.accuracyModifier(1.25);
    entry.damageModifier(0.85);
    entry.doubleHit(true);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getApplyAttackLoopCount() == 0) {
      return;
    }
    if (hooks.getLastHitEvent().getDamage() > 0) {
      return;
    }
    hooks.setDamage(0);
  }
}
