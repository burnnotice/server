package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.MORRIGANS_JAVELIN)
class MorrigansJavelinSpecialAttack extends SpecialAttack {

  MorrigansJavelinSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(806);
    entry.castGraphic(new Graphic(1621, 96));
    entry.projectileId(1622);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var projectileSpeed = hooks.getProjectile().getProjectileSpeed();
    var distance = hooks.getPlayer().getDistance(hooks.getOpponent());
    projectileSpeed.setEventDelay(1);
    if (distance >= 6) {
      projectileSpeed.setEventDelay(2);
    }
    projectileSpeed.setClientSpeed(11 + distance * 5);
    projectileSpeed.setClientDelay(21);
  }
}
