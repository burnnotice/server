package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ELDRITCH_NIGHTMARE_STAFF)
class EldritchNightmareStaffSpecialAttack extends SpecialAttack {

  EldritchNightmareStaffSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(75);
    entry.animation(8532);
    entry.castGraphic(new Graphic(1762, 0, 50));
    entry.impactGraphic(new Graphic(1761));
    entry.magic(true);
    entry.magicDamage(34);
    entry.magicLevelScale(75);
    entry.magicLevelScaleDivider(2.4);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    hooks.getPlayer().getPrayer().changePoints(hooks.getDamage() / 2, 21);
  }
}
