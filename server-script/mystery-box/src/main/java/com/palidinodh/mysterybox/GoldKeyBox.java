package com.palidinodh.mysterybox;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.List;

@ReferenceId(ItemId.GOLD_KEY_32308)
class GoldKeyBox extends MysteryBox {

  private static final List<RandomItem> ITEMS =
      RandomItem.combine(ItemTables.UNCOMMON, ItemTables.COMMON);
  private static final List<RandomItem> WEIGHTLESS = RandomItem.minWeight(ITEMS);

  @Override
  public Item getRandomItem(Player player) {
    return RandomItem.getItem(ITEMS);
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    return WEIGHTLESS;
  }
}
