package com.palidinodh.mysterybox;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.playerplugin.treasuretrail.reward.TreasureTrailReward;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.List;

@ReferenceId(ItemId.MYSTERY_BOX)
class RegularBox extends MysteryBox {

  private static List<RandomItem> weightless =
      RandomItem.minWeight(
          RandomItem.combine(
              ItemTables.VERY_RARE,
              ItemTables.RARE,
              ItemTables.UNCOMMON,
              ItemTables.COMMON,
              ItemTables.BARROWS_PIECES,
              RandomItem.buildList(
                  new RandomItem(ItemId.CLUE_SCROLL_MASTER),
                  new RandomItem(ItemId.CLUE_SCROLL_ELITE),
                  new RandomItem(ItemId.CLUE_SCROLL_HARD),
                  new RandomItem(ItemId.CLUE_SCROLL_MEDIUM),
                  new RandomItem(ItemId.CLUE_SCROLL_EASY))));

  @Override
  public Item getRandomItem(Player player) {
    if (PRandom.randomE(250) == 0) {
      return RandomItem.getItem(ItemTables.VERY_RARE);
    } else if (PRandom.randomE(15) == 0) {
      return PRandom.collectionRandom(TreasureTrailReward.REWARDS.values()).getUnique(player);
    } else if (PRandom.randomE(10) == 0) {
      return RandomItem.getItem(ItemTables.RARE_WITH_BARROWS);
    } else if (PRandom.randomE(5) == 0) {
      return RandomItem.getItem(ItemTables.UNCOMMON_WITH_BARROWS);
    }
    return RandomItem.getItem(ItemTables.COMMON_WITH_BARROWS);
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    return weightless;
  }
}
