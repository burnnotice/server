package com.palidinodh.mysterybox;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.playerplugin.treasuretrail.reward.TreasureTrailReward;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.ArrayList;
import java.util.List;

@ReferenceId(ItemId.BLOODY_KEY_32304)
class BloodyKeyBox extends MysteryBox {

  private static List<RandomItem> weightless =
      RandomItem.minWeight(
          RandomItem.combine(
              RandomItem.buildList(
                  new RandomItem(ItemId.COINS, 50_000), new RandomItem(ItemId.COINS, 2_500_000)),
              ItemTables.VERY_RARE,
              ItemTables.RARE,
              ItemTables.UNCOMMON,
              ItemTables.COMMON,
              ItemTables.BARROWS_PIECES,
              RandomItem.buildList(
                  new RandomItem(ItemId.CLUE_SCROLL_MASTER),
                  new RandomItem(ItemId.CLUE_SCROLL_ELITE),
                  new RandomItem(ItemId.CLUE_SCROLL_HARD),
                  new RandomItem(ItemId.CLUE_SCROLL_MEDIUM),
                  new RandomItem(ItemId.CLUE_SCROLL_EASY))));
  private static List<RandomItem> baseTable =
      RandomItem.combine(ItemTables.COMMON, ItemTables.BARROWS_PIECES);

  @Override
  public List<Item> getAlwaysItems(int itemId, Player player) {
    List<Item> items = new ArrayList<>();
    items.add(new Item(ItemId.BLOOD_MONEY, 10_000));
    return items;
  }

  @Override
  public Item getRandomItem(Player player) {
    if (player.getGameMode().isIronType()) {
      return PRandom.randomE(20) == 0
          ? RandomItem.getItem(ItemTables.BARROWS_PIECES)
          : new RandomItem(ItemId.COINS, 50_000, 500_000).getItem();
    }
    if (PRandom.randomE(30) == 0) {
      return PRandom.collectionRandom(TreasureTrailReward.REWARDS.values()).getUnique(player);
    } else if (PRandom.randomE(20) == 0) {
      return PRandom.randomE(20) == 0
          ? RandomItem.getItem(ItemTables.UNCOMMON)
          : new RandomItem(ItemId.COINS, 500_000, 5_000_000).getItem();
    }
    return PRandom.randomE(20) == 0
        ? RandomItem.getItem(baseTable)
        : new RandomItem(ItemId.COINS, 100_000, 1_000_000).getItem();
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    if (player.getGameMode().isIronType()) {
      var coins =
          RandomItem.buildList(
              new RandomItem(ItemId.COINS, 50_000), new RandomItem(ItemId.COINS, 500_000));
      return RandomItem.minWeight(RandomItem.combine(coins, ItemTables.BARROWS_PIECES));
    }
    return weightless;
  }
}
