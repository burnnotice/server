package com.palidinodh.npcplugin;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.playerplugin.hunter.HunterPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  NpcId.CRIMSON_SWIFT,
  NpcId.GOLDEN_WARBLER,
  NpcId.COPPER_LONGTAIL,
  NpcId.CERULEAN_TWITCH,
  NpcId.TROPICAL_WAGTAIL
})
class HunterBirdNpcPlugin implements NpcPlugin {

  @Inject private Npc npc;

  @Override
  public void tick() {
    if (npc.getMovement().getWalkDir() == -1) {
      return;
    }
    var asObjectId = -1;
    if (npc.getId() == NpcId.CRIMSON_SWIFT) {
      asObjectId = ObjectId.BIRD_SNARE_9373;
    } else if (npc.getId() == NpcId.GOLDEN_WARBLER) {
      asObjectId = ObjectId.BIRD_SNARE_9377;
    } else if (npc.getId() == NpcId.COPPER_LONGTAIL) {
      asObjectId = ObjectId.BIRD_SNARE_9379;
    } else if (npc.getId() == NpcId.CERULEAN_TWITCH) {
      asObjectId = ObjectId.BIRD_SNARE_9375;
    } else if (npc.getId() == NpcId.TROPICAL_WAGTAIL) {
      asObjectId = ObjectId.BIRD_SNARE_9348;
    }
    if (npc.isLocked() || asObjectId == -1) {
      return;
    }
    var mapObject = npc.getController().getSolidMapObject(npc);
    if (mapObject == null || mapObject.getId() != ObjectId.BIRD_SNARE_9345) {
      return;
    }
    if (!(mapObject.getAttachment() instanceof TempMapObject)
        || !(((TempMapObject) mapObject.getAttachment()).getAttachment() instanceof Integer)) {
      return;
    }
    var player =
        Main.getWorld()
            .getPlayerById((Integer) ((TempMapObject) mapObject.getAttachment()).getAttachment());
    if (player == null || !player.getPlugin(HunterPlugin.class).canCaptureTrap(asObjectId)) {
      return;
    }
    var success =
        player
            .getPlugin(HunterPlugin.class)
            .success(npc, HunterPlugin.getCapturedTrapLevelRequirement(asObjectId));
    if (success) {
      mapObject.setId(asObjectId);
      npc.getCombat().startDeath(2);
      npc.setAnimation(6780);
    } else {
      mapObject.setId(ObjectId.BIRD_SNARE);
    }
    ((TempMapObject) mapObject.getAttachment()).setTick(HunterPlugin.TRAP_EXPIRIY);
    npc.getController().sendMapObject(mapObject);
  }
}
