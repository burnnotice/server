package com.palidinodh.npcplugin;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.playerplugin.hunter.HunterPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.FERRET)
class HunterFerretNpcPlugin implements NpcPlugin {

  @Inject private Npc npc;

  @Override
  public void tick() {
    if (npc.getMovement().getWalkDir() == -1) {
      return;
    }
    if (npc.isLocked()) {
      return;
    }
    var mapObject = npc.getController().getSolidMapObject(npc);
    if (mapObject == null || mapObject.getId() != ObjectId.BOX_TRAP_9380) {
      return;
    }
    if (!(mapObject.getAttachment() instanceof TempMapObject)
        || !(((TempMapObject) mapObject.getAttachment()).getAttachment() instanceof Integer)) {
      return;
    }
    var player =
        Main.getWorld()
            .getPlayerById((Integer) ((TempMapObject) mapObject.getAttachment()).getAttachment());
    if (player == null
        || !player.getPlugin(HunterPlugin.class).canCaptureTrap(ObjectId.SHAKING_BOX_9384)) {
      return;
    }
    if (player
        .getPlugin(HunterPlugin.class)
        .success(npc, HunterPlugin.getCapturedTrapLevelRequirement(ObjectId.SHAKING_BOX_9384))) {
      mapObject.setId(ObjectId.SHAKING_BOX_9384);
      npc.getCombat().startDeath(2);
    } else {
      mapObject.setId(ObjectId.BOX_TRAP_9385);
    }
    ((TempMapObject) mapObject.getAttachment()).setTick(HunterPlugin.TRAP_EXPIRIY);
    npc.getController().sendMapObject(mapObject);
  }
}
