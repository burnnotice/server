package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.combat.DropRateBoost;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PTime;

@ReferenceId(ItemId.X15_DROP_BOOST_SCROLL_8_HOURS_32316)
class _50DropBoostScroll8HoursItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getCombat().getDropRateBoost() != null) {
      player.getGameEncoder().sendMessage("You already have a drop rate boost active.");
      return;
    }
    item.remove(1);
    player.getCombat().setDropRateBoost(new DropRateBoost(1.5, (int) PTime.hourToTick(8)));
    player.getGameEncoder().sendMessage("A drop rate boost of 50% has been added for 8 hours.");
  }
}
