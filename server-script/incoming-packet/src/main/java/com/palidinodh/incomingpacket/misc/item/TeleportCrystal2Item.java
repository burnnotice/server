package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.TELEPORT_CRYSTAL_2)
class TeleportCrystal2Item implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(true)) {
      return;
    }
    item.replace(new Item(ItemId.TELEPORT_CRYSTAL_1));
    player
        .getMovement()
        .animatedTeleport(
            new Tile(2352, 3162),
            Magic.TABLET_ANIMATION_START,
            Magic.TABLET_ANIMATION_END,
            -1,
            null,
            Magic.TABLET_GRAPHIC,
            null,
            0,
            2);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
    player.log(PlayerLogType.TELEPORT, "Lletya using a teleport crystal");
  }
}
