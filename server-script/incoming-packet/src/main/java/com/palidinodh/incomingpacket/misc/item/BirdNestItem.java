package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BIRD_NEST,
  ItemId.BIRD_NEST_5071,
  ItemId.BIRD_NEST_5072,
  ItemId.BIRD_NEST_5073,
})
class BirdNestItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (item.getId() == ItemId.BIRD_NEST_5073) {
      var randomItems =
          RandomItem.buildList(
              new RandomItem(5323, 6).weight(125), // Strawberry
              new RandomItem(5320, 6).weight(125), // Sweetcorn
              new RandomItem(5312, 1).weight(100), // Acorn
              new RandomItem(5100, 2).weight(100), // Limpwurt
              new RandomItem(5321, 2).weight(50), // Watermelon
              new RandomItem(5302, 1).weight(40), // Lantadyme
              new RandomItem(5303, 1).weight(40), // Dwarf weed
              new RandomItem(5301, 1).weight(30), // Cadantine
              new RandomItem(5313, 1).weight(20), // Willow
              new RandomItem(5287, 1).weight(20), // Pineapple
              new RandomItem(5288, 1).weight(15), // Papaya
              new RandomItem(5290, 1).weight(15), // Calquat
              new RandomItem(5304, 1).weight(5), // Torstol
              new RandomItem(5317, 1).weight(5), // Spirit seed
              new RandomItem(5314, 1).weight(5), // Maple
              new RandomItem(5295, 1).weight(5), // Ranarr
              new RandomItem(5300, 1).weight(3), // Snapdragon
              new RandomItem(5315, 1).weight(3), // Yew seed
              new RandomItem(5289, 1).weight(2), // Palm
              new RandomItem(5316, 1).weight(2) // Magic
              );
      item.replace(new Item(ItemId.BIRD_NEST_5075));
      player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
    } else if (item.getId() == ItemId.BIRD_NEST) {
      item.replace(new Item(ItemId.BIRD_NEST_5075));
      player.getInventory().addOrDropItem(ItemId.BIRDS_EGG);
    } else if (item.getId() == ItemId.BIRD_NEST_5071) {
      item.replace(new Item(ItemId.BIRD_NEST_5075));
      player.getInventory().addOrDropItem(ItemId.BIRDS_EGG_5078);
    } else if (item.getId() == ItemId.BIRD_NEST_5072) {
      item.replace(new Item(ItemId.BIRD_NEST_5075));
      player.getInventory().addOrDropItem(ItemId.BIRDS_EGG_5077);
    }
  }
}
