package com.palidinodh.incomingpacket.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  NpcId.BABY_CHINCHOMPA,
  NpcId.BABY_CHINCHOMPA_6719,
  NpcId.BABY_CHINCHOMPA_6720,
  NpcId.BABY_CHINCHOMPA_6721
})
class BabyChinchompa implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (npc != player.getPlugin(FamiliarPlugin.class).getFamiliar()) {
      return;
    }
    switch (npc.getId()) {
      case NpcId.BABY_CHINCHOMPA:
      case NpcId.BABY_CHINCHOMPA_6719:
      case NpcId.BABY_CHINCHOMPA_6720:
        if (PRandom.randomE(10000) == 0) {
          player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.BABY_CHINCHOMPA_6721); // Golden
          // chin
        } else {
          player
              .getPlugin(FamiliarPlugin.class)
              .transformPet(NpcId.BABY_CHINCHOMPA + PRandom.randomI(2));
        }
        break;
      case NpcId.BABY_CHINCHOMPA_6721:
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to metamorph the golden chinchompa?",
                new DialogueOption(
                    "Yes, I want to metamorph the golden chinchompa!",
                    (c, s) -> {
                      player
                          .getPlugin(FamiliarPlugin.class)
                          .transformPet(NpcId.BABY_CHINCHOMPA + PRandom.randomI(2));
                    }),
                new DialogueOption("No!")));
        break;
    }
  }
}
