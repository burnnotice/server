package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.COIN_POUCH_22522)
class CoinPouchItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    player.getInventory().addOrDropItem(ItemId.COINS, 25_000 + PRandom.randomI(75_000));
  }
}
