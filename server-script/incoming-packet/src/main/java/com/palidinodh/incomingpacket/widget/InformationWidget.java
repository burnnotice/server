package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.CUSTOM_INFORMATION)
class InformationWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (childId == 2) {
      player.getWidgetManager().setQuestIndex(0);
      player.getWidgetManager().resetQuestText();
    } else if (childId == 5) {
      player.getWidgetManager().setQuestIndex(1);
      player.getWidgetManager().resetQuestText();
    } else if (childId == 8) {
      player.getWidgetManager().setQuestIndex(2);
      player.getWidgetManager().resetQuestText();
    } else if (childId == 11) {
      player.getWidgetManager().setQuestIndex(3);
      player.getWidgetManager().resetQuestText();
    } else if (childId == 14) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.VOTING_1020);
    } else if (childId == 17) {
      player.getPlugin(BondPlugin.class).openPouch();
    } else {
      player.getWidgetManager().questAction(childId - 36);
    }
  }
}
