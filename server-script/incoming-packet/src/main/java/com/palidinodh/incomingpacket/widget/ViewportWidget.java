package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.FixedViewport;
import com.palidinodh.osrscore.io.cache.widget.MobileViewport;
import com.palidinodh.osrscore.io.cache.widget.ResizeableBoxViewport;
import com.palidinodh.osrscore.io.cache.widget.ResizeableLineViewport;
import com.palidinodh.osrscore.io.cache.widget.ViewportIcon;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  WidgetId.FIXED_VIEWPORT,
  WidgetId.RESIZABLE_BOX_VIEWPORT,
  WidgetId.RESIZABLE_LINE_VIEWPORT,
  WidgetId.MOBILE_VIEWPORT
})
class ViewportWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId == WidgetId.FIXED_VIEWPORT) {
      FixedViewport child = FixedViewport.getByChild(childId);
      if (child != null && child.getIcon() != null) {
        player.getOptions().setViewingIcon(child.getIcon());
        if (child.getIcon() == ViewportIcon.QUESTS) {
          player.getWidgetManager().sendQuestOverlay();
        } else if (child.getIcon() == ViewportIcon.MAGIC && option == 1) {
          player.getMagic().setDisableSpellFiltering(!player.getMagic().getDisableSpellFiltering());
        }
      }
    } else if (widgetId == WidgetId.RESIZABLE_BOX_VIEWPORT) {
      ResizeableBoxViewport child = ResizeableBoxViewport.getByChild(childId);
      if (child != null && child.getIcon() != null) {
        player.getOptions().setViewingIcon(child.getIcon());
        if (child.getIcon() == ViewportIcon.QUESTS) {
          player.getWidgetManager().sendQuestOverlay();
        } else if (child.getIcon() == ViewportIcon.MAGIC && option == 1) {
          player.getMagic().setDisableSpellFiltering(!player.getMagic().getDisableSpellFiltering());
        }
      }
    } else if (widgetId == WidgetId.RESIZABLE_LINE_VIEWPORT) {
      ResizeableLineViewport child = ResizeableLineViewport.getByChild(childId);
      if (child != null && child.getIcon() != null) {
        player.getOptions().setViewingIcon(child.getIcon());
        if (child.getIcon() == ViewportIcon.QUESTS) {
          player.getWidgetManager().sendQuestOverlay();
        } else if (child.getIcon() == ViewportIcon.MAGIC && option == 1) {
          player.getMagic().setDisableSpellFiltering(!player.getMagic().getDisableSpellFiltering());
        }
      }
    } else if (widgetId == WidgetId.MOBILE_VIEWPORT) {
      MobileViewport child = MobileViewport.getByChild(childId);
      if (child != null && child.getIcon() != null) {
        player.getOptions().setViewingIcon(child.getIcon());
        if (child.getIcon() == ViewportIcon.QUESTS) {
          player.getWidgetManager().sendQuestOverlay();
        } else if (child.getIcon() == ViewportIcon.MAGIC && option == 1) {
          player.getMagic().setDisableSpellFiltering(!player.getMagic().getDisableSpellFiltering());
        }
      }
    }
  }
}
