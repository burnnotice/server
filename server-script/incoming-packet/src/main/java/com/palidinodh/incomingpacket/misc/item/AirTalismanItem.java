package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Runecrafting;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.AIR_TALISMAN,
  ItemId.EARTH_TALISMAN,
  ItemId.FIRE_TALISMAN,
  ItemId.WATER_TALISMAN,
  ItemId.BODY_TALISMAN,
  ItemId.MIND_TALISMAN,
  ItemId.CHAOS_TALISMAN,
  ItemId.COSMIC_TALISMAN,
  ItemId.DEATH_TALISMAN,
  ItemId.NATURE_TALISMAN,
  ItemId.WRATH_TALISMAN
})
class AirTalismanItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    Runecrafting.talismanTeleport(player, Runecrafting.Altar.getByTalisman(item.getId()));
  }
}
