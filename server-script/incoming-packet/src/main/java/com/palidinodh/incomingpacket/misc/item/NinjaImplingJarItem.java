package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.NINJA_IMPLING_JAR)
class NinjaImplingJarItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems =
        RandomItem.buildList(
            new RandomItem(6328, 1) /* Snakeskin boots */,
            new RandomItem(3391, 1) /* Splitbark gauntlets */,
            new RandomItem(4097, 1) /* Mystic boots */,
            new RandomItem(3385, 1) /* Splitbark helm */,
            new RandomItem(1113, 1) /* Rune chainbody */,
            new RandomItem(6313, 1) /* Opal machete */,
            new RandomItem(3101, 1) /* Rune claws */,
            new RandomItem(1333, 1) /* Rune scimitar */,
            new RandomItem(1347, 1) /* Rune warhammer */,
            new RandomItem(1215, 1) /* Dragon dagger */,
            new RandomItem(892, 70) /* Rune arrow */,
            new RandomItem(811, 70) /* Rune dart */,
            new RandomItem(868, 40) /* Rune knife */,
            new RandomItem(805, 50) /* Rune thrownaxe */,
            new RandomItem(9245, 2) /* Onyx bolts (e) */,
            new RandomItem(1748, 10, 16) /* Black dragonhide (noted) */,
            new RandomItem(140, 4) /* Prayer potion(3) (noted) */,
            new RandomItem(5941, 4) /* Weapon poison(++) (noted) */,
            new RandomItem(6156, 3) /* Dagannoth hide (noted) */,
            new RandomItem(2723, 1, 1).weight(4) /* Clue scroll (hard) */,
            new RandomItem(2364, 4) /* Runite bar (noted) */);
    player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
  }
}
