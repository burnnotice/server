package com.palidinodh.incomingpacket.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({NpcId.MONK_OF_ENTRANA_1166, NpcId.MONK_OF_ENTRANA_1167})
class MonkOfEntranaNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    for (int i = 0; i < player.getInventory().size(); i++) {
      int itemId = player.getInventory().getId(i);
      if (itemId == -1 || ItemDef.getEquipSlot(itemId) == null) {
        continue;
      }
      boolean isAllowed =
          ItemDef.getName(itemId).contains("arrow")
              || itemId == 3840
              || itemId == 3842
              || itemId == 3844
              || itemId == 12608
              || itemId == 12610
              || itemId == 12612;
      if (isAllowed) {
        continue;
      }
      player
          .getGameEncoder()
          .sendMessage("You can't take " + ItemDef.getName(itemId) + " to Entrana.");
      return;
    }
    for (int i = 0; i < player.getEquipment().size(); i++) {
      int itemId = player.getEquipment().getId(i);
      if (itemId == -1 || ItemDef.getEquipSlot(itemId) == null) {
        continue;
      }
      boolean isAllowed =
          ItemDef.getName(itemId).contains("arrow")
              || itemId == 3840
              || itemId == 3842
              || itemId == 3844
              || itemId == 12608
              || itemId == 12610
              || itemId == 12612;
      if (isAllowed) {
        continue;
      }
      player
          .getGameEncoder()
          .sendMessage("You can't take " + ItemDef.getName(itemId) + " to Entrana.");
      return;
    }
    player.getMovement().teleport(2834, 3335);
  }
}
