package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId({
  ItemId.SMALL_POUCH,
  ItemId.MEDIUM_POUCH,
  ItemId.MEDIUM_POUCH_5511,
  ItemId.LARGE_POUCH,
  ItemId.LARGE_POUCH_5513,
  ItemId.GIANT_POUCH,
  ItemId.GIANT_POUCH_5515
})
class SmallPouchItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "fill":
        {
          var pureEssenceCount = player.getInventory().getCount(7936);
          var addingAmount =
              player
                  .getWidgetManager()
                  .getRCPouch(item.getId())
                  .canAddAmount(7936, pureEssenceCount);
          if (pureEssenceCount == 0) {
            player.getGameEncoder().sendMessage("You have no pure essence.");
            return;
          }
          if (addingAmount == 0) {
            player
                .getGameEncoder()
                .sendMessage(
                    "Your "
                        + player.getWidgetManager().getRCPouch(item.getId()).getName()
                        + " is full.");
            return;
          }
          player.getInventory().deleteItem(7936, addingAmount);
          player.getWidgetManager().getRCPouch(item.getId()).addItem(7936, addingAmount);
          break;
        }
      case "empty":
        {
          var pureEssenceCount = player.getWidgetManager().getRCPouch(item.getId()).getCount(7936);
          var addingAmount = player.getInventory().canAddAmount(7936, pureEssenceCount);
          player.getWidgetManager().getRCPouch(item.getId()).deleteItem(7936, addingAmount);
          player.getInventory().addItem(7936, addingAmount);
          break;
        }
      case "check":
        {
          var pureEssenceCount = player.getWidgetManager().getRCPouch(item.getId()).getCount(7936);
          player
              .getGameEncoder()
              .sendMessage(
                  "Your "
                      + player.getWidgetManager().getRCPouch(item.getId()).getName()
                      + " contains "
                      + PNumber.formatNumber(pureEssenceCount)
                      + " pure essence.");
          break;
        }
    }
  }
}
