package com.palidinodh.incomingpacket;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.io.Readers;
import com.palidinodh.io.Stream;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.AchievementDiary;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Prayer;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.Smithing;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.Region;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

class MapObjectOptionDecoder extends IncomingPacketDecoder {

  private static Map<Integer, Method> actionMethods = new HashMap<>();

  private static boolean basicAction(Player player, int option, MapObject mapObject) {
    switch (mapObject.getName().toLowerCase()) {
      case "door":
      case "gate":
      case "large door":
        if (mapObject.getDef().getOptions() == null) {
          break;
        }
        if (!mapObject.getDef().isOption(0, "open")) {
          break;
        }
        if (mapObject.getSizeX() != 1 || mapObject.getSizeY() != 1) {
          break;
        }
        if (mapObject.getDef().getOptions().size() > 1) {
          break;
        }
        Region.openDoors(player, mapObject);
        return true;
      case "bank booth":
      case "bank chest":
      case "bank counter":
        player.getBank().open();
        return true;
      case "bank deposit box":
      case "deposit box":
      case "bank deposit pot":
        player.getBank().openDepositBox();
        return true;
      case "fairy ring":
        player.openDialogue("fairyring", 0);
        return true;
      case "altar":
      case "chaos altar":
        if (!mapObject.getDef().hasOption("pray") && !mapObject.getDef().hasOption("pray-at")) {
          break;
        }
        player.getPrayer().changePoints(player.getController().getLevelForXP(Skills.PRAYER));
        player.setAnimation(Prayer.PRAY_ANIMATION);
        return true;
      case "furnace":
        Smithing.openSmelt(player);
        return true;
    }
    return false;
  }

  @Override
  public boolean execute(Player player, Stream stream) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var mapObjectId = getInt(InStreamKey.MAP_OBJECT_ID);
    var tileX = getInt(InStreamKey.TILE_X);
    var tileY = getInt(InStreamKey.TILE_Y);
    var ctrlRun = getInt(InStreamKey.CTRL_RUN);
    player.clearIdleTime();
    var mapObject =
        player.getController().getMapObject(mapObjectId, tileX, tileY, player.getClientHeight());
    if (mapObject == null) {
      return false;
    }
    if (!mapObject.getDef().hasOptions()) {
      return false;
    }
    if (player.getHeight() != player.getClientHeight()) {
      if (mapObject.getDef().hasOption("open") || mapObject.getDef().hasOption("close")) {
        return false;
      }
    }
    var message =
        "[MapObjectOption("
            + option
            + ")] mapObjectId="
            + mapObjectId
            + "/"
            + ObjectId.valueOf(mapObject.getId())
            + "; tileX="
            + tileX
            + "; tileY="
            + tileY
            + "; ctrlRun="
            + ctrlRun
            + ", type="
            + mapObject.getType()
            + ", direction="
            + mapObject.getDirection();
    player.log(PlayerLogType.DECODER_DEBUG, message);
    if (Settings.getInstance().isLocal()) {
      PLogger.println(message);
    }
    if (player.getOptions().isDebug()) {
      player.getGameEncoder().sendMessage(message);
    }
    RequestManager.addUserPacketLog(player, message);
    if (player.isLocked()) {
      return false;
    }
    if (!player.getMovement().isTeleportStateNone()) {
      return false;
    }
    if (player.getMovement().isViewing()) {
      return false;
    }
    if (player.getObjectOptionDelay() > 0) {
      return false;
    }
    if (player.getDialogue() != null && player.getDialogue().isDisableActions()) {
      return false;
    }
    player.getMovement().fullRoute(mapObject, ctrlRun);
    return true;
  }

  @Override
  public boolean complete(Player player) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var mapObjectId = getInt(InStreamKey.MAP_OBJECT_ID);
    var tileX = getInt(InStreamKey.TILE_X);
    var tileY = getInt(InStreamKey.TILE_Y);
    var mapObject =
        player.getController().getMapObject(mapObjectId, tileX, tileY, player.getClientHeight());
    if (mapObject == null || !mapObject.isVisible()) {
      return true;
    }
    if (player.isLocked()) {
      return false;
    }
    if (!player.getMovement().isTeleportStateNone()) {
      return false;
    }
    var worldEvent =
        player
            .getWorld()
            .getWorldEvents()
            .getIf(e -> e.getMapObjectHandler(player, mapObjectId) != null);
    var handler = worldEvent == null ? null : worldEvent.getMapObjectHandler(player, mapObjectId);
    if (handler == null) {
      handler = player.getArea().getMapObjectHandler(mapObjectId);
    }
    var type = handler != null ? handler.canReach(player, mapObject) : null;
    if (type == MapObjectHandler.ReachType.FALSE) {
      return false;
    }
    if (type == null || type == MapObjectHandler.ReachType.DEFAULT) {
      if (player.getMovement().isRouting()
          && mapObject.getOriginal() == null
          && (player.getX() != mapObject.getX() || player.getY() != mapObject.getY())) {
        return false;
      }
      var range = 1;
      if (mapObject.getType() >= 4 && mapObject.getType() <= 8) {
        range = 0;
      }
      if (mapObject.getId() == ObjectId.THE_INFERNO_30352) { // Entrance
        range = 5;
      } else if (mapObject.getId() == ObjectId.PILLAR_31561) { // Revenants
        range = 2;
      }
      if (!player.withinDistanceC(mapObject, range)) {
        return false;
      }
    }
    player.getMovement().clear();
    if (!player.matchesTile(mapObject)) {
      player.setFaceTile(mapObject);
    }
    AchievementDiary.mapObjectOptionUpdate(player, option, mapObject);
    if (player.getController().mapObjectOptionHook(option, mapObject)) {
      return true;
    }
    if (SkillContainer.mapObjectOptionHooks(player, option, mapObject)) {
      return true;
    }
    if (player.getFarming().mapObjectOptionHook(option, mapObject)) {
      return true;
    }
    if (player.getPlugins().containsIf(p -> p.mapObjectOptionHook(option, mapObject))) {
      return true;
    }
    if (handler != null) {
      handler.mapObjectOption(player, option, mapObject);
      return true;
    }
    if (!actionMethods.containsKey(mapObject.getId())) {
      try {
        var classIndex = mapObject.getId() / 16384;
        if (classIndex == 0 || classIndex == 1) {
          var classReference =
              Readers.getClass("com.palidinodh.incomingpacket.misc.MapObject" + classIndex);
          var methodName = "mapObject" + mapObject.getId();
          var actionMethod =
              classReference.getMethod(methodName, Player.class, Integer.TYPE, MapObject.class);
          if ((actionMethod.getModifiers() & Modifier.STATIC) == 0) {
            actionMethod = null;
          }
          actionMethods.put(mapObject.getId(), actionMethod);
        } else {
          actionMethods.put(mapObject.getId(), null);
        }
      } catch (Exception e) {
        actionMethods.put(mapObject.getId(), null);
      }
    }
    var actionMethod = actionMethods.get(mapObject.getId());
    if (actionMethod == null) {
      if (basicAction(player, option, mapObject)) {
        return true;
      }
      player.getGameEncoder().sendMessage("Nothing interesting happens.");
    } else {
      try {
        actionMethod.invoke(null, player, option, mapObject);
      } catch (Exception e) {
        PLogger.error(e);
        player.getGameEncoder().sendMessage("Nothing interesting happens.");
      }
    }
    return true;
  }
}
