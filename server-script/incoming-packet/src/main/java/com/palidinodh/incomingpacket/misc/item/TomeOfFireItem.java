package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.TOME_OF_FIRE)
class TomeOfFireItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "check":
        player.getGameEncoder().sendMessage("This book has unlimited charges.");
        break;
      case "add or remove pages":
        if (player.getInventory().getRemainingSlots() < 1) {
          player.getInventory().notEnoughSpace();
          return;
        }
        item.replace(new Item(ItemId.TOME_OF_FIRE_EMPTY));
        player.getInventory().addItem(ItemId.BURNT_PAGE, 10);
        break;
    }
  }
}
