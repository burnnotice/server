package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.ViewportContainer;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId({WidgetId.FRIENDS, WidgetId.IGNORES, WidgetId.ACCOUNT_MANAGEMENT})
class FriendsWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId == WidgetId.FRIENDS) {
      if (childId == 1) {
        player.getWidgetManager().sendWidget(ViewportContainer.FRIENDS, WidgetId.IGNORES);
      }
    } else if (widgetId == WidgetId.IGNORES) {
      if (childId == 1) {
        player.getWidgetManager().sendWidget(ViewportContainer.FRIENDS, WidgetId.FRIENDS);
      }
    } else if (widgetId == WidgetId.ACCOUNT_MANAGEMENT) {
      if (childId == 3 || childId == 8) {
        player.getPlugin(BondPlugin.class).openPouch();
      } else if (childId == 15) {
        player.getGameEncoder().sendOpenUrl(Settings.getInstance().getWebsiteUrl());
      } else if (childId == 22) {
        player.getGameEncoder().sendOpenUrl(Settings.getInstance().getSupportUrl());
      } else if (childId == 29) {
        player.getGameEncoder().sendOpenUrl(Settings.getInstance().getWebsiteUrl());
      } else if (childId == 32) {
        player.getGameEncoder().sendOpenUrl(Settings.getInstance().getSupportUrl());
      }
    }
  }
}
