package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.ITEMS_KEPT_ON_DEATH)
class ItemsOnDeathWidget implements WidgetHandler {}
