package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.BONES_TO_PEACHES_8015)
class BonesToPeachesItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var bonesCount = player.getInventory().getCount(526);
    var bigBonesCount = player.getInventory().getCount(532);
    if (bonesCount == 0 && bigBonesCount == 0) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove(1);
    player.getInventory().deleteItem(526, bonesCount);
    player.getInventory().deleteItem(532, bigBonesCount);
    player.getInventory().addItem(6883, bonesCount + bigBonesCount);
  }
}
