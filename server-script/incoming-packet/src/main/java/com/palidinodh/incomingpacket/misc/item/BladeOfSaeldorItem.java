package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.BLADE_OF_SAELDOR)
class BladeOfSaeldorItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "check":
        player
            .getGameEncoder()
            .sendMessage(
                "You currently have "
                    + ItemDef.getDegradeTime(ItemId.BLADE_OF_SAELDOR)
                    + " charges stored.");
        break;
      case "uncharge":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to uncharge the blade? <br> You will not get back any of the shards.",
                new DialogueOption(
                    "Yes, turn it back into a normal Blade of Saeldor!",
                    (c, s) -> item.replace(new Item(ItemId.BLADE_OF_SAELDOR_INACTIVE))),
                new DialogueOption("No!")));
        break;
    }
  }
}
