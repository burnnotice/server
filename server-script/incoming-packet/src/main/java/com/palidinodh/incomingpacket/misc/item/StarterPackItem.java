package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId(ItemId.STARTER_PACK_32288)
class StarterPackItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!Settings.getInstance().isLocal()
        && !Settings.getInstance().isBeta()
        && !player.hasVoted()
        && player.getRights() == Player.RIGHTS_NONE) {
      player.getGameEncoder().sendMessage("To open this, you first need to vote.");
      player.getGameEncoder().sendMessage("Make sure to relog after voting!");
      return;
    }
    item.remove();
    if (player.getGameMode().isIronType()) {
      player.getInventory().addOrDropItem(ItemId.COINS, 50_000);
      player.getInventory().addItem(ItemId.MONKFISH_NOTED, 100);
      player.getInventory().addItem(ItemId.SUPER_ATTACK_4_NOTED, 5);
      player.getInventory().addItem(ItemId.SUPER_STRENGTH_4_NOTED, 5);
      player.getInventory().addItem(ItemId.SUPER_DEFENCE_4_NOTED, 5);
      player.getInventory().addItem(ItemId.PRAYER_POTION_4_NOTED, 20);
    } else {
      player.getInventory().addOrDropItem(ItemId.COINS, 200_000);
    }
  }
}
