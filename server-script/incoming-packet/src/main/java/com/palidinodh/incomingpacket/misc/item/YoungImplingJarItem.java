package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.YOUNG_IMPLING_JAR)
class YoungImplingJarItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems =
        RandomItem.buildList(
            new RandomItem(1539, 5) /* Steel nails */,
            new RandomItem(1901, 1) /* Chocolate slice */,
            new RandomItem(7936, 1) /* Pure essence */,
            new RandomItem(1523, 1) /* Lockpick */,
            new RandomItem(361, 1) /* Tuna */,
            new RandomItem(453, 1) /* Coal */,
            new RandomItem(1777, 1) /* Bow string */,
            new RandomItem(1353, 1) /* Steel axe */,
            new RandomItem(1157, 1) /* Steel full helm */,
            new RandomItem(1097, 1) /* Studded chaps */,
            new RandomItem(2293, 1) /* Meat pizza */,
            new RandomItem(247, 1) /* Jangerberries */,
            new RandomItem(2359, 1) /* Mithril bar */,
            new RandomItem(231, 1) /* Snape grass */,
            new RandomItem(2432, 1) /* Defence potion(4) */,
            new RandomItem(855, 1) /* Yew longbow */);
    player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
    if (PRandom.inRange(player.getCombat().getDropRate(2))) {
      player.getInventory().addOrDropItem(ClueScrollType.EASY.getScrollId());
    }
  }
}
