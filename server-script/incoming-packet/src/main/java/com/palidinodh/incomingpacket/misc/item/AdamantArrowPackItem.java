package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ADAMANT_ARROW_PACK)
class AdamantArrowPackItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    player.getInventory().deleteItem(ItemId.ADAMANT_ARROW_PACK, 1);
    player.getInventory().addOrDropItem(ItemId.ADAMANT_ARROW, 50);
  }
}
