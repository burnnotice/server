package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.BABY_IMPLING_JAR)
class BabyImplingJarItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems =
        RandomItem.buildList(
            new RandomItem(1755, 1) /* Chisel */,
            new RandomItem(1734, 1) /* Thread */,
            new RandomItem(946, 1) /* Knife */,
            new RandomItem(1985, 1) /* Cheese */,
            new RandomItem(2347, 1) /* Hammer */,
            new RandomItem(1759, 1) /* Ball of wool */,
            new RandomItem(1927, 1) /* Bucket of milk */,
            new RandomItem(319, 1) /* Anchovies */,
            new RandomItem(2007, 1) /* Spice */,
            new RandomItem(1779, 1) /* Flax */,
            new RandomItem(7170, 1) /* Mud pie */,
            new RandomItem(401, 1) /* Seaweed */,
            new RandomItem(1438, 1) /* Air talisman */,
            new RandomItem(2355, 1) /* Silver bar */,
            new RandomItem(1607, 1) /* Sapphire */,
            new RandomItem(1743, 1) /* Hard leather */,
            new RandomItem(379, 1) /* Lobster */,
            new RandomItem(1761, 1) /* Soft clay */);
    player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
    if (PRandom.inRange(player.getCombat().getDropRate(0.1))) {
      player.getInventory().addOrDropItem(ClueScrollType.EASY.getScrollId());
    }
  }
}
