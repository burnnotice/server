package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.ViewportContainer;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PLogger;

@ReferenceId({WidgetId.LOGOUT, WidgetId.WORLD_SELECT})
class LogoutWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (player.isLocked()) {
      return;
    }
    if (widgetId == WidgetId.LOGOUT) {
      switch (childId) {
        case 3:
          player.getWidgetManager().sendWidget(ViewportContainer.LOGOUT, WidgetId.WORLD_SELECT);
          player.getGameEncoder().sendWidgetSettings(WidgetId.WORLD_SELECT, 18, 0, 20, 2);
          break;
        case 8:
          player.getController().logoutWidgetHook();
          if (!player.canLogout() || player.getPlugin(WildernessPlugin.class).hasBloodyKey()) {
            player.getGameEncoder().sendMessage("You can't logout right now.");
            player.getCombat().getTzHaar().pause();
            return;
          }
          if (player.isNewAccount()) {
            player
                .getGameEncoder()
                .sendEnterString(
                    "Will you return? If not, why?",
                    value -> {
                      if (!player.canLogout()
                          || player.getPlugin(WildernessPlugin.class).hasBloodyKey()) {
                        player.getGameEncoder().sendMessage("You can't logout right now.");
                        player.getCombat().getTzHaar().pause();
                        return;
                      }
                      player.getGameEncoder().sendLogout();
                      player.setVisible(false);
                      PLogger.println("[New User Feedback] " + player.getUsername() + ": " + value);
                    });
            // } else if (PTime.betweenMilliToHour(player.getCreationTime()) < 4) {
            // player.openDialogue(new FeedbackDialogue(player));
          } else {
            player.getGameEncoder().sendLogout();
            player.setVisible(false);
          }
          break;
      }
    } else if (widgetId == WidgetId.WORLD_SELECT) {
      switch (childId) {
        case 3:
          player.getWidgetManager().sendWidget(ViewportContainer.LOGOUT, WidgetId.LOGOUT);
          player.getWidgetManager().sendLogoutText();
          break;
        case 18:
          player.getController().logoutWidgetHook();
          if (!player.canLogout() || player.getPlugin(WildernessPlugin.class).hasBloodyKey()) {
            player.getGameEncoder().sendMessage("You can't logout right now.");
            player.getCombat().getTzHaar().pause();
            return;
          }
          if (slot == 1) {
            player.putAttribute("swap_world_ip", "world1.battle-scape.com");
            player.putAttribute("swap_world_id", 1);
            player.putAttribute("swap_world_mask", 1 + 33554432);
          } else if (slot == 2) {
            player.putAttribute("swap_world_ip", "s2-world1.battle-scape.com");
            player.putAttribute("swap_world_id", 2);
            player.putAttribute("swap_world_mask", 1 + 33554432);
          } else if (slot == 3) {
            player.putAttribute("swap_world_ip", "s2-world2.battle-scape.com");
            player.putAttribute("swap_world_id", 3);
            player.putAttribute("swap_world_mask", 1 + 33554432);
          }
          player.setVisible(false);
          break;
        case 23:
          player.getController().logoutWidgetHook();
          if (!player.canLogout() || player.getPlugin(WildernessPlugin.class).hasBloodyKey()) {
            player.getGameEncoder().sendMessage("You can't logout right now.");
            player.getCombat().getTzHaar().pause();
            return;
          }
          player.getGameEncoder().sendLogout();
          player.setVisible(false);
          break;
      }
    }
  }

  public static class FeedbackDialogue extends OptionsDialogue {

    public FeedbackDialogue(Player player) {
      addOption(
          "Leave feedback and logout.",
          (c, s) -> {
            player
                .getGameEncoder()
                .sendEnterString(
                    "Feedback:",
                    value -> {
                      if (!player.canLogout()
                          || player.getPlugin(WildernessPlugin.class).hasBloodyKey()) {
                        player.getGameEncoder().sendMessage("You can't logout right now.");
                        player.getCombat().getTzHaar().pause();
                        return;
                      }
                      // JavaCord.sendMessage(DiscordChannel.FEEDBACK,
                      // "[" + player.getId() + "] " + player.getUsername() + ": " + value);
                      player.getGameEncoder().sendLogout();
                      player.setVisible(false);
                    });
          });
      addOption(
          "Logout.",
          (c, s) -> {
            player.getGameEncoder().sendLogout();
            player.setVisible(false);
          });
    }
  }
}
