package com.palidinodh.incomingpacket;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.io.Stream;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;

class RotateWidgetDecoder extends IncomingPacketDecoder {

  @Override
  public boolean execute(Player player, Stream stream) {
    var useWidgetHash = getInt(InStreamKey.USE_WIDGET_HASH);
    var useWidgetId = WidgetHandler.getWidgetId(useWidgetHash);
    var useWidgetChildId = WidgetHandler.getWidgetChildId(useWidgetHash);
    var useWidgetSlot = getInt(InStreamKey.USE_WIDGET_SLOT);
    var useItemId = getInt(InStreamKey.USE_ITEM_ID);
    var onWidgetHash = getInt(InStreamKey.ON_WIDGET_HASH);
    if (!containsKey(InStreamKey.ON_WIDGET_HASH)) {
      onWidgetHash = useWidgetHash;
    }
    var onWidgetId = WidgetHandler.getWidgetId(onWidgetHash);
    var onWidgetChildId = WidgetHandler.getWidgetChildId(onWidgetHash);
    var onWidgetSlot = getInt(InStreamKey.ON_WIDGET_SLOT);
    var onItemId = getInt(InStreamKey.ON_ITEM_ID);
    player.clearIdleTime();
    var message =
        "[RotateWidget] useWidgetId="
            + useWidgetId
            + "/"
            + WidgetId.valueOf(useWidgetId)
            + "; useWidgetChildId="
            + useWidgetChildId
            + "; onWidgetId="
            + onWidgetId
            + "/"
            + WidgetId.valueOf(onWidgetId)
            + "; onWidgetChildId="
            + onWidgetChildId
            + "; useItemId="
            + useItemId
            + "/"
            + ItemId.valueOf(useItemId)
            + "; useWidgetSlot="
            + useWidgetSlot
            + "; onItemId="
            + onItemId
            + "/"
            + ItemId.valueOf(onItemId)
            + "; onWidgetSlot="
            + onWidgetSlot;
    player.log(PlayerLogType.DECODER_DEBUG, message);
    if (Settings.getInstance().isLocal()) {
      PLogger.println(message);
    }
    if (player.getOptions().isDebug()) {
      player.getGameEncoder().sendMessage(message);
    }
    RequestManager.addUserPacketLog(player, message);
    if (player.isLocked()) {
      return true;
    }
    if (!player.getMovement().isTeleportStateNone()) {
      return true;
    }
    if (player.getMovement().isViewing()) {
      return true;
    }
    if (!player.getWidgetManager().hasWidget(useWidgetId)) {
      return false;
    }
    if (!player.getWidgetManager().hasWidget(onWidgetId)) {
      return false;
    }
    if (player.getDialogue() != null && player.getDialogue().isDisableActions()) {
      return true;
    }
    var widgetHandler = WidgetHandler.getHandler(player, useWidgetId);
    if (widgetHandler == null) {
      widgetHandler = WidgetDecoder.WIDGET_HANDLERS.get(useWidgetId);
    }
    if (widgetHandler != null
        && widgetHandler.rotateWidget(
            player,
            useWidgetId,
            useWidgetChildId,
            onWidgetId,
            onWidgetChildId,
            useWidgetSlot,
            useItemId,
            onWidgetSlot,
            onItemId)) {
      return true;
    }
    if (useWidgetId != onWidgetId) {
      widgetHandler = WidgetHandler.getHandler(player, onWidgetId);
      if (widgetHandler == null) {
        widgetHandler = WidgetDecoder.WIDGET_HANDLERS.get(onWidgetId);
      }
      if (widgetHandler != null
          && widgetHandler.rotateWidget(
              player,
              useWidgetId,
              useWidgetChildId,
              onWidgetId,
              onWidgetChildId,
              useWidgetSlot,
              useItemId,
              onWidgetSlot,
              onItemId)) {
        return true;
      }
    }
    return true;
  }
}
