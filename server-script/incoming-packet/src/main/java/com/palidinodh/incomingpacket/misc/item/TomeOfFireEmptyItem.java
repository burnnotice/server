package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.TOME_OF_FIRE_EMPTY)
class TomeOfFireEmptyItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getCount(20718) < 10) {
      player.getGameEncoder().sendMessage("You need 10 burnt pages to do this.");
      return;
    }
    item.remove();
    player.getInventory().deleteItem(20718, 10);
    player.getInventory().addItem(20714, 1);
  }
}
