package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ROGUE_OUTFIT_THIEVING_32295)
class RogueOutfitThievingItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 5 - 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove();
    player.getInventory().addItem(ItemId.ROGUE_MASK);
    player.getInventory().addItem(ItemId.ROGUE_TOP);
    player.getInventory().addItem(ItemId.ROGUE_TROUSERS);
    player.getInventory().addItem(ItemId.ROGUE_BOOTS);
    player.getInventory().addItem(ItemId.ROGUE_GLOVES);
  }
}
