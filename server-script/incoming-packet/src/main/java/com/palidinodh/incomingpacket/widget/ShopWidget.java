package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.npc.NKillLog;
import com.palidinodh.osrscore.model.entity.player.Loadout;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  WidgetId.TOURNAMENT_SUPPLIES,
  WidgetId.SHOP,
  WidgetId.SHOP_INVENTORY,
  WidgetId.SHOP_1010,
  WidgetId.SHOP_1013,
  WidgetId.SHOP_1014
})
class ShopWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (player.isLocked()) {
      return;
    }
    if (widgetId == WidgetId.TOURNAMENT_SUPPLIES) {
      switch (childId) {
        case 31:
          if (option == 0) {
            player.getLoadout().preview(slot);
          } else if (option == 1) {
            player.getLoadout().setAsQuick(slot - Loadout.OFFSET);
          } else if (option == 2) {
            player.getLoadout().rename(slot - Loadout.OFFSET);
          } else if (option == 3) {
            player.getLoadout().replace(slot - Loadout.OFFSET);
          } else if (option == 4) {
            player.getLoadout().insertNew(slot - Loadout.OFFSET);
          } else if (option == 5) {
            player.getLoadout().remove(slot - Loadout.OFFSET);
          }
          break;
        case 33:
          player.getLoadout().apply();
          break;
      }
    } else if (widgetId == WidgetId.SHOP) {
      switch (childId) {
        case 16:
          if (player.getGrandExchange().getViewingUserId() != -1) {
            if (option == 0) {
              player.getGrandExchange().sendShopPrice(itemId, slot - 1);
            } else if (option == 1) {
              player.getGrandExchange().buyShopItem(itemId, slot - 1, 1);
            } else if (option == 2) {
              player.getGrandExchange().buyShopItem(itemId, slot - 1, 5);
            } else if (option == 3) {
              player.getGrandExchange().buyShopItem(itemId, slot - 1, 10);
            } else if (option == 4) {
              player
                  .getGameEncoder()
                  .sendEnterAmount(
                      value -> {
                        player.getGrandExchange().buyShopItem(itemId, slot - 1, value);
                      });
            }
          } else if (player.getShop() == null) {
            NKillLog nKillLog = player.getCombat().getNPCKillLog();
            if (nKillLog == null || nKillLog.getItems() == null) {
              return;
            }
            player
                .getGameEncoder()
                .sendMessage(ItemDef.getName(itemId) + ": " + nKillLog.getItemOnCounts(itemId));
            return;
          }
          break;
      }
    } else if (widgetId == WidgetId.SHOP_INVENTORY) {
      switch (childId) {
        case 0:
          if (player.getShop() != null) {
            if (option == 0) {
              player.getShop().sendInventoryPrice(player, itemId, slot);
            } else if (option == 1) {
              player.getShop().sellInventoryItem(player, itemId, slot, 1);
            } else if (option == 2) {
              player.getShop().sellInventoryItem(player, itemId, slot, 5);
            } else if (option == 3) {
              player.getShop().sellInventoryItem(player, itemId, slot, 10);
            } else if (option == 4) {
              player.getShop().sellInventoryItem(player, itemId, slot, 50);
            } else if (option == 5) {
              player
                  .getGameEncoder()
                  .sendEnterAmount(
                      value -> {
                        player.getShop().sellInventoryItem(player, itemId, slot, value);
                      });
            }
          } else if (player.getGrandExchange().getViewingUserId() != -1) {
            if (option == 0) {
              player.getGrandExchange().sendInventoryPrice(itemId, slot);
            } else if (option == 1) {
              player.getGrandExchange().sellInventoryItem(itemId, slot, 1);
            } else if (option == 2) {
              player.getGrandExchange().sellInventoryItem(itemId, slot, 5);
            } else if (option == 3) {
              player.getGrandExchange().sellInventoryItem(itemId, slot, 10);
            } else if (option == 4) {
              player.getGrandExchange().sellInventoryItem(itemId, slot, 50);
            } else if (option == 5) {
              player
                  .getGameEncoder()
                  .sendEnterAmount(
                      value -> {
                        player.getGrandExchange().sellInventoryItem(itemId, slot, value);
                      });
            }
          }
          break;
      }
    } else if (widgetId == WidgetId.SHOP_1014) {
      if (player.getShop() == null) {
        return;
      }
      switch (childId) {
        case 18:
        case 21:
        case 24:
        case 27:
          player.putAttribute("shop_tab", (childId - 18) / 3);
          player.getShop().sendItems(player);
          break;
        case 32:
          var itemSlot = slot / 4;
          if (option == 0) {
            player.getShop().sendShopPrice(player, itemSlot);
          } else if (option == 1) {
            player.getShop().buyShopItem(player, itemSlot, 1);
          } else if (option == 2) {
            player.getShop().buyShopItem(player, itemSlot, 5);
          } else if (option == 3) {
            player.getShop().buyShopItem(player, itemSlot, 10);
          } else if (option == 4) {
            player.getShop().buyShopItem(player, itemSlot, 50);
          } else if (option == 5) {
            player
                .getGameEncoder()
                .sendEnterAmount(
                    value -> {
                      player.getShop().buyShopItem(player, itemSlot, value);
                    });
          }
          break;
      }
    }
  }
}
