package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.LUMBERYARD_TELEPORT)
class LumberyardTeleportItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var height = 0;
    if ((player.inEdgeville() || player.getArea().inWilderness())
        && player.getClientHeight() == 0) {
      height = player.getHeight();
    }
    var tile = new Tile(3303, 3488, height);
    if (!player.getController().canTeleport(tile, true)) {
      return;
    }
    item.remove();
    player
        .getMovement()
        .animatedTeleport(
            tile,
            Magic.TABLET_ANIMATION_START,
            Magic.TABLET_ANIMATION_END,
            -1,
            null,
            Magic.TABLET_GRAPHIC,
            null,
            0,
            2);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
    player.log(PlayerLogType.TELEPORT, "Lumberyard using a scroll");
  }
}
