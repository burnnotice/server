package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Options;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId({WidgetId.OPTIONS, WidgetId.ADVANCED_OPTIONS})
class OptionsWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId == WidgetId.OPTIONS) {
      if (childId >= 18 && childId <= 21) {
        player.getOptions().setBrightness(childId - 18 + 1);
      } else if (childId >= 45 && childId <= 49) {
        player.getOptions().setMusicVolume(49 - childId);
      } else if (childId >= 51 && childId <= 55) {
        player.getOptions().setSoundEffectVolume(55 - childId);
      } else if (childId >= 57 && childId <= 61) {
        player.getOptions().setAreaSoundVolume(61 - childId);
      }
      switch (childId) {
        case 5:
          player.getOptions().setMouseWheelZoom(!player.getOptions().getMouseWheelZoom());
          break;
        case 24:
          player.getOptions().setFramesPerSecond(1);
          break;
        case 26:
          player.getOptions().setFramesPerSecond(2);
          break;
        case 28:
          player.getOptions().setFramesPerSecond(3);
          break;
        case 30:
          player.getOptions().setFramesPerSecond(0);
          break;
        case 35:
          if (player.isLocked()) {
            break;
          }
          player.getWidgetManager().sendInteractiveOverlay(WidgetId.ADVANCED_OPTIONS);
          break;
        case 39:
          player.getOptions().setClickThroughChatbox(!player.getOptions().getClickThroughChatbox());
          break;
        case 63:
          player.getOptions().setChatEffects(!player.getOptions().getChatEffects());
          break;
        case 65:
          player.getOptions().setSplitPrivateChat(!player.getOptions().getSplitPrivateChat());
          break;
        case 67:
          player.getOptions().setHidePrivateChat(!player.getOptions().getHidePrivateChat());
          break;
        case 69:
          player.getOptions().setProfanityFilter(!player.getOptions().getProfanityFilter());
          break;
        case 73:
          player
              .getOptions()
              .setLogNotificationTimeout(!player.getOptions().getLogNotificationTimeout());
          break;
        case 75:
          player.getGameEncoder().sendOpenUrl(Settings.getInstance().getSupportUrl());
          break;
        case 77:
          player.getOptions().setOneMouseButton(!player.getOptions().getOneMouseButton());
          break;
        case 79:
          player
              .getOptions()
              .setMiddleMouseCameraControl(!player.getOptions().getMiddleMouseCameraControl());
          break;
        case 81:
          player
              .getOptions()
              .setFollowerOptionsPriority(!player.getOptions().getFollowerOptionsPriority());
          player.getPlugin(FamiliarPlugin.class).call(true);
          break;
        case 83:
          player.getOptions().sendKeyBindingInterface();
          break;
        case 85:
          player.getOptions().setShiftClickDrop(!player.getOptions().getShiftClickDrop());
          break;
        case 92:
          player.getOptions().setAcceptAid(!player.getOptions().getAcceptAid());
          break;
        case 95:
          player.getMovement().setRunning(!player.getMovement().isRunning());
          break;
        case 100:
          player.getPlugin(BondPlugin.class).openPouch();
          break;
        case 106:
          player.getOptions().setPlayerAttackOption(slot - 1);
          break;
        case 107:
          player.getOptions().setNPCAttackOption(slot - 1);
          break;
      }
    } else if (widgetId == WidgetId.ADVANCED_OPTIONS) {
      if (player.isLocked()) {
        return;
      }
      switch (childId) {
        case 4:
          player.getOptions().setScrollbarLeft(!player.getOptions().getScrollbarLeft());
          break;
        case 6:
          player.getOptions().setOpaqueSidePanel(!player.getOptions().isOpaqueSidePanel());
          break;
        case 8:
          player.getOptions().setRemainingXPTooltips(!player.getOptions().getRemainingXPTooltips());
          break;
        case 10:
          player.getOptions().setPrayerTooltips(!player.getOptions().getPrayerTooltips());
          break;
        case 12:
          player
              .getOptions()
              .setSpecialAttackTooltips(!player.getOptions().getSpecialAttackTooltips());
          break;
        case 16:
          player.getOptions().setDataOrbs(!player.getOptions().getDataOrbs());
          player.getWidgetManager().sendGameViewport();
          break;
        case 18:
          player.getOptions().setTransparentChatbox(!player.getOptions().getTransparentChatbox());
          break;
        case 20:
          player.getOptions().setClickThroughChatbox(!player.getOptions().getClickThroughChatbox());
          break;
        case 21:
          if (player.getOptions().getResizeType() == Options.ResizeType.LINE) {
            player.getOptions().setResizeType(Options.ResizeType.BOX);
          } else {
            player.getOptions().setResizeType(Options.ResizeType.LINE);
          }
          player.getWidgetManager().sendGameViewport();
          break;
        case 23:
          player.getOptions().setSidePanelsClosable(!player.getOptions().getSidePanelsClosable());
          break;
      }
    }
  }
}
