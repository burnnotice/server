package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({WidgetId.EQUIPMENT, WidgetId.EQUIPMENT_BONUSES, WidgetId.EQUIPMENT_BONUSES_INVENTORY})
class EquipmentWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (player.isLocked()) {
      return;
    }
    if (widgetId == WidgetId.EQUIPMENT || widgetId == WidgetId.EQUIPMENT_BONUSES) {
      if (childId == 17) {
        player.getEquipment().openStats();
      } else if (childId == 21) {
        player.getCombat().openItemsKeptOnDeath();
      } else if (childId == 23) {
        player.getPlugin(FamiliarPlugin.class).call(false);
      }
    } else if (widgetId == WidgetId.EQUIPMENT_BONUSES_INVENTORY) {
      switch (childId) {
        case 0:
          player.getEquipment().equip(itemId, slot);
          break;
      }
    }
  }

  @Override
  public boolean rotateWidget(
      Player player,
      int useWidgetId,
      int useChildId,
      int onWidgetId,
      int onChildId,
      int useSlot,
      int useItemId,
      int onSlot,
      int onItemId) {
    if (useWidgetId == WidgetId.EQUIPMENT_BONUSES_INVENTORY
        && onWidgetId == WidgetId.EQUIPMENT_BONUSES_INVENTORY) {
      player.getInventory().rotateItems(useSlot, onSlot);
      return true;
    }
    return false;
  }
}
