package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.GOURMET_IMPLING_JAR)
class GourmetImplingJarItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems =
        RandomItem.buildList(
            new RandomItem(365, 1) /* Bass */,
            new RandomItem(361, 1) /* Tuna */,
            new RandomItem(2011, 1) /* Curry */,
            new RandomItem(2327, 1) /* Meat pie */,
            new RandomItem(1897, 1) /* Chocolate cake */,
            new RandomItem(5004, 1) /* Frog spawn */,
            new RandomItem(2007, 1) /* Spice */,
            new RandomItem(5970, 1) /* Curry leaf */,
            new RandomItem(1883, 1) /* Ugthanki kebab */,
            new RandomItem(380, 4) /* Lobster (noted) */,
            new RandomItem(386, 3) /* Shark (noted) */,
            new RandomItem(7188, 1) /* Fish pie */,
            new RandomItem(7754, 1) /* Chef's delight */,
            new RandomItem(10137, 5) /* Rainbow fish (noted) */,
            new RandomItem(7179, 6) /* Garden pie (noted) */,
            new RandomItem(374, 3) /* Swordfish (noted) */,
            new RandomItem(5406, 1) /* Strawberries(5) */,
            new RandomItem(3145, 2) /* Cooked karambwan (noted) */);
    player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
    if (PRandom.inRange(player.getCombat().getDropRate(4))) {
      player.getInventory().addOrDropItem(ClueScrollType.EASY.getScrollId());
    }
  }
}
