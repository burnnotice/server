package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ROYAL_SEED_POD)
class RoyalSeedPodItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(30, true)) {
      return;
    }
    var height = 0;
    if ((player.inEdgeville() || player.getArea().inWilderness())
        && player.getClientHeight() == 0) {
      height = player.getHeight();
    }
    var homeTile = player.getWidgetManager().getHomeTile();
    if (height != 0) {
      homeTile = new Tile(World.DEFAULT_TILE).randomize(2);
      homeTile.setHeight(height);
    }
    if (player.getArea().inPvpWorld()) {
      homeTile = new Tile(3093, 3495, height);
    }
    player
        .getMovement()
        .animatedTeleport(
            homeTile,
            Magic.SEEDPOD_ANIMATION_START,
            Magic.SEEDPOD_ANIMATION_MID,
            Magic.SEEDPOD_ANIMATION_END,
            Magic.SEEDPOD_START_GRAPHIC,
            null,
            Magic.SEEDPOD_END_GRAPHIC,
            0,
            2);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
  }
}
