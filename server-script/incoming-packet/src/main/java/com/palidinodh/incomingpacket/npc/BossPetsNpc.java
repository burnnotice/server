package com.palidinodh.incomingpacket.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import java.lang.reflect.Array;

@ReferenceId({
  NpcId.KALPHITE_PRINCESS_6653,
  NpcId.KALPHITE_PRINCESS_6654,
  NpcId.CORPOREAL_CRITTER,
  NpcId.DARK_CORE_388,
  NpcId.SNAKELING_2127,
  NpcId.SNAKELING_2128,
  NpcId.SNAKELING_2129,
  NpcId.IKKLE_HYDRA_8517,
  NpcId.IKKLE_HYDRA_8518,
  NpcId.IKKLE_HYDRA_8519,
  NpcId.IKKLE_HYDRA_8520,
  NpcId.JAL_NIB_REK,
  NpcId.TZREK_ZUK,
  NpcId.MIDNIGHT,
  NpcId.NOON,
  NpcId.VETION_JR,
  NpcId.VETION_JR_5537,
  NpcId.OLMLET,
  NpcId.TEKTINY,
  NpcId.VESPINA,
  NpcId.VANGUARD,
  NpcId.PUPPADILE,
  NpcId.VASA_MINIRIO
})
class BossPetsNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (npc != player.getPlugin(FamiliarPlugin.class).getFamiliar()) {
      return;
    }
    switch (npc.getId()) {
      case NpcId.KALPHITE_PRINCESS_6653:
        player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.KALPHITE_PRINCESS_6654);
        break;
      case NpcId.KALPHITE_PRINCESS_6654:
        player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.KALPHITE_PRINCESS_6653);
        break;
      case NpcId.CORPOREAL_CRITTER:
        player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.DARK_CORE_388);
        break;
      case NpcId.DARK_CORE_388:
        player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.CORPOREAL_CRITTER);
        break;
      case NpcId.SNAKELING_2127:
        switch (PRandom.randomI(1)) {
          case 0:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.SNAKELING_2128);
            break;
          case 1:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.SNAKELING_2129);
            break;
        }
        break;
      case NpcId.SNAKELING_2128:
        switch (PRandom.randomI(1)) {
          case 0:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.SNAKELING_2127);
            break;
          case 1:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.SNAKELING_2129);
            break;
        }
        break;
      case NpcId.SNAKELING_2129:
        switch (PRandom.randomI(1)) {
          case 0:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.SNAKELING_2128);
            break;
          case 1:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.SNAKELING_2127);
            break;
        }
        break;
      case NpcId.IKKLE_HYDRA_8517:
        switch (PRandom.randomE(2)) {
          case 0:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8518);
            break;
          case 1:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8519);
            break;
          case 2:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8520);
            break;
        }
        break;
      case NpcId.IKKLE_HYDRA_8518:
        switch (PRandom.randomI(2)) {
          case 0:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8517);
            break;
          case 1:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8519);
            break;
          case 2:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8520);
            break;
        }
        break;
      case NpcId.IKKLE_HYDRA_8519:
        switch (PRandom.randomI(2)) {
          case 0:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8518);
            break;
          case 1:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8517);
            break;
          case 2:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8520);
            break;
        }
        break;
      case NpcId.IKKLE_HYDRA_8520:
        switch (PRandom.randomI(2)) {
          case 0:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8518);
            break;
          case 1:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8519);
            break;
          case 2:
            player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.IKKLE_HYDRA_8517);
            break;
        }
        break;
      case NpcId.TZREK_ZUK:
        player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.JAL_NIB_REK);
        break;
      case NpcId.JAL_NIB_REK:
        player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.TZREK_ZUK);
        break;
      case NpcId.MIDNIGHT:
        player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.NOON);
        break;
      case NpcId.NOON:
        player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.MIDNIGHT);
        break;
      case NpcId.VETION_JR:
        player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.VETION_JR_5537);
        break;
      case NpcId.VETION_JR_5537:
        player.getPlugin(FamiliarPlugin.class).transformPet(NpcId.VETION_JR);
        break;
      case NpcId.OLMLET:
      case NpcId.VASA_MINIRIO:
      case NpcId.TEKTINY:
      case NpcId.VESPINA:
      case NpcId.VANGUARD:
      case NpcId.PUPPADILE:
        transformOlmlet(player, npc);
        break;
    }
  }

  public void transformOlmlet(Player player, Npc npc) {
    if (!player.getCombat().getMetamorphicDust()) {
      player
          .getGameEncoder()
          .sendMessage("You haven't unlocked the ability to metamorphose your Olmlet.");
      return;
    }

    int[] transformable = {
      NpcId.OLMLET,
      NpcId.TEKTINY,
      NpcId.VESPINA,
      NpcId.VANGUARD,
      NpcId.PUPPADILE,
      NpcId.VASA_MINIRIO
    };

    int transformInto = (int) Array.get(transformable, PRandom.randomI(5));

    player.getPlugin(FamiliarPlugin.class).transformPet(transformInto);
  }
}
