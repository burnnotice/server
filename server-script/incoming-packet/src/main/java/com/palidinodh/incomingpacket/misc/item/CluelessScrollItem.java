package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.CLUELESS_SCROLL)
class CluelessScrollItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "read":
        player
            .getGameEncoder()
            .sendMessage("You attempt to make out the text, but it makes no sense whatsoever.");
        break;
    }
  }
}
