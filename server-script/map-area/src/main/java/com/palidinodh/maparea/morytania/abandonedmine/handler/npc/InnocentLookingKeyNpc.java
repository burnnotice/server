package com.palidinodh.maparea.morytania.abandonedmine.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.INNOCENT_LOOKING_KEY)
class InnocentLookingKeyNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (player.getCombat().getHauntedMine() < 2
        || player.getWorld().getTargetNpc(player, NpcId.TREUS_DAYTH_95) != null) {
      player.getGameEncoder().sendMessage("Nothing interesting happens.");
    } else if (player.getCombat().getHauntedMine() == 2) {
      var treus =
          player
              .getController()
              .addNpc(new NpcSpawn(new Tile(2788, 4457, player.getHeight()), NpcId.TREUS_DAYTH_95));
      treus.getCombat().setTarget(player);
    } else if (player.getCombat().getHauntedMine() >= 3) {
      player.getInventory().addItem(4077, 1);
    }
  }
}
