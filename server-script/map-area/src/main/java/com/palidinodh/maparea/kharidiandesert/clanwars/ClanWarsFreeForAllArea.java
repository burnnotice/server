package com.palidinodh.maparea.kharidiandesert.clanwars;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.playerplugin.clanwars.ClanWarsFreeForAllController;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({13130, 13131, 13386, 13387})
public class ClanWarsFreeForAllArea extends Area {

  @Override
  public void tickPlayer() {
    var player = getPlayer();
    if (!player.isLocked()
        && player.getMovement().isTeleportStateNone()
        && !player.getController().is(ClanWarsFreeForAllController.class)) {
      player.setController(new ClanWarsFreeForAllController());
    }
  }

  @Override
  public boolean inMultiCombat() {
    if (!isEntity()) {
      return false;
    }
    var entity = getEntity();
    return entity.getController().isInstanced()
        || entity.getX() >= 3260
            && entity.getY() >= 4800
            && entity.getX() <= 3390
            && entity.getY() <= 4870;
  }
}
