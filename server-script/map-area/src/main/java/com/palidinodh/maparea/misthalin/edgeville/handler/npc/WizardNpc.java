package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Teleports;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.WIZARD_16048)
class WizardNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (option == 0) {
      Teleports.open(player);
    } else if (option == 1) {
      player.openDialogue("fairyring", 0);
    } else if (option == 2) {
      player.openDialogue("spirittree", 0);
    } else if (option == 3) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.JEWELLERY_BOX);
      player
          .getGameEncoder()
          .sendScript(ScriptId.POH_JEWELLERY_BOX_INIT, 15, "Ornate Jewellery Box", 3);
      player
          .getGameEncoder()
          .sendWidgetSettings(WidgetId.JEWELLERY_BOX, 0, 0, 24, WidgetSetting.OPTION_0);
    }
  }

  @Override
  public void itemOnNpc(Player player, int slot, int itemId, Npc npc) {
    player.getPlugin(FamiliarPlugin.class).insurePet(itemId);
  }
}
