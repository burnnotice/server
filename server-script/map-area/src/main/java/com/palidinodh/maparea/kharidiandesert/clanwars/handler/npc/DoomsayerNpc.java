package com.palidinodh.maparea.kharidiandesert.clanwars.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.clanwars.ClanWarsFreeForAllController;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.DOOMSAYER)
class DoomsayerNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (!player.getController().is(ClanWarsFreeForAllController.class)) {
      return;
    }
    player.openShop("clan_wars_free_for_all");
  }
}
