package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.ADAM)
class AdamNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (option == 0) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.PLAY_STYLE_1011);
    } else if (option == 2) {
      if (player.getGameMode().isIronman() || player.getGameMode().isGroupIronman()) {
        player.openShop("ironman");
      } else if (player.getGameMode().isHardcoreIronman()) {
        player.openShop("hardcore_ironman");
      } else {
        player.getGameEncoder().sendMessage("Adam has no reason to trade you.");
      }
    }
  }
}
