package com.palidinodh.maparea.zeah.karuulm.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.STEPS_34530, ObjectId.STEPS_34531})
class DungeonStepsMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.STEPS_34530:
        player.getMovement().ladderUpTeleport(new Tile(1334, 10205, 1));
        break;
      case ObjectId.STEPS_34531:
        player.getMovement().ladderDownTeleport(new Tile(1329, 10205));
        break;
    }
  }
}
