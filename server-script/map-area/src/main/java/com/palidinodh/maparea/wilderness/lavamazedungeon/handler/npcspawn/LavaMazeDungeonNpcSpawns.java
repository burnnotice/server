package com.palidinodh.maparea.wilderness.lavamazedungeon.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class LavaMazeDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3066, 10255), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3068, 10255), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3068, 10257), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3068, 10259), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(4, new Tile(3070, 10257), NpcId.POISON_SPIDER_64));
    spawns.add(new NpcSpawn(2, new Tile(3047, 10264), NpcId.BLACK_DRAGON_227));
    spawns.add(new NpcSpawn(2, new Tile(3053, 10266), NpcId.BLACK_DRAGON_227));
    spawns.add(new NpcSpawn(2, new Tile(3050, 10269), NpcId.BLACK_DRAGON_227));
    spawns.add(new NpcSpawn(2, new Tile(3053, 10271), NpcId.BLACK_DRAGON_227));
    spawns.add(new NpcSpawn(2, new Tile(3051, 10277), NpcId.BABY_DRAGON_83));
    spawns.add(new NpcSpawn(2, new Tile(3052, 10282), NpcId.BABY_DRAGON_83));
    spawns.add(new NpcSpawn(2, new Tile(3054, 10286), NpcId.BABY_DRAGON_83));
    spawns.add(new NpcSpawn(2, new Tile(3044, 10262), NpcId.BABY_DRAGON_83));
    spawns.add(new NpcSpawn(2, new Tile(3050, 10260), NpcId.BABY_DRAGON_83));
    spawns.add(new NpcSpawn(2, new Tile(3028, 10250), NpcId.GREATER_DEMON_92));
    spawns.add(new NpcSpawn(2, new Tile(3032, 10245), NpcId.GREATER_DEMON_92));
    spawns.add(new NpcSpawn(2, new Tile(3039, 10256), NpcId.GREATER_DEMON_92));
    spawns.add(new NpcSpawn(2, new Tile(3036, 10260), NpcId.GREATER_DEMON_92));
    spawns.add(new NpcSpawn(2, new Tile(3030, 10259), NpcId.GREATER_DEMON_92));
    spawns.add(new NpcSpawn(2, new Tile(3032, 10255), NpcId.GREATER_DEMON_92));
    spawns.add(new NpcSpawn(2, new Tile(3048, 10254), NpcId.GREATER_DEMON_92));

    return spawns;
  }
}
