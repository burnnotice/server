package com.palidinodh.maparea.asgarnia.dwarvenmine.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.BELONA)
class BelonaNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (option == 3) {
      player.getSkills().setMiningMinerals(!player.getSkills().getMiningMinerals());
      player
          .getGameEncoder()
          .sendMessage("Minerals while mining: " + player.getSkills().getMiningMinerals());
    } else {
      player.openShop("skilling");
    }
  }
}
