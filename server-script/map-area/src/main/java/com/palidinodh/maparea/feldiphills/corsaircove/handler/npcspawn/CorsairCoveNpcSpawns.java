package com.palidinodh.maparea.feldiphills.corsaircove.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class CorsairCoveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2566, 2859), NpcId.WIZARD_4399));
    spawns.add(new NpcSpawn(new Tile(2553, 2868), NpcId.MADAME_CALDARIUM));
    spawns.add(new NpcSpawn(new Tile(2569, 2866), NpcId.YUSUF_7982));
    spawns.add(new NpcSpawn(4, new Tile(2547, 2856), NpcId.SEAGULL_3));
    spawns.add(new NpcSpawn(4, new Tile(2561, 2864), NpcId.SEAGULL_3));
    spawns.add(new NpcSpawn(4, new Tile(2562, 2853), NpcId.SEAGULL_3));
    spawns.add(new NpcSpawn(4, new Tile(2564, 2857), NpcId.SEAGULL_3));
    spawns.add(new NpcSpawn(4, new Tile(2578, 2862), NpcId.SEAGULL_3));
    spawns.add(new NpcSpawn(4, new Tile(2581, 2862), NpcId.SEAGULL_3));
    spawns.add(new NpcSpawn(4, new Tile(2575, 2852), NpcId.SEAGULL_3));
    spawns.add(new NpcSpawn(4, new Tile(2571, 2852), NpcId.SEAGULL_3));
    spawns.add(new NpcSpawn(4, new Tile(2579, 2846), NpcId.SEAGULL_3));
    spawns.add(new NpcSpawn(4, new Tile(2580, 2850), NpcId.SEAGULL_3));
    spawns.add(new NpcSpawn(4, new Tile(2578, 2853), NpcId.SEAGULL_3));
    spawns.add(new NpcSpawn(4, new Tile(2577, 2856), NpcId.SEAGULL_3));

    return spawns;
  }
}
