package com.palidinodh.maparea.misthalin.wizardstower;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12337)
public class WizardsTowerArea extends Area {}
