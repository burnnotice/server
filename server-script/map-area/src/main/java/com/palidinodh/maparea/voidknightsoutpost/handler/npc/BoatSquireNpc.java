package com.palidinodh.maparea.voidknightsoutpost.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.SQUIRE_1769)
class BoatSquireNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (player.getController().getExitTile() != null) {
      player.getController().stop();
    }
  }
}
