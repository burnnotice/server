package com.palidinodh.maparea.voidknightsoutpost;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10537)
public class VoidKnightsOutpostArea extends Area {}
