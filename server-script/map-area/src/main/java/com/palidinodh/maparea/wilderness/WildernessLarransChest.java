package com.palidinodh.maparea.wilderness;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import java.util.ArrayList;
import java.util.List;

public class WildernessLarransChest {

  private static final List<RandomItem> BIG_ITEMS;

  static {
    var items = new ArrayList<RandomItem>();
    items.add(new RandomItem(ItemId.UNCUT_DIAMOND, 35, 45).weight(256 / 12));
    items.add(new RandomItem(ItemId.UNCUT_RUBY, 35, 45).weight(256 / 12));
    items.add(new RandomItem(ItemId.COAL, 450, 650).weight(256 / 12));
    items.add(new RandomItem(ItemId.COINS, 75_000, 175_000).weight(256 / 12));
    items.add(new RandomItem(ItemId.GOLD_ORE, 150, 250).weight(256 / 15));
    items.add(new RandomItem(ItemId.DRAGON_ARROWTIPS, 100, 250).weight(256 / 15));
    items.add(new RandomItem(ItemId.IRON_ORE, 500, 644).weight(256 / 20));
    items.add(new RandomItem(ItemId.RUNE_FULL_HELM, 3, 5).weight(256 / 20));
    items.add(new RandomItem(ItemId.RUNE_PLATEBODY, 2, 3).weight(256 / 20));
    items.add(new RandomItem(ItemId.RUNE_PLATELEGS, 2, 3).weight(256 / 20));
    items.add(new RandomItem(ItemId.RAW_TUNA, 229, 462).weight(256 / 20));
    items.add(new RandomItem(ItemId.RAW_LOBSTER, 184, 492).weight(256 / 20));
    items.add(new RandomItem(ItemId.RAW_SWORDFISH, 220, 445).weight(256 / 20));
    items.add(new RandomItem(ItemId.RAW_MONKFISH, 256, 403).weight(256 / 20));
    items.add(new RandomItem(ItemId.RAW_SHARK, 352, 375).weight(256 / 20));
    items.add(new RandomItem(ItemId.RAW_SEA_TURTLE, 127, 288).weight(256 / 20));
    items.add(new RandomItem(ItemId.RAW_MANTA_RAY, 100, 200).weight(256 / 20));
    items.add(new RandomItem(ItemId.RUNITE_ORE, 15, 20).weight(256 / 30));
    items.add(new RandomItem(ItemId.STEEL_BAR, 350, 550).weight(256 / 30));
    items.add(new RandomItem(ItemId.MAGIC_LOGS, 180, 220).weight(256 / 30));
    items.add(new RandomItem(ItemId.DRAGON_DART_TIP, 80, 200).weight(256 / 30));
    items.add(new RandomItem(ItemId.PALM_TREE_SEED, 3, 5).weight(256 / 60));
    items.add(new RandomItem(ItemId.MAGIC_SEED, 3, 4).weight(256 / 60));
    items.add(new RandomItem(ItemId.CELASTRUS_SEED, 3, 5).weight(256 / 60));
    items.add(new RandomItem(ItemId.DRAGONFRUIT_TREE_SEED, 3, 5).weight(256 / 60));
    items.add(new RandomItem(ItemId.REDWOOD_TREE_SEED).weight(256 / 60));
    items.add(new RandomItem(ItemId.TORSTOL_SEED, 4, 6).weight(256 / 60));
    items.add(new RandomItem(ItemId.SNAPDRAGON_SEED, 4, 6).weight(256 / 60));
    items.add(new RandomItem(ItemId.RANARR_SEED, 4, 6).weight(256 / 60));
    items.add(new RandomItem(ItemId.PURE_ESSENCE, 4_500, 7_430).weight(256 / 60));
    items.add(new RandomItem(ItemId.DAGONHAI_HAT).weight(256 / 256));
    items.add(new RandomItem(ItemId.DAGONHAI_ROBE_TOP).weight(256 / 256));
    items.add(new RandomItem(ItemId.DAGONHAI_ROBE_BOTTOM).weight(256 / 256));
    BIG_ITEMS = RandomItem.buildList(items);
  }

  public static void open(Player player, MapObject mapObject) {
    if (!player.getInventory().hasItem(ItemId.LARRANS_KEY)) {
      player.getGameEncoder().sendMessage("You need a key to open this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.LARRANS_KEY);
    Item item;
    if (mapObject.getId() == ObjectId.LARRANS_BIG_CHEST_34832) {
      item = RandomItem.getItem(BIG_ITEMS);
    } else {
      return;
    }
    player
        .getInventory()
        .addOrDropItem(item.getAmount() > 1 ? item.getNotedId() : item.getId(), item.getAmount());
    player.getGameEncoder().sendMessage("You find some treasure in the chest!");
    var plugin = player.getPlugin(SlayerPlugin.class);
    plugin.incrimentLarransKeys();
    player
        .getGameEncoder()
        .sendMessage("You have opened " + plugin.getLarransKeys() + " Larrans chests!");
  }
}
