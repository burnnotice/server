package com.palidinodh.maparea.asgarnia.taverley;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Prayer;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import com.palidinodh.random.PRandom;
import java.util.ArrayList;
import java.util.List;

public class TaverleyCrystalChest {

  private static final List<RandomItem> BRIMSTONE_ITEMS;

  static {
    var items = new ArrayList<RandomItem>();
    items = new ArrayList<>();
    items.add(new RandomItem(ItemId.UNCUT_DIAMOND, 25, 35).weight(1000 / 12));
    items.add(new RandomItem(ItemId.UNCUT_RUBY, 25, 35).weight(1000 / 12));
    items.add(new RandomItem(ItemId.COAL, 300, 500).weight(1000 / 12));
    items.add(new RandomItem(ItemId.COINS, 50_000, 150_000).weight(1000 / 12));
    items.add(new RandomItem(ItemId.GOLD_ORE, 100, 200).weight(1000 / 15));
    items.add(new RandomItem(ItemId.DRAGON_ARROWTIPS, 50, 200).weight(1000 / 15));
    items.add(new RandomItem(ItemId.IRON_ORE, 350, 500).weight(1000 / 20));
    items.add(new RandomItem(ItemId.RUNE_FULL_HELM, 2, 4).weight(1000 / 20));
    items.add(new RandomItem(ItemId.RUNE_PLATEBODY, 1, 2).weight(1000 / 20));
    items.add(new RandomItem(ItemId.RUNE_PLATELEGS, 1, 2).weight(1000 / 20));
    items.add(new RandomItem(ItemId.RAW_TUNA, 100, 350).weight(1000 / 20));
    items.add(new RandomItem(ItemId.RAW_LOBSTER, 100, 350).weight(1000 / 20));
    items.add(new RandomItem(ItemId.RAW_SWORDFISH, 100, 350).weight(1000 / 20));
    items.add(new RandomItem(ItemId.RAW_MONKFISH, 100, 300).weight(1000 / 20));
    items.add(new RandomItem(ItemId.RAW_SHARK, 100, 250).weight(1000 / 20));
    items.add(new RandomItem(ItemId.RAW_SEA_TURTLE, 80, 200).weight(1000 / 20));
    items.add(new RandomItem(ItemId.RAW_MANTA_RAY, 80, 160).weight(1000 / 20));
    items.add(new RandomItem(ItemId.RUNITE_ORE, 10, 15).weight(1000 / 30));
    items.add(new RandomItem(ItemId.STEEL_BAR, 300, 500).weight(1000 / 30));
    items.add(new RandomItem(ItemId.MAGIC_LOGS, 120, 160).weight(1000 / 30));
    items.add(new RandomItem(ItemId.DRAGON_DART_TIP, 40, 160).weight(1000 / 30));
    items.add(new RandomItem(ItemId.PALM_TREE_SEED, 2, 4).weight(1000 / 60));
    items.add(new RandomItem(ItemId.MAGIC_SEED, 2, 4).weight(1000 / 60));
    items.add(new RandomItem(ItemId.CELASTRUS_SEED, 2, 4).weight(1000 / 60));
    items.add(new RandomItem(ItemId.DRAGONFRUIT_TREE_SEED, 2, 4).weight(1000 / 60));
    items.add(new RandomItem(ItemId.REDWOOD_TREE_SEED).weight(1000 / 60));
    items.add(new RandomItem(ItemId.TORSTOL_SEED, 3, 5).weight(1000 / 60));
    items.add(new RandomItem(ItemId.SNAPDRAGON_SEED, 3, 5).weight(1000 / 60));
    items.add(new RandomItem(ItemId.RANARR_SEED, 3, 5).weight(1000 / 60));
    items.add(new RandomItem(ItemId.PURE_ESSENCE, 3_000, 6_000).weight(1000 / 60));
    items.add(new RandomItem(ItemId.DRAGON_HASTA).weight(1000 / 200));
    items.add(new RandomItem(ItemId.MYSTIC_HAT_DUSK).weight(1));
    items.add(new RandomItem(ItemId.MYSTIC_ROBE_TOP_DUSK).weight(1));
    items.add(new RandomItem(ItemId.MYSTIC_ROBE_BOTTOM_DUSK).weight(1));
    items.add(new RandomItem(ItemId.MYSTIC_GLOVES_DUSK).weight(1));
    items.add(new RandomItem(ItemId.MYSTIC_BOOTS_DUSK).weight(1));
    BRIMSTONE_ITEMS = RandomItem.buildList(items);
  }

  public static void open(Player player, MapObject mapObject) {
    if (player.getInventory().hasItem(ItemId.CRYSTAL_KEY)) {
      openCrystalChest(player, mapObject);
    } else if (player.getInventory().hasItem(ItemId.BRIMSTONE_KEY)) {
      openBrimstoneChest(player, mapObject);
    } else {
      player.getGameEncoder().sendMessage("You need a key to open this.");
    }
  }

  private static void openCrystalChest(Player player, MapObject mapObject) {
    if (!player.getInventory().hasItem(ItemId.CRYSTAL_KEY)) {
      player.getGameEncoder().sendMessage("You need a key to open this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.CRYSTAL_KEY);
    var rolls = mapObject.getId() == ObjectId.CLOSED_CHEST_172 ? 2 : 1;
    for (var i = 0; i < rolls; i++) {
      player.getInventory().addOrDropItem(ItemId.UNCUT_DRAGONSTONE_NOTED);
      var item = MysteryBox.getMysteryBox(ItemId.SKILLING_MYSTERY_BOX_32380).getRandomItem(player);
      if (rolls == 1 && item.getAmount() > 1 && !Prayer.isBone(item.getId())) {
        item.setAmount(item.getAmount() * 2);
      }
      player.getInventory().addOrDropItem(item);
      if (PRandom.randomE(4) == 0) {
        var clueType = PRandom.arrayRandom(ClueScrollType.values());
        player.getInventory().addOrDropItem(clueType.getScrollId());
        player.getPlugin(TreasureTrailPlugin.class).resetProgress(clueType);
      }
    }
    player.getGameEncoder().sendMessage("You find some treasure in the chest!");
    var plugin = player.getPlugin(SlayerPlugin.class);
    plugin.incrimentCrystalKeys();
    player
        .getGameEncoder()
        .sendMessage("You have opened " + plugin.getCrystalKeys() + " Crystal chests!");
  }

  private static void openBrimstoneChest(Player player, MapObject mapObject) {
    if (!player.getInventory().hasItem(ItemId.BRIMSTONE_KEY)) {
      player.getGameEncoder().sendMessage("You need a key to open this.");
      return;
    }
    player.getInventory().deleteItem(ItemId.BRIMSTONE_KEY);
    var count = mapObject.getId() == ObjectId.TREASURE_CHEST_18806 ? 2 : 1;
    for (var i = 0; i < count; i++) {
      var item = RandomItem.getItem(BRIMSTONE_ITEMS);
      player
          .getInventory()
          .addOrDropItem(item.getAmount() > 1 ? item.getNotedId() : item.getId(), item.getAmount());
    }
    player.getGameEncoder().sendMessage("You find some treasure in the chest!");
    var plugin = player.getPlugin(SlayerPlugin.class);
    plugin.incrimentBrimstoneKeys();
    player
        .getGameEncoder()
        .sendMessage("You have opened " + plugin.getBrimstoneKeys() + " Brimstone chests!");
  }
}
