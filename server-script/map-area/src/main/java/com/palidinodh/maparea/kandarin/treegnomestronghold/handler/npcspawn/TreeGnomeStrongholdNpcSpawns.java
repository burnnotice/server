package com.palidinodh.maparea.kandarin.treegnomestronghold.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class TreeGnomeStrongholdNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2438, 3419), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2446, 3426, 1), NpcId.GADRIN));
    spawns.add(new NpcSpawn(2, new Tile(2435, 3411), NpcId.PRISSY_SCILLA));
    spawns.add(new NpcSpawn(2, new Tile(2473, 3446), NpcId.BOLONGO));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(2476, 3443), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(2, new Tile(2446, 3432, 1), NpcId.WIZARD_4399));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2443, 3425, 1), NpcId.GNOME_BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2443, 3424, 1), NpcId.GNOME_BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2448, 3427, 1), NpcId.GNOME_BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2448, 3424, 1), NpcId.GNOME_BANKER));
    spawns.add(new NpcSpawn(4, new Tile(2480, 3431), NpcId.GNOME));
    spawns.add(new NpcSpawn(4, new Tile(2479, 3422), NpcId.GNOME));
    spawns.add(new NpcSpawn(4, new Tile(2470, 3436), NpcId.GNOME));

    return spawns;
  }
}
