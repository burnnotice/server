package com.palidinodh.maparea.entrana;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceIdSet(
    primary = 11416,
    secondary = {
      4, 5, 6, 20, 21, 22, 34, 35, 36, 37, 38, 50, 51, 52, 53, 54, 64, 65, 66, 67, 68, 80, 81, 82,
      83, 96, 97, 98, 99, 114
    })
public class EntranaDungeonArea extends Area {}
