package com.palidinodh.maparea.kandarin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({9263, 9264, 9519, 9520, 9521, 9776})
public class CastleWarsArea extends Area {}
