package com.palidinodh.maparea.misthalin.draynor;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12340)
public class DraynorManorArea extends Area {}
