package com.palidinodh.maparea.karamja.tzhaar.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.tzhaar.TzhaarFightPitController;
import com.palidinodh.playerplugin.tzhaar.TzhaarPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.HOT_VENT_DOOR_11845, ObjectId.HOT_VENT_DOOR_11846})
class HotVentDoorMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.HOT_VENT_DOOR_11845:
        {
          if (player.getY() > 5167) {
            break;
          }
          if (!player.getController().is(TzhaarFightPitController.class)) {
            return;
          }
          player.getController().stop();
          break;
        }
      case ObjectId.HOT_VENT_DOOR_11846:
        {
          if (player.getY() > 5176) {
            player.getPlugin(TzhaarPlugin.class).joinFightPit();
            break;
          }
          if (!player.getController().is(TzhaarFightPitController.class)) {
            break;
          }
          player.getController().stop();
          break;
        }
    }
  }
}
