package com.palidinodh.maparea.asgarnia.heroesguildbasement;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11674)
public class HeroesGuildBasementArea extends Area {}
