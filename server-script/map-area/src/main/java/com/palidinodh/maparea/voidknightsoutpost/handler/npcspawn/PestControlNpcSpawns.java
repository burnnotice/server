package com.palidinodh.maparea.voidknightsoutpost.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class PestControlNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2656, 2614), NpcId.SQUIRE_1769));
    spawns.add(new NpcSpawn(new Tile(2656, 2592), NpcId.VOID_KNIGHT_2950));
    spawns.add(new NpcSpawn(new Tile(2628, 2591), NpcId.PORTAL));
    spawns.add(new NpcSpawn(new Tile(2680, 2588), NpcId.PORTAL_1740));
    spawns.add(new NpcSpawn(new Tile(2645, 2569), NpcId.PORTAL_1742));
    spawns.add(new NpcSpawn(new Tile(2669, 2570), NpcId.PORTAL_1741));
    spawns.add(new NpcSpawn(4, new Tile(2629, 2595), NpcId.SPINNER_55));
    spawns.add(new NpcSpawn(4, new Tile(2643, 2570), NpcId.SPINNER_55));
    spawns.add(new NpcSpawn(4, new Tile(2670, 2568), NpcId.SPINNER_55));
    spawns.add(new NpcSpawn(4, new Tile(2684, 2589), NpcId.SPINNER_55));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2594), NpcId.BRAWLER_51));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2594), NpcId.BRAWLER_51));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2593), NpcId.DEFILER_33));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2593), NpcId.DEFILER_33));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2592), NpcId.TORCHER_33));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2592), NpcId.TORCHER_33));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2591), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2591), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2591), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2591), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2590), NpcId.SHIFTER_38));
    spawns.add(new NpcSpawn(32, new Tile(2632, 2590), NpcId.SHIFTER_38));
    spawns.add(new NpcSpawn(32, new Tile(2648, 2573), NpcId.BRAWLER_51));
    spawns.add(new NpcSpawn(32, new Tile(2648, 2573), NpcId.BRAWLER_51));
    spawns.add(new NpcSpawn(32, new Tile(2647, 2573), NpcId.DEFILER_33));
    spawns.add(new NpcSpawn(32, new Tile(2647, 2573), NpcId.DEFILER_33));
    spawns.add(new NpcSpawn(32, new Tile(2646, 2573), NpcId.TORCHER_33));
    spawns.add(new NpcSpawn(32, new Tile(2646, 2573), NpcId.TORCHER_33));
    spawns.add(new NpcSpawn(32, new Tile(2645, 2573), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2645, 2573), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2645, 2573), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2645, 2573), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2644, 2573), NpcId.SHIFTER_38));
    spawns.add(new NpcSpawn(32, new Tile(2644, 2573), NpcId.SHIFTER_38));
    spawns.add(new NpcSpawn(32, new Tile(2672, 2574), NpcId.BRAWLER_51));
    spawns.add(new NpcSpawn(32, new Tile(2672, 2574), NpcId.BRAWLER_51));
    spawns.add(new NpcSpawn(32, new Tile(2671, 2574), NpcId.DEFILER_33));
    spawns.add(new NpcSpawn(32, new Tile(2671, 2574), NpcId.DEFILER_33));
    spawns.add(new NpcSpawn(32, new Tile(2670, 2574), NpcId.TORCHER_33));
    spawns.add(new NpcSpawn(32, new Tile(2670, 2574), NpcId.TORCHER_33));
    spawns.add(new NpcSpawn(32, new Tile(2669, 2574), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2669, 2574), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2669, 2574), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2669, 2574), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2668, 2574), NpcId.SHIFTER_38));
    spawns.add(new NpcSpawn(32, new Tile(2668, 2574), NpcId.SHIFTER_38));
    spawns.add(new NpcSpawn(32, new Tile(2677, 2591), NpcId.BRAWLER_51));
    spawns.add(new NpcSpawn(32, new Tile(2677, 2591), NpcId.BRAWLER_51));
    spawns.add(new NpcSpawn(32, new Tile(2678, 2590), NpcId.DEFILER_33));
    spawns.add(new NpcSpawn(32, new Tile(2678, 2590), NpcId.DEFILER_33));
    spawns.add(new NpcSpawn(32, new Tile(2678, 2589), NpcId.TORCHER_33));
    spawns.add(new NpcSpawn(32, new Tile(2678, 2589), NpcId.TORCHER_33));
    spawns.add(new NpcSpawn(32, new Tile(2678, 2588), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2678, 2588), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2678, 2588), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2678, 2588), NpcId.RAVAGER_36));
    spawns.add(new NpcSpawn(32, new Tile(2678, 2587), NpcId.SHIFTER_38));
    spawns.add(new NpcSpawn(32, new Tile(2678, 2587), NpcId.SHIFTER_38));

    return spawns;
  }
}
