package com.palidinodh.maparea.kandarin.krakencove.handler.mapobject;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.CREVICE_537, ObjectId.CREVICE_538})
class BossCreviceMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.CREVICE_537:
        player.getPlugin(BossPlugin.class).openBossInstanceDialogue(NpcId.KRAKEN_291);
        break;
      case ObjectId.CREVICE_538:
        player.getMovement().ladderUpTeleport(new Tile(2280, 10016));
        player.getController().stopWithTeleport();
        break;
    }
  }
}
