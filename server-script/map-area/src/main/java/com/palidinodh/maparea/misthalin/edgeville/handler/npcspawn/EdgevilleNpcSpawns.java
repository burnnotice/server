package com.palidinodh.maparea.misthalin.edgeville.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.setting.Settings;
import java.util.ArrayList;
import java.util.List;

class EdgevilleNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(3101, 3508), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3100, 3508), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3100, 3510), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3100, 3512), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3100, 3514), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3100, 3516), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3090, 3506), NpcId.GRAND_EXCHANGE_CLERK));
    spawns.add(
        new NpcSpawn(Tile.Direction.EAST, new Tile(3090, 3507), NpcId.GRAND_EXCHANGE_CLERK_2149));
    spawns.add(
        new NpcSpawn(Tile.Direction.NORTH, new Tile(3089, 3508), NpcId.GRAND_EXCHANGE_CLERK));
    spawns.add(
        new NpcSpawn(Tile.Direction.NORTH, new Tile(3088, 3508), NpcId.GRAND_EXCHANGE_CLERK_2149));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3090, 3505), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3087, 3508), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3096, 3505), NpcId.LOYALTY_MANAGER));
    spawns.add(new NpcSpawn(new Tile(3099, 3507), NpcId.VOTE_MANAGER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3088, 3513), NpcId.CAPT_BOND_16018));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3104, 3496), NpcId.WIZARD_16048));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3098, 3517), NpcId.EMBLEM_TRADER_316));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3107, 3510), NpcId.MAGE_OF_ZAMORAK_2582));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3107, 3512), NpcId.PERDU));
    spawns.add(new NpcSpawn(new Tile(3114, 3516), NpcId.COMBAT_DUMMY));
    spawns.add(new NpcSpawn(new Tile(3115, 3516), NpcId.COMBAT_DUMMY));
    spawns.add(new NpcSpawn(new Tile(3079, 3498), NpcId.HEAD_CHEF));
    spawns.add(new NpcSpawn(new Tile(3077, 3498), NpcId.AJJAT));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3076, 3493), NpcId.MAC_126));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3076, 3491), NpcId.PROBITA));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3080, 3490), NpcId.ADAM));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3078, 3487), NpcId.HAIRDRESSER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3087, 3484), NpcId.TWIGGY_OKORN));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3085, 3481), NpcId.JOSSIK));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3084, 3481), NpcId.EVIL_DAVE_4806));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3083, 3481), NpcId.RADIMUS_ERKLE));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3082, 3481), NpcId.GUILDMASTER));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3081, 3481), NpcId.MONK_OF_ENTRANA));
    spawns.add(
        new NpcSpawn(Tile.Direction.NORTH, new Tile(3080, 3481), NpcId.KING_NARNODE_SHAREEN));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3079, 3481), NpcId.ONEIROMANCER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3078, 3482), NpcId.ZEALOT));
    spawns.add(new NpcSpawn(new Tile(3097, 3487), NpcId.WATSON));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3093, 3481), NpcId.NIEVE));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3094, 3481), NpcId.KRYSTILIA));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3116, 3489), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3116, 3488), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3112, 3485), NpcId.SKILLING_SELLER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3111, 3489), NpcId.CAPN_IZZY_NO_BEARD));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3111, 3490), NpcId.BOB_BARTER_HERBS));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3114, 3485), NpcId.TANNER));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3111, 3502), NpcId.MARTIN_THWAIT));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3092, 3472), NpcId.WISE_OLD_MAN));
    spawns.add(new NpcSpawn(2, new Tile(3069, 3517), NpcId.OZIACH));
    spawns.add(new NpcSpawn(8, new Tile(3093, 3510, 1), NpcId.PARTY_PETE));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3088, 3508, 1), NpcId.MEGAN));
    spawns.add(new NpcSpawn(2, new Tile(3090, 3507, 1), NpcId.LUCY));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3101, 3507, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3101, 3508, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3101, 3509, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3101, 3510, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3101, 3511, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3101, 3512, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3101, 3513, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3101, 3514, 1), NpcId.KNIGHT_5793));
    spawns.add(new NpcSpawn(new Tile(3121, 3487), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(3122, 3487), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(3123, 3487), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(3124, 3487), NpcId.FISHING_SPOT_1518));
    spawns.add(new NpcSpawn(new Tile(3125, 3487), NpcId.FISHING_SPOT_1518));
    spawns.add(new NpcSpawn(new Tile(3126, 3487), NpcId.FISHING_SPOT_1518));
    spawns.add(new NpcSpawn(new Tile(3127, 3487), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(3128, 3487), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(3128, 3487), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(3129, 3487), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(3130, 3487), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(3123, 3489), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(3124, 3489), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(3124, 3490), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(3123, 3490), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(3123, 3491), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(3124, 3491), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(3124, 3492), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(3123, 3492), NpcId.FISHING_SPOT_1510));
    spawns.add(new NpcSpawn(new Tile(3127, 3489), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(3128, 3489), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(3127, 3490), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(3128, 3490), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(3128, 3491), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(3127, 3491), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(3127, 3492), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(3128, 3492), NpcId.FISHING_SPOT_1511));
    spawns.add(new NpcSpawn(new Tile(3123, 3495), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(3124, 3495), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(3124, 3496), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(3123, 3496), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(3123, 3497), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(3124, 3497), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(3124, 3498), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(3123, 3498), NpcId.ROD_FISHING_SPOT_6825));
    spawns.add(new NpcSpawn(new Tile(3121, 3500), NpcId.FISHING_SPOT_1542));
    spawns.add(new NpcSpawn(new Tile(3122, 3500), NpcId.FISHING_SPOT_1542));
    spawns.add(new NpcSpawn(new Tile(3123, 3500), NpcId.FISHING_SPOT_1542));
    spawns.add(new NpcSpawn(new Tile(3124, 3500), NpcId.FISHING_SPOT_1542));
    spawns.add(new NpcSpawn(new Tile(3125, 3500), NpcId.ROD_FISHING_SPOT_7676));
    spawns.add(new NpcSpawn(new Tile(3126, 3500), NpcId.ROD_FISHING_SPOT_7676));
    spawns.add(new NpcSpawn(new Tile(3127, 3500), NpcId.ROD_FISHING_SPOT_7676));
    spawns.add(new NpcSpawn(new Tile(3128, 3500), NpcId.FISHING_SPOT_6488));
    spawns.add(new NpcSpawn(new Tile(3129, 3500), NpcId.FISHING_SPOT_6488));
    spawns.add(new NpcSpawn(new Tile(3130, 3500), NpcId.FISHING_SPOT_6488));
    spawns.add(new NpcSpawn(new Tile(3120, 3489), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(3120, 3490), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(3120, 3491), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(3120, 3492), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(3120, 3495), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(3120, 3496), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(3120, 3497), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(3120, 3498), NpcId.FISHING_SPOT_4316));
    spawns.add(new NpcSpawn(new Tile(3131, 3489), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(3131, 3490), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(3131, 3491), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(3131, 3492), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(3131, 3495), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(3131, 3496), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(3131, 3497), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(3131, 3498), NpcId.FISHING_SPOT_4712));
    spawns.add(new NpcSpawn(new Tile(3128, 3497), NpcId.FISHING_SPOT_7730));
    spawns.add(new NpcSpawn(new Tile(3127, 3496), NpcId.FISHING_SPOT_7731));
    if (Settings.getInstance().isLocal() || Settings.getInstance().isBeta()) {
      spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3093, 3504), NpcId.ELISABETA));
    }

    return spawns;
  }
}
