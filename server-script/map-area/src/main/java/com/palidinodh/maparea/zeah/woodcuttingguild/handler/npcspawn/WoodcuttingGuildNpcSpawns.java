package com.palidinodh.maparea.zeah.woodcuttingguild.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class WoodcuttingGuildNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(2, new Tile(1651, 3499), NpcId.PERRY));
    spawns.add(new NpcSpawn(2, new Tile(1607, 3508), NpcId.MURFET));
    spawns.add(new NpcSpawn(2, new Tile(1624, 3515), NpcId.FORESTER_15_7238));
    spawns.add(new NpcSpawn(2, new Tile(1615, 3489), NpcId.FORESTER_15_7238));
    spawns.add(new NpcSpawn(2, new Tile(1593, 3490), NpcId.FORESTER_15_7238));

    return spawns;
  }
}
