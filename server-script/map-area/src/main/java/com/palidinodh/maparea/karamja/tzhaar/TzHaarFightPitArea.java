package com.palidinodh.maparea.karamja.tzhaar;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(9552)
public class TzHaarFightPitArea extends Area {

  @Override
  public boolean inMultiCombat() {
    return true;
  }
}
