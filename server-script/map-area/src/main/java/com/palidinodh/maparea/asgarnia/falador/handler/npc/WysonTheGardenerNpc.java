package com.palidinodh.maparea.asgarnia.falador.handler.npc;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.WYSON_THE_GARDENER)
class WysonTheGardenerNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    var moleItemIds =
        new int[] {
          ItemId.MOLE_CLAW, ItemId.MOLE_CLAW_NOTED, ItemId.MOLE_SKIN, ItemId.MOLE_SKIN_NOTED
        };
    for (var itemId : moleItemIds) {
      var count =
          Math.min(
              player.getInventory().getCount(itemId), player.getInventory().getRemainingSlots());
      player.getInventory().deleteItem(itemId, count);
      for (var i = 0; i < count; i++) {
        if (PRandom.randomE(10) == 0) {
          player.getInventory().addItem(ItemId.BIRD_NEST_5075);
        } else if (PRandom.randomE(5) == 0) {
          player.getInventory().addItem(ItemId.BIRD_NEST_5074);
        } else {
          player.getInventory().addItem(ItemId.BIRD_NEST_5073);
        }
      }
    }
  }
}
