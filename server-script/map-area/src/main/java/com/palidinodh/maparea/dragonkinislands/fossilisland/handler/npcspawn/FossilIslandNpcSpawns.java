package com.palidinodh.maparea.dragonkinislands.fossilisland.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class FossilIslandNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3680, 3722), NpcId.DERANGED_ARCHAEOLOGIST_276));
    spawns.add(new NpcSpawn(4, new Tile(3682, 3755), NpcId.DERANGED_ARCHAEOLOGIST_276));
    spawns.add(new NpcSpawn(4, new Tile(3685, 3767), NpcId.DERANGED_ARCHAEOLOGIST_276));

    return spawns;
  }
}
