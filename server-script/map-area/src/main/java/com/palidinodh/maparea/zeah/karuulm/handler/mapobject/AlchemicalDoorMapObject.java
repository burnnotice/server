package com.palidinodh.maparea.zeah.karuulm.handler.mapobject;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.ALCHEMICAL_DOOR, ObjectId.ALCHEMICAL_DOOR_34554})
class AlchemicalDoorMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (player.getX() <= 1355) {
      player.getPlugin(BossPlugin.class).start(NpcId.ALCHEMICAL_HYDRA_426, true);
    } else {
      player.getController().stopWithTeleport();
      player.getMovement().teleport(new Tile(1355, 10258));
    }
  }
}
