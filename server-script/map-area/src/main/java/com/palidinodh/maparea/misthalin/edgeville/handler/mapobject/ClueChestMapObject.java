package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.clue.ClueChestType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.CLUE_CHEST_65004)
class ClueChestMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    var plugin = player.getPlugin(TreasureTrailPlugin.class);
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Beginner",
                (c, s) -> {
                  plugin.getChest().open(ClueChestType.BEGINNER);
                }),
            new DialogueOption(
                "Easy",
                (c, s) -> {
                  plugin.getChest().open(ClueChestType.EASY);
                }),
            new DialogueOption(
                "Medium",
                (c, s) -> {
                  plugin.getChest().open(ClueChestType.MEDIUM);
                }),
            new DialogueOption(
                "Hard",
                (c, s) -> {
                  plugin.getChest().open(ClueChestType.HARD);
                }),
            new DialogueOption(
                "Elite",
                (c, s) -> {
                  plugin.getChest().open(ClueChestType.ELITE);
                }),
            new DialogueOption(
                "Master",
                (c, s) -> {
                  plugin.getChest().open(ClueChestType.MASTER);
                })));
  }

  @Override
  public void itemOnMapObject(Player player, Item item, MapObject mapObject) {
    var plugin = player.getPlugin(TreasureTrailPlugin.class);
    plugin.getChest().addItem(item.getId());
  }
}
