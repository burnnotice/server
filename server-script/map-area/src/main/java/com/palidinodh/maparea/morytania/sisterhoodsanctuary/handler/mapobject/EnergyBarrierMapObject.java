package com.palidinodh.maparea.morytania.sisterhoodsanctuary.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.ENERGY_BARRIER_37730)
class EnergyBarrierMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Leave.",
                (c, s) -> {
                  player.getController().stopWithTeleport();
                  player.getMovement().teleport(new Tile(3808, 9753, 1));
                  player.getCombat().clearHitEvents();
                }),
            new DialogueOption("Nevermind.")));
  }
}
