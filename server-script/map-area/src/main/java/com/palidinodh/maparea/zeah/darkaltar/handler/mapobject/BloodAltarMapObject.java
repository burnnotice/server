package com.palidinodh.maparea.zeah.darkaltar.handler.mapobject;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.BLOOD_ALTAR)
class BloodAltarMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (player.carryingItem(ItemId.DARK_ESSENCE_FRAGMENTS)
        && player.getCharges().getDarkEssenceFragments() > 0) {
      var runecraftingLvl = player.getSkills().getLevel(Skills.RUNECRAFTING) >= 77;
      var fragmentCharges = player.getCharges().getDarkEssenceFragments();
      if (runecraftingLvl) {
        double xp = fragmentCharges * 24;
        if (player.getEquipment().wearingElidinisOutfit()) {
          xp *= 1.1;
        }
        player.getSkills().addXp(Skills.RUNECRAFTING, (int) xp);
        player.getCharges().decreaseDarkEssenceCharges(fragmentCharges);
        player.getInventory().deleteItem(ItemId.DARK_ESSENCE_FRAGMENTS);
        player.getInventory().addItem(ItemId.BLOOD_RUNE, fragmentCharges);
        player.setAnimation(791);
        player.setGraphic(186, 100);
      } else {
        player
            .getGameEncoder()
            .sendMessage("You need a runecrafting level of 77 to craft Blood runes.");
      }
    } else {
      player.getInventory().deleteItem(ItemId.DARK_ESSENCE_FRAGMENTS);
    }
  }
}
