package com.palidinodh.maparea.kharidiandesert;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({12847, 13103})
public class DesertMiningCampArea extends Area {}
