package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Teleports;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.PORTAL_NEXUS_65000)
class PortalNexusMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (option == 0) {
      Teleports.open(player);
    } else if (option == 1) {
      player.openDialogue("fairyring", 0);
    } else if (option == 2) {
      player.openDialogue("spirittree", 0);
    } else if (option == 3) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.JEWELLERY_BOX);
      player
          .getGameEncoder()
          .sendScript(ScriptId.POH_JEWELLERY_BOX_INIT, 15, "Ornate Jewellery Box", 3);
      player
          .getGameEncoder()
          .sendWidgetSettings(WidgetId.JEWELLERY_BOX, 0, 0, 24, WidgetSetting.OPTION_0);
    }
  }
}
