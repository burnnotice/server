package com.palidinodh.maparea.wilderness.handler.npc;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  NpcId.CURSED_JEWELLED_CRAB_RED_180_16002,
  NpcId.CURSED_JEWELLED_CRAB_GREEN_180_16003,
  NpcId.CURSED_JEWELLED_CRAB_BLUE_180_16004
})
class CursedJewelledCrabNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (!player.getInventory().hasItem(ItemId.HAMMER)
        && player.getEquipment().getWeaponId() != ItemId.DRAGON_WARHAMMER
        && player.getEquipment().getWeaponId() != ItemId.ELDER_MAUL
        && player.getEquipment().getWeaponId() != ItemId.TORAGS_HAMMERS) {
      player.getGameEncoder().sendMessage("This crab can only be smashed by certain hammers.");
      return;
    }
    if (player.getInventory().hasItem(ItemId.HAMMER)) {
      player.setAnimation(1755);
    } else {
      player.setAnimation(player.getCombat().getAttackAnimation());
    }
    npc.getCombat().script("smash");
  }
}
