package com.palidinodh.maparea.zeah.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.MARISI)
class MarisiNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (option == 0) {
      player.getGameEncoder().sendMessage("This gardener will protect your patches for a fee.");
    } else {
      player.openDialogue(
          new OptionsDialogue(
              "Choose an option?",
              new DialogueOption(
                  "Pay for patch protection!",
                  (c, s) -> {
                    player.getFarming().gardenerProtection(npc, option - 2);
                  }),
              new DialogueOption(
                  "Pay for digging up tree!",
                  (c, s) -> {
                    player.getFarming().gardenerRemoval(npc, option - 2);
                  })));
    }
  }
}
