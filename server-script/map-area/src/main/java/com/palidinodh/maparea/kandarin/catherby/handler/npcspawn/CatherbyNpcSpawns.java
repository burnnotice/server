package com.palidinodh.maparea.kandarin.catherby.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class CatherbyNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(2, new Tile(2859, 3431), NpcId.ELLENA));
    spawns.add(new NpcSpawn(new Tile(2807, 3443), NpcId.BANKER));
    spawns.add(new NpcSpawn(new Tile(2810, 3443), NpcId.BANKER));
    spawns.add(new NpcSpawn(new Tile(2811, 3443), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(new Tile(2809, 3443), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2803, 3430), NpcId.ARHEIN));
    spawns.add(new NpcSpawn(new Tile(2797, 3415), NpcId.TRADER_CREWMEMBER_1331));
    spawns.add(new NpcSpawn(4, new Tile(2799, 3414), NpcId.TRADER_CREWMEMBER_1334));
    spawns.add(new NpcSpawn(4, new Tile(2804, 3425), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(2805, 3433), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(2, new Tile(2799, 3439), NpcId.CANDLE_MAKER));
    spawns.add(new NpcSpawn(new Tile(2808, 3454), NpcId.PERDU));
    spawns.add(new NpcSpawn(2, new Tile(2820, 3461), NpcId.VANESSA));
    spawns.add(new NpcSpawn(4, new Tile(2816, 3452), NpcId.CALEB));
    spawns.add(new NpcSpawn(4, new Tile(2820, 3441), NpcId.WOMAN_2_3084));
    spawns.add(new NpcSpawn(2, new Tile(2834, 3443), NpcId.HARRY));
    spawns.add(new NpcSpawn(new Tile(2856, 3435), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2815, 3466), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(2, new Tile(2807, 3463), NpcId.DANTAERA));
    spawns.add(new NpcSpawn(4, new Tile(2823, 3443), NpcId.HICKTON));

    return spawns;
  }
}
