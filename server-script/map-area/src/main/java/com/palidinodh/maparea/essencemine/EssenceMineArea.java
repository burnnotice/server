package com.palidinodh.maparea.essencemine;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11595)
public class EssenceMineArea extends Area {}
