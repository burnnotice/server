package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.SURGEON_GENERAL_TAFANI)
class SergeonGeneralTafaniNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (player.getX() != 3094 || player.getY() != 3498) {
      return;
    }
    player.setGraphic(436);
    player.getGameEncoder().sendMessage(npc.getDef().getName() + " restores you.");
    player.rejuvenate();
  }
}
