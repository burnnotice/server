package com.palidinodh.maparea.kandarin.legendsguild.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.SIEGFRIED_ERKLE)
class SiegfriedErkleNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    player.openShop("legends");
  }
}
