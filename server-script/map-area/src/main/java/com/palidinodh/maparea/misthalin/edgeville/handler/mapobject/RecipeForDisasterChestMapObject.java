package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId(ObjectId.CHEST_12309)
class RecipeForDisasterChestMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (Settings.getInstance().isSpawn()) {
      return;
    }
    player.openDialogue("recipefordisaster", 0);
  }
}
