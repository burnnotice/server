package com.palidinodh.maparea.misthalin.varrock.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.STAIRCASE_11800, ObjectId.STAIRCASE_11805})
public class BankStaircaseMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.STAIRCASE_11800:
        player.getMovement().ladderDownTeleport(new Tile(3190, 9833));
        break;
      case ObjectId.STAIRCASE_11805:
        player.getMovement().ladderUpTeleport(new Tile(3186, 3433));
        break;
    }
  }
}
