package com.palidinodh.maparea.zeah.darkaltar.handler.mapobject;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.SOUL_ALTAR)
class SoulAltarMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (player.carryingItem(ItemId.DARK_ESSENCE_FRAGMENTS)
        && player.getCharges().getDarkEssenceFragments() > 0) {
      var runecraftingLvl = player.getSkills().getLevel(Skills.RUNECRAFTING) >= 90;
      var fragmentCharges = player.getCharges().getDarkEssenceFragments();
      if (runecraftingLvl) {
        double xp = fragmentCharges * 30;
        if (player.getEquipment().wearingElidinisOutfit()) {
          xp *= 1.1;
        }
        player.getSkills().addXp(Skills.RUNECRAFTING, (int) xp);
        player.getCharges().decreaseDarkEssenceCharges(fragmentCharges);
        player.getInventory().deleteItem(ItemId.DARK_ESSENCE_FRAGMENTS);
        player.getInventory().addItem(ItemId.SOUL_RUNE, fragmentCharges);
        player.setAnimation(791);
        player.setGraphic(186, 100);
      } else {
        player
            .getGameEncoder()
            .sendMessage("You need a runecrafting level of 90 to craft Blood runes.");
      }
    } else {
      player.getInventory().deleteItem(ItemId.DARK_ESSENCE_FRAGMENTS);
    }
  }
}
