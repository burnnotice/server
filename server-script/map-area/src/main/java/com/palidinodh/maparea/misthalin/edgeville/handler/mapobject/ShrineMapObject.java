package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.woodcutting.WoodcuttingPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.SHRINE)
class ShrineMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    player.getPlugin(WoodcuttingPlugin.class).checkShrine();
  }
}
