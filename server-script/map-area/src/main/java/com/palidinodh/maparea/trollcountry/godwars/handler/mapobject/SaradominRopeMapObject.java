package com.palidinodh.maparea.trollcountry.godwars.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.ROPE_26371, ObjectId.ROPE_26375})
class SaradominRopeMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.ROPE_26371:
        player.getMovement().ladderUpTeleport(new Tile(2912, 5300, 2));
        break;
      case ObjectId.ROPE_26375:
        player.getMovement().ladderUpTeleport(new Tile(2920, 5276, 1));
        break;
    }
  }
}
