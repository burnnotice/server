package com.palidinodh.maparea.wilderness.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.CAVE)
class CorporealBeastEntranceMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (player.getPlugin(WildernessPlugin.class).hasBloodyKey()) {
      player.getGameEncoder().sendMessage("You can't use this right now.");
      return;
    }
    if (player.getMovement().getTeleportBlock() > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "A teleport block has been cast on you. It should wear off in "
                  + player.getMovement().getTeleportBlockRemaining()
                  + ".");
      return;
    }
    player.getMovement().ladderUpTeleport(new Tile(2964, 4382, 2));
  }
}
