package com.palidinodh.maparea.zeah.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class ZeahNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(1755, 3849), NpcId.BANKER));
    spawns.add(new NpcSpawn(2, new Tile(1817, 3485), NpcId.MARISI));
    spawns.add(new NpcSpawn(new Tile(1811, 3490), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(4, new Tile(1445, 3696), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1433, 3699), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1421, 3701), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1421, 3712), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1429, 3719), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1439, 3717), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1438, 3707), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1486, 3697), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1500, 3701), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1511, 3693), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1523, 3691), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1535, 3688), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1547, 3695), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1551, 3679), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1740, 3471), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1745, 3476), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1749, 3471), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1759, 3470), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1757, 3478), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1766, 3467), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1768, 3473), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1774, 3464), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1780, 3469), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1773, 3469), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1789, 3462), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1791, 3471), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1798, 3463), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1802, 3468), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1805, 3458), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1805, 3449), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1802, 3443), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1809, 3443), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1806, 3440), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1813, 3448), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1810, 3455), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1819, 3451), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1818, 3457), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1826, 3447), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1832, 3449), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1828, 3453), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1827, 3459), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1837, 3451), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1834, 3456), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1838, 3461), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1844, 3459), NpcId.SAND_CRAB_15));
    spawns.add(new NpcSpawn(4, new Tile(1843, 3466), NpcId.SAND_CRAB_15));

    return spawns;
  }
}
