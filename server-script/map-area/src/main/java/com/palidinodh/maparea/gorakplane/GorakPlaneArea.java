package com.palidinodh.maparea.gorakplane;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12115)
public class GorakPlaneArea extends Area {

  @Override
  public boolean inMultiCombat() {
    return getTile().getHeight() == 4;
  }
}
