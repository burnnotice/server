package com.palidinodh.maparea.kandarin.sorcererstower.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class SorcerersTowerNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2722, 3433), NpcId.IGNATIUS_VULCAN));
    spawns.add(new NpcSpawn(4, new Tile(2742, 3443), NpcId.FLAX_KEEPER));
    spawns.add(new NpcSpawn(4, new Tile(2734, 3413), NpcId.SHERLOCK));

    return spawns;
  }
}
