package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.LADDER_31704, ObjectId.LADDER_31705})
class LadderMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.LADDER_31704:
        player
            .getMovement()
            .ladderUpTeleport(new Tile(player.getX(), player.getY(), player.getHeight() + 1));
        break;
      case ObjectId.LADDER_31705:
        player
            .getMovement()
            .ladderDownTeleport(new Tile(player.getX(), player.getY(), player.getHeight() - 1));
        break;
    }
  }
}
