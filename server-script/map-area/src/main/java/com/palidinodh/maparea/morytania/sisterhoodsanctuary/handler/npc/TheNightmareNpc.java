package com.palidinodh.maparea.morytania.sisterhoodsanctuary.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.maparea.morytania.sisterhoodsanctuary.SisterhoodSanctuaryArea;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  NpcId.THE_NIGHTMARE_814_9460,
  NpcId.THE_NIGHTMARE_814_9461,
  NpcId.THE_NIGHTMARE_814_9462,
  NpcId.THE_NIGHTMARE_814_9463,
  NpcId.THE_NIGHTMARE_814_9464
})
class TheNightmareNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    player.getArea().as(SisterhoodSanctuaryArea.class).joinPublic(player);
  }
}
