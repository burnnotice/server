package com.palidinodh.maparea.karamja.taibwowannai.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class TaiBwoWannaiNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2799, 3101), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(2, new Tile(2799, 3097), NpcId.IMIAGO));

    return spawns;
  }
}
