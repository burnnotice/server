package com.palidinodh.maparea.asgarnia.donator.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.ORNATE_JEWELLERY_BOX)
class DonatorOrnateJewelleryBoxMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.JEWELLERY_BOX);
    player
        .getGameEncoder()
        .sendScript(ScriptId.POH_JEWELLERY_BOX_INIT, 15, "Ornate Jewellery Box", 3);
    player
        .getGameEncoder()
        .sendWidgetSettings(WidgetId.JEWELLERY_BOX, 0, 0, 24, WidgetSetting.OPTION_0);
  }
}
