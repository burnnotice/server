package com.palidinodh.maparea.asgarnia.cerberuslair.handler.mapobject;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId(ObjectId.IRON_WINCH)
class IronWinchMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (!player.getSkills().isAnySlayerTask(NpcId.CERBERUS_318)
        && !Settings.getInstance().isLocal()
        && !Settings.getInstance().isBeta()
        && !Settings.getInstance().isSpawn()) {
      player.getGameEncoder().sendMessage("You need an appropriate task to enter.");
      return;
    }
    if (mapObject.getX() == 1291 && mapObject.getY() == 1254) {
      player.getMovement().teleport(1240, 1227);
    } else if (mapObject.getX() == 1328 && mapObject.getY() == 1254) {
      player.getMovement().teleport(1368, 1227);
    } else if (mapObject.getX() == 1307 && mapObject.getY() == 1269) {
      player.getPlugin(BossPlugin.class).openBossInstanceDialogue(NpcId.CERBERUS_318);
    }
  }
}
