package com.palidinodh.maparea.wilderness.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.maparea.wilderness.WildernessLarransChest;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.LARRANS_BIG_CHEST_34832)
class LarransChestMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    WildernessLarransChest.open(player, mapObject);
  }
}
