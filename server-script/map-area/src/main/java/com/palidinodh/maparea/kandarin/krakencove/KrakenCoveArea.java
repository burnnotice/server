package com.palidinodh.maparea.kandarin.krakencove;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(9116)
public class KrakenCoveArea extends Area {}
