package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.BOB_BARTER_HERBS)
class BobBarterNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (option == 0) {
      player.openDialogue("bobbarter", 0);
    } else if (option == 2) {
      player.openShop("herb_exchange");
    } else if (option == 3) {
      player.getSkills().decantAllPotions();
    }
  }
}
