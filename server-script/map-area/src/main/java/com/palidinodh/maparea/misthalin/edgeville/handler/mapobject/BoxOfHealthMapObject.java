package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.BOX_OF_HEALTH)
class BoxOfHealthMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (player.getArea().inPvpWorldUnsafe()) {
      player.getGameEncoder().sendMessage("You can't use this here.");
      return;
    }
    player.setGraphic(436);
    player.getGameEncoder().sendMessage("The pool restores you.");
    player.rejuvenate();
  }
}
