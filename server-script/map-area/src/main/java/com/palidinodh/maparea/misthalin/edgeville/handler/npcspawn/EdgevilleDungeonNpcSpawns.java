package com.palidinodh.maparea.misthalin.edgeville.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class EdgevilleDungeonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(3091, 3516), NpcId.DEATH_16050));
    spawns.add(new NpcSpawn(4, new Tile(3130, 9904), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3134, 9905), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3132, 9908), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3130, 9911), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3133, 9912), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3130, 9915), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3148, 9907), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3147, 9902), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3143, 9900), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3142, 9904), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3140, 9908), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3147, 9897), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3150, 9893), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3150, 9886), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3148, 9882), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3144, 9884), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3139, 9886), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3139, 9891), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3145, 9891), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3146, 9887), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3140, 9895), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3125, 9864), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3127, 9862), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3130, 9861), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3128, 9865), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3124, 9861), NpcId.ZOMBIE_24));
    spawns.add(new NpcSpawn(4, new Tile(3116, 9870), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3119, 9872), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3122, 9874), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3123, 9878), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3125, 9874), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3118, 9867), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3128, 9874), NpcId.HOBGOBLIN_28));
    spawns.add(new NpcSpawn(4, new Tile(3118, 9849), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3121, 9844), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3117, 9840), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3114, 9836), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3119, 9833), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3112, 9831), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3107, 9827), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3106, 9832), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3107, 9838), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3100, 9835), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3098, 9829), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3110, 9844), NpcId.HILL_GIANT_28));
    spawns.add(new NpcSpawn(4, new Tile(3097, 9910), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3099, 9913), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3101, 9908), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3099, 9905), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3096, 9906), NpcId.SKELETON_22));
    spawns.add(new NpcSpawn(4, new Tile(3095, 9881), NpcId.GIANT_SPIDER_2));
    spawns.add(new NpcSpawn(4, new Tile(3095, 9884), NpcId.GIANT_SPIDER_2));
    spawns.add(new NpcSpawn(4, new Tile(3098, 9882), NpcId.GIANT_SPIDER_2));
    spawns.add(new NpcSpawn(4, new Tile(3101, 9884), NpcId.GIANT_SPIDER_2));
    spawns.add(new NpcSpawn(4, new Tile(3101, 9881), NpcId.GIANT_SPIDER_2));
    spawns.add(new NpcSpawn(4, new Tile(3105, 9883), NpcId.GIANT_SPIDER_2));
    spawns.add(new NpcSpawn(4, new Tile(3117, 9891), NpcId.GIANT_RAT_3));
    spawns.add(new NpcSpawn(4, new Tile(3120, 9893), NpcId.GIANT_RAT_3));
    spawns.add(new NpcSpawn(4, new Tile(3122, 9889), NpcId.GIANT_RAT_3));
    spawns.add(new NpcSpawn(4, new Tile(3119, 9886), NpcId.GIANT_RAT_3));
    spawns.add(new NpcSpawn(4, new Tile(3120, 9890), NpcId.GIANT_RAT_3));

    return spawns;
  }
}
