package com.palidinodh.maparea.asgarnia.heroesguildbasement.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class HeroesGuildBasementNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2908, 9904), NpcId.BLUE_DRAGON_111));
    spawns.add(new NpcSpawn(4, new Tile(2894, 9911), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2889, 9907), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2936, 9896), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2936, 9890), NpcId.GIANT_BAT_27));
    spawns.add(new NpcSpawn(4, new Tile(2931, 9887), NpcId.GIANT_BAT_27));

    return spawns;
  }
}
