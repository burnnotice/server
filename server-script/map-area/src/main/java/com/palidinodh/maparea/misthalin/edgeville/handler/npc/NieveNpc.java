package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.NIEVE)
class NieveNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (option == 0) {
      plugin.openMasterMenuDialogue();
    } else if (option == 2) {
      plugin.openChooseMasterDialogue();
    } else if (option == 3) {
      player.openShop("slayer");
    } else if (option == 4) {
      plugin.openRewards();
    }
  }
}
