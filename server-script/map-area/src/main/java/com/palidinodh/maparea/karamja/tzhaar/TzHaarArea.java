package com.palidinodh.maparea.karamja.tzhaar;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({9043, 9551, 9807, 9808, 10063, 10064})
public class TzHaarArea extends Area {}
