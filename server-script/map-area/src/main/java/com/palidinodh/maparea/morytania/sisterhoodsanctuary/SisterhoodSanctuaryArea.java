package com.palidinodh.maparea.morytania.sisterhoodsanctuary;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.boss.BossInstanceController;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PEventTasks;

@ReferenceId({14999, 15000, 15001, 15255, 15256, 15257, 15511, 15512, 15513, 15515})
public class SisterhoodSanctuaryArea extends Area {

  private static final NpcSpawn BOSS_SPAWN =
      new NpcSpawn(new Tile(3870, 9949, 3), NpcId.THE_NIGHTMARE_814_9433);

  @Override
  public boolean inMultiCombat() {
    return true;
  }

  public void joinPublic(Player player) {
    if (BossPlugin.getIdleNightmare(player) != null) {
      player.openDialogue(
          new MessageDialogue(
              "A group is already fighting the Nightmare. You'll have to wait until they are done."));
      return;
    }
    player.setController(new BossInstanceController());
    if (player.getController().getNpc(NpcId.THE_NIGHTMARE_814_9432, NpcId.THE_NIGHTMARE_814_9433)
        == null) {
      player.getController().addNpc(BOSS_SPAWN);
    }
    enter(player);
  }

  private void enter(Player player) {
    player.openDialogue(
        new MessageDialogue("The Nightmare pulls you into her dream as you approach her.", true));
    player.setAnimation(8584);
    player.getGameEncoder().sendFadeOut();
    player.lock();
    var tasks = new PEventTasks();
    tasks.execute(
        2, t -> player.getMovement().teleport(new Tile(3870 + PRandom.randomI(4), 9948, 3)));
    tasks.execute(
        2,
        t -> {
          player.setAnimation(8583);
          player.getGameEncoder().sendFadeIn();
        });
    tasks.execute(
        2,
        t -> {
          player.unlock();
          player.openDialogue(
              new MessageDialogue("The Nightmare pulls you into her dream as you approach her."));
        });
    player.getController().addEvent(tasks);
  }
}
