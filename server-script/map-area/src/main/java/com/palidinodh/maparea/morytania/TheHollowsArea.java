package com.palidinodh.maparea.morytania;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(13876)
public class TheHollowsArea extends Area {}
