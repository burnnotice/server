package com.palidinodh.maparea.kharidiandesert.kalphitehive;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({13204, 13205, 13460})
public class KalphiteHiveArea extends Area {}
