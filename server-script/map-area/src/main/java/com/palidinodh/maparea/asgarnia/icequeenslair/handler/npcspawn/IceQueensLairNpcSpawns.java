package com.palidinodh.maparea.asgarnia.icequeenslair.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class IceQueensLairNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2841, 9913), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(2836, 9905), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(2834, 9920), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(2833, 9934), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(2834, 9946), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(2839, 9961), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(2825, 9900), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(2820, 9916), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(2819, 9926), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(2819, 9937), NpcId.ICE_WARRIOR_57));
    spawns.add(new NpcSpawn(4, new Tile(2827, 9933), NpcId.ICE_SPIDER_61));
    spawns.add(new NpcSpawn(4, new Tile(2826, 9951), NpcId.ICE_SPIDER_61));
    spawns.add(new NpcSpawn(4, new Tile(2858, 9971), NpcId.ICE_SPIDER_61));
    spawns.add(new NpcSpawn(4, new Tile(2873, 9971), NpcId.ICE_SPIDER_61));
    spawns.add(new NpcSpawn(4, new Tile(2883, 9964), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(2884, 9959), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(2887, 9954), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(2891, 9949), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(2891, 9943), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(2885, 9935), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(2880, 9927), NpcId.ICE_GIANT_53));
    spawns.add(new NpcSpawn(4, new Tile(2860, 9916), NpcId.ICE_SPIDER_61));
    spawns.add(new NpcSpawn(4, new Tile(2866, 9955), NpcId.ICE_QUEEN_111));
    spawns.add(new NpcSpawn(4, new Tile(2863, 9952), NpcId.ICE_WARRIOR_57_2851));
    spawns.add(new NpcSpawn(4, new Tile(2868, 9952), NpcId.ICE_WARRIOR_57_2851));
    spawns.add(new NpcSpawn(4, new Tile(2866, 9949), NpcId.ICE_WARRIOR_57_2851));
    spawns.add(new NpcSpawn(4, new Tile(2863, 9957), NpcId.ICE_WARRIOR_57_2851));
    spawns.add(new NpcSpawn(4, new Tile(2868, 9957), NpcId.ICE_WARRIOR_57_2851));
    spawns.add(new NpcSpawn(4, new Tile(2865, 9962), NpcId.ICE_WARRIOR_57_2851));

    return spawns;
  }
}
