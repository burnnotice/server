package com.palidinodh.maparea.asgarnia.warriorsguild;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId(11319)
@ReferenceIdSet(
    primary = 11675,
    secondary = {52, 53, 54, 68, 69, 70, 84, 85, 86, 100, 101, 102, 116, 117, 118})
public class WarriorsGuildArea extends Area {}
