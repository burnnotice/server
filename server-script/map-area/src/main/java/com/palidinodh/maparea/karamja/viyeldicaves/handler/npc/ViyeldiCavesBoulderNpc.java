package com.palidinodh.maparea.karamja.viyeldicaves.handler.npc;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.BOULDER_3967)
class ViyeldiCavesBoulderNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (player.getCombat().getLegendsQuest() == 2) {
      player
          .getGameEncoder()
          .sendMessage("You search around the rock and discover a dagger on the ground.");
      player.getGameEncoder().sendMessage("Ungadulu might be able to do something with this.");
      player.getInventory().addOrDropItem(ItemId.DARK_DAGGER);
    }
  }
}
