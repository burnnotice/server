package com.palidinodh.maparea.wilderness.corporealbeastcave.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.PASSAGE)
class PassageMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    player.getCombat().setDamageInflicted(0);
    if (player.getX() <= 2970) {
      player.getMovement().teleport(new Tile(2974, 4384, 2));
    } else {
      player.getMovement().teleport(new Tile(2970, 4384, 2));
    }
    player.getController().stopWithTeleport();
  }
}
