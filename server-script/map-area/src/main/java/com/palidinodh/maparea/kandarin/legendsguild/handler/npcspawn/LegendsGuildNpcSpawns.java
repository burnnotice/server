package com.palidinodh.maparea.kandarin.legendsguild.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class LegendsGuildNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(6, new Tile(2727, 3379), NpcId.SIEGFRIED_ERKLE));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2733, 3374), NpcId.BANKER));

    return spawns;
  }
}
