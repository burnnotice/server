package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.WATSON)
class WatsonNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    if (!player.getInventory().hasItem(ClueScrollType.EASY.getScrollId())) {
      player.getGameEncoder().sendMessage("You are missing a easy clue scroll.");
      return;
    }
    if (!player.getInventory().hasItem(ClueScrollType.MEDIUM.getScrollId())) {
      player.getGameEncoder().sendMessage("You are missing a medium clue scroll.");
      return;
    }
    if (!player.getInventory().hasItem(ClueScrollType.HARD.getScrollId())) {
      player.getGameEncoder().sendMessage("You are missing a hard clue scroll.");
      return;
    }
    if (!player.getInventory().hasItem(ClueScrollType.ELITE.getScrollId())) {
      player.getGameEncoder().sendMessage("You are missing a elite clue scroll.");
      return;
    }
    player.getInventory().deleteItem(ClueScrollType.EASY.getScrollId());
    player.getInventory().deleteItem(ClueScrollType.MEDIUM.getScrollId());
    player.getInventory().deleteItem(ClueScrollType.HARD.getScrollId());
    player.getInventory().deleteItem(ClueScrollType.ELITE.getScrollId());
    player.getInventory().addOrDropItem(ClueScrollType.MASTER.getScrollId());
  }
}
