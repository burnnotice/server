package com.palidinodh.maparea.kandarin.crashsite;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({8280, 8536})
public class CrashSiteCavernArea extends Area {}
