package com.palidinodh.maparea.fremennikprovince.jormungandsprison.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class JormungandsPrisonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2417, 10373), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2423, 10377), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2427, 10383), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2434, 10388), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2436, 10395), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2421, 10384), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2414, 10387), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2406, 10385), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2409, 10392), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2416, 10398), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2424, 10397), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2451, 10379), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2456, 10380), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2460, 10384), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2450, 10384), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2455, 10385), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2451, 10390), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2459, 10390), NpcId.BASILISK_KNIGHT_204));
    spawns.add(new NpcSpawn(4, new Tile(2459, 10397), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2464, 10397), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2466, 10401), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2461, 10402), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2461, 10407), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2475, 10402), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2480, 10401), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2484, 10406), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2478, 10406), NpcId.BASILISK_61));
    spawns.add(new NpcSpawn(4, new Tile(2481, 10410), NpcId.BASILISK_61));

    return spawns;
  }
}
