package com.palidinodh.maparea.kharidiandesert;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12843)
public class MenaphosArea extends Area {}
