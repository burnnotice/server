package com.palidinodh.maparea.misthalin.lumbridge.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class LumbridgeNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(2, new Tile(3190, 3232), NpcId.FAYETH));
    spawns.add(new NpcSpawn(new Tile(3189, 3234), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(4, new Tile(3212, 3248), NpcId.SHOP_KEEPER));
    spawns.add(new NpcSpawn(4, new Tile(3217, 3249), NpcId.DONIE));
    spawns.add(new NpcSpawn(4, new Tile(3223, 3237), NpcId.DONIE));
    spawns.add(new NpcSpawn(4, new Tile(3214, 3220), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(3210, 3215), NpcId.COOK_4626));
    spawns.add(new NpcSpawn(4, new Tile(3221, 3222), NpcId.HANS));
    spawns.add(new NpcSpawn(4, new Tile(3222, 3216), NpcId.MAN_2_3080));
    spawns.add(new NpcSpawn(4, new Tile(3216, 3206), NpcId.WOMAN_2_3083));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3230, 3215), NpcId.PERDU));
    spawns.add(new NpcSpawn(4, new Tile(3234, 3221), NpcId.MAN_2_3079));
    spawns.add(new NpcSpawn(4, new Tile(3234, 3216), NpcId.HATIUS_COSAINTUS));
    spawns.add(new NpcSpawn(4, new Tile(3231, 3208), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(3230, 3203), NpcId.BOB));
    spawns.add(new NpcSpawn(4, new Tile(3235, 3202), NpcId.MAN_2_3080));
    spawns.add(new NpcSpawn(4, new Tile(3236, 3205), NpcId.WOMAN_2_3083));
    spawns.add(new NpcSpawn(4, new Tile(3235, 3208), NpcId.WOMAN_2_3084));
    spawns.add(new NpcSpawn(4, new Tile(3243, 3210), NpcId.FATHER_AERECK));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3238, 3199), NpcId.COUNT_CHECK));
    spawns.add(new NpcSpawn(new Tile(3233, 3232), NpcId.DOOMSAYER));
    spawns.add(new NpcSpawn(new Tile(3232, 3232), NpcId.GUIDE));
    spawns.add(new NpcSpawn(4, new Tile(3227, 3240), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(3230, 3237), NpcId.MAN_2_3079));
    spawns.add(new NpcSpawn(4, new Tile(3231, 3239), NpcId.WOMAN_2_3085));
    spawns.add(new NpcSpawn(8, new Tile(3226, 3235), NpcId.IMP_7));
    spawns.add(new NpcSpawn(4, new Tile(3217, 3237), NpcId.GEE));

    return spawns;
  }
}
