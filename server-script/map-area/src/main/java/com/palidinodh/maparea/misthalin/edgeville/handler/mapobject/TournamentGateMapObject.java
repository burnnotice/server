package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.worldevent.pvptournament.PvpTournamentEvent;

@ReferenceId({ObjectId.GATE_26081, ObjectId.GATE_26082})
class TournamentGateMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    player.getWorld().getWorldEvent(PvpTournamentEvent.class).addPlayer(player);
  }
}
