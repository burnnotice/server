package com.palidinodh.maparea.kandarin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10290)
public class ClockTowerArea extends Area {}
