package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Prayer;
import com.palidinodh.osrscore.model.entity.player.WidgetManager;
import com.palidinodh.osrscore.model.entity.player.dialogue.MakeXDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.ALTAR_OF_THE_OCCULT_65002)
class OccultAltarMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (option == 0) {
      player.rejuvenate();
      player.setAnimation(645);
    } else if (option == 1) {
      player.getMagic().setSpellbook(Magic.STANDARD_MAGIC);
      player.getGameEncoder().sendMessage("Your spellbook has been set to Standard.");
    } else if (option == 2) {
      player.getMagic().setSpellbook(Magic.ANCIENT_MAGIC);
      player.getGameEncoder().sendMessage("Your spellbook has been set to Ancient.");
    } else if (option == 3) {
      player.getMagic().setSpellbook(Magic.LUNAR_MAGIC);
      player.getGameEncoder().sendMessage("Your spellbook has been set to Lunar.");
    }
  }

  @Override
  public void itemOnMapObject(Player player, Item item, MapObject mapObject) {
    if (!Prayer.isBone(item.getId())) {
      return;
    }
    player.openDialogue(
        new MakeXDialogue(
            WidgetManager.MakeXType.USE,
            player.getInventory().getCount(item.getId()),
            (c, s) -> {
              player.setAction(new Prayer.BonesOnAltar(player, item.getId(), mapObject, s));
            },
            item.getId()));
  }
}
