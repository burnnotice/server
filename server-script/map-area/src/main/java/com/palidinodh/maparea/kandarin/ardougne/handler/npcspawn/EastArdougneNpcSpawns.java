package com.palidinodh.maparea.kandarin.ardougne.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class EastArdougneNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2672, 3381), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(2, new Tile(2637, 3363), NpcId.MASTER_FARMER));
    spawns.add(new NpcSpawn(new Tile(2651, 3295), NpcId.ZENESHA));
    spawns.add(new NpcSpawn(2, new Tile(2663, 3375), NpcId.KRAGEN));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(2651, 3280), NpcId.WIZARD_4399));
    spawns.add(new NpcSpawn(new Tile(2652, 3311), NpcId.MILES));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2672, 3301), NpcId.MILES));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2657, 3286), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2657, 3283), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2657, 3280), NpcId.BANKER));
    spawns.add(new NpcSpawn(4, new Tile(2653, 3307), NpcId.GUARD_20));
    spawns.add(new NpcSpawn(4, new Tile(2654, 3316), NpcId.GUARD_20));
    spawns.add(new NpcSpawn(4, new Tile(2659, 3311), NpcId.GUARD_20));
    spawns.add(new NpcSpawn(4, new Tile(2659, 3305), NpcId.GUARD_20));
    spawns.add(new NpcSpawn(4, new Tile(2664, 3302), NpcId.GUARD_20));
    spawns.add(new NpcSpawn(4, new Tile(2660, 3301), NpcId.GUARD_20));
    spawns.add(new NpcSpawn(4, new Tile(2665, 3309), NpcId.GUARD_20));
    spawns.add(new NpcSpawn(4, new Tile(2668, 3314), NpcId.GUARD_20));
    spawns.add(new NpcSpawn(4, new Tile(2669, 3298), NpcId.GUARD_20));
    spawns.add(new NpcSpawn(4, new Tile(2650, 3301), NpcId.HERO_69));
    spawns.add(new NpcSpawn(4, new Tile(2661, 3307), NpcId.PALADIN_62));
    spawns.add(new NpcSpawn(4, new Tile(2658, 3300), NpcId.KNIGHT_OF_ARDOUGNE_46));
    spawns.add(new NpcSpawn(4, new Tile(2650, 3307), NpcId.KNIGHT_OF_ARDOUGNE_46));
    spawns.add(new NpcSpawn(4, new Tile(2665, 3299), NpcId.KNIGHT_OF_ARDOUGNE_46));
    spawns.add(new NpcSpawn(4, new Tile(2656, 3321), NpcId.KNIGHT_OF_ARDOUGNE_46));
    spawns.add(new NpcSpawn(4, new Tile(2662, 3320), NpcId.HERO_69));
    spawns.add(new NpcSpawn(4, new Tile(2671, 3317), NpcId.GUARD_20));
    spawns.add(new NpcSpawn(4, new Tile(2667, 3291), NpcId.PALADIN_62));
    spawns.add(new NpcSpawn(4, new Tile(2650, 3313), NpcId.PALADIN_62));
    spawns.add(new NpcSpawn(4, new Tile(2671, 3305), NpcId.HERO_69));

    return spawns;
  }
}
