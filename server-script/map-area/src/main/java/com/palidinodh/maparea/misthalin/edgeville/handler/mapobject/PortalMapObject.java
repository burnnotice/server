package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.clanwars.ClanWarsFreeForAllController;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.PORTAL_65007)
class PortalMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Current Event",
                (c, s) -> {
                  eventTeleport(player);
                }),
            new DialogueOption(
                "Safe Free-For-All",
                (c, s) -> {
                  if (!player.getInventory().isEmpty() || !player.getEquipment().isEmpty()) {
                    player
                        .getGameEncoder()
                        .sendMessage("You can't take items into the free-for-all arena.");
                    return;
                  }
                  player.setController(new ClanWarsFreeForAllController());
                }),
            new DialogueOption(
                "Risk Zone",
                (c, s) -> {
                  player.getMovement().teleport(2655, 5471);
                })));
  }

  private void eventTeleport(Player player) {
    var name = player.getWorld().worldEventScript("world_event_name") + " Event";
    var tile = (Tile) player.getWorld().worldEventScript("world_event_tile");
    if (name == null || tile == null) {
      player.getGameEncoder().sendMessage("There are no events running.");
      return;
    }
    var teleportOption = "Teleport me there!";
    var inWilderness = Area.inWilderness(tile);
    var wildernessLevel = Area.getWildernessLevel(tile);
    if (inWilderness) {
      name = "<img=9> " + name + " (Level " + wildernessLevel + ")";
      teleportOption = "<img=9> Teleport me into the wilderness! (Level " + wildernessLevel + ")";
    }
    player.openDialogue(
        new OptionsDialogue(
            name,
            new DialogueOption(
                teleportOption,
                (c, s) -> {
                  SpellTeleport.normalTeleport(player, tile);
                }),
            new DialogueOption("Stay here.")));
  }
}
