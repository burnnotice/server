package com.palidinodh.maparea.kharidiandesert.clanwars.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.PORTAL_26646)
class FreeForAllExitPortalMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    player.getController().stop();
  }
}
