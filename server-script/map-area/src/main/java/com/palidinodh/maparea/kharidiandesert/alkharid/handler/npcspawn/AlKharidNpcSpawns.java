package com.palidinodh.maparea.kharidiandesert.alkharid.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class AlKharidNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3272, 3170), NpcId.WIZARD_4399));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3272, 3164), NpcId.GADRIN));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3287, 3178), NpcId.MILES));
    spawns.add(new NpcSpawn(4, new Tile(3288, 3176), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3297, 3176), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3297, 3172), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3297, 3168), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3288, 3168), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3288, 3172), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3293, 3168), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3292, 3176), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3284, 3173), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3285, 3169), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3300, 3175), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3301, 3170), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3286, 3161), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3290, 3163), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3297, 3162), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3301, 3164), NpcId.AL_KHARID_WARRIOR_9));
    spawns.add(new NpcSpawn(4, new Tile(3288, 3190), NpcId.ZEKE));
    spawns.add(new NpcSpawn(4, new Tile(3294, 3194), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(3303, 3199), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(3298, 3200), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(new Tile(3300, 3203), NpcId.SILK_TRADER));
    spawns.add(new NpcSpawn(4, new Tile(3308, 3210), NpcId.ELLY_THE_CAMEL));
    spawns.add(new NpcSpawn(4, new Tile(3315, 3205), NpcId.AYESHA));
    spawns.add(new NpcSpawn(4, new Tile(3321, 3193), NpcId.DOMMIK));
    spawns.add(new NpcSpawn(4, new Tile(3316, 3175), NpcId.LOUIE_LEGS));
    spawns.add(new NpcSpawn(4, new Tile(3315, 3180), NpcId.SHOP_KEEPER));
    spawns.add(new NpcSpawn(4, new Tile(3314, 3162), NpcId.RANAEL));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3304, 3211), NpcId.ALI_MORRISANE));
    spawns.add(new NpcSpawn(4, new Tile(3298, 3227), NpcId.CAM_THE_CAMEL));
    spawns.add(new NpcSpawn(4, new Tile(3290, 3208), NpcId.OLLIE_THE_CAMEL));
    spawns.add(new NpcSpawn(4, new Tile(3289, 3211), NpcId.GEM_TRADER));
    spawns.add(new NpcSpawn(new Tile(3284, 3212), NpcId.CAPTAIN_DALBUR));
    spawns.add(new NpcSpawn(4, new Tile(3287, 3204), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(3285, 3198), NpcId.CAMEL));
    spawns.add(new NpcSpawn(4, new Tile(3282, 3192), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(3274, 3192), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(3273, 3190), NpcId.ELLIS));
    spawns.add(new NpcSpawn(4, new Tile(3273, 3180), NpcId.KARIM));
    spawns.add(new NpcSpawn(4, new Tile(3277, 3165), NpcId.AL_THE_CAMEL));
    spawns.add(new NpcSpawn(4, new Tile(3272, 3159), NpcId.PING_839));
    spawns.add(new NpcSpawn(4, new Tile(3277, 3143), NpcId.SCORPION_14));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3267, 3164), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3267, 3167), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3267, 3169), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3267, 3166), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3267, 3168), NpcId.BANKER_395));

    return spawns;
  }
}
