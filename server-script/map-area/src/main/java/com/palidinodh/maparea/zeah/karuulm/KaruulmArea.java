package com.palidinodh.maparea.zeah.karuulm;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(5179)
public class KaruulmArea extends Area {}
