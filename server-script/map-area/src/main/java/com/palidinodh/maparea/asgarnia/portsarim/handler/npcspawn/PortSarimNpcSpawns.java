package com.palidinodh.maparea.asgarnia.portsarim.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class PortSarimNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(2, new Tile(3014, 3195), NpcId.THIEF_16));
    spawns.add(new NpcSpawn(1, new Tile(3013, 3193), NpcId.CAPN_HAND));
    spawns.add(new NpcSpawn(1, new Tile(3014, 3191), NpcId.PIRATE_23));
    spawns.add(new NpcSpawn(2, new Tile(3014, 3189), NpcId.WORMBRAIN_2));
    spawns.add(new NpcSpawn(2, new Tile(3018, 3189), NpcId.MUGGER_6));
    spawns.add(new NpcSpawn(new Tile(3019, 3185), NpcId.GUARD_1551));
    spawns.add(new NpcSpawn(1, new Tile(3018, 3180), NpcId.BLACK_KNIGHT_33));
    spawns.add(new NpcSpawn(4, new Tile(3012, 3184, 1), NpcId.GUARD_21));
    spawns.add(new NpcSpawn(4, new Tile(3016, 3183, 1), NpcId.GUARD_21_1547));
    spawns.add(new NpcSpawn(4, new Tile(3012, 3181, 1), NpcId.GUARD_21_1548));
    spawns.add(new NpcSpawn(4, new Tile(3018, 3180, 1), NpcId.GUARD_21_1549));
    spawns.add(new NpcSpawn(4, new Tile(3019, 3186, 1), NpcId.GUARD_21_1550));
    spawns.add(new NpcSpawn(4, new Tile(3013, 3190, 1), NpcId.SECURITY_GUARD));
    spawns.add(new NpcSpawn(2, new Tile(2999, 3190), NpcId.GIANT_RAT_26));
    spawns.add(new NpcSpawn(2, new Tile(2998, 3194), NpcId.GIANT_RAT_26));
    spawns.add(new NpcSpawn(new Tile(3060, 3263), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(2, new Tile(3063, 3259), NpcId.FRIZZY_SKERNIP));
    spawns.add(new NpcSpawn(4, new Tile(3047, 3258), NpcId.LONGBOW_BEN));
    spawns.add(new NpcSpawn(4, new Tile(3051, 3256), NpcId.JACK_SEAGULL));
    spawns.add(new NpcSpawn(new Tile(3049, 3256), NpcId.AHAB));
    spawns.add(new NpcSpawn(2, new Tile(3045, 3257), NpcId.BARTENDER_1313));
    spawns.add(new NpcSpawn(4, new Tile(3052, 3247), NpcId.REDBEARD_FRANK));
    spawns.add(new NpcSpawn(4, new Tile(3049, 3235), NpcId.MONK_OF_ENTRANA_1166));
    spawns.add(new NpcSpawn(4, new Tile(3047, 3236), NpcId.MONK_OF_ENTRANA_1166));
    spawns.add(new NpcSpawn(4, new Tile(3044, 3235), NpcId.MONK_OF_ENTRANA_1167));
    spawns.add(new NpcSpawn(4, new Tile(3028, 3235), NpcId.SEAGULL_2));
    spawns.add(new NpcSpawn(4, new Tile(3026, 3233), NpcId.SEAGULL_2));
    spawns.add(new NpcSpawn(4, new Tile(3028, 3231), NpcId.SEAGULL_2));
    spawns.add(new NpcSpawn(4, new Tile(3027, 3219), NpcId.SEAMAN_LORRIS));
    spawns.add(new NpcSpawn(4, new Tile(3026, 3216), NpcId.CAPTAIN_TOBIAS));
    spawns.add(new NpcSpawn(4, new Tile(3028, 3215), NpcId.SEAMAN_THRESNOR));
    spawns.add(new NpcSpawn(4, new Tile(3025, 3207), NpcId.SEAGULL_2));
    spawns.add(new NpcSpawn(4, new Tile(3028, 3205), NpcId.SEAGULL_2));
    spawns.add(new NpcSpawn(4, new Tile(3026, 3202), NpcId.SEAGULL_2));
    spawns.add(new NpcSpawn(4, new Tile(3029, 3202), NpcId.SEAGULL_2));
    spawns.add(new NpcSpawn(2, new Tile(3014, 3206), NpcId.WYDIN_1791));
    spawns.add(new NpcSpawn(2, new Tile(3014, 3224), NpcId.GERRANT_1790));
    spawns.add(new NpcSpawn(2, new Tile(3020, 3229), NpcId.THE_FACE));
    spawns.add(new NpcSpawn(2, new Tile(3017, 3231), NpcId.ZAHUR));
    spawns.add(new NpcSpawn(4, new Tile(3012, 3232), NpcId.WOMAN_2_3083));
    spawns.add(new NpcSpawn(4, new Tile(3022, 3237), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(3017, 3238), NpcId.THIEF_16_3093));
    spawns.add(new NpcSpawn(4, new Tile(3014, 3231), NpcId.THIEF_16_3093));
    spawns.add(new NpcSpawn(2, new Tile(3013, 3246), NpcId.GRUM));
    spawns.add(new NpcSpawn(2, new Tile(3028, 3248), NpcId.BRIAN));
    spawns.add(new NpcSpawn(2, new Tile(3014, 3258), NpcId.BETTY));
    spawns.add(new NpcSpawn(4, new Tile(3039, 3193), NpcId.TRADER_STAN));

    return spawns;
  }
}
