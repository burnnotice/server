package com.palidinodh.maparea.tirannwn.lletya.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class LletyaNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(2348, 3159), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(2, new Tile(2344, 3164), NpcId.LILIWEN));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2353, 3159), NpcId.WIZARD_4399));
    spawns.add(new NpcSpawn(4, new Tile(2345, 3167), NpcId.GOREU));
    spawns.add(new NpcSpawn(4, new Tile(2353, 3180), NpcId.YSGAWYN));
    spawns.add(new NpcSpawn(4, new Tile(2353, 3173), NpcId.ARVEL));
    spawns.add(new NpcSpawn(4, new Tile(2340, 3186), NpcId.MAWRTH));
    spawns.add(new NpcSpawn(4, new Tile(2324, 3179), NpcId.KELYN));
    spawns.add(new NpcSpawn(4, new Tile(2327, 3168), NpcId.GOREU));
    spawns.add(new NpcSpawn(4, new Tile(2333, 3160, 1), NpcId.YSGAWYN));
    spawns.add(new NpcSpawn(4, new Tile(2336, 3184, 1), NpcId.ARVEL));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2356, 3175, 1), NpcId.MILES));
    spawns.add(new NpcSpawn(new Tile(2352, 3166), NpcId.BANKER_1479));
    spawns.add(new NpcSpawn(new Tile(2354, 3166), NpcId.BANKER_1480));
    spawns.add(new NpcSpawn(4, new Tile(2331, 3168), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2333, 3172), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2335, 3178), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2340, 3180), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2346, 3176), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2343, 3171), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2340, 3165), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2335, 3162), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2337, 3158), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2344, 3157), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2351, 3157), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2328, 3162), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2320, 3162), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2322, 3167), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2329, 3182), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2323, 3183), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2320, 3178), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2320, 3191), NpcId.TYRAS_GUARD_110_3433));
    spawns.add(new NpcSpawn(4, new Tile(2324, 3189), NpcId.TYRAS_GUARD_110_3433));
    spawns.add(new NpcSpawn(4, new Tile(2346, 3186), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2352, 3184), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2350, 3172), NpcId.TYRAS_GUARD_110_3433));
    spawns.add(new NpcSpawn(4, new Tile(2353, 3176), NpcId.TYRAS_GUARD_110_3433));
    spawns.add(new NpcSpawn(4, new Tile(2343, 3175, 1), NpcId.TYRAS_GUARD_110_3433));
    spawns.add(new NpcSpawn(4, new Tile(2335, 3177, 1), NpcId.TYRAS_GUARD_110_3433));
    spawns.add(new NpcSpawn(4, new Tile(2343, 3169, 1), NpcId.TYRAS_GUARD_110_3433));
    spawns.add(new NpcSpawn(4, new Tile(2337, 3167, 1), NpcId.TYRAS_GUARD_110_3433));
    spawns.add(new NpcSpawn(4, new Tile(2350, 3169, 1), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2350, 3174, 1), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2353, 3179, 1), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2352, 3163, 1), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2352, 3157, 1), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2347, 3156, 1), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2339, 3159, 1), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2351, 3187, 1), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2343, 3185, 1), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2335, 3180, 1), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2325, 3177, 1), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2326, 3174, 2), NpcId.ELF_WARRIOR_90));
    spawns.add(new NpcSpawn(4, new Tile(2326, 3167, 2), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2325, 3165, 1), NpcId.ELF_WARRIOR_108));
    spawns.add(new NpcSpawn(4, new Tile(2324, 3164), NpcId.ILFEEN));

    return spawns;
  }
}
