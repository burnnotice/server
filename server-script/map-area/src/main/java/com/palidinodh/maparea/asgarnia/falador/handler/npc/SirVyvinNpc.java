package com.palidinodh.maparea.asgarnia.falador.handler.npc;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.SIR_VYVIN)
class SirVyvinNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, int option, Npc npc) {
    player.openShop("white_knight");
  }
}
