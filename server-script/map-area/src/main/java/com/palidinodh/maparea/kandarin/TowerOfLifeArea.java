package com.palidinodh.maparea.kandarin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10546)
public class TowerOfLifeArea extends Area {}
