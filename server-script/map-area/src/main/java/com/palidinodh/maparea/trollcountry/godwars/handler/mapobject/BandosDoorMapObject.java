package com.palidinodh.maparea.trollcountry.godwars.handler.mapobject;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.BIG_DOOR)
class BandosDoorMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, int option, MapObject mapObject) {
    if (player.getX() >= 2851) {
      player.getMovement().teleport(new Tile(2850, 5333, 2));
    } else {
      player.getMovement().teleport(new Tile(2851, 5333, 2));
    }
  }
}
