package com.palidinodh.maparea.misthalin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({13364, 13365})
public class DigsiteArea extends Area {}
