package com.palidinodh.maparea.kandarin.seersvillage.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class SeersVillageNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2721, 3495), NpcId.BANKER));
    spawns.add(new NpcSpawn(new Tile(2724, 3495), NpcId.BANKER));
    spawns.add(new NpcSpawn(new Tile(2728, 3495), NpcId.BANKER));
    spawns.add(new NpcSpawn(new Tile(2729, 3495), NpcId.BANKER));
    spawns.add(new NpcSpawn(new Tile(2722, 3495), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(new Tile(2726, 3496), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(new Tile(2727, 3495), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(new Tile(2729, 3496), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(4, new Tile(2718, 3484), NpcId.SEER));
    spawns.add(new NpcSpawn(4, new Tile(2705, 3480), NpcId.SEER));
    spawns.add(new NpcSpawn(4, new Tile(2701, 3474), NpcId.PHANTUWTI_FANSTUWI_FARSIGHT));
    spawns.add(new NpcSpawn(4, new Tile(2707, 3476), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(2707, 3469), NpcId.SEER));
    spawns.add(new NpcSpawn(4, new Tile(2718, 3474), NpcId.SEER));
    spawns.add(new NpcSpawn(4, new Tile(2739, 3480), NpcId.TOWN_CRIER_280));
    spawns.add(new NpcSpawn(4, new Tile(2737, 3468), NpcId.GUARD_4218));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2760, 3476), NpcId.THE_WEDGE));
    spawns.add(new NpcSpawn(4, new Tile(2737, 3502), NpcId.ESTATE_AGENT));
    spawns.add(new NpcSpawn(4, new Tile(2683, 3486), NpcId.TRAMP));
    spawns.add(new NpcSpawn(4, new Tile(2696, 3496), NpcId.POISON_SALESMAN));
    spawns.add(new NpcSpawn(4, new Tile(2694, 3497), NpcId.WOMAN_2_3083));
    spawns.add(new NpcSpawn(4, new Tile(2696, 3494), NpcId.WOMAN_2_3083));
    spawns.add(new NpcSpawn(4, new Tile(2694, 3492), NpcId.WOMAN_2_3083));
    spawns.add(new NpcSpawn(4, new Tile(2691, 3489), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(2696, 3490), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(2694, 3495), NpcId.MAN_2_3078));
    spawns.add(new NpcSpawn(4, new Tile(2690, 3493), NpcId.BARTENDER_1318));

    return spawns;
  }
}
