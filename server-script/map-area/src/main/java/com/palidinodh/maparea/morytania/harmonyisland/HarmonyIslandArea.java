package com.palidinodh.maparea.morytania.harmonyisland;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(15148)
public class HarmonyIslandArea extends Area {}
