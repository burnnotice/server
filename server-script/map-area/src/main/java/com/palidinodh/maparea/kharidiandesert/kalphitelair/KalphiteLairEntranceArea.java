package com.palidinodh.maparea.kharidiandesert.kalphitelair;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12848)
public class KalphiteLairEntranceArea extends Area {}
