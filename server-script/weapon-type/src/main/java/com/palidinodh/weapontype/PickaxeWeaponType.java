package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BRONZE_PICKAXE,
  ItemId.IRON_PICKAXE,
  ItemId.STEEL_PICKAXE,
  ItemId.ADAMANT_PICKAXE,
  ItemId.MITHRIL_PICKAXE,
  ItemId.RUNE_PICKAXE,
  ItemId.RUNE_PICKAXE_NZ,
  ItemId.MITHRIL_PICKAXE_NZ,
  ItemId.IRON_PICKAXE_NZ,
  ItemId.DRAGON_PICKAXE,
  ItemId.BLACK_PICKAXE,
  ItemId.DRAGON_PICKAXE_12797,
  ItemId.INFERNAL_PICKAXE,
  ItemId.INFERNAL_PICKAXE_UNCHARGED,
  ItemId._3RD_AGE_PICKAXE,
  ItemId.GILDED_PICKAXE,
  ItemId.DRAGON_PICKAXE_OR,
  ItemId.CRYSTAL_PICKAXE,
  ItemId.CRYSTAL_PICKAXE_INACTIVE,
  ItemId.CORRUPTED_PICKAXE,
  ItemId.CRYSTAL_PICKAXE_23863
})
class PickaxeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_11);
    type.equipSound(new Sound(2240));
    type.attackSpeed(5);
    type.defendAnimation(397);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(400).attackSound(new Sound(2498)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(401)
            .build());
    return type;
  }
}
