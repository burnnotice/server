package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.IRON_MACE,
  ItemId.BRONZE_MACE,
  ItemId.STEEL_MACE,
  ItemId.BLACK_MACE,
  ItemId.MITHRIL_MACE,
  ItemId.ADAMANT_MACE,
  ItemId.RUNE_MACE,
  ItemId.DRAGON_MACE,
  ItemId.WHITE_MACE,
  ItemId.ANGER_MACE,
  ItemId.ANCIENT_MACE
})
class MaceWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_16);
    type.equipSound(new Sound(2244));
    type.attackSpeed(5);
    type.defendAnimation(403);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(401).attackSound(new Sound(1323)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_STAB)
            .attackAnimation(400)
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.VERACS_FLAIL,
  ItemId.VERACS_FLAIL_100,
  ItemId.VERACS_FLAIL_75,
  ItemId.VERACS_FLAIL_50,
  ItemId.VERACS_FLAIL_25
})
class VeracsFlailWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_16);
    type.renderAnimations(new int[] {2061, 2060, 2060, 2060, 2060, 2060, 824});
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(5);
    type.defendAnimation(2063);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(2062).attackSound(new Sound(1323)).build());
    return type;
  }
}

@ReferenceId(ItemId.BARRELCHEST_ANCHOR)
class BarrelchestAnchorWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_16);
    type.renderAnimations(new int[] {5869, 5867, 5867, 5867, 5867, 5867, 5868});
    type.twoHanded(true);
    type.attackSpeed(6);
    type.defendAnimation(5866);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(5865).attackSound(new Sound(3454)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.BLACK_CANE,
  ItemId.ADAMANT_CANE,
  ItemId.RUNE_CANE,
  ItemId.TWISTED_CANE,
  ItemId.BRUMA_TORCH
})
class CaneWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_16);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.attackSpeed(5);
    type.defendAnimation(420);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2555)).build());
    return type;
  }
}

@ReferenceId({ItemId.VIGGORAS_CHAINMACE_U, ItemId.VIGGORAS_CHAINMACE})
class ViggorasChainmaceWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_16);
    type.renderAnimations(new int[] {244, 247, 247, 247, 247, 247, 248});
    type.equipSound(new Sound(2244));
    type.attackSpeed(4);
    type.defendAnimation(2063);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(245).attackSound(new Sound(1323)).build());
    return type;
  }
}
