package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.STAFF_OF_THE_DEAD,
  ItemId.TOXIC_STAFF_UNCHARGED,
  ItemId.TOXIC_STAFF_OF_THE_DEAD,
  ItemId.STAFF_OF_LIGHT,
  ItemId.STAFF_OF_BALANCE,
  ItemId.STAFF_OF_THE_DEAD_23613
})
class BladedStaffWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_21);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(430);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(440).attackSound(new Sound(2524)).build());
    return type;
  }
}
