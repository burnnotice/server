package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BRONZE_SPEAR,
  ItemId.IRON_SPEAR,
  ItemId.STEEL_SPEAR,
  ItemId.MITHRIL_SPEAR,
  ItemId.ADAMANT_SPEAR,
  ItemId.RUNE_SPEAR,
  ItemId.DRAGON_SPEAR,
  ItemId.BRONZE_SPEAR_P,
  ItemId.IRON_SPEAR_P,
  ItemId.STEEL_SPEAR_P,
  ItemId.MITHRIL_SPEAR_P,
  ItemId.ADAMANT_SPEAR_P,
  ItemId.RUNE_SPEAR_P,
  ItemId.DRAGON_SPEAR_P,
  ItemId.BRONZE_SPEAR_KP,
  ItemId.IRON_SPEAR_KP,
  ItemId.STEEL_SPEAR_KP,
  ItemId.MITHRIL_SPEAR_KP,
  ItemId.ADAMANT_SPEAR_KP,
  ItemId.RUNE_SPEAR_KP,
  ItemId.DRAGON_SPEAR_KP,
  ItemId.BLACK_SPEAR,
  ItemId.BLACK_SPEAR_P,
  ItemId.BLACK_SPEAR_KP,
  ItemId.BRONZE_SPEAR_P_PLUS,
  ItemId.IRON_SPEAR_P_PLUS,
  ItemId.STEEL_SPEAR_P_PLUS,
  ItemId.MITHRIL_SPEAR_P_PLUS,
  ItemId.ADAMANT_SPEAR_P_PLUS,
  ItemId.RUNE_SPEAR_P_PLUS,
  ItemId.DRAGON_SPEAR_P_PLUS,
  ItemId.BRONZE_SPEAR_P_PLUS_PLUS,
  ItemId.IRON_SPEAR_P_PLUS_PLUS,
  ItemId.STEEL_SPEAR_P_PLUS_PLUS,
  ItemId.MITHRIL_SPEAR_P_PLUS_PLUS,
  ItemId.ADAMANT_SPEAR_P_PLUS_PLUS,
  ItemId.RUNE_SPEAR_P_PLUS_PLUS,
  ItemId.DRAGON_SPEAR_P_PLUS_PLUS,
  ItemId.BLACK_SPEAR_P_PLUS,
  ItemId.BLACK_SPEAR_P_PLUS_PLUS,
  ItemId.ANGER_SPEAR,
  ItemId.LEAF_BLADED_SPEAR,
  ItemId.GILDED_SPEAR,
  ItemId.VESTAS_SPEAR,
  ItemId.VESTAS_SPEAR_CHARGED_32256
})
class SpearWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_15);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.twoHanded(true);
    type.attackSpeed(5);
    type.defendAnimation(430);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(428).attackSound(new Sound(1328)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_SLASH)
            .attackAnimation(440)
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_CRUSH)
            .attackAnimation(429)
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.BRONZE_HASTA,
  ItemId.IRON_HASTA,
  ItemId.STEEL_HASTA,
  ItemId.MITHRIL_HASTA,
  ItemId.ADAMANT_HASTA,
  ItemId.RUNE_HASTA,
  ItemId.BRONZE_HASTA_P,
  ItemId.BRONZE_HASTA_KP,
  ItemId.BRONZE_HASTA_P_PLUS,
  ItemId.BRONZE_HASTA_P_PLUS_PLUS,
  ItemId.IRON_HASTA_P,
  ItemId.IRON_HASTA_KP,
  ItemId.IRON_HASTA_P_PLUS,
  ItemId.IRON_HASTA_P_PLUS_PLUS,
  ItemId.STEEL_HASTA_P,
  ItemId.STEEL_HASTA_KP,
  ItemId.STEEL_HASTA_P_PLUS,
  ItemId.STEEL_HASTA_P_PLUS_PLUS,
  ItemId.MITHRIL_HASTA_P,
  ItemId.MITHRIL_HASTA_KP,
  ItemId.MITHRIL_HASTA_P_PLUS,
  ItemId.ADAMANT_HASTA_P,
  ItemId.ADAMANT_HASTA_KP,
  ItemId.ADAMANT_HASTA_P_PLUS,
  ItemId.ADAMANT_HASTA_P_PLUS_PLUS,
  ItemId.RUNE_HASTA_P,
  ItemId.RUNE_HASTA_KP,
  ItemId.RUNE_HASTA_P_PLUS,
  ItemId.RUNE_HASTA_P_PLUS_PLUS,
  ItemId.GILDED_HASTA
})
class Hasta5TickWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_15);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.attackSpeed(5);
    type.defendAnimation(430);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(428).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_SLASH)
            .attackAnimation(440)
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_CRUSH)
            .attackAnimation(429)
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.ZAMORAKIAN_HASTA,
  ItemId.DRAGON_HASTA,
  ItemId.DRAGON_HASTA_P,
  ItemId.DRAGON_HASTA_P_PLUS,
  ItemId.DRAGON_HASTA_P_PLUS_PLUS,
  ItemId.DRAGON_HASTA_KP
})
class Hasta4TickWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_15);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.attackSpeed(4);
    type.defendAnimation(430);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(428).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_SLASH)
            .attackAnimation(440)
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_CRUSH)
            .attackAnimation(429)
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.GUTHANS_WARSPEAR,
  ItemId.GUTHANS_WARSPEAR_100,
  ItemId.GUTHANS_WARSPEAR_75,
  ItemId.GUTHANS_WARSPEAR_50,
  ItemId.GUTHANS_WARSPEAR_25
})
class GuthansWarspearWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_15);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.twoHanded(true);
    type.attackSpeed(5);
    type.defendAnimation(2079);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(2080).attackSound(new Sound(1337)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_SLASH)
            .attackAnimation(2081)
            .attackSound(new Sound(1336))
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_CRUSH)
            .attackAnimation(2082)
            .attackSound(new Sound(1335))
            .build());
    return type;
  }
}

@ReferenceId(ItemId.ZAMORAKIAN_SPEAR)
class ZamorakianSpearWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_15);
    type.renderAnimations(new int[] {1713, 1702, 1703, 1704, 1706, 1705, 1707});
    type.equipSound(new Sound(2238));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(1709);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(1711).attackSound(new Sound(1328)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(1712)
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_CRUSH)
            .attackAnimation(1710)
            .build());
    return type;
  }
}

@ReferenceId(ItemId.DRAGON_HUNTER_LANCE)
class DragonHunterLanceWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_15);
    type.renderAnimations(new int[] {813, 1205, 1205, 1206, 1207, 1208, 2563});
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(1709);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(1711).attackSound(new Sound(1328)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_STAB)
            .attackAnimation(8288)
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_SLASH)
            .attackAnimation(8289)
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_CRUSH)
            .attackAnimation(8290)
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.DEFENSIVE_STAB)
            .attackAnimation(8288)
            .build());
    return type;
  }
}

@ReferenceId(ItemId.BUTTERFLY_NET)
class ButterflyNetWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_15);
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(422).attackSound(new Sound(2566)).build());
    return type;
  }
}

@ReferenceId(ItemId.MAGIC_BUTTERFLY_NET)
class MagicButterflyNetWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_15);
    type.renderAnimations(new int[] {6604, 6611, 6607, 6608, 6609, 6610, 6603});
    type.equipSound(new Sound(2238));
    type.attackSpeed(6);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(422).attackSound(new Sound(2566)).build());
    return type;
  }
}
