package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.IRON_KNIFE,
  ItemId.BRONZE_KNIFE,
  ItemId.STEEL_KNIFE,
  ItemId.MITHRIL_KNIFE,
  ItemId.ADAMANT_KNIFE,
  ItemId.RUNE_KNIFE,
  ItemId.BLACK_KNIFE,
  ItemId.BRONZE_KNIFE_P,
  ItemId.IRON_KNIFE_P,
  ItemId.STEEL_KNIFE_P,
  ItemId.MITHRIL_KNIFE_P,
  ItemId.BLACK_KNIFE_P,
  ItemId.ADAMANT_KNIFE_P,
  ItemId.RUNE_KNIFE_P,
  ItemId.BRONZE_KNIFE_P_PLUS,
  ItemId.IRON_KNIFE_P_PLUS,
  ItemId.STEEL_KNIFE_P_PLUS,
  ItemId.MITHRIL_KNIFE_P_PLUS,
  ItemId.BLACK_KNIFE_P_PLUS,
  ItemId.ADAMANT_KNIFE_P_PLUS,
  ItemId.RUNE_KNIFE_P_PLUS,
  ItemId.BRONZE_KNIFE_P_PLUS_PLUS,
  ItemId.IRON_KNIFE_P_PLUS_PLUS,
  ItemId.STEEL_KNIFE_P_PLUS_PLUS,
  ItemId.MITHRIL_KNIFE_P_PLUS_PLUS,
  ItemId.BLACK_KNIFE_P_PLUS_PLUS,
  ItemId.ADAMANT_KNIFE_P_PLUS_PLUS,
  ItemId.RUNE_KNIFE_P_PLUS_PLUS,
  ItemId.HOLY_WATER
})
class ThrownKnifeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_19);
    type.equipSound(new Sound(2238));
    type.attackSpeed(3);
    type.attackDistance(4);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(806).attackSound(new Sound(2707)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.BRONZE_THROWNAXE,
  ItemId.IRON_THROWNAXE,
  ItemId.STEEL_THROWNAXE,
  ItemId.MITHRIL_THROWNAXE,
  ItemId.ADAMANT_THROWNAXE,
  ItemId.RUNE_THROWNAXE,
  ItemId.DRAGON_THROWNAXE,
  ItemId.DRAGON_THROWNAXE_21207,
  ItemId.MORRIGANS_THROWING_AXE
})
class ThrownAxeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_19);
    type.equipSound(new Sound(2238));
    type.attackSpeed(5);
    type.attackDistance(4);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(806).attackSound(new Sound(2706)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.BRONZE_DART,
  ItemId.IRON_DART,
  ItemId.STEEL_DART,
  ItemId.MITHRIL_DART,
  ItemId.ADAMANT_DART,
  ItemId.RUNE_DART,
  ItemId.BRONZE_DART_P,
  ItemId.IRON_DART_P,
  ItemId.STEEL_DART_P,
  ItemId.MITHRIL_DART_P,
  ItemId.ADAMANT_DART_P,
  ItemId.RUNE_DART_P,
  ItemId.BLACK_DART,
  ItemId.BLACK_DART_P,
  ItemId.BRONZE_DART_P_PLUS,
  ItemId.IRON_DART_P_PLUS,
  ItemId.STEEL_DART_P_PLUS,
  ItemId.BLACK_DART_P_PLUS,
  ItemId.MITHRIL_DART_P_PLUS,
  ItemId.ADAMANT_DART_P_PLUS,
  ItemId.RUNE_DART_P_PLUS,
  ItemId.BRONZE_DART_P_PLUS_PLUS,
  ItemId.IRON_DART_P_PLUS_PLUS,
  ItemId.STEEL_DART_P_PLUS_PLUS,
  ItemId.BLACK_DART_P_PLUS_PLUS,
  ItemId.MITHRIL_DART_P_PLUS_PLUS,
  ItemId.ADAMANT_DART_P_PLUS_PLUS,
  ItemId.RUNE_DART_P_PLUS_PLUS,
  ItemId.DRAGON_DART,
  ItemId.DRAGON_DART_P,
  ItemId.DRAGON_DART_P_PLUS,
  ItemId.DRAGON_DART_P_PLUS_PLUS
})
class ThrownDartWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_19);
    type.equipSound(new Sound(2238));
    type.attackSpeed(3);
    type.attackDistance(3);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(806).attackSound(new Sound(2696)).build());
    return type;
  }
}

@ReferenceId(ItemId.DRAGON_KNIFE)
class DragonKnifeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_19);
    type.equipSound(new Sound(2238));
    type.attackSpeed(3);
    type.attackDistance(4);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(8194).attackSound(new Sound(2696)).build());
    return type;
  }
}

@ReferenceId({ItemId.DRAGON_KNIFE_P, ItemId.DRAGON_KNIFE_P_PLUS, ItemId.DRAGON_KNIFE_P_PLUS_PLUS})
class DragonKnifePoisonedWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_19);
    type.equipSound(new Sound(2238));
    type.attackSpeed(3);
    type.attackDistance(4);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(8195).attackSound(new Sound(2696)).build());
    return type;
  }
}

@ReferenceId(ItemId.TOKTZ_XIL_UL)
class ToktzXilUlWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_19);
    type.attackSpeed(4);
    type.attackDistance(7);
    type.defendAnimation(388);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(3353).build());
    return type;
  }
}

@ReferenceId(ItemId.TOXIC_BLOWPIPE)
class ToxicBlowpipeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_19);
    type.twoHanded(true);
    type.attackSpeed(3);
    type.attackDistance(5);
    type.defendAnimation(388);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(5061).build());
    return type;
  }
}

@ReferenceId({ItemId.MORRIGANS_JAVELIN, ItemId.MORRIGANS_JAVELIN_23619})
class MorrigansJavelinWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_19);
    type.equipSound(new Sound(2238));
    type.attackSpeed(6);
    type.attackDistance(9);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(929).attackSound(new Sound(2699)).build());
    return type;
  }
}
