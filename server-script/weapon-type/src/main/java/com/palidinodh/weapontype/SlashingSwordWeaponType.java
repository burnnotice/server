package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.EXCALIBUR,
  ItemId.BLURITE_SWORD,
  ItemId.MACHETE,
  ItemId.BRONZE_LONGSWORD,
  ItemId.IRON_LONGSWORD,
  ItemId.STEEL_LONGSWORD,
  ItemId.BLACK_LONGSWORD,
  ItemId.MITHRIL_LONGSWORD,
  ItemId.ADAMANT_LONGSWORD,
  ItemId.RUNE_LONGSWORD,
  ItemId.DECORATIVE_SWORD,
  ItemId.DECORATIVE_SWORD_4503,
  ItemId.DECORATIVE_SWORD_4508,
  ItemId.SILVERLIGHT,
  ItemId.SILVER_SICKLE,
  ItemId.SILVER_SICKLE_B,
  ItemId.FREMENNIK_BLADE,
  ItemId.IRON_SICKLE,
  ItemId.OPAL_MACHETE,
  ItemId.JADE_MACHETE,
  ItemId.RED_TOPAZ_MACHETE,
  ItemId.WHITE_LONGSWORD,
  ItemId.SILVERLIGHT_6745,
  ItemId.DARKLIGHT,
  ItemId.RUNE_LONGSWORD_6897,
  ItemId._3RD_AGE_LONGSWORD,
  ItemId.GRANITE_LONGSWORD,
  ItemId.VESTAS_LONGSWORD,
  ItemId.VESTAS_LONGSWORD_23615,
  ItemId.VESTAS_BLIGHTED_LONGSWORD,
  ItemId.VESTAS_LONGSWORD_CHARGED_32254
})
class LongswordWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_9);
    type.equipSound(new Sound(2242));
    type.attackSpeed(5);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(390).attackSound(new Sound(2500)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_STAB)
            .attackAnimation(386)
            .attackSound(new Sound(2501))
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.BRONZE_SCIMITAR,
  ItemId.IRON_SCIMITAR,
  ItemId.STEEL_SCIMITAR,
  ItemId.BLACK_SCIMITAR,
  ItemId.MITHRIL_SCIMITAR,
  ItemId.ADAMANT_SCIMITAR,
  ItemId.RUNE_SCIMITAR,
  ItemId.DRAGON_SCIMITAR,
  ItemId.WHITE_SCIMITAR,
  ItemId.LUCKY_CUTLASS,
  ItemId.HARRYS_CUTLASS,
  ItemId.BRINE_SABRE,
  ItemId.GILDED_SCIMITAR,
  ItemId.ARCLIGHT,
  ItemId.DRAGON_SCIMITAR_OR,
  ItemId.RUNE_SCIMITAR_20402,
  ItemId.DRAGON_SCIMITAR_20406,
  ItemId.STARTER_SWORD,
  ItemId.EMERALD_SICKLE_B,
  ItemId.ENCHANTED_EMERALD_SICKLE_B,
  ItemId.RUNE_SCIMITAR_23330,
  ItemId.RUNE_SCIMITAR_23332,
  ItemId.RUNE_SCIMITAR_23334,
  ItemId.BLADE_OF_SAELDOR,
  ItemId.BLADE_OF_SAELDOR_INACTIVE,
  ItemId.BLADE_OF_SAELDOR_C,
  ItemId.BLIGHTED_DRAGON_SCIMITAR_32369
})
class ScimitarWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_9);
    type.equipSound(new Sound(2242));
    type.attackSpeed(4);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(390).attackSound(new Sound(2500)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_STAB)
            .attackAnimation(386)
            .attackSound(new Sound(2501))
            .build());
    return type;
  }
}

@ReferenceId(ItemId.DRAGON_LONGSWORD)
class DragonLongswordWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_9);
    type.renderAnimations(new int[] {809, 823, 819, 820, 821, 822, 824});
    type.equipSound(new Sound(2242));
    type.attackSpeed(5);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(390).attackSound(new Sound(2500)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_STAB)
            .attackAnimation(386)
            .attackSound(new Sound(2501))
            .build());
    return type;
  }
}

@ReferenceId(ItemId.KATANA)
class KatanaWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_9);
    type.equipSound(new Sound(2242));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(390).attackSound(new Sound(2500)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_STAB)
            .attackAnimation(386)
            .attackSound(new Sound(2501))
            .build());
    return type;
  }
}
