package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BRONZE_HALBERD,
  ItemId.IRON_HALBERD,
  ItemId.STEEL_HALBERD,
  ItemId.BLACK_HALBERD,
  ItemId.MITHRIL_HALBERD,
  ItemId.ADAMANT_HALBERD,
  ItemId.RUNE_HALBERD,
  ItemId.DRAGON_HALBERD,
  ItemId.SPEAR,
  ItemId.CRYSTAL_HALBERD,
  ItemId.CRYSTAL_HALBERD_INACTIVE,
  ItemId.CRYSTAL_HALBERD_24125
})
class HalberdWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_12);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.twoHanded(true);
    type.attackSpeed(7);
    type.attackDistance(2);
    type.defendAnimation(430);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(428).attackSound(new Sound(2562)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(440)
            .attackSound(new Sound(2524))
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.CORRUPTED_HALBERD_BASIC_32381,
  ItemId.CORRUPTED_HALBERD_ATTUNED_32382,
  ItemId.CORRUPTED_HALBERD_PERFECTED_32383
})
class CorruptedHalberdWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_24);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.twoHanded(true);
    type.attackSpeed(4);
    type.attackDistance(2);
    type.defendAnimation(430);
    type.multiTarget(true);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(428).attackSound(new Sound(2562)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(440)
            .attackSound(new Sound(2524))
            .build());
    return type;
  }
}
