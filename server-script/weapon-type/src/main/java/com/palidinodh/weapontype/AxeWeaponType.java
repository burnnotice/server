package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.IRON_BATTLEAXE,
  ItemId.STEEL_BATTLEAXE,
  ItemId.BLACK_BATTLEAXE,
  ItemId.MITHRIL_BATTLEAXE,
  ItemId.ADAMANT_BATTLEAXE,
  ItemId.RUNE_BATTLEAXE,
  ItemId.BRONZE_BATTLEAXE,
  ItemId.DRAGON_BATTLEAXE,
  ItemId.WHITE_BATTLEAXE,
  ItemId.ANGER_BATTLEAXE,
  ItemId.RUNE_BATTLEAXE_20552
})
class BattleaxeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_1);
    type.equipSound(new Sound(2240));
    type.attackSpeed(6);
    type.defendAnimation(397);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(395).attackSound(new Sound(2498)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(401)
            .attackSound(new Sound(2497))
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.IRON_AXE,
  ItemId.BRONZE_AXE,
  ItemId.STEEL_AXE,
  ItemId.MITHRIL_AXE,
  ItemId.ADAMANT_AXE,
  ItemId.RUNE_AXE,
  ItemId.BLACK_AXE,
  ItemId.DRAGON_AXE,
  ItemId.BLESSED_AXE,
  ItemId.INFERNAL_AXE,
  ItemId.INFERNAL_AXE_UNCHARGED,
  ItemId._3RD_AGE_AXE,
  ItemId.GILDED_AXE,
  ItemId.CRYSTAL_AXE,
  ItemId.CRYSTAL_AXE_INACTIVE,
  ItemId.CORRUPTED_AXE,
  ItemId.CRYSTAL_AXE_23862
})
class HatchetWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_1);
    type.equipSound(new Sound(2240));
    type.attackSpeed(5);
    type.defendAnimation(397);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(395).attackSound(new Sound(2498)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(401)
            .attackSound(new Sound(2497))
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.DHAROKS_GREATAXE,
  ItemId.DHAROKS_GREATAXE_100,
  ItemId.DHAROKS_GREATAXE_75,
  ItemId.DHAROKS_GREATAXE_50,
  ItemId.DHAROKS_GREATAXE_25
})
class DharoksGreataxeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_1);
    type.renderAnimations(new int[] {2065, 2064, 2064, 2064, 2064, 2064, 824});
    type.equipSound(new Sound(2240));
    type.twoHanded(true);
    type.attackSpeed(7);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(2067).attackSound(new Sound(1321)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(2066)
            .build());
    return type;
  }
}

@ReferenceId(ItemId.LEAF_BLADED_BATTLEAXE)
class LeafBladedBattleaxeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_1);
    type.equipSound(new Sound(2240));
    type.attackSpeed(6);
    type.defendAnimation(397);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(7004).attackSound(new Sound(2498)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(401)
            .attackSound(new Sound(2497))
            .build());
    return type;
  }
}
