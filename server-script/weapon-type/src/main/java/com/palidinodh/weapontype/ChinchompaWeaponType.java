package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.CHINCHOMPA, ItemId.RED_CHINCHOMPA, ItemId.BLACK_CHINCHOMPA})
class ChinchompaWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_7);
    type.attackSpeed(4);
    type.attackDistance(9);
    type.defendAnimation(424);
    type.multiTarget(true);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(2779).build());
    return type;
  }
}
