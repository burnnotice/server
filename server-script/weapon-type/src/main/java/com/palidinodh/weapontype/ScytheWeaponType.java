package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.SCYTHE)
class ScytheWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_14);
    type.renderAnimations(new int[] {847, 823, 819, 820, 821, 822, 824});
    type.attackSpeed(6);
    type.defendAnimation(430);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(440).attackSound(new Sound(2524)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_STAB)
            .attackAnimation(428)
            .attackSound(new Sound(2524))
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(429)
            .attackSound(new Sound(2522))
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.SCYTHE_OF_VITUR,
  ItemId.SCYTHE_OF_VITUR_UNCHARGED,
  ItemId.SCYTHE_OF_VITUR_22664
})
class ScytheOfViturWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_14);
    type.renderAnimations(new int[] {8057, 823, 819, 820, 821, 822, 824});
    type.twoHanded(true);
    type.attackSpeed(5);
    type.defendAnimation(430);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(8056).attackSound(new Sound(2524)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_STAB)
            .attackAnimation(8056)
            .attackSound(new Sound(2524))
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(8056)
            .attackSound(new Sound(2522))
            .build());
    return type;
  }
}
