package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.SARADOMIN_BANNER,
  ItemId.ZAMORAK_BANNER,
  ItemId.CUTTHROAT_FLAG,
  ItemId.GUILDED_SMILE_FLAG,
  ItemId.BRONZE_FIST_FLAG,
  ItemId.LUCKY_SHOT_FLAG,
  ItemId.TREASURE_FLAG,
  ItemId.PHASMATYS_FLAG
})
class FlagWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_24);
    type.renderAnimations(new int[] {1421, 1426, 1422, 1423, 1424, 1425, 1427});
    type.attackSpeed(4);
    type.defendAnimation(430);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(428).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(440)
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_CRUSH)
            .attackAnimation(429)
            .build());
    return type;
  }
}
