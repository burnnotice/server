package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.SHORTBOW,
  ItemId.OAK_SHORTBOW,
  ItemId.WILLOW_SHORTBOW,
  ItemId.MAPLE_SHORTBOW,
  ItemId.YEW_SHORTBOW,
  ItemId.MAGIC_SHORTBOW,
  ItemId.TRAINING_BOW,
  ItemId.MAGIC_SHORTBOW_I,
  ItemId.YEW_SHORTBOW_20401,
  ItemId.MAPLE_SHORTBOW_20403,
  ItemId.MAGIC_SHORTBOW_20558,
  ItemId.STARTER_BOW,
  ItemId.CRAWS_BOW_U,
  ItemId.CRAWS_BOW,
  ItemId.RAIN_BOW
})
class ShortbowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_3);
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.attackDistance(7);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(426).attackSound(new Sound(2693)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.LONGBOW,
  ItemId.OAK_LONGBOW,
  ItemId.WILLOW_LONGBOW,
  ItemId.MAPLE_LONGBOW,
  ItemId.YEW_LONGBOW,
  ItemId.MAGIC_LONGBOW,
  ItemId.SIGNED_OAK_BOW
})
class LongbowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_3);
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(6);
    type.attackDistance(9);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(426).attackSound(new Sound(2693)).build());
    return type;
  }
}

@ReferenceId({ItemId.WILLOW_COMP_BOW, ItemId.YEW_COMP_BOW, ItemId.MAGIC_COMP_BOW})
class CompositeBowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_3);
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(5);
    type.attackDistance(10);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(426).attackSound(new Sound(2693)).build());
    return type;
  }
}

@ReferenceId({ItemId.CRYSTAL_BOW, ItemId.CRYSTAL_BOW_INACTIVE, ItemId.CRYSTAL_BOW_24123})
class CrystalBowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_3);
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(5);
    type.attackDistance(10);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(426).attackSound(new Sound(2693)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.CORRUPTED_BOW_BASIC_32387,
  ItemId.CORRUPTED_BOW_ATTUNED_32388,
  ItemId.CORRUPTED_BOW_PERFECTED_32389
})
class CorruptedBowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_3);
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(5);
    type.attackDistance(10);
    type.defendAnimation(424);
    type.multiTarget(true);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(426).attackSound(new Sound(2693)).build());
    return type;
  }
}

@ReferenceId(ItemId.SEERCULL)
class SeercullWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_3);
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(5);
    type.attackDistance(8);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(426).attackSound(new Sound(2693)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.DARK_BOW,
  ItemId.DARK_BOW_12765,
  ItemId.DARK_BOW_12766,
  ItemId.DARK_BOW_12767,
  ItemId.DARK_BOW_12768,
  ItemId.DARK_BOW_20408
})
class DarkBowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_3);
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(8);
    type.attackDistance(10);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(426).attackSound(new Sound(3731)).build());
    return type;
  }
}

@ReferenceId(ItemId._3RD_AGE_BOW)
class ThirdAgeBowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_3);
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.attackDistance(9);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(426).attackSound(new Sound(2693)).build());
    return type;
  }
}

@ReferenceId(ItemId.TWISTED_BOW)
class TwistedBowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_3);
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(6);
    type.attackDistance(10);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(426).attackSound(new Sound(2693)).build());
    return type;
  }
}
