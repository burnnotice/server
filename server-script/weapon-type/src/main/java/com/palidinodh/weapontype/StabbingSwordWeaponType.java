package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BRONZE_SWORD,
  ItemId.IRON_SWORD,
  ItemId.STEEL_SWORD,
  ItemId.BLACK_SWORD,
  ItemId.MITHRIL_SWORD,
  ItemId.ADAMANT_SWORD,
  ItemId.RUNE_SWORD,
  ItemId.TOKTZ_XIL_AK,
  ItemId.WHITE_SWORD,
  ItemId.WOODEN_SWORD,
  ItemId.ANGER_SWORD,
  ItemId.TRAINING_SWORD,
  ItemId.WILDERNESS_SWORD_1,
  ItemId.WILDERNESS_SWORD_2,
  ItemId.WILDERNESS_SWORD_3,
  ItemId.WILDERNESS_SWORD_4,
  ItemId.TOKTZ_XIL_AK_20554,
  ItemId.DRAGON_SWORD,
  ItemId.DRAGON_SWORD_21206,
  ItemId.PROP_SWORD,
  ItemId.LEAF_BLADED_SWORD
})
class SwordWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_17);
    type.equipSound(new Sound(2242));
    type.attackSpeed(4);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(386).attackSound(new Sound(2549)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(390)
            .attackSound(new Sound(2548))
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.DARK_DAGGER,
  ItemId.GLOWING_DAGGER,
  ItemId.IRON_DAGGER,
  ItemId.BRONZE_DAGGER,
  ItemId.STEEL_DAGGER,
  ItemId.MITHRIL_DAGGER,
  ItemId.ADAMANT_DAGGER,
  ItemId.RUNE_DAGGER,
  ItemId.BLACK_DAGGER,
  ItemId.IRON_DAGGER_P,
  ItemId.BRONZE_DAGGER_P,
  ItemId.STEEL_DAGGER_P,
  ItemId.MITHRIL_DAGGER_P,
  ItemId.ADAMANT_DAGGER_P,
  ItemId.RUNE_DAGGER_P,
  ItemId.BLACK_DAGGER_P,
  ItemId.IRON_DAGGER_P_PLUS,
  ItemId.BRONZE_DAGGER_P_PLUS,
  ItemId.STEEL_DAGGER_P_PLUS,
  ItemId.MITHRIL_DAGGER_P_PLUS,
  ItemId.ADAMANT_DAGGER_P_PLUS,
  ItemId.RUNE_DAGGER_P_PLUS,
  ItemId.BLACK_DAGGER_P_PLUS,
  ItemId.IRON_DAGGER_P_PLUS_PLUS,
  ItemId.BRONZE_DAGGER_P_PLUS_PLUS,
  ItemId.STEEL_DAGGER_P_PLUS_PLUS,
  ItemId.MITHRIL_DAGGER_P_PLUS_PLUS,
  ItemId.ADAMANT_DAGGER_P_PLUS_PLUS,
  ItemId.RUNE_DAGGER_P_PLUS_PLUS,
  ItemId.BLACK_DAGGER_P_PLUS_PLUS,
  ItemId.TOKTZ_XIL_EK,
  ItemId.WHITE_DAGGER,
  ItemId.WHITE_DAGGER_P,
  ItemId.WHITE_DAGGER_P_PLUS,
  ItemId.WHITE_DAGGER_P_PLUS_PLUS,
  ItemId.BONE_DAGGER,
  ItemId.BONE_DAGGER_P,
  ItemId.BONE_DAGGER_P_PLUS,
  ItemId.BONE_DAGGER_P_PLUS_PLUS,
  ItemId.KITCHEN_KNIFE
})
class DaggerWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_17);
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(386).attackSound(new Sound(2549)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(390)
            .attackSound(new Sound(2548))
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.DRAGON_DAGGER,
  ItemId.DRAGON_DAGGER_P,
  ItemId.DRAGON_DAGGER_P_PLUS,
  ItemId.DRAGON_DAGGER_P_PLUS_PLUS,
  ItemId.KERIS,
  ItemId.KERIS_P,
  ItemId.KERIS_P_PLUS,
  ItemId.KERIS_P_PLUS_PLUS,
  ItemId.DRAGON_DAGGER_20407,
  ItemId.BLIGHTED_DRAGON_DAGGER_P_PLUS_PLUS_32368
})
class DaggerReversedWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_17);
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(397);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(396).attackSound(new Sound(2549)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(395)
            .attackSound(new Sound(2548))
            .build());
    return type;
  }
}

@ReferenceId({ItemId.RAPIER, ItemId.GHRAZI_RAPIER, ItemId.GHRAZI_RAPIER_23628})
class RapierWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_17);
    type.renderAnimations(new int[] {809, 823, 819, 820, 821, 822, 824});
    type.attackSpeed(4);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(8145).attackSound(new Sound(2549)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(390)
            .attackSound(new Sound(2548))
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.ABYSSAL_DAGGER,
  ItemId.ABYSSAL_DAGGER_P,
  ItemId.ABYSSAL_DAGGER_P_PLUS,
  ItemId.ABYSSAL_DAGGER_P_PLUS_PLUS
})
class AbyssalDaggerWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_17);
    type.renderAnimations(new int[] {3296, 823, 819, 820, 821, 822, 824});
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(3295);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(3297).attackSound(new Sound(2549)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(3294)
            .attackSound(new Sound(2548))
            .build());
    return type;
  }
}

@ReferenceId(ItemId.SWIFT_BLADE)
class SwiftBladeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_17);
    type.equipSound(new Sound(2238));
    type.attackSpeed(3);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(386).attackSound(new Sound(2549)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(390)
            .attackSound(new Sound(2548))
            .build());
    return type;
  }
}

@ReferenceId(ItemId.DUAL_SAI)
class DualSaiWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_17);
    type.equipSound(new Sound(2238));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(386).attackSound(new Sound(2549)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(390)
            .attackSound(new Sound(2548))
            .build());
    return type;
  }
}

@ReferenceId({ItemId.MAGIC_SECATEURS, ItemId.MAGIC_SECATEURS_NZ})
class MagicSecateursWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_17);
    type.equipSound(new Sound(2242));
    type.attackSpeed(5);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(386).attackSound(new Sound(2549)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(390)
            .attackSound(new Sound(2548))
            .build());
    return type;
  }
}

@ReferenceId({ItemId.DRAGON_HARPOON, ItemId.INFERNAL_HARPOON, ItemId.INFERNAL_HARPOON_UNCHARGED})
class HarpoonWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_17);
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(397);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(396).attackSound(new Sound(2549)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(395)
            .attackSound(new Sound(2548))
            .build());
    return type;
  }
}
