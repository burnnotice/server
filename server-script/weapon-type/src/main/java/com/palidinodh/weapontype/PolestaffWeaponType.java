package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.GUTHIX_MJOLNIR, ItemId.SARADOMIN_MJOLNIR, ItemId.ZAMORAK_MJOLNIR})
class MjolnirWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_13);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.twoHanded(true);
    type.attackSpeed(6);
    type.defendAnimation(430);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(428).attackSound(new Sound(1328)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(440)
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.ACCURATE_CRUSH)
            .attackAnimation(429)
            .build());
    return type;
  }
}

@ReferenceId(ItemId.DRAGON_CANE)
class DragonCaneWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_13);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.attackSpeed(5);
    type.defendAnimation(420);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2555)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.WESTERN_BANNER_1,
  ItemId.WESTERN_BANNER_2,
  ItemId.WESTERN_BANNER_3,
  ItemId.WESTERN_BANNER_4,
  ItemId.ARCEUUS_BANNER,
  ItemId.HOSIDIUS_BANNER,
  ItemId.LOVAKENGJ_BANNER,
  ItemId.PISCARILIUS_BANNER,
  ItemId.SHAYZIEN_BANNER
})
class PolestaffBannerWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_13);
    type.renderAnimations(new int[] {1421, 1426, 1422, 1423, 1424, 1425, 1427});
    type.attackSpeed(5);
    type.defendAnimation(430);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(428).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.ACCURATE_CRUSH)
            .attackAnimation(440)
            .build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(429)
            .build());
    return type;
  }
}

@ReferenceId({
  ItemId.PHARAOHS_SCEPTRE_3,
  ItemId.PHARAOHS_SCEPTRE_2,
  ItemId.PHARAOHS_SCEPTRE_1,
  ItemId.PHARAOHS_SCEPTRE,
  ItemId.PHARAOHS_SCEPTRE_8,
  ItemId.PHARAOHS_SCEPTRE_7,
  ItemId.PHARAOHS_SCEPTRE_6,
  ItemId.PHARAOHS_SCEPTRE_5,
  ItemId.PHARAOHS_SCEPTRE_4
})
class PharaohsSceptreWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_13);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(420);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2555)).build());
    return type;
  }
}

@ReferenceId(ItemId.ROYAL_SCEPTRE)
class RoyalSceptreWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_13);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(420);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2555)).build());
    return type;
  }
}

@ReferenceId({ItemId.SKULL_SCEPTRE, ItemId.SKULL_SCEPTRE_I})
class SkullSceptreWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_13);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.attackSpeed(5);
    type.defendAnimation(420);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2555)).build());
    return type;
  }
}
