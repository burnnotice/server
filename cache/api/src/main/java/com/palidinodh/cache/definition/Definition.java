package com.palidinodh.cache.definition;

import com.palidinodh.cache.store.util.Stream;

public interface Definition {

  void load(Stream stream);

  default void postLoad() {}

  Stream save(Stream stream);

  int getId();

  Definition[] allDefinitions();
}
