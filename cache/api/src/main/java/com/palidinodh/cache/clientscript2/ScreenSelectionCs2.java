package com.palidinodh.cache.clientscript2;

import com.palidinodh.cache.id.ScriptId;
import lombok.Getter;

@Getter
public class ScreenSelectionCs2 extends ClientScript2 {

  private String title = "Choose an Option";
  private boolean showControls;
  private int entryLines = 1;
  private int textAlignment = 1;
  private String menuOption = "Select";
  private String allText;

  public ScreenSelectionCs2() {
    super(ScriptId.SCREEN_SELECTION_8197);
  }

  public static ScreenSelectionCs2 builder() {
    return new ScreenSelectionCs2();
  }

  public ScreenSelectionCs2 title(String s) {
    title = s;
    return this;
  }

  public ScreenSelectionCs2 showControls(boolean b) {
    showControls = b;
    return this;
  }

  public ScreenSelectionCs2 entryLines(int i) {
    entryLines = i;
    return this;
  }

  public ScreenSelectionCs2 textAlignment(int i) {
    textAlignment = i;
    return this;
  }

  public ScreenSelectionCs2 menuOption(String s) {
    menuOption = s;
    return this;
  }

  public ScreenSelectionCs2 allText(String s) {
    allText = s;
    return this;
  }

  @Override
  public byte[] build() {
    if (allText.endsWith("|")) {
      allText = allText.substring(0, allText.length() - 1);
    }
    addString(title);
    addInt(showControls ? 1 : 0);
    addInt(entryLines * 20);
    addInt(textAlignment);
    addString(menuOption);
    addString(allText);
    return super.build();
  }
}
