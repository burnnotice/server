package com.palidinodh.cache.id;

public final class ScriptId {

  public static final int CHATBOX_MULTI_INIT = 58;
  public static final int DUEL_CHECK_BUTTON = 99;
  public static final int MESLAYER_MODE7 = 108;
  public static final int MESLAYER_MODE9 = 110;
  public static final int PET_INSURANCE_RECLAIMABLES = 202;
  public static final int TRADE_PARTNER_SET = 209;
  public static final int MENU = 217;
  public static final int WILDERNESS_LOOTINGBAG_SETUP = 495;
  public static final int CLANWARS_CONFIRM_SETUP = 554;
  public static final int CLANWARS_VIEW_SETUP = 562;
  public static final int CLANWARS_GAMEOVER = 592;
  public static final int IF_SETTEXTALIGN = 600;
  public static final int MESLAYER_MODE14 = 750;
  public static final int OPTIONS_WINDOWMODE_SET = 847;
  public static final int TOPLEVEL_SIDEBUTTON_SWITCH = 915;
  public static final int FADE_OVERLAY = 948;
  public static final int FADE_OVERLAY_INCREMENT = 951;
  public static final int DUEL_OPTIONS_CHANGED = 968;
  public static final int DEADMAN_TOURNAMENT_SENDNAMES = 1266;
  public static final int CATA_ALTAR_UPDATE = 1313;
  public static final int DUEL_WAIT_BUTTON = 1441;
  public static final int DUELING_OPPONENT_EQUIPMENT = 1447;
  public static final int DUEL_STAKE_CHANGED = 1450;
  public static final int DUELING_OPPONENT_INVENTORY = 1452;
  public static final int POH_JEWELLERY_BOX_INIT = 1685;
  public static final int SKILLMULTI_SETUP = 2046;
  public static final int CHATDEFAULT_RESTOREINPUT = 2158;
  public static final int TOPLEVEL_CHATBOX_RESETBACKGROUND = 2379;
  public static final int SCROLL_LINES = 2523;
  public static final int TOURNAMENT_SUPPLIES_LOADOUTS = 2687;
  public static final int TUT2_DEFAULT_SETTINGS = 3390;
  public static final int GRAVESTONE_TRANSMIT_DATA = 3478;
  public static final int SET_TOOLTIP_8192 = 8192;
  public static final int SCROLL_MAX_8193 = 8193;
  public static final int SHOP_ITEM_8194 = 8194;
  public static final int WIDGET_SIZE_8195 = 8195;
  public static final int COLLECTION_DRAW_LOG_8196 = 8196;
  public static final int SCREEN_SELECTION_8197 = 8197;
  private static final NameIdLookup LOOKUP = new NameIdLookup(ScriptId.class);

  public static int valueOf(String name) {
    return LOOKUP.nameToId(name);
  }

  public static String valueOf(int id) {
    return LOOKUP.idToName(id);
  }
}
