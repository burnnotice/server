package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import lombok.Getter;

@Getter
public class HitMarkDefinition {

  private static final HitMarkDefinition DEFAULT = new HitMarkDefinition(-1);

  @Getter private static HitMarkDefinition[] definitions;

  private int id;
  private String stringFormat = "";
  private int varbitId = -1;
  private int leftSprite = -1;
  private int leftSprite2 = -1;
  private int rightSprite = -1;
  private int fontType = -1;
  private int backgroundSprite = -1;
  private int varpId = -1;
  private int useDamage = -1;
  private int textColor = 0xffffff;
  private int displayCycles = 70;
  private int[] multiHitMarks;
  private int scrollToOffsetX = 0;
  private int fadeStartCycle = -1;
  private int scrollToOffsetY = 0;
  private int textOffsetY = 0;

  public HitMarkDefinition(int id) {
    this.id = id;
  }

  public static HitMarkDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static int size() {
    return definitions.length;
  }

  public static void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getFiles(IndexType.CONFIG, ConfigType.HIT_MARK);
      definitions = new HitMarkDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new HitMarkDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.readOpcodeValues(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void readOpcodeValues(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      readValues(stream, opcode);
    }
  }

  private void readValues(Stream stream, int opcode) {
    switch (opcode) {
      case 1:
        fontType = stream.readBigSmart();
        break;
      case 2:
        textColor = stream.readTriByte();
        break;
      case 3:
        leftSprite = stream.readBigSmart();
        break;
      case 4:
        leftSprite2 = stream.readBigSmart();
        break;
      case 5:
        backgroundSprite = stream.readBigSmart();
        break;
      case 6:
        rightSprite = stream.readBigSmart();
        break;
      case 7:
        scrollToOffsetX = stream.readShort();
        break;
      case 8:
        stringFormat = stream.readJString();
        break;
      case 9:
        displayCycles = stream.readUnsignedShort();
        break;
      case 10:
        scrollToOffsetY = stream.readShort();
        break;
      case 11:
        fadeStartCycle = 0;
        break;
      case 12:
        useDamage = stream.readUnsignedByte();
        break;
      case 13:
        textOffsetY = stream.readShort();
        break;
      case 14:
        fadeStartCycle = stream.readUnsignedShort();
        break;
      case 17:
      case 18:
        {
          varbitId = stream.readUnsignedShort();
          if (varbitId == 65535) {
            varbitId = -1;
          }
          varpId = stream.readUnsignedShort();
          if (varpId == 65535) {
            varpId = -1;
          }
          var id = -1;
          if (opcode == 18) {
            id = stream.readUnsignedShort();
            if (id == 65535) {
              id = -1;
            }
          }
          var length = stream.readUnsignedByte();
          multiHitMarks = new int[length + 2];
          for (var i = 0; i <= length; i++) {
            multiHitMarks[i] = stream.readUnsignedShort();
            if (multiHitMarks[i] == 65535) {
              multiHitMarks[i] = -1;
            }
          }
          multiHitMarks[length + 1] = id;
          break;
        }
      default:
        System.out.println("HitMark Definitions Unknown Opcode: " + opcode);
        break;
    }
  }
}
