package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import lombok.Getter;

@Getter
public class AnimationDefinition {

  private static final AnimationDefinition DEFAULT = new AnimationDefinition(-1);

  @Getter private static AnimationDefinition[] definitions;

  private int id;
  private int[] frameIds;
  private int[] chatFrameIds;
  private int[] frameLengths;
  private int[] frameSounds;
  private int frameStep = -1;
  private int[] interleaveLeave;
  private boolean stretches;
  private int forcedPriority = 5;
  private int leftHandItem = -1;
  private int rightHandItem = -1;
  private int maxLoops = 99;
  private int precedenceAnimating = -1;
  private int priority = -1;
  private int replyMode = 2;

  public AnimationDefinition(int id) {
    this.id = id;
  }

  public static AnimationDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static int size() {
    return definitions.length;
  }

  public static void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getFiles(IndexType.CONFIG, ConfigType.ANIMATION);
      definitions = new AnimationDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new AnimationDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.readOpcodeValues(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void readOpcodeValues(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      readValues(stream, opcode);
    }
  }

  private void readValues(Stream stream, int opcode) {
    switch (opcode) {
      case 1:
        {
          var length = stream.readUnsignedShort();
          frameLengths = new int[length];
          for (var i = 0; i < length; i++) {
            frameLengths[i] = stream.readUnsignedShort();
          }
          frameIds = new int[length];
          for (var i = 0; i < length; i++) {
            frameIds[i] = stream.readUnsignedShort();
          }
          for (var i = 0; i < length; i++) {
            frameIds[i] += stream.readUnsignedShort() << 16;
          }
          break;
        }
      case 2:
        frameStep = stream.readUnsignedShort();
        break;
      case 3:
        {
          var length = stream.readUnsignedByte();
          interleaveLeave = new int[length + 1];
          for (var i = 0; i < length; i++) {
            interleaveLeave[i] = stream.readUnsignedByte();
          }
          interleaveLeave[length] = 9999999;
          break;
        }
      case 4:
        stretches = true;
        break;
      case 5:
        forcedPriority = stream.readUnsignedByte();
        break;
      case 6:
        leftHandItem = stream.readUnsignedShort();
        break;
      case 7:
        rightHandItem = stream.readUnsignedShort();
        break;
      case 8:
        maxLoops = stream.readUnsignedByte();
        break;
      case 9:
        precedenceAnimating = stream.readUnsignedByte();
        break;
      case 10:
        priority = stream.readUnsignedByte();
        break;
      case 11:
        replyMode = stream.readUnsignedByte();
        break;
      case 12:
        {
          var length = stream.readUnsignedByte();
          chatFrameIds = new int[length];
          for (var i = 0; i < length; i++) {
            chatFrameIds[i] = stream.readUnsignedShort();
          }
          for (var i = 0; i < length; i++) {
            chatFrameIds[i] += stream.readUnsignedShort() << 16;
          }
          break;
        }
      case 13:
        {
          var length = stream.readUnsignedByte();
          frameSounds = new int[length];
          for (var i = 0; i < length; i++) {
            frameSounds[i] = stream.readTriByte();
          }
          break;
        }
      default:
        System.out.println("Animation Definitions Unknown Opcode: " + opcode);
        break;
    }
  }
}
