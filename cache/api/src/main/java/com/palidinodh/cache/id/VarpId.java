package com.palidinodh.cache.id;

public final class VarpId {

  public static final int CHOOSE_ADVANCE_SKILL = 261;
  public static final int SLAYER_QUANTITY = 261;
  public static final int DEATHS_RETRIEVAL_COFFER = 261;
  public static final int DEATHS_RETRIEVAL_ITEM_SLOT = 262;
  public static final int DEATHS_RETRIEVAL_ITEM_FEE = 263;
  public static final int SLAYER_TASK_IDENTIFIER = 262;
  public static final int DUEL_OPTIONS = 286;
  public static final int SPELL_SELECT_WEAPON = 664;
  public static final int FIGHT_PIT_FOES_REMAINING = 560;
  public static final int INSURED_PET_IDS_1 = 866;
  public static final int WILDERNESS_DEATHS = 1102;
  public static final int WILDERNESS_KILLS = 1103;
  public static final int HEALTH_OVERLAY_TYPE = 1683;
  private static final NameIdLookup LOOKUP = new NameIdLookup(VarpId.class);

  public static int valueOf(String name) {
    return LOOKUP.nameToId(name);
  }

  public static String valueOf(int id) {
    return LOOKUP.idToName(id);
  }
}
