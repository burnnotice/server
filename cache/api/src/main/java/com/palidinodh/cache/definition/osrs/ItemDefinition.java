package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.definition.Definition;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

@SuppressWarnings("FieldNotUsedInToString")
@Getter
public class ItemDefinition implements Definition {

  private static final ItemDefinition DEFAULT = new ItemDefinition(-1);

  @Getter private static ItemDefinition[] definitions;

  private transient int id;
  private String name = "null";
  private transient String lowerCaseName = "null";
  private boolean stackable;
  private int value;
  private boolean tradeable;
  private boolean members;
  private String[] optionsArray = {null, null, null, null, "Drop"};
  private String[] mapOptionsArray = {null, null, "Take", null, null};
  private int notedId = -1;
  private int notedTemplateId = -1;
  private int boughtId = -1;
  private int boughtTemplateId = -1;
  private int placeholderId = -1;
  private int placeholderTemplateId = -1;
  private int shiftClickDropIndex = -2;
  private int team;
  private int inventoryModelId = -1;
  private int zoom2d = 2000;
  private int xan2d;
  private int yan2d;
  private int zan2d;
  private int offsetX2d;
  private int offsetY2d;
  private int[] stackAmounts = new int[10];
  private int[] stackIds = new int[10];
  private int maleModelId0 = -1;
  private int maleModelId1 = -1;
  private int maleModelId2 = -1;
  private int maleOffset;
  private int femaleModelId0 = -1;
  private int femaleModelId1 = -1;
  private int femaleModelId2 = -1;
  private int femaleOffset;
  private int maleHeadModelId = -1;
  private int maleHeadModelId2 = -1;
  private int femaleHeadModelId = -1;
  private int femaleHeadModelId2 = -1;
  private int[] colorToFind = new int[0];
  private int[] colorToReplace = new int[0];
  private int[] textureToFind = new int[0];
  private int[] textureToReplace = new int[0];
  private int resizeX = 128;
  private int resizeY = 128;
  private int resizeZ = 128;
  private int ambient;
  private int contrast;
  private Map<Integer, Object> attributes = new HashMap<>();
  private transient List<DefinitionOption> options;
  private transient List<DefinitionOption> mapOptions;
  private transient List<DefinitionOption> equipmentOptions;

  public ItemDefinition(int id) {
    this.id = id;
  }

  public static ItemDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static String getName(int id) {
    if (definitions == null) {
      return DEFAULT.getName();
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id].getName()
        : DEFAULT.getName();
  }

  public static String getLowerCaseName(int id) {
    if (definitions == null) {
      return DEFAULT.getLowerCaseName();
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id].getLowerCaseName()
        : DEFAULT.getLowerCaseName();
  }

  public static int size() {
    return definitions.length;
  }

  public static void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getIndex(IndexType.CONFIG).getArchive(ConfigType.ITEM).loadFiles();
      definitions = new ItemDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new ItemDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.load(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return id + "-" + name;
  }

  @Override
  public void load(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      switch (opcode) {
        case 1:
          inventoryModelId = stream.readUnsignedShort();
          break;
        case 2:
          {
            name = stream.readString();
            lowerCaseName = name.toLowerCase();
            break;
          }
        case 4:
          zoom2d = stream.readUnsignedShort();
          break;
        case 5:
          xan2d = stream.readUnsignedShort();
          break;
        case 6:
          yan2d = stream.readUnsignedShort();
          break;
        case 7:
          {
            offsetX2d = stream.readUnsignedShort();
            if (offsetX2d > 32767) {
              offsetX2d -= 65536;
            }
            break;
          }
        case 8:
          {
            offsetY2d = stream.readUnsignedShort();
            if (offsetY2d > 32767) {
              offsetY2d -= 65536;
            }
            break;
          }
        case 11:
          stackable = true;
          break;
        case 12:
          value = stream.readInt();
          break;
        case 16:
          members = true;
          break;
        case 23:
          {
            maleModelId0 = stream.readUnsignedShort();
            if (maleModelId0 == 0xFFFF) {
              maleModelId0 = -1;
            }
            maleOffset = stream.readUnsignedByte();
            break;
          }
        case 24:
          maleModelId1 = stream.readUnsignedShort();
          break;
        case 25:
          {
            femaleModelId0 = stream.readUnsignedShort();
            if (femaleModelId0 == 0xFFFF) {
              femaleModelId0 = -1;
            }
            femaleOffset = stream.readUnsignedByte();
            break;
          }
        case 26:
          femaleModelId1 = stream.readUnsignedShort();
          break;
        case 30:
        case 31:
        case 32:
        case 33:
        case 34:
          {
            mapOptionsArray[opcode - 30] = stream.readString();
            if (mapOptionsArray[opcode - 30].equalsIgnoreCase("Hidden")) {
              mapOptionsArray[opcode - 30] = null;
            }
            break;
          }
        case 35:
        case 36:
        case 37:
        case 38:
        case 39:
          optionsArray[opcode - 35] = stream.readString();
          break;
        case 40:
          {
            var length = stream.readUnsignedByte();
            colorToFind = new int[length];
            colorToReplace = new int[length];
            for (var i = 0; i < length; i++) {
              colorToFind[i] = stream.readUnsignedShort();
              colorToReplace[i] = stream.readUnsignedShort();
            }
            break;
          }
        case 41:
          {
            var length = stream.readUnsignedByte();
            textureToFind = new int[length];
            textureToReplace = new int[length];
            for (var i = 0; i < length; i++) {
              textureToFind[i] = stream.readUnsignedShort();
              textureToReplace[i] = stream.readUnsignedShort();
            }
            break;
          }
        case 42:
          shiftClickDropIndex = stream.readByte();
          break;
        case 65:
          tradeable = true;
          break;
        case 78:
          maleModelId2 = stream.readUnsignedShort();
          break;
        case 79:
          femaleModelId2 = stream.readUnsignedShort();
          break;
        case 90:
          maleHeadModelId = stream.readUnsignedShort();
          break;
        case 91:
          femaleHeadModelId = stream.readUnsignedShort();
          break;
        case 92:
          maleHeadModelId2 = stream.readUnsignedShort();
          break;
        case 93:
          femaleHeadModelId2 = stream.readUnsignedShort();
          break;
        case 95:
          zan2d = stream.readUnsignedShort();
          break;
        case 97:
          {
            notedId = stream.readUnsignedShort();
            if (notedId == 0xFFFF) {
              notedId = -1;
            }
            break;
          }
        case 98:
          {
            notedTemplateId = stream.readUnsignedShort();
            if (notedTemplateId == 0xFFFF) {
              notedTemplateId = -1;
            }
            break;
          }
        case 100:
        case 101:
        case 102:
        case 103:
        case 104:
        case 105:
        case 106:
        case 107:
        case 108:
        case 109:
          {
            stackIds[opcode - 100] = stream.readUnsignedShort();
            if (stackIds[opcode - 100] == 0xFFFF) {
              stackIds[opcode - 100] = -1;
            }
            stackAmounts[opcode - 100] = stream.readUnsignedShort();
            break;
          }
        case 110:
          resizeX = stream.readUnsignedShort();
          break;
        case 111:
          resizeY = stream.readUnsignedShort();
          break;
        case 112:
          resizeZ = stream.readUnsignedShort();
          break;
        case 113:
          ambient = stream.readByte();
          break;
        case 114:
          contrast = stream.readByte();
          break;
        case 115:
          team = stream.readUnsignedByte();
          break;
        case 139:
          {
            boughtId = stream.readUnsignedShort();
            if (boughtId == 0xFFFF) {
              boughtId = -1;
            }
            break;
          }
        case 140:
          {
            boughtTemplateId = stream.readUnsignedShort();
            if (boughtTemplateId == 0xFFFF) {
              boughtTemplateId = -1;
            }
            break;
          }
        case 148:
          {
            placeholderId = stream.readUnsignedShort();
            if (placeholderId == 0xFFFF) {
              placeholderId = -1;
            }
            break;
          }
        case 149:
          {
            placeholderTemplateId = stream.readUnsignedShort();
            if (placeholderTemplateId == 0xFFFF) {
              placeholderTemplateId = -1;
            }
            break;
          }
        case 249:
          attributes = stream.readScript();
          break;
        default:
          System.out.println("Item Definitions Unknown Opcode: " + opcode);
          break;
      }
    }
    postLoad();
  }

  @Override
  public void postLoad() {
    options = new ArrayList<>();
    for (var i = 0; i < optionsArray.length; i++) {
      if (optionsArray[i] == null) {
        continue;
      }
      options.add(new DefinitionOption(i, optionsArray[i].toLowerCase()));
    }
    options = Collections.unmodifiableList(options);
    mapOptions = new ArrayList<>();
    for (var i = 0; i < mapOptionsArray.length; i++) {
      if (mapOptionsArray[i] == null) {
        continue;
      }
      mapOptions.add(new DefinitionOption(i, mapOptionsArray[i].toLowerCase()));
    }
    mapOptions = Collections.unmodifiableList(mapOptions);
    var equipOptionsArray =
        new String[] {
          "Remove",
          (String) getAttribute(Attribute.EQUIP_1),
          (String) getAttribute(Attribute.EQUIP_2),
          (String) getAttribute(Attribute.EQUIP_3),
          (String) getAttribute(Attribute.EQUIP_4),
          (String) getAttribute(Attribute.EQUIP_5),
          (String) getAttribute(Attribute.EQUIP_6),
          (String) getAttribute(Attribute.EQUIP_7),
          (String) getAttribute(Attribute.EQUIP_8)
        };
    equipmentOptions = new ArrayList<>();
    for (var i = 0; i < equipOptionsArray.length; i++) {
      if (equipOptionsArray[i] == null) {
        continue;
      }
      equipmentOptions.add(new DefinitionOption(i, equipOptionsArray[i].toLowerCase()));
    }
    equipmentOptions = Collections.unmodifiableList(equipmentOptions);
  }

  @Override
  public Stream save(Stream stream) {
    if (inventoryModelId != -1) {
      if (inventoryModelId < 0 || inventoryModelId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": inventoryModelId");
      }
      stream.writeByte(1);
      stream.writeShort(inventoryModelId);
    }
    if (name != null && !name.isEmpty() && !name.equals("null")) {
      stream.writeByte(2);
      stream.writeString(name);
    }
    if (zoom2d != 2000) {
      if (zoom2d < 0 || zoom2d > 0xFFFF) {
        throw new IllegalArgumentException(id + ": zoom2d");
      }
      stream.writeByte(4);
      stream.writeShort(zoom2d);
    }
    if (xan2d != 0) {
      if (xan2d < 0 || xan2d > 0xFFFF) {
        throw new IllegalArgumentException(id + ": xan2d");
      }
      stream.writeByte(5);
      stream.writeShort(xan2d);
    }
    if (yan2d != 0) {
      if (yan2d < 0 || yan2d > 0xFFFF) {
        throw new IllegalArgumentException(id + ": yan2d");
      }
      stream.writeByte(6);
      stream.writeShort(yan2d);
    }
    if (offsetX2d != 0) {
      stream.writeByte(7);
      stream.writeShort(offsetX2d);
    }
    if (offsetY2d != 0) {
      stream.writeByte(8);
      stream.writeShort(offsetY2d);
    }
    if (stackable) {
      stream.writeByte(11);
    }
    if (value != 0) {
      stream.writeByte(12);
      stream.writeInt(value);
    }
    if (members) {
      stream.writeByte(16);
    }
    if (maleModelId0 != -1 || maleOffset != 0) {
      if (maleModelId0 < 0 || maleModelId0 > 0xFFFF) {
        throw new IllegalArgumentException(id + ": maleModelId0");
      }
      if (maleOffset < 0 || maleOffset > 0xFF) {
        throw new IllegalArgumentException(id + ": maleOffset");
      }
      stream.writeByte(23);
      stream.writeShort(maleModelId0);
      stream.writeByte(maleOffset);
    }
    if (maleModelId1 != -1) {
      if (maleModelId1 < 0 || maleModelId1 > 0xFFFF) {
        throw new IllegalArgumentException(id + ": maleModelId1");
      }
      stream.writeByte(24);
      stream.writeShort(maleModelId1);
    }
    if (femaleModelId0 != -1 || femaleOffset != 0) {
      if (femaleModelId0 < 0 || femaleModelId0 > 0xFFFF) {
        throw new IllegalArgumentException(id + ": femaleModelId0");
      }
      if (femaleOffset < 0 || femaleOffset > 0xFF) {
        throw new IllegalArgumentException(id + ": femaleOffset");
      }
      stream.writeByte(25);
      stream.writeShort(femaleModelId0);
      stream.writeByte(femaleOffset);
    }
    if (femaleModelId1 != -1) {
      if (femaleModelId1 < 0 || femaleModelId1 > 0xFFFF) {
        throw new IllegalArgumentException(id + ": femaleModelId1");
      }
      stream.writeByte(26);
      stream.writeShort(femaleModelId1);
    }
    if (mapOptionsArray != null && mapOptionsArray.length != 0) {
      if (mapOptionsArray.length != 5) {
        throw new IllegalArgumentException(id + ": optionsArray.length");
      }
      for (var i = 0; i < mapOptionsArray.length; i++) {
        var option = mapOptionsArray[i];
        if (option == null) {
          continue;
        }
        if (option.equalsIgnoreCase("null")) {
          continue;
        }
        if (option.equalsIgnoreCase("Hidden")) {
          continue;
        }
        stream.writeByte(30 + i);
        stream.writeString(option);
      }
    }
    if (optionsArray != null && optionsArray.length != 0) {
      if (optionsArray.length != 5) {
        throw new IllegalArgumentException(id + ": optionsArray.length");
      }
      for (var i = 0; i < optionsArray.length; i++) {
        var option = optionsArray[i];
        if (option == null) {
          continue;
        }
        if (option.equalsIgnoreCase("null")) {
          continue;
        }
        if (option.equalsIgnoreCase("Hidden")) {
          continue;
        }
        stream.writeByte(35 + i);
        stream.writeString(option);
      }
    }
    if (colorToFind != null
        && colorToFind.length != 0
        && colorToReplace != null
        && colorToReplace.length != 0) {
      if (colorToFind.length > 0xFF) {
        throw new IllegalArgumentException(id + ": colorToFind.length");
      }
      if (colorToFind.length != colorToReplace.length) {
        throw new IllegalArgumentException(id + ": colorToFind.length != colorToReplace.length");
      }
      stream.writeByte(40);
      stream.writeByte(colorToFind.length);
      for (var i = 0; i < colorToFind.length; i++) {
        stream.writeShort(colorToFind[i]);
        stream.writeShort(colorToReplace[i]);
      }
    }
    if (textureToFind != null
        && textureToFind.length != 0
        && textureToReplace != null
        && textureToReplace.length != 0) {
      if (textureToFind.length > 0xFF) {
        throw new IllegalArgumentException(id + ": colorToFind.length");
      }
      if (textureToFind.length != textureToReplace.length) {
        throw new IllegalArgumentException(
            id + ": textureToFind.length != textureToReplace.length");
      }
      stream.writeByte(41);
      stream.writeByte(textureToFind.length);
      for (var i = 0; i < textureToFind.length; i++) {
        stream.writeShort(textureToFind[i]);
        stream.writeShort(textureToReplace[i]);
      }
    }
    if (shiftClickDropIndex != -2) {
      stream.writeByte(42);
      stream.writeByte(shiftClickDropIndex);
    }
    if (tradeable) {
      stream.writeByte(65);
    }
    if (maleModelId2 != -1) {
      if (maleModelId2 < 0 || maleModelId2 > 0xFFFF) {
        throw new IllegalArgumentException(id + ": maleModelId2");
      }
      stream.writeByte(78);
      stream.writeShort(maleModelId2);
    }
    if (femaleModelId2 != -1) {
      if (femaleModelId2 < 0 || femaleModelId2 > 0xFFFF) {
        throw new IllegalArgumentException(id + ": femaleModelId2");
      }
      stream.writeByte(79);
      stream.writeShort(femaleModelId2);
    }
    if (maleHeadModelId != -1) {
      if (maleHeadModelId < 0 || maleHeadModelId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": maleHeadModelId");
      }
      stream.writeByte(90);
      stream.writeShort(maleHeadModelId);
    }
    if (femaleHeadModelId != -1) {
      if (femaleHeadModelId < 0 || femaleHeadModelId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": femaleHeadModelId");
      }
      stream.writeByte(91);
      stream.writeShort(femaleHeadModelId);
    }
    if (maleHeadModelId2 != -1) {
      if (maleHeadModelId2 < 0 || maleHeadModelId2 > 0xFFFF) {
        throw new IllegalArgumentException(id + ": maleHeadModelId2");
      }
      stream.writeByte(92);
      stream.writeShort(maleHeadModelId2);
    }
    if (femaleHeadModelId2 != -1) {
      if (femaleHeadModelId2 < 0 || femaleHeadModelId2 > 0xFFFF) {
        throw new IllegalArgumentException(id + ": femaleHeadModelId2");
      }
      stream.writeByte(93);
      stream.writeShort(femaleHeadModelId2);
    }
    if (zan2d != 0) {
      if (zan2d < 0 || zan2d > 0xFFFF) {
        throw new IllegalArgumentException(id + ": zan2d");
      }
      stream.writeByte(95);
      stream.writeShort(zan2d);
    }
    if (notedId != -1) {
      if (notedId < 0 || notedId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": notedId");
      }
      stream.writeByte(97);
      stream.writeShort(notedId);
    }
    if (notedTemplateId != -1) {
      if (notedTemplateId < 0 || notedTemplateId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": notedTemplateId");
      }
      stream.writeByte(98);
      stream.writeShort(notedTemplateId);
    }
    if (stackIds != null
        && stackIds.length != 0
        && !Arrays.stream(stackIds).allMatch(i -> i == -1)
        && !Arrays.stream(stackIds).allMatch(i -> i == 0)) {
      if (stackIds.length != 10) {
        throw new IllegalArgumentException(id + ": stackIds.length");
      }
      if (stackAmounts.length != 10) {
        throw new IllegalArgumentException(id + ": stackAmounts.length");
      }
      for (var i = 0; i < stackIds.length; i++) {
        var stackId = stackIds[i];
        var stackAmount = stackAmounts[i];
        if (stackId == -1 || stackId == 0) {
          continue;
        }
        if (stackId < 0 || stackId > 0xFFFF) {
          throw new IllegalArgumentException(id + ": stackId");
        }
        stream.writeByte(100 + i);
        stream.writeShort(stackId);
        stream.writeShort(stackAmount);
      }
    }
    if (resizeX != 128) {
      if (resizeX < 0 || resizeX > 0xFFFF) {
        throw new IllegalArgumentException(id + ": resizeX");
      }
      stream.writeByte(110);
      stream.writeShort(resizeX);
    }
    if (resizeY != 128) {
      if (resizeY < 0 || resizeY > 0xFFFF) {
        throw new IllegalArgumentException(id + ": resizeY");
      }
      stream.writeByte(111);
      stream.writeShort(resizeY);
    }
    if (resizeZ != 128) {
      if (resizeZ < 0 || resizeZ > 0xFFFF) {
        throw new IllegalArgumentException(id + ": resizeZ");
      }
      stream.writeByte(112);
      stream.writeShort(resizeZ);
    }
    if (ambient != 0) {
      stream.writeByte(113);
      stream.writeByte(ambient);
    }
    if (contrast != 0) {
      stream.writeByte(114);
      stream.writeByte(contrast);
    }
    if (team != 0) {
      if (team < 0 || team > 0xFFFF) {
        throw new IllegalArgumentException(id + ": team");
      }
      stream.writeByte(115);
      stream.writeByte(team);
    }
    if (boughtId != -1) {
      if (boughtId < 0 || boughtId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": boughtId");
      }
      stream.writeByte(139);
      stream.writeByte(boughtId);
    }
    if (boughtTemplateId != -1) {
      if (boughtTemplateId < 0 || boughtTemplateId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": boughtTemplateId");
      }
      stream.writeByte(140);
      stream.writeByte(boughtTemplateId);
    }
    if (placeholderId != -1) {
      if (placeholderId < 0 || placeholderId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": placeholderId");
      }
      stream.writeByte(148);
      stream.writeShort(placeholderId);
    }
    if (placeholderTemplateId != -1) {
      if (placeholderTemplateId < 0 || placeholderTemplateId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": placeholderTemplateId");
      }
      stream.writeByte(149);
      stream.writeShort(placeholderTemplateId);
    }
    if (attributes != null && !attributes.isEmpty()) {
      if (attributes.size() > 0xFF) {
        throw new IllegalArgumentException(id + ": attributes.size()");
      }
      stream.writeByte(249);
      stream.writeScript(attributes);
    }
    stream.writeByte(0);
    return stream;
  }

  @Override
  public ItemDefinition[] allDefinitions() {
    return definitions;
  }

  public int getNotedId() {
    return notedTemplateId == -1 ? notedId : -1;
  }

  public int getUnnotedId() {
    return notedTemplateId == -1 ? -1 : notedId;
  }

  public boolean isUnnoted() {
    return notedTemplateId == -1;
  }

  public boolean isNoted() {
    return notedTemplateId != -1;
  }

  public int getPlaceholderId() {
    return placeholderTemplateId == -1 ? placeholderId : -1;
  }

  public int getUnplaceholderId() {
    return placeholderTemplateId == -1 ? -1 : placeholderId;
  }

  public boolean isPlaceholder() {
    return placeholderTemplateId != -1;
  }

  public String getName() {
    if (getUnnotedId() != -1) {
      return getDefinition(getUnnotedId()).getName() + " (noted)";
    }
    if (placeholderTemplateId != -1 && placeholderId != -1) {
      return getDefinition(placeholderId).getName() + " (placeholder)";
    }
    return name;
  }

  public String getLowerCaseName() {
    if (getUnnotedId() != -1) {
      return getDefinition(getUnnotedId()).getLowerCaseName() + " (noted)";
    }
    if (placeholderTemplateId != -1 && placeholderId != -1) {
      return getDefinition(placeholderId).getLowerCaseName() + " (placeholder)";
    }
    return lowerCaseName;
  }

  public boolean isMembers() {
    if (getUnnotedId() != -1) {
      return getDefinition(getUnnotedId()).isMembers();
    }
    if (placeholderTemplateId != -1 && placeholderId != -1) {
      return getDefinition(placeholderId).isMembers();
    }
    return members;
  }

  public int getValue() {
    return getUnnotedId() == -1 ? value : getDefinition(getUnnotedId()).getValue();
  }

  public boolean hasOptions() {
    return options != null && !options.isEmpty();
  }

  public boolean hasOption(String search) {
    if (!hasOptions()) {
      return false;
    }
    for (var option : options) {
      if (!search.equalsIgnoreCase(option.getText())) {
        continue;
      }
      return true;
    }
    return false;
  }

  public boolean isOption(int index, String search) {
    var option = getOption(index);
    return option != null && search.equalsIgnoreCase(option.getText());
  }

  public DefinitionOption getOption(int index) {
    if (!hasOptions()) {
      return null;
    }
    for (var option : options) {
      if (index != option.getIndex()) {
        continue;
      }
      return option;
    }
    return null;
  }

  public String getOptionText(int index) {
    var option = getOption(index);
    return option == null ? null : option.getText();
  }

  public boolean hasEquipmentOption(String search) {
    if (equipmentOptions == null || equipmentOptions.isEmpty()) {
      return false;
    }
    for (var option : equipmentOptions) {
      if (!search.equalsIgnoreCase(option.getText())) {
        continue;
      }
      return true;
    }
    return false;
  }

  public boolean isEquipmentOption(int index, String search) {
    var option = getEquipmentOption(index);
    return option != null && search.equalsIgnoreCase(option.getText());
  }

  public DefinitionOption getEquipmentOption(int index) {
    if (equipmentOptions == null || equipmentOptions.isEmpty()) {
      return null;
    }
    for (var option : equipmentOptions) {
      if (index != option.getIndex()) {
        continue;
      }
      return option;
    }
    return null;
  }

  public Object getAttribute(Attribute attribute) {
    return attributes != null ? attributes.get(attribute.getIndex()) : null;
  }

  @AllArgsConstructor
  @Getter
  public enum Attribute {
    EQUIP_1(451),
    EQUIP_2(452),
    EQUIP_3(453),
    EQUIP_4(454),
    EQUIP_5(455),
    EQUIP_6(456),
    EQUIP_7(457),
    EQUIP_8(458);

    private final int index;
  }
}
