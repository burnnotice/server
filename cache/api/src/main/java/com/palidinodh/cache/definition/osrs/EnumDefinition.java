package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.definition.Definition;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import lombok.Getter;

@Getter
public class EnumDefinition implements Definition {

  private static final EnumDefinition DEFAULT = new EnumDefinition(-1);

  @Getter private static EnumDefinition[] definitions;

  private int id;
  private char char1;
  private char char2;
  private String defaultStringValue;
  private int defaultIntValue;
  private Map<Integer, Integer> intMap = new HashMap<>();
  private transient Map<Integer, Integer> reversedIntMap;
  private Map<Integer, String> stringMap = new HashMap<>();
  private transient Map<String, Integer> reversedStringMap;

  public EnumDefinition(int id) {
    this.id = id;
  }

  public static EnumDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static int size() {
    return definitions.length;
  }

  public static void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getFiles(IndexType.CONFIG, ConfigType.ENUM);
      definitions = new EnumDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new EnumDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.load(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return Integer.toString(id);
  }

  @Override
  public void load(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      switch (opcode) {
        case 1:
          char1 = (char) stream.readUnsignedByte();
          break;
        case 2:
          char2 = (char) stream.readUnsignedByte();
          break;
        case 3:
          defaultStringValue = stream.readString();
          break;
        case 4:
          defaultIntValue = stream.readInt();
          break;
        case 5:
          {
            int tableSize = stream.readUnsignedShort();
            stringMap = new LinkedHashMap<>();
            reversedStringMap = new LinkedHashMap<>();
            for (int i = 0; i < tableSize; i++) {
              var key = stream.readInt();
              var value = stream.readString();
              stringMap.put(key, value);
              reversedStringMap.put(value, key);
            }
            break;
          }
        case 6:
          {
            int tableSize = stream.readUnsignedShort();
            intMap = new LinkedHashMap<>();
            reversedIntMap = new LinkedHashMap<>();
            for (int i = 0; i < tableSize; i++) {
              var key = stream.readInt();
              var value = stream.readInt();
              intMap.put(key, value);
              reversedIntMap.put(value, key);
            }
            break;
          }
        default:
          System.out.println("Enum Definitions Unknown Opcode: " + opcode);
          break;
      }
    }
  }

  @Override
  public Stream save(Stream stream) {
    return null;
  }

  @Override
  public EnumDefinition[] allDefinitions() {
    return definitions;
  }

  public int getIntValue(int key) {
    return intMap != null && intMap.containsKey(key) ? intMap.get(key) : defaultIntValue;
  }

  public int getIntKey(int value) {
    return reversedIntMap != null && reversedIntMap.containsKey(value)
        ? reversedIntMap.get(value)
        : -1;
  }

  public String getStringValue(int key) {
    return stringMap != null && stringMap.containsKey(key)
        ? stringMap.get(key)
        : defaultStringValue;
  }

  public int getStringKey(String value) {
    return reversedStringMap != null && reversedStringMap.containsKey(value)
        ? reversedStringMap.get(value)
        : -1;
  }
}
