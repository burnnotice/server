package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.definition.Definition;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import java.util.Map;
import lombok.Getter;

@Getter
public class StructDefinition implements Definition {

  private static final StructDefinition DEFAULT = new StructDefinition(-1);

  @Getter private static StructDefinition[] definitions;

  private transient int id;
  private Map<Integer, Object> attributes;

  public StructDefinition(int id) {
    this.id = id;
  }

  public static StructDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static int size() {
    return definitions.length;
  }

  public static void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getFiles(IndexType.CONFIG, ConfigType.STRUCT);
      definitions = new StructDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new StructDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.load(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return Integer.toString(id);
  }

  @Override
  public void load(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      switch (opcode) {
        case 249:
          attributes = stream.readScript();
          break;
        default:
          System.out.println("Enum Definitions Unknown Opcode: " + opcode);
          break;
      }
    }
  }

  @Override
  public Stream save(Stream stream) {
    return null;
  }

  @Override
  public StructDefinition[] allDefinitions() {
    return definitions;
  }
}
