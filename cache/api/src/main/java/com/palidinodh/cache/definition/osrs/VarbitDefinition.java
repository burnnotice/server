package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import lombok.Getter;

@Getter
public class VarbitDefinition {

  private static final VarbitDefinition DEFAULT = new VarbitDefinition(-1);

  @Getter private static VarbitDefinition[] definitions;

  private int id;
  private int varpId;
  private int leastSignificantBit;
  private int mostSignificantBit;

  public VarbitDefinition(int id) {
    this.id = id;
  }

  public static VarbitDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static int size() {
    return definitions.length;
  }

  public static void printDetails() {
    if (definitions == null) {
      return;
    }
    for (var def : definitions) {
      if (def == null) {
        continue;
      }
      System.out.println(
          "varbit id: "
              + def.getId()
              + ", varp: "
              + def.getVarpId()
              + ", value: "
              + def.getValue(0, 1)
              + ", least: "
              + def.getLeastSignificantBit()
              + ", most: "
              + def.getMostSignificantBit());
    }
  }

  public static void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getFiles(IndexType.CONFIG, ConfigType.VARBIT);
      definitions = new VarbitDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new VarbitDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.readOpcodeValues(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public int getValue(int varpValue, int bitValue) {
    var mask = (1 << mostSignificantBit - leastSignificantBit + 1) - 1;
    return varpValue & ~(mask << leastSignificantBit) | (bitValue & mask) << leastSignificantBit;
  }

  public void readOpcodeValues(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      readValues(stream, opcode);
    }
  }

  private void readValues(Stream stream, int opcode) {
    switch (opcode) {
      case 1:
        {
          varpId = stream.readUnsignedShort();
          leastSignificantBit = stream.readUnsignedByte();
          mostSignificantBit = stream.readUnsignedByte();
          break;
        }
      default:
        System.out.println("Varbit Definitions Unknown Opcode: " + opcode);
        break;
    }
  }
}
