package com.palidinodh.cache.id;

public final class EnumId {

  public static final int SLAYER_REWARDS_ITEM_IDS = 840;
  public static final int SLAYER_REWARDS_ITEM_QUANTITIES = 841;
  public static final int SLAYER_REWARDS_ITEM_PRICES = 842;
  public static final int PET_INSURANCE_ITEMS = 985;
  private static final NameIdLookup LOOKUP = new NameIdLookup(EnumId.class);

  public static int valueOf(String name) {
    return LOOKUP.nameToId(name);
  }

  public static String valueOf(int id) {
    return LOOKUP.idToName(id);
  }
}
