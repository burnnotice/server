package com.palidinodh.cache.store.util;

import com.palidinodh.cache.store.fs.Index;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

public final class IOUtils {

  public static final int EOF = -1;
  private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

  private IOUtils() {}

  public static void saveIndex(
      File baseDir, Index index, String fileExtension, boolean directoryPerArchive) {
    try {
      for (var archive : index.getArchives()) {
        saveArchive(baseDir, index, archive.getId(), fileExtension, directoryPerArchive);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void saveArchive(
      File baseDir, Index index, int archiveId, String fileExtension, boolean directoryPerArchive) {
    try {
      var archive = index.getArchive(archiveId);
      for (var file : archive.getFiles()) {
        saveArchiveFile(
            baseDir, index, archiveId, file.getId(), fileExtension, directoryPerArchive);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void saveArchiveFile(
      File baseDir,
      Index index,
      int archiveId,
      int fileId,
      String fileExtension,
      boolean directoryPerArchive) {
    try {
      if (!fileExtension.isEmpty() && !fileExtension.startsWith(".")) {
        fileExtension = "." + fileExtension;
      }
      var indexDir = new File(baseDir, IndexType.getName(index.getId()));
      var archiveDir = indexDir;
      if (directoryPerArchive) {
        archiveDir = new File(indexDir, ConfigType.getName(index.getId(), archiveId));
      }
      archiveDir.mkdirs();
      var fs = index.getArchive(archiveId).loadFile(fileId, null);
      var filename = fileId + fileExtension;
      if (!directoryPerArchive) {
        filename = archiveId + "_" + filename;
      }
      writeFile(new File(archiveDir, filename), fs.getContents());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static long copy(InputStream input, OutputStream output, int bufferSize)
      throws IOException {
    return copyLarge(input, output, new byte[bufferSize]);
  }

  public static int copy(InputStream input, OutputStream output) throws IOException {
    var count = copyLarge(input, output);
    if (count > Integer.MAX_VALUE) {
      return -1;
    }
    return (int) count;
  }

  public static long copyLarge(InputStream input, OutputStream output) throws IOException {
    return copy(input, output, DEFAULT_BUFFER_SIZE);
  }

  public static long copyLarge(InputStream input, OutputStream output, byte[] buffer)
      throws IOException {
    var count = 0L;
    int n;
    while (EOF != (n = input.read(buffer))) {
      output.write(buffer, 0, n);
      count += n;
    }
    return count;
  }

  public static byte[] readFile(File file) {
    if (!file.exists()) {
      return null;
    }
    byte[] bytes = null;
    try (DataInputStream in =
        new DataInputStream(new BufferedInputStream(new FileInputStream(file)))) {
      bytes = new byte[(int) file.length()];
      in.readFully(bytes);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return bytes;
  }

  public static File getResourceFile(String name) {
    return new File(IOUtils.class.getResource(name).getFile());
  }

  public static String readTextFile(File file) {
    byte[] bytes = readFile(file);
    return bytes != null ? new String(bytes) : null;
  }

  public static List<String> readTextFileList(File file) {
    String text = readTextFile(file);
    return text != null ? Arrays.asList(text.replace("\r", "").split("\n")) : null;
  }

  public static void writeFile(File file, byte[] bytes) {
    if (file.getParentFile() != null) {
      file.getParentFile().mkdirs();
    }
    try (DataOutputStream out =
        new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
      out.write(bytes);
      out.flush();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
