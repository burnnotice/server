package com.palidinodh.cache.store.fs;

import com.palidinodh.cache.store.util.ArchiveUpdater;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.Djb2;
import com.palidinodh.cache.store.util.IndexType;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import lombok.Getter;

@SuppressWarnings({
  "toString",
  "FieldNotUsedInToString",
  "NonFinalFieldReferenceInEquals",
  "NonFinalFieldReferencedInHashCode"
})
public class Index {

  @Getter private final int id;
  @Getter private final List<Archive> archives = new LinkedList<>();
  private final Storage storage;
  @Getter private int protocol = 6;
  @Getter private int revision;
  @Getter private boolean named = true;
  @Getter private int crc;
  @Getter private int compression;

  public Index(int id, Storage storage) {
    this.id = id;
    this.storage = storage;
  }

  @Override
  public int hashCode() {
    var hash = 3;
    hash = 97 * hash + id;
    hash = 97 * hash + revision;
    hash = 97 * hash + Objects.hashCode(archives);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    var other = (Index) obj;
    if (id != other.id) {
      return false;
    }
    if (revision != other.revision) {
      return false;
    }
    return Objects.equals(archives, other.archives);
  }

  @Override
  public String toString() {
    var name = Integer.toString(id);
    var type = getType();
    if (type != null && !type.name().contains("UNKNOWN")) {
      name += "-" + type.name();
    }
    return name;
  }

  public void load(int uProtocol, int uRevision, boolean uNamed, int uCrc, int uCompression) {
    protocol = uProtocol;
    revision = uRevision;
    named = uNamed;
    crc = uCrc;
    compression = uCompression;
  }

  public IndexType getType() {
    return IndexType.get(id);
  }

  public Archive getArchive(ConfigType configType) {
    return getArchive(configType.getId());
  }

  public Archive getArchive(int archiveId) {
    for (var a : archives) {
      if (a.getId() != archiveId) {
        continue;
      }
      return a;
    }
    return null;
  }

  public Archive getArchiveByName(String name) {
    var hash = Djb2.hash(name);
    for (var a : archives) {
      if (a.getNameHash() != hash) {
        continue;
      }
      return a;
    }
    return null;
  }

  public int getArchiveIdByName(String name) {
    var archive = getArchiveByName(name);
    return archive == null ? -1 : archive.getId();
  }

  public ArchiveUpdater getArchiveUpdater() {
    return new ArchiveUpdater(this, storage);
  }
}
