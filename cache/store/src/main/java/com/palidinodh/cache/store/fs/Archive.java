package com.palidinodh.cache.store.fs;

import com.palidinodh.cache.store.index.ArchiveData;
import com.palidinodh.cache.store.index.FileData;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.Djb2;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.RegionHash;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import lombok.Getter;

@Getter
@SuppressWarnings({
  "toString",
  "FieldNotUsedInToString",
  "NonFinalFieldReferenceInEquals",
  "NonFinalFieldReferencedInHashCode"
})
public class Archive {

  private final Index index;
  private final Storage storage;
  private final int id;
  private ArchiveData archiveData;
  private int nameHash;
  private int crc;
  private int revision;
  private int compression;
  private List<FileData> files = new LinkedList<>();

  public Archive(Index index, Storage storage, ArchiveData archiveData) {
    this.index = index;
    this.storage = storage;
    this.archiveData = archiveData;
    id = archiveData.getId();
  }

  @Override
  public int hashCode() {
    var hash = 7;
    hash = 47 * hash + id;
    hash = 47 * hash + nameHash;
    hash = 47 * hash + revision;
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    var other = (Archive) obj;
    if (id != other.id) {
      return false;
    }
    if (nameHash != other.nameHash) {
      return false;
    }
    return revision == other.revision;
  }

  @Override
  public String toString() {
    var name = Integer.toString(id);
    if (index.getType() == IndexType.CONFIG) {
      var configType = ConfigType.get(id);
      if (configType != null && !configType.name().contains("UNKNOWN")) {
        name += "-" + configType.name();
      }
    }
    if (nameHash != 0) {
      var unhashedName = Djb2.getUnhashedName(nameHash);
      if (unhashedName == null && index.getType() == IndexType.MAP) {
        var nameLookup = RegionHash.findRegionName(nameHash);
        if (!Integer.toString(nameHash).equals(nameLookup)) {
          unhashedName = nameLookup;
        }
      }
      if (unhashedName != null) {
        name += "-" + unhashedName;
      } else {
        name += "-(" + nameHash + ")";
      }
    }
    return name;
  }

  public void load(
      int uRevision, int uNameHash, int uCrc, int uCompression, List<FileData> uFiles) {
    revision = uRevision;
    nameHash = uNameHash;
    crc = uCrc;
    compression = uCompression;
    files = uFiles;
  }

  public FileData getFile(int fileId) {
    for (var file : files) {
      if (file.getId() != fileId) {
        continue;
      }
      return file;
    }
    return null;
  }

  public FsFile loadFile(int fileId, int[] keys) throws IOException {
    var list = loadFiles(keys);
    for (var file : list) {
      if (file.getFileId() != fileId) {
        continue;
      }
      return file;
    }
    return null;
  }

  public List<FsFile> loadFiles() throws IOException {
    return loadFiles(null);
  }

  public List<FsFile> loadFiles(int[] keys) throws IOException {
    return loadArchiveFiles(keys).getFiles();
  }

  public ArchiveFiles loadArchiveFiles(int[] keys) throws IOException {
    var decompressedData = decompress(readArchiveData(), keys);
    var unpackedFiles = new ArchiveFiles();
    for (var fileEntry : files) {
      var file = new FsFile(fileEntry.getId());
      file.setNameHash(fileEntry.getNameHash());
      unpackedFiles.addFile(file);
    }
    unpackedFiles.loadContents(decompressedData);
    return unpackedFiles;
  }

  public byte[] readArchiveData() throws IOException {
    return storage.loadArchive(this);
  }

  private byte[] decompress(byte[] data, int[] keys) throws IOException {
    if (data == null) {
      throw new IOException("Archive is null for " + index.getId() + "/" + id);
    }
    var container = Container.decompress(data, keys);
    if (container == null) {
      throw new IOException("Unable to decrypt archive for " + index.getId() + "/" + id);
    }
    var decompressedData = container.data;
    if (crc != container.crc) {
      throw new IOException(
          "CRC mismatch for " + index.getId() + "/" + id + "; " + crc + "/" + container.crc);
    }
    if (container.revision != -1 && revision != container.revision) {
      revision = container.revision;
    }
    compression = container.compression;
    return decompressedData;
  }
}
