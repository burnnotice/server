package com.palidinodh.rs.ban;

import com.palidinodh.io.FileManager;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Bans {

  private static File bansFile;
  private static Bans instance;

  private List<UserBan> userBans;
  private List<ComputerBan> computerBans;

  public static boolean isBanned(BannedUser user) {
    for (UserBan ban : instance.getUserBans()) {
      if (ban.matches(user)) {
        return true;
      }
    }
    for (ComputerBan ban : instance.getComputerBans()) {
      if (ban.matches(user)) {
        return true;
      }
    }
    return false;
  }

  public static void addBan(Ban ban) {
    if (ban instanceof UserBan) {
      instance.getUserBans().add((UserBan) ban);
    } else if (ban instanceof ComputerBan) {
      instance.getComputerBans().add((ComputerBan) ban);
    }
    save();
  }

  public static void removeExpired() {
    instance.getUserBans().removeIf(b -> b.isExpired());
    instance.getComputerBans().removeIf(b -> b.isExpired());
  }

  public static void load(File file) {
    bansFile = file;
    if (bansFile.exists()) {
      instance = FileManager.fromJsonFile(bansFile, Bans.class);
    }
    if (instance == null) {
      instance = new Bans();
    }
    save();
  }

  private static void save() {
    if (bansFile == null || instance == null) {
      return;
    }
    removeExpired();
    FileManager.toJson(bansFile, instance);
  }

  public List<UserBan> getUserBans() {
    if (userBans == null) {
      userBans = new ArrayList<>();
    }
    return userBans;
  }

  public List<ComputerBan> getComputerBans() {
    if (computerBans == null) {
      computerBans = new ArrayList<>();
    }
    return computerBans;
  }
}
