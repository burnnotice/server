package com.palidinodh.util;

import java.util.ArrayDeque;
import java.util.Deque;
import lombok.Getter;

public class PEventTasks extends PEvent {

  private Deque<Task> tasks = new ArrayDeque<>();
  private Task constantTask;
  private Task stopTask;

  @Override
  public void execute() {
    if (tasks.isEmpty()) {
      stop();
      return;
    }
    if (constantTask != null && --constantTask.delay <= 0) {
      constantTask.delay(constantTask.originalDelay);
      constantTask.execution.execute(constantTask);
      constantTask.executions++;
    }
    var task = tasks.peek();
    if (--task.delay > 0) {
      return;
    }
    if (task.execution != null) {
      task.execution.execute(task);
    } else if (task.conditional != null) {
      if (!task.conditional.canContinue(task)) {
        task.delay();
      }
    }
    task.executions++;
    if (task.getDelay() > 0) {
      return;
    }
    tasks.remove();
  }

  @Override
  public void stopHook() {
    if (stopTask == null) {
      return;
    }
    stopTask.execution.execute(stopTask);
  }

  public void execute(TaskExecution action) {
    execute(1, action);
  }

  public void execute(long delay, TaskExecution action) {
    tasks.add(new Task(this, (int) delay, action, null));
  }

  public void condition(TaskConditional action) {
    condition(1, action);
  }

  public void condition(long delay, TaskConditional action) {
    tasks.add(new Task(this, (int) delay, null, action));
  }

  public void constant(TaskExecution action) {
    constant(1, action);
  }

  public void constant(long delay, TaskExecution action) {
    constantTask = new Task(this, (int) delay, action, null);
  }

  public void delay() {
    delay(1);
  }

  public void delay(long delay) {
    tasks.add(new Task(this, (int) delay, null, null));
  }

  public void stop(TaskExecution action) {
    stopTask = new Task(this, 0, action, null);
  }

  public boolean hasInstantTask() {
    return getExecutions() == 0 && !tasks.isEmpty() && tasks.peek().getDelay() == 0;
  }

  public interface TaskExecution {

    void execute(Task task);
  }

  public interface TaskConditional {

    boolean canContinue(Task task);
  }

  public static class Task {

    private PEventTasks parent;
    private int originalDelay;
    @Getter private int delay;
    @Getter private int executions;
    private TaskExecution execution;
    private TaskConditional conditional;

    public Task(
        PEventTasks tasks, int delay, TaskExecution execution, TaskConditional conditional) {
      parent = tasks;
      originalDelay = this.delay = delay;
      this.execution = execution;
      this.conditional = conditional;
    }

    public void delay() {
      originalDelay = delay = 1;
    }

    public void delay(long withDelay) {
      originalDelay = delay = (int) withDelay;
    }

    public void constant(TaskExecution action) {
      parent.constant(1, action);
    }

    public void constant(long withDelay, TaskExecution action) {
      parent.constant(withDelay, action);
    }

    public void cancelConstant() {
      parent.constantTask = null;
    }
  }
}
