package com.palidinodh.util;

public class PStringBuilder {

  @SuppressWarnings("StringBufferField")
  private StringBuilder builder = new StringBuilder();

  @SuppressWarnings("FieldNotUsedInToString")
  private String delimiter;

  public PStringBuilder(String delimiter) {
    this.delimiter = delimiter;
  }

  public PStringBuilder append(boolean b) {
    builder.append(b).append(delimiter);
    return this;
  }

  public PStringBuilder append(char c) {
    builder.append(c).append(delimiter);
    return this;
  }

  public PStringBuilder append(char[] str) {
    builder.append(str).append(delimiter);
    return this;
  }

  public PStringBuilder append(CharSequence s, int start, int end) {
    builder.append(s, start, end).append(delimiter);
    return this;
  }

  public PStringBuilder append(double d) {
    builder.append(d).append(delimiter);
    return this;
  }

  public PStringBuilder append(float f) {
    builder.append(f).append(delimiter);
    return this;
  }

  public PStringBuilder append(int i) {
    builder.append(i).append(delimiter);
    return this;
  }

  public PStringBuilder append(long lng) {
    builder.append(lng).append(delimiter);
    return this;
  }

  @SuppressWarnings("OverloadedMethodsWithSameNumberOfParameters")
  public PStringBuilder append(Object obj) {
    builder.append(obj).append(delimiter);
    return this;
  }

  public PStringBuilder append(String str) {
    builder.append(str).append(delimiter);
    return this;
  }

  public PStringBuilder append(StringBuffer sb) {
    builder.append(sb).append(delimiter);
    return this;
  }

  public int length() {
    return builder.length();
  }

  public String substring(int start) {
    return builder.substring(start);
  }

  public String substring(int start, int end) {
    return builder.substring(start, end);
  }

  public String toString() {
    return builder.toString();
  }
}
