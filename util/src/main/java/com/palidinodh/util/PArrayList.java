package com.palidinodh.util;

import com.palidinodh.random.PRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.function.Consumer;
import java.util.function.Predicate;
import lombok.Setter;

public class PArrayList<T> extends ArrayList<T> {

  private static final long serialVersionUID = 20200127;

  private Lock modLock = new UnsafeLock();
  private List<Consumer<PArrayList<T>>> pending = new ArrayList<>();
  @Setter private Consumer<? super T> onAdd;
  @Setter private Consumer<? super T> onRemove;

  public PArrayList(int initialCapacity) {
    super(initialCapacity);
  }

  public PArrayList(Collection<? extends T> collection) {
    super(collection);
  }

  @SafeVarargs
  public PArrayList(T... array) {
    super(Arrays.asList(array));
  }

  public PArrayList() {}

  private static long[] nBits(int n) {
    return new long[((n - 1) >> 6) + 1];
  }

  private static void setBit(long[] bits, int i) {
    bits[i >> 6] |= 1L << i;
  }

  private static boolean isClear(long[] bits, int i) {
    return (bits[i >> 6] & (1L << i)) == 0;
  }

  private static int shiftTailOverGap(Object[] es, int lo, int hi, int size) {
    System.arraycopy(es, hi, es, lo, size - hi);
    for (int to = size, i = (size -= hi - lo); i < to; i++) {
      es[i] = null;
    }
    return size;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    checkPending();
    return super.containsAll(c);
  }

  @Override
  public void trimToSize() {
    doAction(l -> super.trimToSize());
  }

  @Override
  public void ensureCapacity(int minCapacity) {
    doAction(l -> super.ensureCapacity(minCapacity));
  }

  @Override
  public int size() {
    checkPending();
    return super.size();
  }

  @Override
  public boolean isEmpty() {
    checkPending();
    return super.isEmpty();
  }

  @Override
  public boolean contains(Object o) {
    checkPending();
    return super.contains(o);
  }

  @Override
  public int indexOf(Object o) {
    checkPending();
    return super.indexOf(o);
  }

  @Override
  public int lastIndexOf(Object o) {
    checkPending();
    return super.lastIndexOf(o);
  }

  @Override
  public Object clone() {
    checkPending();
    return super.clone();
  }

  @Override
  public Object[] toArray() {
    checkPending();
    return super.toArray();
  }

  @Override
  public <E> E[] toArray(E[] a) {
    checkPending();
    return super.toArray(a);
  }

  @Override
  public T get(int index) {
    checkPending();
    return super.get(index);
  }

  @Override
  public T set(int index, T element) {
    checkPending();
    T existing = get(index);
    if (onAdd != null) {
      try {
        onAdd.accept(element);
      } catch (Exception e) {
        PLogger.error(e);
      }
    }
    if (onRemove != null) {
      try {
        onRemove.accept(existing);
      } catch (Exception e) {
        PLogger.error(e);
      }
    }
    doAction(l -> super.set(index, element));
    return existing;
  }

  @Override
  public boolean add(T e) {
    checkPending();
    if (onAdd != null) {
      try {
        onAdd.accept(e);
      } catch (Exception exception) {
        PLogger.error(exception);
      }
    }
    doAction(l -> super.add(e));
    return true;
  }

  @Override
  public void add(int index, T element) {
    checkPending();
    if (onAdd != null) {
      try {
        onAdd.accept(element);
      } catch (Exception e) {
        PLogger.error(e);
      }
    }
    doAction(l -> super.add(index, element));
  }

  @Override
  public T remove(int index) {
    checkPending();
    T existing = get(index);
    if (onRemove != null) {
      try {
        onRemove.accept(existing);
      } catch (Exception e) {
        PLogger.error(e);
      }
    }
    doAction(l -> super.remove(index));
    return existing;
  }

  @SuppressWarnings("unchecked")
  @Override
  public boolean remove(Object o) {
    checkPending();
    if (!contains(o)) {
      return false;
    }
    if (onRemove != null) {
      try {
        onRemove.accept((T) o);
      } catch (Exception e) {
        PLogger.error(e);
      }
    }
    doAction(l -> super.remove(o));
    return true;
  }

  @Override
  public void clear() {
    checkPending();
    modLock.lock();
    try {
      if (onRemove != null) {
        forEach(
            e -> {
              try {
                onRemove.accept(e);
              } catch (Exception exception) {
                PLogger.error(exception);
              }
            });
      }
      super.clear();
      pending.clear();
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
    }
  }

  @Override
  public boolean addAll(Collection<? extends T> c) {
    checkPending();
    if (onAdd != null) {
      c.forEach(
          e -> {
            try {
              onAdd.accept(e);
            } catch (Exception exception) {
              PLogger.error(exception);
            }
          });
    }
    doAction(l -> super.addAll(new ArrayList<T>(c)));
    return true;
  }

  @Override
  public boolean addAll(int index, Collection<? extends T> c) {
    checkPending();
    if (onAdd != null) {
      c.forEach(
          e -> {
            try {
              onAdd.accept(e);
            } catch (Exception exception) {
              PLogger.error(exception);
            }
          });
    }
    doAction(l -> super.addAll(index, new ArrayList<T>(c)));
    return true;
  }

  @Override
  @SuppressWarnings({"rawtypes", "unchecked"})
  public boolean removeAll(Collection<?> c) {
    checkPending();
    if (onRemove != null) {
      c.forEach(
          e -> {
            if (!contains(e)) {
              return;
            }
            try {
              onRemove.accept((T) e);
            } catch (Exception exception) {
              PLogger.error(exception);
            }
          });
    }
    doAction(l -> super.removeAll(new ArrayList(c)));
    return true;
  }

  @Override
  @SuppressWarnings({"rawtypes", "unchecked"})
  public boolean retainAll(Collection<?> c) {
    // Needs onRemove
    // this should prob not be mod safe
    checkPending();
    doAction(l -> super.retainAll(new ArrayList(c)));
    return true;
  }

  @Override
  public ListIterator<T> listIterator(int index) {
    checkPending();
    return super.listIterator(index);
  }

  @Override
  public ListIterator<T> listIterator() {
    checkPending();
    return super.listIterator();
  }

  @Override
  public Iterator<T> iterator() {
    checkPending();
    return super.iterator();
  }

  @Override
  public List<T> subList(int fromIndex, int toIndex) {
    checkPending();
    return super.subList(fromIndex, toIndex);
  }

  @Override
  public void forEach(Consumer<? super T> action) {
    checkPending();
    boolean locked = modLock.tryLock();
    try {
      for (T t : this) {
        try {
          action.accept(t);
        } catch (Exception te) {
          PLogger.error(te);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      if (locked) {
        modLock.unlock();
        checkPending();
      }
    }
  }

  @Override
  public Spliterator<T> spliterator() {
    checkPending();
    return super.spliterator();
  }

  @Override
  public boolean removeIf(Predicate<? super T> filter) {
    // Needs onRemove
    // May need general work
    checkPending();
    modLock.lock();
    try {
      T[] es = toTypeArray();
      int i = 0;
      int end = es.length;
      for (; i < end; i++) {
        try {
          if (filter.test(es[i])) {
            break;
          }
        } catch (Exception te) {
          PLogger.error(te);
          break;
        }
      }
      if (i < end) {
        int beg = i;
        long[] deathRow = nBits(end - beg);
        deathRow[0] = 1L;
        for (i = beg + 1; i < end; i++) {
          try {
            if (filter.test(es[i])) {
              setBit(deathRow, i - beg);
            }
          } catch (Exception te) {
            PLogger.error(te);
            setBit(deathRow, i - beg);
          }
        }
        int w = beg;
        for (i = beg; i < end; i++) {
          if (isClear(deathRow, i - beg)) {
            es[w++] = es[i];
          }
        }
        int size = shiftTailOverGap(es, w, end, es.length);
        super.clear();
        for (int ii = 0; ii < size; ii++) {
          super.add(es[ii]);
        }
        return true;
      } else {
        return false;
      }
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
    }
    return false;
  }

  @Override
  public void sort(Comparator<? super T> c) {
    checkPending();
    try {
      super.sort(c);
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
      checkPending();
    }
  }

  public T getFirst() {
    checkPending();
    return super.isEmpty() ? null : super.get(0);
  }

  public T getLast() {
    checkPending();
    return super.isEmpty() ? null : super.get(super.size() - 1);
  }

  public T removeFirst() {
    checkPending();
    if (super.isEmpty()) {
      return null;
    }
    T removed = null;
    modLock.lock();
    try {
      if (onRemove != null) {
        try {
          onRemove.accept(super.get(0));
        } catch (Exception e) {
          PLogger.error(e);
        }
      }
      removed = super.remove(0);
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
      checkPending();
    }
    return removed;
  }

  public T removeLast() {
    checkPending();
    if (super.isEmpty()) {
      return null;
    }
    T removed = null;
    modLock.lock();
    try {
      int index = super.size() - 1;
      if (onRemove != null) {
        try {
          onRemove.accept(super.get(index));
        } catch (Exception e) {
          PLogger.error(e);
        }
      }
      removed = super.remove(index);
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
      checkPending();
    }
    return removed;
  }

  public T getIf(Predicate<? super T> filter) {
    checkPending();
    boolean locked = modLock.tryLock();
    try {
      for (T t : this) {
        if (filter.test(t)) {
          return t;
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      if (locked) {
        modLock.unlock();
        checkPending();
      }
    }
    return null;
  }

  public T getRandomIf(Predicate<? super T> filter) {
    checkPending();
    boolean locked = modLock.tryLock();
    try {
      if (!containsIf(filter)) {
        return null;
      }
      T value = null;
      while (value == null) {
        value = PRandom.listRandom(this);
        if (!filter.test(value)) {
          value = null;
        }
      }
      return value;
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      if (locked) {
        modLock.unlock();
        checkPending();
      }
    }
    return null;
  }

  public boolean containsIf(Predicate<? super T> filter) {
    return getIf(filter) != null;
  }

  public boolean meetsIf(Predicate<? super T> filter) {
    checkPending();
    boolean locked = modLock.tryLock();
    try {
      for (T t : this) {
        if (!filter.test(t)) {
          return false;
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
      return false;
    } finally {
      if (locked) {
        modLock.unlock();
        checkPending();
      }
    }
    return true;
  }

  public int countMeetsIf(Predicate<? super T> filter) {
    checkPending();
    int count = 0;
    boolean locked = modLock.tryLock();
    try {
      for (T t : this) {
        if (filter.test(t)) {
          count++;
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      if (locked) {
        modLock.unlock();
        checkPending();
      }
    }
    return count;
  }

  public void addIf(T t, Predicate<? super T> filter) {
    checkPending();
    boolean locked = modLock.tryLock();
    try {
      if (filter.test(t)) {
        add(t);
      }
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      if (locked) {
        modLock.unlock();
        checkPending();
      }
    }
  }

  public void addIf(Collection<? extends T> collection, Predicate<? super T> filter) {
    checkPending();
    boolean locked = modLock.tryLock();
    try {
      for (T entry : collection) {
        try {
          if (filter.test(entry)) {
            add(entry);
          }
        } catch (Exception e) {
          PLogger.error(e);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      if (locked) {
        modLock.unlock();
        checkPending();
      }
    }
  }

  public void removeEach(Consumer<? super T> action) {
    checkPending();
    modLock.lock();
    try {
      for (T t : this) {
        try {
          action.accept(t);
        } catch (Exception e) {
          PLogger.error(e);
        }
      }
      clear();
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
      checkPending();
    }
  }

  public void shuffle() {
    checkPending();
    modLock.lock();
    try {
      Collections.shuffle(this);
    } catch (Exception e) {
      PLogger.error(e);
    } finally {
      modLock.unlock();
    }
  }

  public void checkPending() {
    if (pending.isEmpty()) {
      return;
    }
    if (modLock.tryLock()) {
      try {
        List<Consumer<PArrayList<T>>> list = new ArrayList<>(pending);
        pending.clear();
        for (Consumer<PArrayList<T>> t : list) {
          try {
            t.accept(this);
          } catch (Exception e) {
            PLogger.error(e);
          }
        }
      } catch (Exception e) {
        PLogger.error(e);
      } finally {
        modLock.unlock();
      }
    }
  }

  @SuppressWarnings("unchecked")
  public T[] toTypeArray() {
    checkPending();
    return (T[]) super.toArray();
  }

  public int pendingSize() {
    return pending.size();
  }

  private boolean doAction(Consumer<PArrayList<T>> action) {
    checkPending();
    if (modLock.tryLock()) {
      try {
        action.accept(this);
      } catch (Exception e) {
        PLogger.error(e);
      } finally {
        modLock.unlock();
      }
      return true;
    }
    pending.add(action);
    return false;
  }

  private class UnsafeLock implements Lock {

    private boolean locked;

    @Override
    public void lock() {
      if (locked) {
        throw new IllegalMonitorStateException();
      }
      locked = true;
    }

    @Override
    public void lockInterruptibly() {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean tryLock() {
      return !locked && (locked = true);
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void unlock() {
      locked = false;
    }

    @Override
    public Condition newCondition() {
      throw new UnsupportedOperationException();
    }
  }
}
