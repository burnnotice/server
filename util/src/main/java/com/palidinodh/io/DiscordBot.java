package com.palidinodh.io;

import com.palidinodh.rs.communication.ResponseServer;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PString;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

public class DiscordBot {

  @Getter private static DiscordApi api;
  private static long clanChatChannelId = -1;
  private static Map<String, Command> commands = new HashMap<>();
  private static int worldId;

  public static void init(String token, int _worldId) {
    if (token == null) {
      return;
    }
    worldId = _worldId;
    var builder = new DiscordApiBuilder();
    builder.setToken(token);
    builder.addMessageCreateListener(DiscordBot::checkMessage);
    var login = builder.login();
    login.thenAccept(a -> {
      api = a;
      join();
    });
    // login.thenAccept() is much faster than login.join() for login action
    /*api = */login.join();
  }

  public static void sendMessage(DiscordChannel channelType, String msg) {
    if (api == null) {
      return;
    }
    try {
      var channelName = Settings.getInstance().getDiscordChannel(channelType);
      if (channelName == null) {
        return;
      }
      msg = msg.replaceAll("<.*>", "");
      if (msg.endsWith(" @everyone")) {
        int everyoneIndex = msg.lastIndexOf(" @everyone");
        msg = "```" + msg.substring(0, everyoneIndex) + "``` @everyone";
      } else {
        msg = "```" + msg + "```";
      }
      var channel = getTextChannelByName(channelName);
      if (channel == null) {
        return;
      }
      new MessageBuilder().append(msg).send(channel);
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  public static void disconnect() {
    if (api == null) {
      return;
    }
    api.disconnect();
    api = null;
  }

  public static void addCommand(String name, Command command) {
    commands.put(name, command);
  }

  private static TextChannel getTextChannelByName(String name) {
    var channels = api.getTextChannelsByNameIgnoreCase(name).toArray(new TextChannel[0]);
    return channels.length == 0 ? null : channels[0];
  }

  private static void join() {
    var clanChatChannelName = Settings.getInstance().getDiscordChannel(DiscordChannel.CLAN_CHAT);
    var clanChatChannel =
        clanChatChannelName != null ? getTextChannelByName(clanChatChannelName) : null;
    clanChatChannelId = clanChatChannel == null ? -1 : clanChatChannel.getId();
    if (Settings.getInstance().isLocal()) {
      return;
    }
    if (Settings.getInstance().isBeta()) {
      return;
    }
    sendMessage(DiscordChannel.ANNOUNCEMENTS, Settings.getInstance().getName() + " is online!");
  }

  private static void checkMessage(MessageCreateEvent event) {
    if (event.getMessageContent().startsWith("::")) {
      checkCommandMessage(event);
      return;
    }
    if (event.getChannel().getId() == clanChatChannelId) {
      checkClanChatMessage(event);
    }
  }

  private static void checkCommandMessage(MessageCreateEvent event) {
    if (!event.getMessageAuthor().isServerAdmin()) {
      return;
    }
    var message = event.getMessageContent();
    var commandName = message.substring(2).split(" ")[0].toLowerCase();
    var command = commands.get(commandName);
    if (command == null) {
      return;
    }
    try {
      var commandNameLength = 2 + commandName.length() + 1;
      message = commandNameLength < message.length() ? message.substring(commandNameLength) : "";
      command.execute(event, message);
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  private static void checkClanChatMessage(MessageCreateEvent event) {
    if (worldId != -1) {
      return;
    }
    if (event.getMessageAuthor().isBotUser()) {
      return;
    }
    var chatIcon = "[D]";
    if (Settings.getInstance().getDiscordChatIcon() != -1) {
      chatIcon = "<img=" + Settings.getInstance().getDiscordChatIcon() + ">";
    }
    var username = PString.cleanName(event.getMessageAuthor().getDisplayName());
    if (username.isEmpty()) {
      username = PString.cleanName(event.getMessageAuthor().getName());
    }
    if (!username.isEmpty()) {
      var message = PString.removeHtml(event.getMessageContent());
      message = PString.cleanString(message);
      ResponseServer.getInstance().sendGlobalClanMessage(chatIcon + username, message);
    }
  }

  public interface Command {

    void execute(MessageCreateEvent event, String message);
  }
}
